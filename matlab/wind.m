 addpath '/home/lapenta/netcdf'

if (0==0)
clear all
close all

nome='../Cel.dat'


% per una lista delle variabili disponibili
%inqnc(nome)
bx=getnc(nome,'bx');
by=getnc(nome,'by');
bz=getnc(nome,'bz');
vx=getnc(nome,'uvel');
vz=getnc(nome,'wvel');
jy=getnc(nome,'density');

x=getnc(nome,'x');
z=getnc(nome,'z');
t=getnc(nome,'t');

wci=1
L=1

end

indexf=1
[nt nz ny nx]=size(x)

figure
bzEND=squeeze(bz(2:end,end-1,2,2:end-1))';
bzINI=squeeze(bz(2:end,2,2,2:end-1))';
bxEND=squeeze(vz(2:end,end-1,2,2:end-1))';
bxINI=squeeze(vz(2:end,2,2,2:end-1))';

subplot(2,2,1)
pcolor(bxINI)
shading interp
colorbar
subplot(2,2,2)
pcolor(bxEND)
shading interp
colorbar
subplot(2,2,3)
pcolor(bzINI)
shading interp
colorbar
subplot(2,2,4)
pcolor(bzEND)
shading interp
colorbar

set(gcf, 'Renderer', 'zbuffer');
print('-dpng',[num2str(indexf,'%3.3i')])

shading interp

