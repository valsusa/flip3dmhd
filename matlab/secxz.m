 addpath '/home/lapenta/netcdf'

if (0==0)
clear all
close all

%nome='/home/gianni/soteria/flip/Cel.dat'
nome='/home/gianni/Documents/workspace/FlipMHD/Cel.dat'
%nome='/home/gianni/Documents/fortran/f3d_cusp/Cel.dat'

% per una lista delle variabili disponibili
%inqnc(nome)
vx=getnc(nome,'bx');
vy=getnc(nome,'by');
vz=getnc(nome,'bz');
vux=getnc(nome,'uvel');
vuz=getnc(nome,'wvel');
jy=getnc(nome,'density');
vu=sqrt(vux.^2+vuz.^2);

x=getnc(nome,'x');
z=getnc(nome,'z');
t=getnc(nome,'t');

wci=1
L=1

end

indexf=1
[nt nz ny nx]=size(x)
for it=1:nt
%it=5
%for it=1:30
aver=0
t(it)*wci
iy=2;
bx=squeeze(sum(vx(it-aver:it,2:end-1,iy,2:end-1),1))/(aver+1);
by=squeeze(sum(vy(it-aver:it,2:end-1,iy,2:end-1),1))/(aver+1);
bz=squeeze(sum(vz(it-aver:it,2:end-1,iy,2:end-1),1))/(aver+1);
ux=squeeze(sum(vux(it-aver:it,2:end-1,iy,2:end-1),1))/(aver+1);
uz=squeeze(sum(vuz(it-aver:it,2:end-1,iy,2:end-1),1))/(aver+1);
jjy=squeeze(sum(jy(it-aver:it,2:end-1,iy,2:end-1),1))/(aver+1);
xx=squeeze(x(it,2:end-1,iy,2:end-1));
zz=squeeze(z(it,2:end-1,iy,2:end-1));

ay=vecpot(xx,zz,bx,bz);
b=sqrt(bx.^2+by.^2+bz.^2);

xx=xx/L;zz=zz/L;


h=figure(1);
set(h,'Position' , [5 5 560 420]);
coplot(xx,zz,uz,ay,'x/L','z/L',['Density   t= ' num2str(t(it))])

set(gcf, 'Renderer', 'zbuffer');
print('-dpng',['movie/' num2str(indexf,'%3.3i')])



% h=figure(3);
% set(h,'Position' , [600 5 560 420]);
% %mesh(xx,zz,jjy)
% plot(xx',jjy')
% 
% h=figure(4)
% set(h,'Position' , [600 550 560 420]);
% plot(zz,jjy)
% zlim([0 5])

% h=figure(2)
% set(h,'Position' , [5 550 560 420]);
% surf(xx,zz,jjy,'edgecolor','none','facecolor','blue')
% lighting phong
% shading interp
% camlight(0,90) % luce dall'alto
% view(2) %visione dall'alto
% axis tight
% xlabel('x/L')
% ylabel('z/L')
% title(['Jy   t= ' num2str(t(it))])
% %ylim([0 75])
% F2(indexf)=getframe(gcf);

indexf=indexf+1;
pause(.1)
end

return
movie2avi(F1,'dens.avi','fps',[2],'quality',[100])

movie2avi(F2,'vel.avi','fps',[2],'quality',[100])

!ffmpeg -i dens.avi -sameq -r 24 dens.mpg
!ffmpeg -i vel.avi -sameq -r 24 vel.mpg
