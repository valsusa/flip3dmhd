! Compile with 
! ifort read_netcdf.f90 -lnetcdf
! gfortran -I/usr/local/include -L/usr/local/lib read_netcdf_3d.f90 -lnetcdff -lnetcdf

program read_netcdf
  use netcdf
  implicit none
  integer status
 ! This is the name of the data file we will read.
  character (len = 20), parameter :: FILE_NAME = "Cel.dat"
  character (len = 20):: nome       
  character (len = 20):: fileout 
  character (len = 1) :: order1
  character (len = 2) :: order2
  character (len = 3) :: order3
  character (len = 4) :: fixname ="flip" 
  integer :: len
  integer :: fid
  integer, parameter :: dimensions = 6
  integer, dimension(dimensions) :: nd
  integer :: ncid, nDims, nVars, nGlobalAtts, unlimdimid,dimid,varid,varid1,varid2,varid3 &
               ,varIdBx, varIdBy, varIdBz &
             ,varIdJx, varIdJy, varIdJz
         integer  :: xtype, ndimsvar
         integer:: nAtts
  integer, dimension(4) ::start
  integer :: nx,ny,nz,nt
  integer :: ix,iy,it,is
  real(8) :: dx,dy,dz,value1,value2
  real, dimension(102,102) ::pot
    
CALL system('rm *.vtk')    
CALL system('rm *.vtk.gz')   

 ! Open the file.
 status =  nf90_open(FILE_NAME, nf90_nowrite, ncid)
if (status /= nf90_noerr) call handle_err(status)
 status = nf90_inquire(ncid, nDims, nVars, nGlobalAtts, unlimdimid)
if (status /= nf90_noerr) call handle_err(status)
 write(*,*)nDims, nVars, nGlobalAtts, unlimdimid

do dimid=1,nDims 
 status = nf90_inquire_dimension(ncid, dimid, nome, len)
 write(*,*), nome, len
 nd(dimid)=len
enddo
nx=nd(2)
ny=nd(3)
nz=nd(4)
nt=nd(1)
do varid=1,nVars 
 status =  nf90_inquire_variable(ncid, varid, nome, xtype, ndimsvar, nd, nAtts)
 write(*,*), nome, ndimsvar
 nd(dimid)=len
enddo

status = nf90_inq_varid(ncid, "x", VarId)
status = nf90_get_var(ncid, varid, value1, start=(/ 2,2,2,1 /)) 
status = nf90_get_var(ncid, varid, value2, start=(/ 3,2,2,1 /)) 
write(*,*),value1,value2
dx=value2-value1
status = nf90_inq_varid(ncid, "y", VarId)
status = nf90_get_var(ncid, varid, value1, start=(/ 2,2,2,1 /)) 
status = nf90_get_var(ncid, varid, value2, start=(/ 2,3,2,1 /)) 
dy=value2-value1
status = nf90_inq_varid(ncid, "z", VarId)
status = nf90_get_var(ncid, varid, value1, start=(/ 2,2,2,1 /)) 
status = nf90_get_var(ncid, varid, value2, start=(/ 2,2,3,1 /)) 
dz=value2-value1

write(*,*), nx,ny,nz,nt
write(*,*), dx,dy,dz

status = nf90_inq_varid(ncid, "density", VarId1)
status =  nf90_inquire_variable(ncid, VarId1, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "pressure", VarId2)
status =  nf90_inquire_variable(ncid, varid2, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "sie", VarId3)
status =  nf90_inquire_variable(ncid, varid3, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "bx", VarIdBx)
status =  nf90_inquire_variable(ncid, varidBx, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "by", VarIdBy)
status =  nf90_inquire_variable(ncid, varidBy, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "bz", VarIdBz)
status =  nf90_inquire_variable(ncid, varidBz, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "jx", VarIdjx)
status =  nf90_inquire_variable(ncid, varidjx, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "jy", VarIdjy)
status =  nf90_inquire_variable(ncid, varidjy, nome, xtype, ndimsvar, nd, nAtts)

status = nf90_inq_varid(ncid, "jz", VarIdjz)
status =  nf90_inquire_variable(ncid, varidjz, nome, xtype, ndimsvar, nd, nAtts)


do it=1,nt

if(it.lt.10) then
write(order1,'(i1)')it
fileout=fixname //order1//'.vtk'
else if(it.ge.10.and.it.lt.100) then
write(order2,'(i2)')it
fileout=fixname //order2//'.vtk'
else if(it.ge.100.and.it.lt.1000) then
write(order3,'(i3)')it
fileout=fixname //order3//'.vtk'
else
write(*,*)'Cannot save the vtks, too many time slices'
stop
end if
write(*,*)fileout

fid=10
call open_vtk(fid,fileout,nx,ny,nz,dx,dy,dz)
call write_var3D_vtk(ncid,fid,'density',varid1,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'pressure',varid2,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'sie',varid3,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'bx',varidBx,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'by',varidBy,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'bz',varidBz,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'jx',varidjx,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'jy',varidjy,nx,ny,nz,it)
call write_var3D_vtk(ncid,fid,'jz',varidjz,nx,ny,nz,it)
close(fid)

enddo
end  program read_netcdf

 subroutine handle_err(status)
integer :: status
write(*,*)'errore'
stop
end subroutine handle_err

subroutine open_vtk(fid,fileout,nx,ny,nz,dx,dy,dz)
implicit none
  character (len = 20):: fileout  
  integer :: nx,ny,nz,fid
  real(8) :: dx,dy,dz 
open (unit = fid, file = fileout)

write(fid,1), '# vtk DataFile Version 2.0'
    write(fid,1), 'Comment goes here'
    write(fid,1), 'ASCII'
    write(fid,*) 
    write(fid,1), 'DATASET STRUCTURED_POINTS'
    write(fid,2), 'DIMENSIONS    ', nx-2, ny-2, nz-2
    write(fid,*)
    write(fid,1), 'ORIGIN    0.000   0.000   0.000'
    write(fid,5), 'SPACING',dx,dy,dz    
    write(fid,*)
    write(fid,3), 'POINT_DATA',  (nx-2)*(ny-2)*(nz-2)

1 format (A)
2 format (A,3(1X,I5))
5 format (A,3(1X,E10.5))
3 format (A,1X,I5,1X)
end subroutine open_vtk

subroutine write_var2D_vtk(ncid,fid,varname,varid,nx,ny,it)
  use netcdf
  implicit none
  character (len = *):: varname   
  integer :: status,nx,ny,fid,ix,iy,it,varid,ncid
  real(8) :: value
  integer, dimension(3) ::start
    write(fid,4),'SCALARS ', varname ,' float'
    write(fid,1), 'LOOKUP_TABLE default'
    write(fid,1)
1 format (A)
4 format (3(A,1X))

do iy=1,ny
do ix=1,nx

status = nf90_get_var(ncid, varid, value, start=(/ ix,iy,it /)) 
write(fid,*)value
enddo
enddo
end subroutine write_var2D_vtk

subroutine write_var3D_vtk(ncid,fid,varname,varid,nx,ny,nz,it)
  use netcdf
  implicit none
  character (len = *):: varname   
  integer :: status,nx,ny,nz,fid,ix,iy,iz,it,varid,ncid
  real(8) :: value
  integer, dimension(4) ::start
    write(fid,4),'SCALARS ', varname ,' float'
    write(fid,1), 'LOOKUP_TABLE default'
    write(fid,1)
1 format (A)
4 format (3(A,1X))

do iz=2,nz-1
do iy=2,ny-1
do ix=2,nx-1


status = nf90_get_var(ncid, varid, value, start=(/ ix,iy,iz,it /)) 
if(status /= nf90_NoErr) write(*,*)status
write(fid,*)value
enddo
enddo
enddo
end subroutine write_var3D_vtk
