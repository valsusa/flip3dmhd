      subroutine residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     p,gradpx,gradpy,gradpz,residu)
c
c     a routine to calculate the residual error in the solution
c     of poisson's equation
c
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      dimension ijkcell(*),ijkvtx(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),p(*),div(*),srce(*),
     &     gradpx(*),gradpy(*),gradpz(*),residu(*)
c
c     calculate residue
c     NOTE:  if periodic in x or y, the residue is calculated only
c     at vertices i=2,ibp1; j=2,jbp1
c
c     with periodic boundary conditions, BC_SCALAR
c     imposes periodicity on p 
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     p)
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     p,gradpx,gradpy,gradpz)
c
      zero=0.0
c
c     if periodic in x and y, TORUSBC sets values of
c     gradpj in i=1,j=1 logical planes, which allows correct 
c     calculation of div at in i=2, j=2 logical planes
c
      call torusbc(ibp1+1,jbp1+1,kbp1+1,
     &     cdlt,sdlt,zero,zero,
     &     periodic_x,periodic_y,periodic_z,
     &     gradpx,gradpy,gradpz)
c
c     calculate div(grad(p))
c
      call divv(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     gradpx,gradpy,gradpz,div)
c
      do 15 n=1,nvtx
      ijk=ijkvtx(n)
c      if (n.lt.5) then
c         print *,'in residue_vtx after div:',n,ijk,
c     &        srce(ijk),div(ijk),p(ijk-kwid)
c      end if
      residu(ijk)=srce(ijk)-div(ijk)
   15 continue
c
      return
      end
