      subroutine axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     gradxf)
c
      implicit real*8 (a-h,o-z)
c
      dimension gradxf(*)
c
c     calculate gradient at the axis by averaging gradients in the poloidal angle
c
      do 1 j=2,jbp1+1
c
      gradx=0.0
c
      do 11 i=2,ibp1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
      gradx=gradx+gradxf(ijk)
   11 continue
c
      do 12 i=2,ibp1+1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
      gradxf(ijk)=gradx
   12 continue
c
    1 continue
c
      return
      end
