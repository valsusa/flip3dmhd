*dk celdex
      subroutine celdex(i,j,k,iwid,jwid,kwid,ijkc)
c
      implicit real*8 (a-h,o-z)
c
c     a routine to calculate the indices of cell centers
c
      dimension ijkc(27)
c
      ijk=1+iwid*(i-1)+jwid*(j-1)+kwid*(k-1)
c
      ijkc(1)=ijk
      ijkc(2)=ijk+iwid
      ijkc(3)=ijk+iwid+jwid
      ijkc(4)=ijk+jwid
      ijkc(5)=ijk-iwid+jwid
      ijkc(6)=ijk-iwid
      ijkc(7)=ijk-iwid-jwid
      ijkc(8)=ijk-jwid
      ijkc(9)=ijk+iwid-jwid
c
      ijkc(10)=ijk-kwid
      ijkc(11)=ijk+iwid-kwid
      ijkc(12)=ijk+iwid+jwid-kwid
      ijkc(13)=ijk+jwid-kwid
      ijkc(14)=ijk-iwid+jwid-kwid
      ijkc(15)=ijk-iwid-kwid
      ijkc(16)=ijk-iwid-jwid-kwid
      ijkc(17)=ijk-jwid-kwid
      ijkc(18)=ijk+iwid-jwid-kwid
c
      ijkc(19)=ijk+kwid
      ijkc(20)=ijk+iwid+kwid
      ijkc(21)=ijk+iwid+jwid+kwid
      ijkc(22)=ijk+jwid+kwid
      ijkc(23)=ijk-iwid+jwid+kwid
      ijkc(24)=ijk-iwid+kwid
      ijkc(25)=ijk-iwid-jwid+kwid
      ijkc(26)=ijk-jwid+kwid
      ijkc(27)=ijk+iwid-jwid+kwid
c
      return
      end
