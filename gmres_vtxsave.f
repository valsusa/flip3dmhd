      subroutine gmres_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,PRECON,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     itsub,iter,error,rnorm,DT,CNTR,srce,
     &     div,residu,Aq,q,bnorm,
     &     gradpx,gradpy,gradpz,p,diag)
c
c
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          p(*),
     &          Aq(*),q(itdim,20),qtemp(itdim),
     &          vol(*),div(*),residu(*),srce(*),
     &          gradpx(*),gradpy(*),gradpz(*),diag(*)
c
       dimension g(21,2),s(21),h(21,21)
C
      LOGICAL PRECON
c     
c     solving system Ap=srce
C
c
c     ZERO WORKING ARRAYS
c
      do 1112 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=0.0
 1112 continue
c
      do 1114 ii=1,itsub+1
      s(ii)=0.0
      g(ii,1)=0.0
      g(ii,2)=0.0
      do 1114 jj=1,itsub+1
         h(ii,jj)=0.0
 1114 continue
c
      do 1113 ii=1,itsub
      do 1113 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,ii)=0.0
 1113 continue
c
c
C     CALCULATE THE INITIAL RESIDUAL ERROR, residu=srce-Ap
C
c
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     p,gradpx,gradpy,gradpz,residu)
c
c
      dot=0.0d0
c
      do 22 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+residu(ijk)*residu(ijk)
   22 continue
c
c
      dot=sqrt(dot)
      s(1)=dot
c
      write(*,*) 'in gmres, s(1) (L_2(r_0)) =',s(1)
c
c     first krylov vector..q_1 = (srce-Ap)/||srce-Ap||

      if(dot.gt.0.0d0) then
      do 25 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,1)=residu(ijk)/dot
  25  continue

      else
c
      do n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,1)=0.0d0
      enddo
c
      endif
c
c     ****************************************************************
c
c     begin gmres
c
c     ****************************************************************
c
      do 1 iter=1,itsub
c     
c     apply preconditioner
c
      do 32 n=1,nvtx
      ijk=ijkvtx(n)
c      Aq(ijk)=-q(ijk,iter)/diag(ijk)
      Aq(ijk)=q(ijk,iter)/diag(ijk)
      qtemp(ijk)=Aq(ijk)
  32  continue
c
      zero=0.0d0
c
c     compute A times preconditioned q 
c
cdir$ ivdep
      do 33 n=1,nvtx
      ijk=ijkvtx(n)
cSJC      Aq(ijk)=p(ijk)+Aq(ijk)
      qtemp(ijk)=p(ijk)+Aq(ijk)
   33 continue
c
C
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     qtemp,gradpx,gradpy,gradpz,Aq)
c
c
cdir$ ivdep
      do 35 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=(residu(ijk)-Aq(ijk))
   35 continue

c       call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
c     &     periodic_x,periodic_y,periodic_z,
c     &     cdlt,sdlt,strait,dz,
c     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
c     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
c     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
c     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
c     &     div,srce,
c     &     qtemp,gradpx,gradpy,gradpz,Aq)

c       do 35 n=1,nvtx
c          ijk=ijkvtx(n)
c          Aq(ijk)=-Aq(ijk)+srce(ijk)
c 35   continue

c
c    orthogonalize:
c
      do 13 k=1,iter
      dot=0.0d0
      do 11 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+Aq(ijk)*q(ijk,k)
   11 continue
      h(k,iter)=dot
cdir$ ivdep
      do 12 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=Aq(ijk)-dot*q(ijk,k)
   12 continue
   13 continue
c
c
      dot=0.0d0
      do 14 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+Aq(ijk)*Aq(ijk)
   14 continue
      dot=sqrt(dot)
c
      iterp1=iter+1
      h(iterp1,iter)=dot
c
c     apply previous Givens rotations to h (update QR factorization)
c
      do 16 k=1,iter-1
      ws=g(k,1)*h(k,iter)-g(k,2)*h(k+1,iter)
      h(k+1,iter)=g(k,2)*h(k,iter)+g(k,1)*h(k+1,iter)
      h(k,iter)=ws
  16  continue 
c     
c     compute next Givens rotation
      g1=h(iter,iter)
      g2=h(iterp1,iter)
      ws=sqrt(g1*g1+g2*g2)+1.d-50
      g1=g1/ws
      g2=-g2/ws
      g(iter,1)=g1
      g(iter,2)=g2
c
c     apply g to h
      h(iter,iter)=g1*h(iter,iter)-g2*h(iterp1,iter)
      h(iterp1,iter)=0.0
c
c     apply g to s
      ws=g1*s(iter)-g2*s(iterp1)
      s(iterp1)=g2*s(iter)+g1*s(iterp1)
      s(iter)=ws
c
c     |s(iter+1)| is the norm of the current residual
c     check for convergence
      rnorm=abs(s(iterp1))
      write(*,*) 'in gmres, iter, rnorm) =',iter,rnorm
      if((rnorm.le.error*bnorm).or.(iter.eq.itsub)) go to 2
c
c     normalize next q
c
      do 15 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,iterp1)=Aq(ijk)/dot
  15  continue
c
    1 continue
c
    2 continue
c    
c    update the solution
c    solve h*y=sbar  (sbar contains elements 1,..,iter of s)
c    store y in s
      s(iter)=s(iter)/(h(iter,iter)+1.e-50)
      do ii=iter-1,1,-1
        ws=0.0
          do jj=ii+1,iter 
          ws=ws+h(ii,jj)*s(jj)
          enddo
        s(ii)=(s(ii)-ws)/(h(ii,ii)+1.e-50)
      end do
c
c    compute new p
c      do 17 n=1,nvtx
c      ijk=ijkvtx(n)
c      p(ijk)=-diag(ijk)*p(ijk)
c      p(ijk)=diag(ijk)*p(ijk)
c  17  continue
c
      do 18 ii=1,iter
cdir$ ivdep
      do 18 n=1,nvtx
      ijk=ijkvtx(n)
      p(ijk)=p(ijk)+s(ii)*q(ijk,ii)
  18  continue
c

c     solved for y=Mp, now find p=M^(-1)y 
      do 19 n=1,nvtx
      ijk=ijkvtx(n)
c      p(ijk)=-p(ijk)/diag(ijk)
      p(ijk)=p(ijk)/diag(ijk)
  19  continue
c
c  compute actual residual at end of cycle
c
c
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     p,gradpx,gradpy,gradpz,residu)
c
c
      dot=0.0
c
      do 222 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+residu(ijk)*residu(ijk)
  222 continue
      dot=dsqrt(dot)
c
      write(51,*)'actual residual at end of cycle =',dot

      return
      end
c
c
