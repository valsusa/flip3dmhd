*dk budget
      subroutine budget
*                                                       *
*********************************************************
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'cophys.com'
      include 'blcom.com'
      include 'flip3d.com'
      include 'numpar.com'
c
c
      nptotl=0
      toti=0.0
      totb=0.0d0
      totk=0.0d0
      tote=0.0d0
      totv=0.0d0
      totm=0.0d0
      tpmox=0.0
      tpmoy=0.0
      tpmoz=0.0
      tpmass=0.0
      do 25 kr=0,nreg
        numtot(kr)=0
   25 continue
c
c
c    calculate time advanced strain:
      call strain(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     ul,vl,wl,vol,wate(1,1),wate(1,2),wate(1,3))
c
      do n=1,ncells
      ijk=ijkcell(n)
      divu(ijk)=exx(ijk)+eyy(ijk)+ezz(ijk)
      end do
c
c
      tgke=0.0
      tgbsq=0.0
      tgbsqx=0.0
      tgbsqy=0.0
      tgbsqz=0.0
      tginte = 0.0
      tgkex=0.0
      tgkey=0.0
      tgkez=0.0
      tgmox=0.0
      tgmoy=0.0
      tgmoyn=0.0
      tgmoys=0.0
      tgmoz=0.0
      tgradx=0.0
      tgrady=0.0
      tgradz=0.0
      tgmass=0.0
      tgheat=0.0
      reconnected_flux(ncyc)=0.d0
      tgheat_par=0.0
      tgheat_per=0.0
      tgJ_dot_Em=0.0
      tgjxc = 0.0
      tgjyc = 0.0
      tgjzc = 0.0
      tgj_par = 0.0
      tgj_per = 0.0
      gcur_max=-1.e20
      vz_max=0.0
      avg_z=0.0
      avg_mass=0.0
c
      istop=ibp2
      jstop=jbp2
      kstop=kbp2
      if(periodic_x) istop=ibp1
      if(periodic_y) jstop=jbp1
      if(periodic_z) kstop=kbp1
cDAK      do 30 k=3,kbp1
      do 30 k=2,kstop
      do 30 j=2,jstop
      do 30 i=2,istop
      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
      tgke=tgke+0.5*mv(ijk)*(u(ijk)**2+v(ijk)**2+w(ijk)**2)
      if (ringingout) then
      vol_norm=volpc(ijk)
      else
      vol_norm=vol(ijk)
      end if
      tgbsq=tgbsq + (bxn(ijk)**2+byn(ijk)**2+bzn(ijk)**2)/2.0*vol_norm
      tgbsqx=tgbsqx + (bxn(ijk)**2)/2.0*vol_norm
      tgbsqy=tgbsqy + (byn(ijk)**2)/2.0*vol_norm
      tgbsqz=tgbsqz + (bzn(ijk)**2)/2.0*vol_norm
      tginte=tginte + p(ijk)*vol(ijk)/gm1
      tgkex=tgkex+0.5*mv(ijk)*(u(ijk)**2)
      tgkey=tgkey+0.5*mv(ijk)*(v(ijk)**2)
      tgkez=tgkez+0.5*mv(ijk)*(w(ijk)**2)
      tgmox=tgmox+mv(ijk)*u(ijk)
      tgmoy=tgmoy+mv(ijk)*v(ijk)
      if(j .ge. 21)then
       tgmoyn=tgmoyn+mv(ijk)*v(ijk)
      elseif(j .le. 20)then
       tgmoys=tgmoys+mv(ijk)*v(ijk)
      endif
      tgmoz=tgmoz+mv(ijk)*w(ijk)
      wsjxc=0.125*(jx(ijk)+jx(ijk+iwid)
     &     +jx(ijk+iwid+jwid)+jx(ijk+jwid)
     &     +jx(ijk+kwid)+jx(ijk+iwid+kwid)
     &     +jx(ijk+iwid+jwid+kwid)+jx(ijk+jwid+kwid))
      wsjyc=0.125*(jy(ijk)+jy(ijk+iwid)
     &     +jy(ijk+iwid+jwid)+jy(ijk+jwid)
     &     +jy(ijk+kwid)+jy(ijk+iwid+kwid)
     &     +jy(ijk+iwid+jwid+kwid)+jy(ijk+jwid+kwid))
      wsjzc=0.125*(jz(ijk)+jz(ijk+iwid)
     &     +jz(ijk+iwid+jwid)+jz(ijk+jwid)
     &     +jz(ijk+kwid)+jz(ijk+iwid+kwid)
     &     +jz(ijk+iwid+jwid+kwid)+jz(ijk+jwid+kwid))
      wsjsq=(wsjxc**2+wsjyc**2+wsjzc**2)
      if(sqrt(wsjsq)*resistivity .gt. rmax_abs_E)
     &   rmax_abs_E = sqrt(wsjsq)*resistivity
      tgheat=tgheat+resistivity*
     &     wsjsq*vol(ijk)
      if(resistivity*wsjsq .gt. rmax_gheat)
     &   rmax_gheat = resistivity*wsjsq
      tgjxc = tgjxc + abs(wsjxc)*vol(ijk)
      tgjyc = tgjyc + abs(wsjyc)*vol(ijk)
      tgjzc = tgjzc + abs(wsjzc)*vol(ijk)
      rsum_abs_bx = rsum_abs_bx +  abs(bxn(ijk))*vol(ijk)
      rsum_abs_by = rsum_abs_by +  abs(byn(ijk))*vol(ijk)
      rsum_abs_bz = rsum_abs_bz +  abs(bzn(ijk))*vol(ijk)
      if(abs(bxn(ijk)) .gt. rmax_abs_bx)
     &   rmax_abs_bx = abs(bxn(ijk))
      if(abs(byn(ijk)) .gt. rmax_abs_by)
     &   rmax_abs_by = abs(byn(ijk))
      if(abs(bzn(ijk)) .gt. rmax_abs_bz)
     &   rmax_abs_bz = abs(bzn(ijk))
c
      if(abs(byn(ijk)) .lt. 0.1)
     &   rsum_vol_abs_by01 = rsum_vol_abs_by01 + vol(ijk)
      if(abs(byn(ijk)) .lt. 0.01)
     &   rsum_vol_abs_by001 = rsum_vol_abs_by001 + vol(ijk)
c
      bdotj=bxn(ijk)*wsjxc+byn(ijk)*wsjyc+bzn(ijk)*wsjzc
      rbsq=1./(bxn(ijk)**2+byn(ijk)**2+bzn(ijk)**2+1.e-20)
      wsjparx=bxn(ijk)*bdotj*rbsq
      wsjpary=byn(ijk)*bdotj*rbsq
      wsjparz=bzn(ijk)*bdotj*rbsq
      wsjperx=wsjxc-wsjparx
      wsjpery=wsjyc-wsjpary
      wsjperz=wsjzc-wsjparz
c
      wsjparsq=wsjparx**2+wsjpary**2+wsjparz**2
      wsjpersq=wsjperx**2+wsjpery**2+wsjperz**2
      tgj_par = tgj_par + sqrt(wsjparsq)*vol(ijk)
      tgj_per = tgj_per + sqrt(wsjpersq)*vol(ijk)
c
c
      tgheat_par=tgheat_par+resistivity*wsjparsq*vol(ijk)
      tgheat_per=tgheat_per+resistivity*wsjpersq*vol(ijk)
c
      if(resistivity*wsjparsq .gt. rmax_gheat_par)
     &   rmax_gheat_par = resistivity*wsjparsq
      if(resistivity*wsjpersq .gt. rmax_gheat_per)
     &   rmax_gheat_per = resistivity*wsjpersq
c
      gcur_max=max(gcur_max,sqrt(wsjsq))
      vz_max=max(vz_max,w(ijk))
c     tgradx=tgradx+gradx(ijk)
c     tgrady=tgrady+grady(ijk)
c     tgradz=tgradz+gradz(ijk)
c
c  new code to compute J dot E_motional
c
      wsulc=0.125*(ul(ijk)+ul(ijk+iwid)
     &     +ul(ijk+iwid+jwid)+ul(ijk+jwid)
     &     +ul(ijk+kwid)+ul(ijk+iwid+kwid)
     &     +ul(ijk+iwid+jwid+kwid)+ul(ijk+jwid+kwid))
      wsvlc=0.125*(vl(ijk)+vl(ijk+iwid)
     &     +vl(ijk+iwid+jwid)+vl(ijk+jwid)
     &     +vl(ijk+kwid)+vl(ijk+iwid+kwid)
     &     +vl(ijk+iwid+jwid+kwid)+vl(ijk+jwid+kwid))
      wswlc=0.125*(wl(ijk)+wl(ijk+iwid)
     &     +wl(ijk+iwid+jwid)+wl(ijk+jwid)
     &     +wl(ijk+kwid)+wl(ijk+iwid+kwid)
     &     +wl(ijk+iwid+jwid+kwid)+wl(ijk+jwid+kwid))
c
c    E_motional
c
      E_mx = -wsvlc*bzn(ijk) + wswlc*byn(ijk)
      E_my = wsulc*bzn(ijk) - wswlc*bxn(ijk)
      E_mz = -wsulc*byn(ijk) + wsvlc*bxn(ijk)
c
      tgJ_dot_Em = tgJ_dot_Em + (wsjxc*E_mx +
     &    wsjyc*E_my + wsjzc*E_mz )*vol(ijk)
c
  30  continue
      k=2
      i=2
      do 31 j=2,jbp1
      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
      tgke=tgke+0.5*mv(ijk)*(u(ijk)**2+v(ijk)**2+w(ijk)**2)
      tgkex=tgkex+0.5*mv(ijk)*(u(ijk)**2)
      tgkey=tgkey+0.5*mv(ijk)*(v(ijk)**2)
      tgkez=tgkez+0.5*mv(ijk)*(w(ijk)**2)
      tgmox=tgmox+mv(ijk)*u(ijk)
      tgmoy=tgmoy+mv(ijk)*v(ijk)
      tgmoz=tgmoz+mv(ijk)*w(ijk)
   31 continue
c     do 32 n=1,ncells
c     ijk=ijkcell(n)
c     tgmass=tgmass+mc(ijk)
c  32 continue
c
c
c
cdakcdir$ ivdep
        newcell=0
        do 100 n=1,ncells
        ijk=ijkcell(n)
        np=iphead(ijk)
        if(np.ne.0) then
        newcell=newcell+1
        ijkctmp(newcell)=ijk
        iphd2(ijk)=np
        endif
  100   continue
c
c
    1 continue
        if(newcell.eq.0) go to 900
c
      do 600 n=1,newcell
      ijk=ijkctmp(n)
      np=iphd2(ijk)
      if(np.ne.0) then
      nptotl=nptotl+1
      kr=ico(np)
c
c     calculate the particle energy
      toti=toti+ep(np)
c
c     mu has dimensions flux
c

      totb=totb+0.5*(mupx(np)**2+mupy(np)**2+mupz(np)**2)
     &     /volp(np)
      totk=totk+0.5*mass(np)*(up(np)**2+vp(np)**2+wp(np)**2)
      tote=toti+totk+totb
      tpmox=tpmox+mass(np)*up(np)
      tpmoy=tpmoy+mass(np)*vp(np)
      tpmoz=tpmoz+mass(np)*wp(np)
c     tpmass=tpmass+mass(np)
c
      if(kr.eq.1) then
         avg_z=avg_z+pz(np)*mass(np)
         avg_mass=avg_mass+mass(np)
      end if
c
c     save total number in each region	
      numtot(kr)=numtot(kr)+1
c
      totv=totv+volp(np)
      totm=totm+mass(np)
      endif
c
  600 continue
c
      newcell_nxt=0
      do 500 n=1,newcell
      ijk=ijkctmp(n)
      np=link(iphd2(ijk))
      if(np.gt.0) then
      newcell_nxt=newcell_nxt+1
      iphd2(ijk)=np
      ijkctmp(newcell_nxt)=ijk
      endif
  500 continue
c
      newcell=newcell_nxt
c
      go to 1
c
  900 continue
cjub      write(*,*)'budget: ncyc, nptotl, totv, totm=',ncyc,nptotl,
cjub     &totv,totm
      write(*,*)'budget: ncyc, toti,totb,totk=',ncyc,toti,totb,totk
c
c    calculate the reconnected flux
c
      call magnetopause(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     color,gradx,grady,gradz,
     &     bxn,byn,bzn,cjump,
     &     flux,surface_area)
c
      reconnected_flux(ncyc)=flux
c
c     calculated reconnected flux - solar physics
c
	if(ncyc.gt.2) then
      call reconnection_ay(ncells,ijkcell,iwid,jwid,kwid,
     &     ibp2,jbp2,kbp2,dx,dy,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     bxn,byn,bzn,rfsolar)
        end if
c
c
c    calculate pdv, viscous dissipation 
c    add resistive heating
c
      do 1000 n=1,ncells
      ijk=ijkcell(n)
      pdv(ijk)=(-p(ijk)*divu(ijk)
     &        +exx(ijk)*pixx(ijk)+eyy(ijk)*piyy(ijk)+ezz(ijk)*pizz(ijk)
     & +2.0*(exy(ijk)*pixy(ijk)+exz(ijk)*pixz(ijk)+eyz(ijk)*piyz(ijk)))
     &    *vol(ijk)*dt/(mc(ijk)+1.e-10)
     &     +Ohmic_heating(ijk)/(mc(ijk)+1.e-10)
c
      vol1=vol(ijk)*(1.+divu(ijk)*dt)
c      vol1=volpc(ijk)*(1.+divu(ijk)*dt)
c
      divuphix(ijk)=0.0
      divuphiy(ijk)=0.0
      divuphiz(ijk)=0.0
      
      dbvx(ijk)=(bxl(ijk)*vol1-bxn(ijk)*vol(ijk)-divuphix(ijk)*dt)
     &     /(mc(ijk)+1.e-20)
      dbvy(ijk)=(byl(ijk)*vol1-byn(ijk)*vol(ijk)-divuphiy(ijk)*dt)
     &     /(mc(ijk)+1.e-20)
      dbvz(ijk)=(bzl(ijk)*vol1-bzn(ijk)*vol(ijk)-divuphiz(ijk)*dt)
     &     /(mc(ijk)+1.e-20)
c
c      dbvx(ijk)=(bxl(ijk)*vol1-bxn(ijk)*volpc(ijk)-divuphix(ijk)*dt)
c     &     /(mc(ijk)+1.e-20)
c      dbvy(ijk)=(byl(ijk)*vol1-byn(ijk)*volpc(ijk)-divuphiy(ijk)*dt)
c     &     /(mc(ijk)+1.e-20)
c      dbvz(ijk)=(bzl(ijk)*vol1-bzn(ijk)*volpc(ijk)-divuphiz(ijk)*dt)
c     &     /(mc(ijk)+1.e-20)
 1000 continue
c
c    periodic boundary conditions in i and j:
c
      dummy=0.0
c
      call torusbc(ibp2,jbp2,kbp2,
     &     cdlt,sdlt,dummy,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     dbvx,dbvy,dbvz)
c
c     no accumulation in this routine, so can call with pdv for
c     all three components
c     must change if grid surfaces are not parallel
c
      call torusbc(ibp2,jbp2,kbp2,
     &     cdlt,sdlt,dummy,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     pdv,pdv,pdv)
c
c     reflect in k
c     omf=-1./(real(kbar)+0.5)
      do 1001 i=1,ibp2
cdir$ ivdep
      do 1001 j=1,jbp2
      ijkl=1+(i-1)*iwid+(j-1)*jwid
      ijkr=1+(i-1)*iwid+(j-1)*jwid+kbp1*kwid
      pdv(ijkl)=pdv(ijkl+kwid)
      pdv(ijkr)=pdv(ijkr-kwid)
      dbvx(ijkl)=dbvx(ijkl+kwid)
      dbvx(ijkr)=dbvx(ijkr-kwid)
      dbvy(ijkl)=dbvy(ijkl+kwid)
      dbvy(ijkr)=dbvy(ijkr-kwid)
      dbvz(ijkl)=dbvz(ijkl+kwid)
      dbvz(ijkr)=dbvz(ijkr-kwid)
 1001 continue
c
      if(init_flag .eq. 1) then
       tgheat_norm = tgheat
       tgjxc_norm = tgjxc
       rmax_abs_by_norm = rmax_abs_by
       rmax_gheat_norm = rmax_gheat
       init_flag = 0
      endif
c
 1092 format(" total particle ke=",1pe10.3," total grid ke=",1pe10.3)
 1093 format(" total particle int nrg=",1pe10.3, 
     &      " total grid int nrg=",1pe10.3)
 1094 format(" total particle nrg=",1pe10.3," total grid nrg=",1pe10.3)
 1095 format(5e12.4)
 1096 format(6e12.4)
 1097 format(8e12.4)
c
      itec = 0
c
c	temporary for KH
c
c      rfsolar=avg_z/avg_mass
c      write(68,*)t,vz_max,avg_z/avg_mass
c      write(*,*)'vz_max=',vz_max,avg_z/avg_mass

	do kr=1,nrg
	write(*,*)'budget',numtot(kr)
	enddo
      return
      end

