      subroutine precond(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     s,sbx,sby,sbz,
     &     dro,dbx,dby,dbz)
c
c     a routine to calculate an approximate inverse
c
      dimension
     &     a11(*),a12(*),a13(*),a14(*),
     &     a21(*),a22(*),a23(*),a24(*),
     &     a31(*),a32(*),a33(*),a34(*),
     &     a41(*),a42(*),a43(*),a44(*),
     &     s(*),sbx(*),sby(*),sbz(*),
     &     dro(*),dbx(*),dby(*),dbz(*)
c
      do 2029 n=1,ncells
c
      y1=s(n)
      y2=sbx(n)-(a21(n)*y1)
      y3=sby(n)-(a31(n)*y1+(a32(n)*y2))
      y4=sbz(n)-(a41(n)*y1+(a42(n)*y2+(a43(n)*y3)))
c
      dbz(n)=-y4*a44(n)
      dby(n)=-(y3-(a34(n)*dbz(n)))*a33(n)
      dbx(n)=-(y2-(a23(n)*dby(n)+(a24(n)*dbz(n))))*a22(n)
      dro(n)=-(y1-(a12(n)*dbx(n)+(a13(n)*dby(n)+(a14(n)*dbz(n)))))
     &     *a11(n)
c
c
2029  continue
c
      return
      end
