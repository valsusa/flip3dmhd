
      subroutine strain(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     u,v,w,vol,gradcx,gradcy,gradcz)
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     ijkcell(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     u(*),v(*),w(*),vol(*),gradcx(*),gradcy(*),gradcz(*)
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     u,gradcx,gradcy,gradcz)
c
cdir$ ivdep
      do 100 n=1,ncells
      ijk=ijkcell(n)
      exx(ijk)=gradcx(ijk)
      exy(ijk)=0.5*gradcy(ijk)
      exz(ijk)=0.5*gradcz(ijk) 
  100 continue
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     v,gradcx,gradcy,gradcz)
c
cdir$ ivdep
      do 200 n=1,ncells
      ijk=ijkcell(n)
      exy(ijk)=exy(ijk)+0.5*gradcx(ijk)
      eyy(ijk)=gradcy(ijk)
      eyz(ijk)=0.5*gradcz(ijk) 
  200 continue
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     w,gradcx,gradcy,gradcz)
c
cdir$ ivdep
      do 300 n=1,ncells
      ijk=ijkcell(n)
      exz(ijk)=exz(ijk)+0.5*gradcx(ijk)
      eyz(ijk)=eyz(ijk)+0.5*gradcy(ijk)
      ezz(ijk)=gradcz(ijk) 
  300 continue
c
      return
      end
