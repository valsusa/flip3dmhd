PROGRAM TMOD

!  This is the test progrm for the F-90 based MP translation modules.
!
!  David H. Bailey   June 2, 1994

USE MPMODULE
IMPLICIT TYPE (MP_REAL) (A-H, O-Z)
TYPE (MP_INTEGER) IA, IB, IC
TYPE (MP_COMPLEX) C, D, E
PARAMETER (N = 25)
DIMENSION A(N), B(N)

CALL MPINIT

!   Character-to-MP assignment, generic MPREAL function, pi and e.

X = '1.234567890 1234567890 1234567890 D-100'
EE = EXP (MPREAL (1.D0))
CALL MPWRITE (6, X, MPPIC, EE)
S = 0.D0

!   Loop with subscripted MP variables.

DO I = 1, N
  A(I) = 2 * I + 1
  B(I) = 2.D0 * A(I) * (A(I) + 1.D0)
  S = S + B(I) ** 2
ENDDO
CALL MPWRITE (6, S)

!   Expression with mixed MPI and MPR entities.

IA = S
IB = 262144
S = (S + 327.25D0) * MOD (IA, 4 * IB)
CALL MPWRITE (6, S)

!   A complex square root reference.

E = SQRT (MPCMPL (2.D0 * S, S))
CALL MPWRITE (6, E)

!   External and intrinsic MP function references in expressions.

S = DOT (N, A, B)
T = 2.D0 * SQRT (S) ** 2
CALL MPWRITE (6, S, T)

S = S / 1048576.D0
T = S + 2.D0 * LOG (S)
X = 3 + NINT (T) * 5
CALL MPWRITE (6, S, T, X)

!   A deeply nested expression with function references.

X = (S + (2 * (S - 5) + 3 * (T - 5))) * EXP (COS (LOG (S)))
CALL MPWRITE (6, X)

!   A special MP subroutine call (computes both cos and sin of S).

CALL MPCSSNF (S, X, Y)
T = 1.D0 - (X ** 2 + Y ** 2)

!   IF-THEN-ELSE construct involving MP variables.

IF (ABS (T) .LT. MPEPS) THEN
  CALL MPWRITE (6, T)
ELSE
  CALL MPWRITE (6, MPEPS)
ENDIF

STOP
END

FUNCTION DOT (N, A, B)

!   MP function subprogram.

USE MPMODULE
TYPE (MP_REAL) A(N), B(N), DOT, S

S = 0.D0

DO I = 1, N
  S = S + A(I) * B(I)
ENDDO

DOT = S
RETURN
END
