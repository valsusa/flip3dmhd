      subroutine poisson_cg(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,PRECON,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     q,qtilde,Aqtilde,itmax,error,srce,
     &     residu,Aq,gradpx,gradpy,gradpz,p,diag,div)
c
c     a routine to solve poisson's equation for the pressure
C     USING A CONJUGATE RESIDUAL SOLVER
c     boundary conditions correspond to a topological torus
c     that is periodic in i and j, and with Dirichlet bc's at k=kbp2
c
      implicit real*8 (a-h,o-z)
      real*8 lambda
      logical periodic_x,periodic_y,periodic_z
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          q(*),qtilde(*),p(*),vvol(*),vvolaxis(*),div(*),
     &          Aqtilde(*),Aq(*),srce(*),
     &          vol(*),residu(*),gradpx(*),gradpy(*),gradpz(*),diag(*)
c
C
      LOGICAL PRECON
C
      write(*,*)'ncells,nvtx,itmax,error=',ncells,nvtx,itmax,error
c
c      if(precon) then
c
      call diagonal_vtx(nvtx,ijkvtx,
     &     iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     dt,vol,diag)
c
c      else
c
      DO 1111 N=1,NVTX
      DIAG(IJKvtx(N))=1.
 1111 CONTINUE
C
c      endif
c
      write(*,*) 'done with precond'
c
      do 1112 k=1,kbp1+1
      do 1112 j=1,jbp1+1
      do 1112 i=1,ibp1+1
      ijk=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
      q(ijk)=0.0
      Aq(ijk)=0.0
      qtilde(ijk)=0.0
      residu(ijk)=0.0
 1112 continue
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     p)
c
      write(*,*) 'CALCULATE THE INITIAL RESIDUAL ERROR'
c
C     CALCULATE THE INITIAL RESIDUAL ERROR
C
      call residue_vtx(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     p,gradpx,gradpy,gradpz,vvol,residu)
c
      write(*,*) 'CALCULATE THE INITIAL RESIDUAL ERROR:after'
c
      rsum=0.0
      bsum=0.0
c
      do 32 n=1,nvtx
      ijk=ijkvtx(n)
      bsum=bsum+abs(srce(ijk))
      rsum=rsum+abs(residu(ijk))
   32 continue
c
      if(rsum.le.error) then
      go to 2
      endif
c
      if(bsum.le.error) bsum=1.0
c
      do 33 n=1,nvtx
      ijk=ijkvtx(n)
      qtilde(ijk)=residu(ijk)/diag(ijk)
      q(ijk)=qtilde(ijk)+p(ijk)
   33 continue
C
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     q)
c
      call residue_vtx(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     q,gradpx,gradpy,gradpz,vvol,Aq)
c
      rdot=0.0
      do 34 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk)=q(ijk)-p(ijk)
      Aq(ijk)=residu(ijk)-Aq(ijk)
      rdot=rdot+residu(ijk)*qtilde(ijk)
   34 continue
c
c     ****************************************************************
c
c     begin conjugate gradient iteration
       write(*,*) 'begin conjugate gradient iteration'
c
c     ****************************************************************
c
      do 1 iter=1,itmax
c
      alfa=0.0
      do 5 n=1,nvtx
      ijk=ijkvtx(n)
      alfa=alfa+q(ijk)*Aq(ijk)
  5   continue
c
      alfa=rdot/alfa
c
      do 14 n=1,nvtx
      ijk=ijkvtx(n)
      p(ijk)=p(ijk)+ALFA*q(ijk)
      residu(ijk)=residu(ijk)-ALFA*Aq(ijk)
   14 continue

c
      rnorm=0.0
      dxnorm=0.0
      xnorm=0.0
      do 13 n=1,nvtx
      ijk=ijkvtx(n)
      rnorm=rnorm+abs(residu(ijk))
      dxnorm=dxnorm+abs(q(ijk))
      xnorm=xnorm+abs(p(ijk))
   13 continue
      write(*,*) 'it,rnorm=',iter,rnorm
      dxnorm=dxnorm*abs(alfa)
c
      if(dxnorm.le.error*xnorm.and.rnorm.le.error*bsum)
     &     go to 2
c
c     apply pre-conditioner
      do 10 n=1,nvtx
      ijk=ijkvtx(n)
      qtilde(ijk)=residu(ijk)/diag(ijk)
   10 continue
c
c     calculate beta
c
c
      alfa=0.0
      do 11 n=1,nvtx
      ijk=ijkvtx(n)
      alfa=alfa+qtilde(ijk)*residu(ijk)
   11 continue
c
      beta=alfa/rdot
      rdot=alfa
c
cdir$ ivdep
      do 12 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk)=qtilde(ijk)+beta*q(ijk)+p(ijk)
   12 continue
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     q)
c
c     calculate Aq
c
c
      call residue_vtx(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     q,gradpx,gradpy,gradpz,vvol,
     &     Aq)
c
c
      do 15 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=(residu(ijk)-Aq(ijk))
      q(ijk)=q(ijk)-p(ijk)
   15 continue
c
    1 continue
c
      write(6,*)'poisson: iteration fails '
      write(6,*)'dxnorm,xnorm,rnorm,bsum'
      write(6,*) dxnorm,xnorm,rnorm,bsum
      return
c
    2 continue
c     iteration has converged
      write(6,*)'poisson: converges in ',iter
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     p)
c
      return
      end
