      subroutine compute_e
c
c     a routine to compute complete e
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
	  integer :: ijk2
c
c	Add the moving frame part of E
c
c
c      call cross_product(ncells,ijkcell,iwid,jwid,kwid, 
c     &     ul,vl,wl,bxn,byn,bzn,gradx,grady,gradz)
      call cross_product(nvtx,ijkvtx,iwid,jwid,kwid, 
     &     ul,vl,wl,bxv,byv,bzv,gradx,grady,gradz)

c
c      call bc_scalar(ibp1+1,jbp1+1,kbp1+1, 
c     &     periodic_x,periodic_y,periodic_z, 
c     &     gradx)
c	  call bc_scalar(ibp1+1,jbp1+1,kbp1+1, 
c     &     periodic_x,periodic_y,periodic_z, 
c     &     grady)
c      call bc_scalar(ibp1+1,jbp1+1,kbp1+1, 
c     &     periodic_x,periodic_y,periodic_z, 
c     &     gradz)


      do n=1,nvtx
c
      ijk=ijkvtx(n)
c
      ex(ijk)=ex(ijk)-gradx(ijk)	
      ey(ijk)=ey(ijk)-grady(ijk)	
      ez(ijk)=ez(ijk)-gradz(ijk)	
c
      enddo
c
c     compute charge
c
      call divc(ncells,ijkcell,iwid,jwid,kwid, 
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x, 
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y, 
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol, 
     &                   ex,ey,ez,chden)
c
      write(*,*)'computed charge',sum(chden(ijkcell(1:ncells)))
c
	 
      do n=1,ncells
	  	  
         ijk=ijkcell(n)
         chden(ijk)=.5d0*(ex(ijk+iwid)-ex(ijk))/dx 
     &	 +.5d0*(ez(ijk+kwid)-ez(ijk))/dz
     &    +.5d0*(ex(ijk+iwid+kwid)-ex(ijk+kwid))/dx
     &    +.5d0*(ez(ijk+kwid+iwid)-ez(ijk+iwid))/dz
	     ijk2=ijk
	     ijk=ijk+jwid
	          chden(ijk2)=chden(ijk2)+.5d0*(ex(ijk+iwid)-ex(ijk))/dx 
     &	 +.5d0*(ez(ijk+kwid)-ez(ijk))/dz
     &    +.5d0*(ex(ijk+iwid+kwid)-ex(ijk+kwid))/dx
     &    +.5d0*(ez(ijk+kwid+iwid)-ez(ijk+iwid))/dz
		chden(ijk2)=chden(ijk2)/2d0
      enddo
	 
      write(*,*)'computed charge',sum(chden(ijkcell(1:ncells)))
c
c     compute electrostatic potential
c
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1, 
     &     periodic_x,periodic_y,periodic_z, 
     &     chden)
c
        errorore=1d-2
        iterazmax=100
c
        call poisson_fft(ncells, ijkcell, nvtx, ijkvtx, itdim, iwid, 
     & jwid, kwid, tiny, relax, ibp1, jbp1, kbp1, cdlt, sdlt, strait,
     & dx,dy, dz, 
     & c1x, c2x, c3x, c4x, c5x,c6x,c7x,c8x,c1y,c2y,c3y,c4y,c5y,
     & c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z, c6z, c7z, c8z, vol, 
     & vvol,vvolaxis,wate(1,1),wate(1,2),wate(1,3),wate(1,4),
     & iterazmax, 
     & errorore,chden,rdt,wate(1,7), wate(1,5), wate(1,6), 
     & elpot, wate(1,7))
      write(*,*)'computed pot',sum(elpot(ijkcell(1:ncells))**2)
	  
c
c     compute divergence of velocity
c
	  
		call divc(ncells,ijkcell,iwid,jwid,kwid, 
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x, 
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y, 
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol, 
     &                   ul,vl,wl,divergenceu)
	 
      write(*,*)'computed divergenceu',
     &   sum(divergenceu(ijkcell(1:ncells)))

c
c     compute potential to divc
c
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1, 
     &     periodic_x,periodic_y,periodic_z, 
     &     divergenceu)
c
        errorore=1d-2
        iterazmax=100
c
        call poisson_fft(ncells, ijkcell, nvtx, ijkvtx, itdim, iwid, 
     & jwid, kwid, tiny, relax, ibp1, jbp1, kbp1, cdlt, sdlt, strait,
     & dx,dy, dz, 
     & c1x, c2x, c3x, c4x, c5x,c6x,c7x,c8x,c1y,c2y,c3y,c4y,c5y,
     & c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z, c6z, c7z, c8z, vol, 
     & vvol,vvolaxis,wate(1,1),wate(1,2),wate(1,3),wate(1,4),
     & iterazmax, 
     & errorore,divergenceu,rdt, wate(1,7), wate(1,5),
     & wate(1,6), potdiv, wate(1,7))
	 
      write(*,*)'computed potdiv',sum(potdiv(ijkcell(1:ncells))**2)
c
c     compute vorticity
c

	        call curlc(ncells,ijkcell,iwid,jwid,kwid, 
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x, 
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y, 
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol, 
     &                   ul,vl,wl,curlcx,curlcy,curlcz)
	 
      write(*,*)'computed curlcy',sum(curlcy(ijkcell(1:ncells)))
c
c     compute stream function
c
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1, 
     &     periodic_x,periodic_y,periodic_z, 
     &     curlcy)
c
        errorore=1d-2
        iterazmax=100
c
        call poisson_fft(ncells, ijkcell, nvtx, ijkvtx, itdim, iwid, 
     & jwid, kwid, tiny, relax, ibp1, jbp1, kbp1, cdlt, sdlt, strait,
     & dx,dy, dz, 
     & c1x, c2x, c3x, c4x, c5x,c6x,c7x,c8x,c1y,c2y,c3y,c4y,c5y,
     & c6y, c7y, c8y, c1z, c2z, c3z, c4z, c5z, c6z, c7z, c8z, vol, 
     & vvol,vvolaxis,wate(1,1),wate(1,2),wate(1,3),wate(1,4),
     & iterazmax, 
     & errorore,curlcy,rdt, wate(1,7), wate(1,5), wate(1,6), 
     & streamf, wate(1,7))
      write(*,*)'computed streamf',sum(streamf(ijkcell(1:ncells))**2)
c
      end subroutine compute_e