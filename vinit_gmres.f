      subroutine vinit
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
c
c     a routine to prepare varibles for the next computation cycle
c     ****************************************************
c
      dummy=0.0
c
       if(.not.cartesian) then
c
c      this stuff is a carryover from the toroidal version
c
       if(rmaj.gt.0.0) then
       call torusbcv(ibp1+1,jbp1+1,kbp1+1,
     &     cdlt,sdlt,DUMMY,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     umom,vmom,wmom)
       else
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     umom)
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     vmom)
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     wmom)
       end if
c
      call torusbc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     mv)
c
      call torusbc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     numberv)
c
      call torusbc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     color)
c
       call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     umom)
       call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     vmom)
       call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     wmom)
       call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     mv)
       call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     numberv)
       call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     color)
c
      else
c
c      this routine imposes periodicity in x and y
c
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     umom)
c
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     vmom)
c
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     wmom)
c
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     mv)
c
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     numberv)
c
       call torusbc_scalar(ibp2,jbp2,kbp2,
     &     periodic_x,periodic_y,periodic_z,
     &     color)
c
      endif
c
      do 6 n=1,nvtx
      ijk=ijkvtx(n)
      factor=mv(ijk)/(mv(ijk)+1.e-10)**2
c
      u(ijk)=umom(ijk)*factor
      v(ijk)=vmom(ijk)*factor
      w(ijk)=wmom(ijk)*factor
c
      ul(ijk)=u(ijk)
      vl(ijk)=v(ijk)
      wl(ijk)=w(ijk)
c
      factor=numberv(ijk)/(numberv(ijk)+1.e-10)**2
c
      color(ijk)=color(ijk)*factor
c
    6 continue
c
c  apply rigid, free-slip wall conditions
c
      call bc_wall(ibp1,jbp1,kbp1,iwid,jwid,kwid,
     c             dx,dy,dz,x,y,z,
     &             c5x,c6x,c7x,c8x,
     &             c5y,c6y,c7y,c8y,
     &             c5z,c6z,c7z,c8z,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & t,tramp,
     &             ul,vl,wl)
c
c     if(.not.periodic_x)
c     set vl=vvi(1) and wl=wvi(1) at i=2
c     set vl=vvi(2) and wl=wvi(2) at i=ibp2
c     if(.not.periodic_y)
c     set ul=uvi(1) and wl=wvi(1) at j=2
c     set ul=uvi(2) and wl=wvi(2) at j=jbp2
c     if(.not.periodic_z)
c     set ul=uvi(1) and vl=vvi(1) at k=2
c     set ul=uvi(2) and vl=vvi(2) at k=kbp2
c
      zero=0.0d0
      if(.not.periodic_x) then
c
c     impose no slip conditions on i=2,i=ibp2 boundaries
c
      call list(2,2,2,ibp2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_open(nvtxtmp,ijktmp2,zero,vvi(1),wvi(1),
     &     ul,vl,wl,iwid)
c
      call list(ibp2,ibp2,2,jbp2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_open(nvtxtmp,ijktmp2,zero,vvi(2),wvi(2),
     &     ul,vl,wl,-iwid)
c
      endif
c
c
      if(.not.periodic_y) then
c
c     impose no slip conditions on j=2,j=jbp2 boundaries
c
      call list(2,ibp2,2,2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_noslip(nvtxtmp,ijktmp2,uvi(1),zero,wvi(1),
     &     ul,vl,wl)
c
      call list(2,ibp2,jbp2,jbp2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_noslip(nvtxtmp,ijktmp2,uvi(2),zero,wvi(2),
     &     ul,vl,wl)
c
      endif
c
      if(.not.periodic_z) then
c
c     impose no slip conditions on k=2,k=kbp2 boundaries
c
!      call list(2,ibp2,2,jbp2,2,2,iwid,jwid,kwid,
!     &     nvtxtmp,ijktmp2)
!c
!      call bc_open(nvtxtmp,ijktmp2,uvi(1),vvi(1),zero,
!     &     ul,vl,wl,kwid)
!c
!      call list(2,ibp2,2,jbp2,kbp2,kbp2,iwid,jwid,kwid,
!     &     nvtxtmp,ijktmp2)
!c
!      call bc_open(nvtxtmp,ijktmp2,uvi(2),vvi(2),zero,
!     &     ul,vl,wl,-kwid)
c
      call bc_wall(ibp1,jbp1,kbp1,iwid,jwid,kwid,
     c             dx,dy,dz,x,y,z,
     &             c5x,c6x,c7x,c8x,
     &             c5y,c6y,c7y,c8y,
     &             c5z,c6z,c7z,c8z,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & t,tramp,
     &             ul,vl,wl)
c
      endif

c
c
c
      do 50 n=1,ncells
      ijk=ijkcell(n)
      if (ringingout) then
      vol_norm=volpc(ijk)
      else
      vol_norm=vol(ijk)
      end if
      rho(ijk)=mc(ijk)/(vol_norm+1.e-10)
      bxn(ijk)=bxn(ijk)/(vol_norm+1.e-10)
      byn(ijk)=byn(ijk)/(vol_norm+1.e-10)
      bzn(ijk)=bzn(ijk)/(vol_norm+1.e-10)
c      rho(ijk)=mc(ijk)/(vol(ijk)+1.e-10)
c      bxn(ijk)=bxn(ijk)/(vol(ijk)+1.e-10)
c      byn(ijk)=byn(ijk)/(vol(ijk)+1.e-10)
c      bzn(ijk)=bzn(ijk)/(vol(ijk)+1.e-10)
c     save the magnetization
      bmagx(ijk)=bxn(ijk)
      bmagy(ijk)=byn(ijk)
      bmagz(ijk)=bzn(ijk)
c     compute the Alfven speed
      vasq(ijk)=(bxn(ijk)**2+byn(ijk)**2+bzn(ijk)**2)/(rho(ijk)+1.e-20)
      if(mc(ijk).ne.0.0) then
        sie(ijk)=sie1p(ijk)/(mc(ijk)+1.e-10)
      else
        sie(ijk)=0.0
      end if
  50  continue
c
c     set ghost cell values of b to zero
c
cjub      call bc_ghost(ibp1+1,jbp1+1,kbp1+1,iwid,jwid,kwid,
cjub     &     bxn,byn,bzn)
c
      zero=0.0
c
      call torusbc(ibp1+1,jbp1+1,kbp1+1,
     &     cdlt,sdlt,zero,zero,
     &     periodic_x,periodic_y,periodic_z,
     &     bxn,byn,bzn)
c
      if(.not.cartesian) then
c    adjust rho on the k=2 and k=kbp1 boundaries
      fmo=1./(real(kbar)+0.5)
      do 60 i=1,ibp2
cdir$ ivdep
      do 60 j=1,jbp2
      ijkl=1+(i-1)*iwid+(j-1)*jwid
      ijkr=1+(i-1)*iwid+(j-1)*jwid+kbp1*kwid
      rho(ijkl+kwid)=rho(ijkl+kwid)
     &        *(mc(ijkl+kwid)-2.*mc(ijkl))/mc(ijkl+kwid)
      rho(ijkr-kwid)=rho(ijkr-kwid)
     &        *(mc(ijkr-kwid)+mc(ijkr)*fmo)/mc(ijkr-kwid)
   60 continue
c
      endif
c
c
c     zero working arrays
c
      call list(1,ibp2,1,jbp2,1,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call setzero(nvtxtmp,ijktmp2,gradx)
      call setzero(nvtxtmp,ijktmp2,grady)
      call setzero(nvtxtmp,ijktmp2,gradz)
      call setzero(nvtxtmp,ijktmp2,pixx)
      call setzero(nvtxtmp,ijktmp2,pixy)
      call setzero(nvtxtmp,ijktmp2,pixz)
      call setzero(nvtxtmp,ijktmp2,piyy)
      call setzero(nvtxtmp,ijktmp2,piyz)
      call setzero(nvtxtmp,ijktmp2,pizz)

c     calculate the magnetic field intensity from the magnetization
c     by projection
c
      if(.not.nomag) then
c
c     1.  Calculate the divergence of M
c
      call divv(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     bxn,byn,bzn,wate(1,1))
c
cjub      call torusbc_scalar(ibp1+1,jbp1+1,kbp1+1,
cjub     &     periodic_x,periodic_y,periodic_z,
cjub     &     wate(1,1))
c
      call setzero(nvtx,ijkvtx,wate(1,7))
c
      itsub=15
c
c     impose Dirichlet boundary conditions as default
c     by setting range of do loops
c
      istart=3
      istop=ibp1
      jstart=3
      jstop=jbp1
      kstart=3
      kstop=kbp1
c
      if(periodic_x) istart=2
      if(periodic_y) jstart=2
      if(periodic_z) kstart=2
c
      call list(istart,istop,jstart,jstop,kstart,kstop,
     &     iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      do n=1,nvtxtmp
      ijk=ijktmp2(n)
c
c     set initial guess equal to source
c
      wate(ijk,7)=wate(ijk,1)+1.e-10
c      wate(ijk,1)=0.0
      enddo
c
      call bc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     wate(1,7))
c
      itest = 1
      itmax=100
      error=1.e-5
      if(itest .eq. 1)then
      call poisson_vtx(ncells,ijkcell,nvtxtmp,ijktmp2,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,PRECON,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     itsub,itmax,error,rnorm,dt,cntr,wate(1,1),
     &     wate(1,2),wate(1,3),wate(1,4),wate(1,5),
     &     wate(1,6),wate(1,10),bnorm,
     &     gradx,grady,gradz,wate(1,7),
     &     wate(1,9))
c
      else
c
      call poisson_cg(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,PRECON,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     wate(1,10),wate(1,11),wate(1,12),itmax,error,wate(1,1),
     &     wate(1,3),wate(1,4),gradx,grady,gradz,wate(1,7),
     &     wate(1,9),wate(1,2))
c
      endif

c
c     calculate divergence free magnetic field
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     wate(1,7),gradx,grady,gradz)
c
      do n=1,ncells
      ijk=ijkcell(n)
      bxn(ijk)=bxn(ijk)-gradx(ijk)
      bxl(ijk)=bxn(ijk)
      byn(ijk)=byn(ijk)-grady(ijk)
      byl(ijk)=byn(ijk)
      bzn(ijk)=bzn(ijk)-gradz(ijk)
      bzl(ijk)=bzn(ijk)
      enddo
c
      call bc_ghost(ibp1+1,jbp1+1,kbp1+1,iwid,jwid,kwid,
     &     bxn,byn,bzn,periodicx,periodicy,periodicz)
c
      call bc_ghost(ibp1+1,jbp1+1,kbp1+1,iwid,jwid,kwid,
     &     bxl,byl,bzl,periodicx,periodicy,periodicz)
c
c
      endif
c
      return
      end
