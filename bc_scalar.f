      subroutine bc_scalar(nxp,nyp,nzp,
     &     periodic_x,periodic_y,periodic_z,
     &     x)
c
c     a routine to impose double periodicity
c     for toroidal geometry
c     sets ghost cell values only
c     for cell-centered, scalar variables
c
c     called by DIVB_PROJECTION, POISSON_CG, VINIT_GMRES
c
      implicit real*8 (a-h,o-z)
c
      dimension x(nxp,nyp,*)
      logical periodic_x,periodic_y,periodic_z
c
      do 10 k=2,nzp
c
c     periodicity in the toroidal angle
c
      if(periodic_y) then
c
      do 1 i=2,nxp
c
      x(i,nyp,k)=x(i,2,k)
      x(i,1,k)=x(i,nyp-1,k)
c
    1 continue
c
      else
c
      do i=2,nxp
c
      x(i,1,k)=x(i,2,k)
cjub      x(i,nyp,k)=x(i,nyp-1,k)
c
      enddo
c
      endif
c
c     periodicity in the poloidal angle
c
      if(periodic_x) then
c
      do 2 j=2,nyp
c
      x(nxp,j,k)=x(2,j,k)
      x(1,j,k)=x(nxp-1,j,k)
c
    2 continue
c
      else
c
      do j=2,nyp
c
      x(1,j,k)=x(2,j,k)
cjub      x(nxp,j,k)=x(nxp-1,j,k)
c
      enddo
      endif
   10 continue
c
      if (periodic_z) then
c
      do i=2,nxp
      do j=2,nyp
c
      x(i,j,nzp)=x(i,j,2)
      x(i,j,1)=x(i,j,nzp-1)
c
      enddo
      enddo
c
      else
c
      do i=2,nxp
      do j=2,nyp
c
c      x(i,j,nzp)=x(i,j,2)
c      x(i,j,1)=x(i,j,nzp-1)
      x(i,j,1)=x(i,j,2)

c      x(i,j,2)=0.0

c      x(i,j,2)=x(i,j,3)
c      x(i,j,1)=0.0
c 	  putting double Neman is a bad idea: ill conditioned
c      x(i,j,nzp)=x(i,j,nzp-1)
c	  setting the volgende to nul or not setting anything appears to be the same
c      x(i,j,nzp)=0.d0

c
      enddo
      enddo
c
      endif
c
      return
      end
