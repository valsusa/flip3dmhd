      subroutine bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     phi)
c
c     a routine to impose boundary conditions for parcelc
c     data can be either periodic or Neumann in i,j,k
c     for cell-centered data
c
c      called by PARCELC
c
      integer ibar,jbar,kbar,iwid,jwid,kwid
c
      logical periodic_x,periodic_y,periodic_z
c
      real*8 phi(*)
c
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
c     impose boundary conditions in i (logical x)
c
      if(periodic_x) then
c
        do j=1,jbp1+1
        do k=1,kbp1+1
c
        ijkl=(k-1)*kwid+(j-1)*jwid+iwid+1
        ijkr=(k-1)*kwid+(j-1)*jwid+ibar*iwid+1
c
        phi(ijkl)=phi(ijkl)+phi(ijkr+iwid)
        phi(ijkr)=phi(ijkr)+phi(ijkl-iwid)
        phi(ijkr+iwid)=phi(ijkl)
        phi(ijkl-iwid)=phi(ijkr)
c
        enddo
        enddo
c
      else
c
        do j=1,jbp1+1
        do k=1,kbp1+1
c
        ijkl=(k-1)*kwid+(j-1)*jwid+iwid+1
        ijkr=(k-1)*kwid+(j-1)*jwid+ibar*iwid+1
c
        phi(ijkl)=phi(ijkl)+phi(ijkl-iwid)
        phi(ijkr)=phi(ijkr)+phi(ijkr+iwid)
        phi(ijkl-iwid)=phi(ijkl)
        phi(ijkr+iwid)=phi(ijkr)
c 
        enddo
        enddo
c
      endif
c
c
c      impose boundary conditions in j (logical y)
c
      if(periodic_y) then
c
        do i=1,ibp1+1
        do k=1,kbp1+1
c
        ijkl=(k-1)*kwid+jwid+(i-1)*iwid+1
        ijkr=(k-1)*kwid+jbar*jwid+(i-1)*iwid+1
c
        phi(ijkl)=phi(ijkl)+phi(ijkr+jwid)
        phi(ijkr)=phi(ijkr)+phi(ijkl-jwid)
        phi(ijkr+jwid)=phi(ijkl)
        phi(ijkl-jwid)=phi(ijkr)
c
        enddo
        enddo
c
      else
c
        do i=1,ibp1+1
        do k=1,kbp1+1
c
        ijkl=(k-1)*kwid+jwid+(i-1)*iwid+1
        ijkr=(k-1)*kwid+jbar*jwid+(i-1)*iwid+1
c
        phi(ijkl)=phi(ijkl)+phi(ijkl-jwid)
        phi(ijkr)=phi(ijkr)+phi(ijkr+jwid)
        phi(ijkl-jwid)=phi(ijkl)
        phi(ijkr+jwid)=phi(ijkr)
c
        enddo
        enddo
c
      endif
c
c     impose boundary conditions in k (logical z)
c
      if(periodic_z) then
c
        do i=1,ibp1+1
        do j=1,jbp1+1
c
        ijkl=kwid+(j-1)*jwid+(i-1)*iwid+1
        ijkr=kbar*kwid+(j-1)*jwid+(i-1)*iwid+1
c
        phi(ijkl)=phi(ijkl)+phi(ijkr+kwid)
        phi(ijkr)=phi(ijkr)+phi(ijkl-kwid)
        phi(ijkr+kwid)=phi(ijkl)
        phi(ijkl-kwid)=phi(ijkr)
c
        enddo
        enddo

      else
c
        do i=1,ibp1+1
        do j=1,jbp1+1
c
        ijkl=kwid+(j-1)*jwid+(i-1)*iwid+1
        ijkr=kbar*kwid+(j-1)*jwid+(i-1)*iwid+1
c
        phi(ijkl)=phi(ijkl)+phi(ijkl-kwid)
        phi(ijkr)=phi(ijkr)+phi(ijkr+kwid)
        phi(ijkl-kwid)=phi(ijkl)
        phi(ijkr+kwid)=phi(ijkr)
c
        enddo
        enddo
c
      endif
c
      return
      end
