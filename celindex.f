*dk celindex
      subroutine celindex(i1,i2,j1,j2,k1,k2,iwid,jwid,kwid,
     &     ijkcell,ijkctmp,ncells,ijkvtx,nvtxkm,nvtx)
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),ijkvtx(*),ijkctmp(*)
c
      ncells=0
c
      do 50 k=k1,k2
      do 50 j=j1,j2
      do 50 i=i1,i2
c
      ncells=ncells+1
c
      ijkcell(ncells)=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
      ijkctmp(ncells)=ijkcell(ncells)
c
   50 continue
c
c
      nvtx=0
c
      do 60 k=k1,k2
      do 60 j=j1,j2+1
      do 60 i=i1,i2+1
c
      nvtx=nvtx+1
c
      ijkvtx(nvtx)=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
c
   60 continue
c
      nvtxkm=nvtx
c
      do 70 j=j1,j2+1
      do 70 i=i1,i2+1
c
      nvtx=nvtx+1
c
      ijkvtx(nvtx)=1+(i-1)*iwid+(j-1)*jwid+k2*kwid
c
   70 continue
      return
      end
