*cd cindex
cll   common /cindex/ ................................. 77/01/10 ....
      dimension  ibp(10),iep(10),iv(8),irt(8),ibk(8)
      common /cindex/
     .        ibp,    iep,    irt,    ibk,    com2(1)       ,
     .        ibar  , jbar  , kbar  , kplas , kvac  , kvm   , kvp   ,
     .        ibp1  , ibp2  , jbp1  , jbp2  , kbp1  , kbp2  , i,j,k ,
     .        ipjk  , ipjpk , ijpk  , ijk   ,
     .        ipjkp , ipjpkp, ijpkp , ijkp  ,
     .        nay   , nay2  , nayp  , kvm2  ,
     .        fibar , fjbar , fkbar , rfibar, rfjbar, rfkbar, rbjbkb,
     .        inx   , iper  , jper  , iperp , jperp ,
     .        lp    , lpm   , ilim  ,
     .        idtc  , jdtc  , kdtc  , idtv  , jdtv  , kdtv  ,
     .        iwid  , jwid  , kwid  , ncells ,  nvtx,
     &     nh,  nphist,   nrg,   nvtxkm,  ncellsp,
     .        c2last
      equivalence  (ipjk,iv(1))
c
