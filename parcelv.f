      subroutine parcelv(ncells,ijkcell,nvtx,ijkvtx,nsp,iwid,jwid,kwid,
     &     DT,ijkctmp,
     &     itdim,iphead,iphd2,link,
     &     ico,mass,pxi,peta,pzta,up,vp,wp,
     &     wate,
     &     mv,umom,vmom,wmom,numberv,color)
c
      implicit real*8 (a-h,o-z)
c
      dimension ico(0:*),link(0:*),mass(0:*),
     &          pxi(0:*),peta(0:*),pzta(0:*),
     &          up(0:*),vp(0:*),wp(0:*),
     &          wate(itdim,27),
     &     ijkcell(*),ijkvtx(*),iphead(*),iphd2(*),
     &     ijkctmp(*),
     &     mv(*),umom(*),vmom(*),wmom(*),
     &     numberv(*),color(*)
c
      dimension ijkvstep(8)
c
      real*8 nu,mv,mass,numberv,color
c
c      a routine to interpolate particle data to the vertices of grid
c
      call vtxindx(iwid,jwid,kwid,ijkvstep)
c
c
        do 11 l=1,8
cdir$ ivdep
      do 11 n=1,nvtx
        wate(ijkvtx(n),l)=0.0
   11   continue
c
c
c     **************************************
c
c     set accumulators to zero
c
c     ********************************************
c
cdir$ ivdep
      do 25 n=1,nvtx
      ijk=ijkvtx(n)
      mv(ijk)=0.0
      umom(ijk)=0.0
      vmom(ijk)=0.0
      wmom(ijk)=0.0
      numberv(ijk)=0.0d0
      color(ijk)=0.0d0
   25 continue
c
c
        newcell=0
        do 100 n=1,ncells
        ijk=ijkcell(n)
        np=iphead(ijk)
        if(np.ne.0) then
        newcell=newcell+1
        ijkctmp(newcell)=ijkcell(n)
        iphd2(ijk)=np
        endif
  100   continue
c
c
    1 continue
c
      if(newcell.eq.0) go to 1000
c
      call watev(newcell,ijkctmp,iphd2,itdim,
     &     pxi,peta,pzta,wate)
c
c
       do 300 l=1,8
cdir$ ivdep
      do 312 n=1,newcell
c
      ijk=ijkctmp(n)
      np=iphd2(ijk)
c
      mv(ijk+ijkvstep(l))=mv(ijk+ijkvstep(l))+mass(np)
     &     *wate(ijk,l)
c
      umom(ijk+ijkvstep(l))=umom(ijk+ijkvstep(l))+
     &       mass(np)*up(np)*wate(ijk,l)
c
      vmom(ijk+ijkvstep(l))=vmom(ijk+ijkvstep(l))+
     &       mass(np)*vp(np)*wate(ijk,l)
c
      wmom(ijk+ijkvstep(l))=wmom(ijk+ijkvstep(l))+
     &       mass(np)*wp(np)*wate(ijk,l)
c
      numberv(ijk+ijkvstep(l))=numberv(ijk+ijkvstep(l))
     &     +wate(ijk,l)
c
      color(ijk+ijkvstep(l))=color(ijk+ijkvstep(l))
     &     +ico(np)*wate(ijk,l)
c
  312 continue
  300 continue
c
c
  303 continue
c
      newcell_nxt=0
      do 500 n=1,newcell
      ijk=ijkctmp(n)
      np=link(iphd2(ijk))
      if(np.gt.0) then
      newcell_nxt=newcell_nxt+1
      iphd2(ijk)=np
      ijkctmp(newcell_nxt)=ijk
      endif
  500 continue
c
c
      newcell=newcell_nxt
      go to 1
c
c
 1000 continue
      return
      end
