      subroutine mfnk_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     dt,error,r4pi,eps,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     roq,bxq,byq,bzq,rotil,bxtil,bytil,bztil,ptil,
     &     arotil,abxtil,abytil,abztil,paro,pabx,paby,pabz,
     &     aroq,abxq,abyq,abzq,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     u,v,w,mv,gm1,numit,itmax,totnrg,
     &     ul,vl,wl,vol,csq,p,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,
     &     s,sbx,sby,sbz,eh,
     &     dro,dbx,dby,dbz,
     &     celerr)
c
C***BEGIN PROLOGUE MFNK_3dMHD
C***DATE WRITTEN 1/99
c*** constructed from itercg_3dmhd
C***KEYWORD Matrix-Free Newton Krylov ITERATION, GMRES
C***AUTHORS D.A. Knoll and J.U. Brackbill (LOS ALAMOS NATIONAL LABORATORY)
C***PURPOSE
c     To solve the implicit MHD equations, including the mass continuity,
c     Faraday's, and momentum equations, J * dx = - residual 
c     where J depends upon ro, Bx, By, Bz
C
C     xnorm is the L1 norm of the solution vector
C     ynorm is the L1 norm of the RHS
C     dxnorm is the L2 norm of the newton update
C     rnorm is the L2 norm of the nonlinear residual
C     eps is the error tolerance for convergence
C     SASUM is a "BLAS" routine to sum the absolute vale of a vector
C     SDOT is a "BLAS" routine to form the inner product of two vectors
C***ROUTINES CALLED SENSITIV LUDECOMP RESIDUM PRECOND
C***END PROLOGUE ITERCG
c
c  !!!!!!!   NOTES !!!!!!!!!!!!!!!!!
c
c
c   need storage for q,  to gmres_3dmhd, this will be done
c   in nkmhd.com with q_mhd(4*ncells,itsub_mhd)
c
c    storage for Jq will be taken from {paro,pabx,paby,pabz}
c    storage for wk1 will be taken from {arotil,abxtil,abytil,abztil}
c
c   variables cdlt,sdlt,straight,dz,delta, resistivity are sent in but not
c   declared.  most seem to be needed by residue_3dmhd
c
c!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c
      logical resist
c
      integer ncells,ijkcell(*),iwid,jwid,kwid,numit,itmax,
     &     ijktmp1(*),ijktmp2(*),
     &     nvtx,ijkvtx(*),ibar,jbar,kbar
c
      real*8 divu_min,ws
c
      real*8
     &     dt,error,r4pi,eps,
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     a11(*),a12(*),a13(*),a14(*),
     &     a21(*),a22(*),a23(*),a24(*),
     &     a31(*),a32(*),a33(*),a34(*),
     &     a41(*),a42(*),a43(*),a44(*),
     &     roq(*),bxq(*),byq(*),bzq(*),
     &     rotil(*),bxtil(*),bytil(*),bztil(*),ptil(*),
     &     arotil(*),abxtil(*),abytil(*),abztil(*),
     &     paro(*),pabx(*),paby(*),pabz(*),
     &     aroq(*),abxq(*),abyq(*),abzq(*),
     &     gradx(*),grady(*),gradz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     u(*),v(*),w(*),mv(*),gm1,totnrg,
     &     ul(*),vl(*),wl(*),vol(*),csq(*),p(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),
     &     s(*),sbx(*),sby(*),sbz(*),eh(*),
     &     dro(*),dbx(*),dby(*),dbz(*),
     &     celerr(*),jx(*),jy(*),jz(*),vvol(*)
c
      real rnorm, rnorm0, dxnorm, rfac, rdamp, tol_GMRES, ynorm
      integer iprecond
c
      include "corgan.com"
      include "nkmhd.com"
c
c ********************************************************************
c
      numit=0
      iprecond = 0
c
c   old code form itercg_3dmhd, not currently used
c
c     calculate L1 norm of RHS
c
      ynorm=0.0

      do 2001 n=1,ncells
      ijk=ijkcell(n)
      ynorm=ynorm
     &     +(abs(ro(ijk))+abs(bxn(ijk))+abs(byn(ijk))+abs(bzn(ijk)))
     &     *vol(ijk)
 2001 continue
c
c     calculate the block diagonal of
c     the Jacobian matrix for pre-conditioner, P
c
c   where have I set initial quess, i.e. rol = ro .....
c
      if(iprecond .gt. 0)then
      delta=1.e-6
      dt_test=dt
      call sensitiv_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt_test,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil,
     &     s,sbx,sby,sbz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44)
      endif

c
c     perform LU decomposition of the block diagonals
c     of the  preconditioning matrix
c    
c
      if(iprecond .eq. 1) then
      call ludecomp(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44)
      endif
c
      if(iprecond .eq. 2) then
      call ludecomp3(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44)
      endif
c
c     ********************************************************************
c
c     BEGIN Newton ITERATION
c
c     *******************************************************************
c
   25 continue
      numit=numit+1
c
c     residu_3dmhd evaluates the residual error in the solution
c     with current values of the magnetic field and pressure
c
c
c     residu_3dmhd returns s,sbx,sby,sbz ... residual errors in
c     continuity and Faraday's equations
c
      do n=1,ncells
      ijk=ijkcell(n)
      ijktmp2(ijk)=n
      enddo
c
c
      if(numit .eq. 1)then
c
      call residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)

c
c     if div(u)<-1, stop the calculation
c
      divu_min=1.e20
      do n=1,ncells
      ijk=ijkcell(n)
      ws=dudx(ijk)+dvdy(ijk)+dwdz(ijk)
      divu_min=min(divu_min,ws)
      enddo
c
cjub      if(divu_min*dt.le.-1.d0) then
cjub      write(*,*)'mfnk: iteration will produce negative cell volume'
cjub      write(*,*),'divu_min=',divu_min
cjub      stop 'mfnk'
cjub      endif
c
c     compute L2 norm of residue
c
      rnorm = 0.0e0
c
      do m=1,ncells
c
      rnorm= rnorm + s(m)**2
     &     + sbx(m)**2
     &     + sby(m)**2
     &     + sbz(m)**2
c
      enddo
c
      rnorm = sqrt(rnorm)
      if(numit.eq.1) rnorm0 = rnorm
cdak      rnorms=sasum(ncells,s,1)
cdak      rnormbx=sasum(ncells,sbx,1)
cdak      rnormby=sasum(ncells,sby,1)
cdak      rnormbz=sasum(ncells,sbz,1)
c     changed by Brackbill 2 May 1996
c      if(numit.gt.10.and.rnorm.gt.YNORM) then
c      numit=itmax+1
c      return
c      endif
c
      endif
c
c     call preconditioned GMRES
c     returns dro, dBx,dBy,dBz ... Newton updates
c
c     GMRES tolerance will be 0.01 * rnorm (inexact newton method)
c
c  Need to send in storage of Jq, q, and wk1 as well as value for itsub
c
      tol_GMRES = error*rnorm
c
c      write(*,*) 'calling GMRES'
      call gmres_3dmhd(tol_GMRES,itsub_mhd,q_mhd,
     &     ijktmp1,ijktmp2,
     &     paro(1),
     &     arotil(1),
     &     dro, dbx, dby, dbz,
     &     ncells,num_eq,ijkcell,iwid,jwid,kwid,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44)
c
c     compute Newton damping scalar, rdamp, based on a maximum
c     allowable change in density, rol, of 50 %
c
      rdamp = 1.0e0
c
      do m = 1,ncells
       ijk = ijkcell(m)

      if((rol(ijk)+dro(m)) .lt. 0.5e0*rol(ijk) ) then
        rfac = -(0.5e0)*rol(ijk)/dro(m)
        if(rfac .lt. rdamp) rdamp = rfac
       elseif((rol(ijk)+dro(m)) .gt. 1.5e0*rol(ijk) ) then
        rfac = (0.5e0)*rol(ijk)/dro(m)
        if(rfac .lt. rdamp) rdamp = rfac
       endif

      end do
c
      do 2029 m=1,ncells
      ijk=ijkcell(m)
c
c
c     increment the iteration variables 
c
      rol(ijk)=rol(ijk)+ rdamp*dro(m)
      bxl(ijk)=bxl(ijk)+ rdamp*dbx(m)
      byl(ijk)=byl(ijk)+ rdamp*dby(m)
      bzl(ijk)=bzl(ijk)+ rdamp*dbz(m)
      p(ijk)=p(ijk)+csq(ijk)*rdamp*dro(m)
c
c
 2029  continue
c
c
c     calculate residual with  updated solution
c
      call residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)

c     compute L2 norm of residue
c
      rnorm = 0.0e0
      dxnorm = 0.0e0
c
      do m=1,ncells
      ijk = ijkcell(m)
c
      rnorm= rnorm + s(m)**2
     &     + sbx(m)**2
     &     + sby(m)**2
     &     + sbz(m)**2
c
      dxnorm= dxnorm + (dro(m)/rol(ijk))**2
     &     + (dbx(m)/bxl(ijk))**2
     &     + (dby(m)/byl(ijk))**2
     &     + (dbz(m)/bzl(ijk))**2
c
      enddo
c
      rnorm = sqrt(rnorm)
      dxnorm = sqrt(dxnorm)/4.0d0/float(ncells)
cdak      rnorms=sasum(ncells,s,1)
cdak      rnormbx=sasum(ncells,sbx,1)
cdak      rnormby=sasum(ncells,sby,1)
cdak      rnormbz=sasum(ncells,sbz,1)
c
c
c
c     when both nonlinear residual and newton update are
c     both sufficiently small,
c     iteration has converged
c
c      ratio=rnorm/(ynorm+1.e-50)
c      xratio=dxnorm/(xnorm+1.e-50)
c
c    where is eps set ?
c
c      if(rnorm.le.eps.and.dxnorm.le.eps.and.numit.gt.1) go to 1025
      if(rnorm/rnorm0.le.eps) go to 1025
C
      if(numit.gt.itmax) go to 9925
      go to 25
 9925 continue
      write(6,9926) numit,rnorm
c
c    should we cut time step and try again
c
 9926 format(" mfnk failed to converge, numit=",i4,"rnorm=",1pe10.2)
      return
 1025 continue
c
      return
      end
