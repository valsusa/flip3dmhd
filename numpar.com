*cd numpar
cll   common /numpar/ ................................. 77/06/08 ....
      dimension  grdbn(24)
      character*1 iddt
      real*8 mu,lam
      common /numpar/ com3(1)       ,
     .        grdbn , mu    , lam   ,
     .        a0    , b0    , colamu, omga  ,
     .        t     , dt    , rdt   , dtv   , dtc   , dtpos , dtvsav,
     .        dtx   , rdtx  ,
     .        dtcsav, anc   , omanc , rat   , rtnc  , omgav ,
     .        pi    , thrd  , sxth  , gtype , tfrcn , zmutrn, zmulon,
     .        alp   , hpalp , hmalp , trgrd , trglst, dtrg  ,
     .        frrg  , alrg  ,epsvac, 
     &        taug,r4pi,
     &        dtcon,dtvis,dtgrow,dtmin,dtpdv,dtmax,
     .        maxit , nomnit, numitv, maxitv, ndrft ,
     &        numgrid,kxi,iddt
c
