      subroutine bc_wall(ibp1,jbp1,kbp1,iwid,jwid,kwid,
     c                   dx,dy,dz,x,y,z,
     c                   c5x,c6x,c7x,c8x,
     c                   c5y,c6y,c7y,c8y,
     c                   c5z,c6z,c7z,c8z,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & t,tramp,
     c                   ul,vl,wl)
c
	  use boundary_module
c
      implicit real*8 (a-h,o-z)
c
c     a routine to impose rigid, free-slip wall conditions
c
       dimension
     c   c5x(*),c6x(*),c7x(*),c8x(*),
     c   c5y(*),c6y(*),c7y(*),c8y(*),
     c   c5z(*),c6z(*),c7z(*),c8z(*),
     c   x(*),y(*),z(*),
     c   ul(*),vl(*),wl(*)
c
      real*8 normx,normy,normz
c
      integer modex_bottom,modey_bottom,ibar,jbar
      ibar=ibp1-1
      jbar=jbp1-1
c
      pi=acos(-1.d0)
      ktop=kbp1+1
      kbot=2
      do 1 j=2,jbp1+1
      do 1 i=2,ibp1+1
          ijk=1+(i-1)*iwid+(j-1)*jwid+(ktop-1)*kwid
		  ijkbot=1+(i-1)*iwid+(j-1)*jwid+(kbot-1)*kwid
          normx=c8x(ijk-kwid)+c5x(ijk-iwid-kwid)
     &         +c6x(ijk-iwid-jwid-kwid)+c7x(ijk-jwid-kwid)
          normy=c8y(ijk-kwid)+c5y(ijk-iwid-kwid)
     &         +c6y(ijk-iwid-jwid-kwid)+c7y(ijk-jwid-kwid)
          normz=c8z(ijk-kwid)+c5z(ijk-iwid-kwid)
     &         +c6z(ijk-iwid-jwid-kwid)+c7z(ijk-jwid-kwid)
c
          udotn=(ul(ijk)*normx+vl(ijk)*normy+wl(ijk)*normz)
     &         /(normx**2+normy**2+normz**2+1.e-10)
c
c          ul(ijk)=ul(ijk)-normx*udotn
c          vl(ijk)=vl(ijk)-normy*udotn
c          wl(ijk)=wl(ijk)-normz*udotn
c      wl(ijk)=0.0
        if (boundary_top.eq.1) then
	    ul(ijk)=0.d0
	    vl(ijk)=0.d0
	    wl(ijk)=0.d0
        else if (boundary_top.eq.2) then
	    ! not implemented
	else if (boundary_top.eq.3) then
	    ul(ijk)=ul(ijkbot+kwid)
	    vl(ijk)=vl(ijkbot+kwid)
	    wl(ijk)=wl(ijkbot+kwid)
	else
	    ul(ijk)=ul(ijk-kwid*2)
	    vl(ijk)=vl(ijk-kwid*2)
	    wl(ijk)=wl(ijk-kwid*2)
	    ul(ijk-kwid)=ul(ijk-kwid*2)
	    vl(ijk-kwid)=vl(ijk-kwid*2)
	    wl(ijk-kwid)=wl(ijk-kwid*2)
	end if
c
        ijk=1+(i-1)*iwid+(j-1)*jwid+(kbot-1)*kwid
		ijktop=1+(i-1)*iwid+(j-1)*jwid+(ktop-1)*kwid
c
        if(boundary_bottom.eq.4) then
	   ul(ijk)=ul(ijk+kwid*2)
	   vl(ijk)=vl(ijk+kwid*2)
	   wl(ijk)=wl(ijk+kwid*2)
	   ul(ijk+kwid)=ul(ijk+kwid*2)
	   vl(ijk+kwid)=vl(ijk+kwid*2)
	   wl(ijk+kwid)=wl(ijk+kwid*2)
	 else if (boundary_bottom.eq.3) then
	    ul(ijk)=ul(ijktop-kwid)
	    vl(ijk)=vl(ijktop-kwid)
	    wl(ijk)=wl(ijktop-kwid)
	 
	 else if (boundary_bottom.eq.2) then
c
c     tangential velocity can be prescribed
c

      call initial_state(x(ijk),y(ijk),0.,
     &        bxshindler,byshindler,bzshindler,
     &        pdummy,xc,xsep,uprof,rhoprof)

      wave_number_x=2.*pi*modex_bottom/(ibar*dx)
      wave_number_y=2.*pi*modey_bottom/(jbar*dy)
      ftime=t/tramp
      ftime=sqrt(max(0d0,ftime*(1d0-ftime)*4d0))
	ftime=1d0
      if(ftime.le.0.0) ftime=0.0
c      ftime=sin(2.0*pi*t/tramp)
c      fspace=(x(ijk)+.1d0)**(-3.d0/2.d0)
      fspace=sin(wave_number_x*x(ijk))
c      if(abs(fspace).lt.0.5) then
c	fspace=0.0
c      else if (fspace.ge.0.5) then
c        fspace=fspace-.5
c      else
c        fspace=fspace+.5
c      end if
c
c      vl(ijk)=vwall_bottom
c     &     +vwall_bottom_perturbed*fspace
c      vl(ijk)=vl(ijk)*ftime
c      vl(ijk)=0.0	
c      ul(ijk)=0.0
c      wl(ijk)=vwall_bottom*uprof
	babs=sqrt(bxshindler**2+byshindler**2+bzshindler**2*0.)
	if(bzshindler<0.) then
	bxshindler=-bxshindler
	byshindler=-byshindler
	bzshindler=-bzshindler
	endif
      ul(ijk)=vwall_bottom*uprof*bxshindler/babs*ftime
      wl(ijk)=0.d0*vwall_bottom*uprof*bzshindler/babs*ftime
      vl(ijk)=vwall_bottom*uprof*byshindler/babs*ftime

      ul(ijk)=0d0
      wl(ijk)=vwall_bottom*uprof*ftime
      vl(ijk)=0d0


c
          normx=c8x(ijk-kwid)+c5x(ijk-iwid-kwid)
     &         +c6x(ijk-iwid-jwid-kwid)+c7x(ijk-jwid-kwid)
          normy=c8y(ijk-kwid)+c5y(ijk-iwid-kwid)
     &         +c6y(ijk-iwid-jwid-kwid)+c7y(ijk-jwid-kwid)
          normz=c8z(ijk-kwid)+c5z(ijk-iwid-kwid)
     &         +c6z(ijk-iwid-jwid-kwid)+c7z(ijk-jwid-kwid)
c
          udotn=(ul(ijk)*normx+vl(ijk)*normy+wl(ijk)*normz)
     &         /(normx**2+normy**2+normz**2+1.e-10)
c
c          ul(ijk)=ul(ijk)-normx*udotn
c          vl(ijk)=vl(ijk)-normy*udotn
c          wl(ijk)=wl(ijk)-normz*udotn
c      wl(ijk)=0.0
c
       else 
c
		ul(ijk)=0.d0
		vl(ijk)=0.d0
		wl(ijk)=0.d0
c
      end if
c
  1   continue
c
c      write(*,*)'bcwall:',maxval(vl(1:ibp1*jbp1*kbp1))
      return
      end
 





