      subroutine triple(x,y,z,ijk,iwid,jwid,kwid,vol)
c
c     a routine to calculate the sub-volume of a hexagon
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*)
c
      vol=((x(ijk+iwid)-x(ijk))*(y(ijk+jwid)-y(ijk))
     &    -(y(ijk+iwid)-y(ijk))*(x(ijk+jwid)-x(ijk)))
     &    *(z(ijk+kwid)-z(ijk))
c
     &   +((y(ijk+iwid)-y(ijk))*(z(ijk+jwid)-z(ijk))
     &    -(z(ijk+iwid)-z(ijk))*(y(ijk+jwid)-y(ijk)))
     &    *(x(ijk+kwid)-x(ijk))
c
     &   +((z(ijk+iwid)-z(ijk))*(x(ijk+jwid)-x(ijk))
     &    -(x(ijk+iwid)-x(ijk))*(z(ijk+jwid)-z(ijk)))
     &    *(y(ijk+kwid)-y(ijk))
c
      return
      end
