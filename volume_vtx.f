      subroutine volume_vtx(ncells,ijkcell,iwid,jwid,kwid,
     &    x,y,z,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     vvol)
c
c     a routine to calculate a semi-correct vertex volume
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),
     &     x(*),y(*),z(*),vvol(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*)
c
      do 1 n=1,ncells
      ijk=ijkcell(n)
c
      xc=0.125*(x(ijk+iwid)
     &         +x(ijk+iwid+jwid)
     &         +x(ijk+jwid)
     &         +x(ijk)
     &         +x(ijk+iwid+kwid)
     &         +x(ijk+iwid+jwid+kwid)
     &         +x(ijk+jwid+kwid)
     &         +x(ijk+kwid))
c
      vvol(ijk+iwid)=vvol(ijk+iwid)
     &      +c1x(ijk)*(x(ijk+iwid)-xc)
c
      vvol(ijk+iwid+jwid)=vvol(ijk+iwid+jwid)
     &      +c2x(ijk)*(x(ijk+iwid+jwid)-xc)
c
      vvol(ijk+jwid)=vvol(ijk+jwid)
     &      +c3x(ijk)*(x(ijk+jwid)-xc)
c
      vvol(ijk)=vvol(ijk)
     &      +c4x(ijk)*(x(ijk)-xc)
c
      vvol(ijk+iwid+kwid)=vvol(ijk+iwid+kwid)
     &      +c5x(ijk)*(x(ijk+iwid+kwid)-xc)
c
      vvol(ijk+iwid+jwid+kwid)=vvol(ijk+iwid+jwid+kwid)
     &      +c6x(ijk)*(x(ijk+iwid+jwid+kwid)-xc)
c
      vvol(ijk+jwid+kwid)=vvol(ijk+jwid+kwid)
     &      +c7x(ijk)*(x(ijk+jwid+kwid)-xc)
c
      vvol(ijk+kwid)=vvol(ijk+kwid)
     &      +c8x(ijk)*(x(ijk+kwid)-xc)
c
    1 continue
c
      return
      end
