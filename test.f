      subroutine axisgrad(ibp1,jbp1,iwid,jwid,kwid,
     &     gradxf,gradyf,gradzf)
c
      implicit real*8 (a-h,o-z)
c
      dimension gradxf(*),gradyf(*),gradzf(*)
c
c     calculate gradient at the axis by averaging gradients in the poloidal angle
c
      do 1 j=2,jbp1+1
c
      gradx=0.0
      grady=0.0
      gradz=0.0
c
      do 11 i=2,ibp1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
      gradx=gradx+gradxf(ijk)
      grady=grady+gradyf(ijk)
      gradz=gradz+gradzf(ijk)
   11 continue
c
      do 12 i=2,ibp1+1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
      gradxf(ijk)=gradx
      gradyf(ijk)=grady
      gradzf(ijk)=gradz
   12 continue
c
    1 continue
c
      return
      end
      subroutine axisvec(ibp1,jbp1,iwid,jwid,kwid,
     &     vvol,vvolaxis,vecx,vecy,vecz)
c
      implicit real*8 (a-h,o-z)
c
      dimension vecx(*),vecy(*),vecz(*),
     &     vvol(*),vvolaxis(*)
c
c     calculate average vector at the axis by volume averaging
c
      do 1 j=2,jbp1+1
c
      avgx=0.0
      avgy=0.0
      avgz=0.0
c
      do 11 i=2,ibp1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
      avgx=avgx+vecx(ijk)*vvolaxis(ijk)
      avgy=avgy+vecy(ijk)*vvolaxis(ijk)
      avgz=avgz+vecz(ijk)*vvolaxis(ijk)
   11 continue
c
      do 12 i=2,ibp1+1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
      rvvol=1./vvol(ijk)
      vecx(ijk)=avgx*rvvol
      vecy(ijk)=avgy*rvvol
      vecz(ijk)=avgz*rvvol
   12 continue
c
    1 continue
c
      return
      end
      subroutine axisvol(ibp1,jbp1,iwid,jwid,kwid,
     &     VVOL,NVTX,IJKVTX,
     &     vol,vvolaxis)
c
      implicit real*8 (a-h,o-z)
c
      dimension vol(*),vvolaxis(*),
     &     vvol(*),ijkvtx(*)
c
      do 11 n=1,nvtx
      vvolaxis(ijkvtx(n))=vvol(ijkvtx(n))
   11 continue
c
c
      do 1 j=2,jbp1+1
      do 1 i=2,ibp1+1
c
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid
c
      vvolaxis(ijk)=0.125*(vol(ijk)
     &                    +vol(ijk-iwid)
     &                    +vol(ijk-iwid-jwid)
     &                    +vol(ijk-jwid))
c
    1 continue
c
      return
      end
      subroutine bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,phi)
c
c     a routine to impose doubly periodic boundary conditions on phi
c
      implicit real*8 (a-h,o-z)
c
      dimension phi(*)
c
      do 5 k=2,kbp1+1
c
cdir$ ivdep
      do 51 j=1,jbp1+1
      ijkl=1+(j-1)*jwid+(k-1)*kwid
      ijkr=1+ibp1*iwid+(j-1)*jwid+(k-1)*kwid
      phi(ijkr)=phi(ijkl+iwid)
      phi(ijkl)=phi(ijkr-iwid)
   51 continue
c
cdir$ ivdep
      do 52 i=1,ibp1+1
      ijkb=1+(i-1)*iwid+(k-1)*kwid
      ijkt=1+(i-1)*iwid+jbp1*jwid+(k-1)*kwid
      phi(ijkb)=phi(ijkt-jwid)
      phi(ijkt)=phi(ijkb+jwid)
   52 continue
c
    5 continue
c
c
c     k=kbp2 plane
c
      do 1 j=2,jbp1
      do 1 i=2,ibp1
      ijk=1+(i-1)*iwid+(j-1)*jwid+kbp1*kwid
      phi(ijk)=phi(ijk-kwid)
    1 continue
c
c     minor axis of torus
c
      do 4 j=2,jbp1
cdir$ ivdep
      do 42 i=2,ibp1
      ijk=1+(i-1)*iwid+(j-1)*jwid
      phi(ijk)=phi(ijk+kwid)*0d0
   42 continue
c
    4 continue
c
      return
      end
c
      subroutine curlc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     vx,vy,vz,curlcx,curlcy,curlcz)
c
c     a routine to calculate a cell-centered curl from a vertex-centered
c     vector with components vx,vy,vz
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),
     &     ijkcell(*),
     &     vx(*),vy(*),vz(*),
     &     curlcx(*),curlcy(*),curlcz(*)
c
cdir$ ivdep
      do 100 n=1,ncells
c
      ijk=ijkcell(n)
c
      rvol=1./vol(ijk)
c
      curlcx(ijk)=(c1y(ijk)*vz(ijk+iwid)
     &           +c2y(ijk)*vz(ijk+iwid+jwid)
     &           +c3y(ijk)*vz(ijk+jwid)
     &           +c4y(ijk)*vz(ijk)
     &           +c5y(ijk)*vz(ijk+iwid+kwid)
     &           +c6y(ijk)*vz(ijk+iwid+jwid+kwid)
     &           +c7y(ijk)*vz(ijk+jwid+kwid)
     &           +c8y(ijk)*vz(ijk+kwid)
     &           -c1z(ijk)*vy(ijk+iwid)
     &           -c2z(ijk)*vy(ijk+iwid+jwid)
     &           -c3z(ijk)*vy(ijk+jwid)
     &           -c4z(ijk)*vy(ijk)
     &           -c5z(ijk)*vy(ijk+iwid+kwid)
     &           -c6z(ijk)*vy(ijk+iwid+jwid+kwid)
     &           -c7z(ijk)*vy(ijk+jwid+kwid)
     &           -c8z(ijk)*vy(ijk+kwid))
     &             *rvol
c
      curlcy(ijk)=(c1z(ijk)*vx(ijk+iwid)
     &           +c2z(ijk)*vx(ijk+iwid+jwid)
     &           +c3z(ijk)*vx(ijk+jwid)
     &           +c4z(ijk)*vx(ijk)
     &           +c5z(ijk)*vx(ijk+iwid+kwid)
     &           +c6z(ijk)*vx(ijk+iwid+jwid+kwid)
     &           +c7z(ijk)*vx(ijk+jwid+kwid)
     &           +c8z(ijk)*vx(ijk+kwid)
     &           -c1x(ijk)*vz(ijk+iwid)
     &           -c2x(ijk)*vz(ijk+iwid+jwid)
     &           -c3x(ijk)*vz(ijk+jwid)
     &           -c4x(ijk)*vz(ijk)
     &           -c5x(ijk)*vz(ijk+iwid+kwid)
     &           -c6x(ijk)*vz(ijk+iwid+jwid+kwid)
     &           -c7x(ijk)*vz(ijk+jwid+kwid)
     &           -c8x(ijk)*vz(ijk+kwid))
     &             *rvol
c
      curlcz(ijk)=(c1x(ijk)*vy(ijk+iwid)
     &           +c2x(ijk)*vy(ijk+iwid+jwid)
     &           +c3x(ijk)*vy(ijk+jwid)
     &           +c4x(ijk)*vy(ijk)
     &           +c5x(ijk)*vy(ijk+iwid+kwid)
     &           +c6x(ijk)*vy(ijk+iwid+jwid+kwid)
     &           +c7x(ijk)*vy(ijk+jwid+kwid)
     &           +c8x(ijk)*vy(ijk+kwid)
     &           -c1y(ijk)*vx(ijk+iwid)
     &           -c2y(ijk)*vx(ijk+iwid+jwid)
     &           -c3y(ijk)*vx(ijk+jwid)
     &           -c4y(ijk)*vx(ijk)
     &           -c5y(ijk)*vx(ijk+iwid+kwid)
     &           -c6y(ijk)*vx(ijk+iwid+jwid+kwid)
     &           -c7y(ijk)*vx(ijk+jwid+kwid)
     &           -c8y(ijk)*vx(ijk+kwid))
     &             *rvol
c
  100 continue
c
      return
      end
c
      subroutine curlv(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     vx,vy,vz,curlvx,curlvy,curlvz)
c
c     a routine to calculate a vertex-centered curl
c     from a cell-centered vector
c     vector with components vx,vy,vz
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     ijkvtx(*),
     &     vx(*),vy(*),vz(*),
     &     curlvx(*),curlvy(*),curlvz(*)
c
cdir$ ivdep
      do 100 n=1,nvtx
c
      ijk=ijkvtx(n)
c
c
      curlvx(ijk)=-(c1y(ijk-iwid)*vz(ijk-iwid)
     &           +c2y(ijk-iwid-jwid)*vz(ijk-iwid-jwid)
     &           +c3y(ijk-jwid)*vz(ijk-jwid)
     &           +c4y(ijk)*vz(ijk)
     &           +c5y(ijk-iwid-kwid)*vz(ijk-iwid-kwid)
     &           +c6y(ijk-iwid-jwid-kwid)*vz(ijk-iwid-jwid-kwid)
     &           +c7y(ijk-jwid-kwid)*vz(ijk-jwid-kwid)
     &           +c8y(ijk-kwid)*vz(ijk-kwid)
     &           -c1z(ijk-iwid)*vy(ijk-iwid)
     &           -c2z(ijk-iwid-jwid)*vy(ijk-iwid-jwid)
     &           -c3z(ijk-jwid)*vy(ijk-jwid)
     &           -c4z(ijk)*vy(ijk)
     &           -c5z(ijk-iwid-kwid)*vy(ijk-iwid-kwid)
     &           -c6z(ijk-iwid-jwid-kwid)*vy(ijk-iwid-jwid-kwid)
     &           -c7z(ijk-jwid-kwid)*vy(ijk-jwid-kwid)
     &           -c8z(ijk-kwid)*vy(ijk-kwid))
c
      curlvy(ijk)=-(c1z(ijk-iwid)*vx(ijk-iwid)
     &           +c2z(ijk-iwid-jwid)*vx(ijk-iwid-jwid)
     &           +c3z(ijk-jwid)*vx(ijk-jwid)
     &           +c4z(ijk)*vx(ijk)
     &           +c5z(ijk-iwid-kwid)*vx(ijk-iwid-kwid)
     &           +c6z(ijk-iwid-jwid-kwid)*vx(ijk-iwid-jwid-kwid)
     &           +c7z(ijk-jwid-kwid)*vx(ijk-jwid-kwid)
     &           +c8z(ijk-kwid)*vx(ijk-kwid)
     &           -c1x(ijk-iwid)*vz(ijk-iwid)
     &           -c2x(ijk-iwid-jwid)*vz(ijk-iwid-jwid)
     &           -c3x(ijk-jwid)*vz(ijk-jwid)
     &           -c4x(ijk)*vz(ijk)
     &           -c5x(ijk-iwid-kwid)*vz(ijk-iwid-kwid)
     &           -c6x(ijk-iwid-jwid-kwid)*vz(ijk-iwid-jwid-kwid)
     &           -c7x(ijk-jwid-kwid)*vz(ijk-jwid-kwid)
     &           -c8x(ijk-kwid)*vz(ijk-kwid))
c
      curlvz(ijk)=-(c1x(ijk-iwid)*vy(ijk-iwid)
     &           +c2x(ijk-iwid-jwid)*vy(ijk-iwid-jwid)
     &           +c3x(ijk-jwid)*vy(ijk-jwid)
     &           +c4x(ijk)*vy(ijk)
     &           +c5x(ijk-iwid-kwid)*vy(ijk-iwid-kwid)
     &           +c6x(ijk-iwid-jwid-kwid)*vy(ijk-iwid-jwid-kwid)
     &           +c7x(ijk-jwid-kwid)*vy(ijk-jwid-kwid)
     &           +c8x(ijk-kwid)*vy(ijk-kwid)
     &           -c1y(ijk-iwid)*vx(ijk-iwid)
     &           -c2y(ijk-iwid-jwid)*vx(ijk-iwid-jwid)
     &           -c3y(ijk-jwid)*vx(ijk-jwid)
     &           -c4y(ijk)*vx(ijk)
     &           -c5y(ijk-iwid-kwid)*vx(ijk-iwid-kwid)
     &           -c6y(ijk-iwid-jwid-kwid)*vx(ijk-iwid-jwid-kwid)
     &           -c7y(ijk-jwid-kwid)*vx(ijk-jwid-kwid)
     &           -c8y(ijk-kwid)*vx(ijk-kwid))
c
  100 continue
c
      return
      end
c
      subroutine divc(ncells,ijkcell,iwid,jwid,kwid,
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &                   ex,ey,ez,dive)
c
c     a routine to calculate the divergence of a vector e
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),ex(*),ey(*),ez(*),dive(*)
c
cdir$ ivdep
      do 1 n=1,ncells
c
      ijk=ijkcell(n)
c
      divex=(c1x(ijk)*ex(ijk+iwid)
     &     +(c2x(ijk)*ex(ijk+iwid+jwid)
     &     +(c3x(ijk)*ex(ijk+jwid)
     &     +(c4x(ijk)*ex(ijk)
     &     +(c5x(ijk)*ex(ijk+iwid+kwid)
     &     +(c6x(ijk)*ex(ijk+iwid+jwid+kwid)
     &     +(c7x(ijk)*ex(ijk+jwid+kwid)
     &     +(c8x(ijk)*ex(ijk+kwid)))))))))
c
      divey=(c1y(ijk)*ey(ijk+iwid)
     &     +(c2y(ijk)*ey(ijk+iwid+jwid)
     &     +(c3y(ijk)*ey(ijk+jwid)
     &     +(c4y(ijk)*ey(ijk)
     &     +(c5y(ijk)*ey(ijk+iwid+kwid)
     &     +(c6y(ijk)*ey(ijk+iwid+jwid+kwid)
     &     +(c7y(ijk)*ey(ijk+jwid+kwid)
     &     +(c8y(ijk)*ey(ijk+kwid)))))))))
c
      divez=(c1z(ijk)*ez(ijk+iwid)
     &     +(c2z(ijk)*ez(ijk+iwid+jwid)
     &     +(c3z(ijk)*ez(ijk+jwid)
     &     +(c4z(ijk)*ez(ijk)
     &     +(c5z(ijk)*ez(ijk+iwid+kwid)
     &     +(c6z(ijk)*ez(ijk+iwid+jwid+kwid)
     &     +(c7z(ijk)*ez(ijk+jwid+kwid)
     &     +(c8z(ijk)*ez(ijk+kwid)))))))))
c
      dive(ijk)=(divex+divey+divez)/vol(ijk)
c
    1 continue
c
      return
      end
c
      subroutine divv(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     vx,vy,vz,diverge)
c
c     a routine to calculate a vertex-centered divergence
c     from a cell-centered vector
c     vector with components vx,vy,vz
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vx(*),vy(*),vz(*),
     &     diverge(*),ijkvtx(*)
c
cdir$ ivdep
      do 100 n=1,nvtx
c
      ijk=ijkvtx(n)
c
c
c
      divergex=-(c1x(ijk-iwid)*vx(ijk-iwid)
     &           +c2x(ijk-iwid-jwid)*vx(ijk-iwid-jwid)
     &           +c3x(ijk-jwid)*vx(ijk-jwid)
     &           +c4x(ijk)*vx(ijk)
     &           +c5x(ijk-iwid-kwid)*vx(ijk-iwid-kwid)
     &           +c6x(ijk-iwid-jwid-kwid)*vx(ijk-iwid-jwid-kwid)
     &           +c7x(ijk-jwid-kwid)*vx(ijk-jwid-kwid)
     &           +c8x(ijk-kwid)*vx(ijk-kwid))
c
      divergey=-(c1y(ijk-iwid)*vy(ijk-iwid)
     &           +c2y(ijk-iwid-jwid)*vy(ijk-iwid-jwid)
     &           +c3y(ijk-jwid)*vy(ijk-jwid)
     &           +c4y(ijk)*vy(ijk)
     &           +c5y(ijk-iwid-kwid)*vy(ijk-iwid-kwid)
     &           +c6y(ijk-iwid-jwid-kwid)*vy(ijk-iwid-jwid-kwid)
     &           +c7y(ijk-jwid-kwid)*vy(ijk-jwid-kwid)
     &           +c8y(ijk-kwid)*vy(ijk-kwid))
c
      divergez=-(c1z(ijk-iwid)*vz(ijk-iwid)
     &           +c2z(ijk-iwid-jwid)*vz(ijk-iwid-jwid)
     &           +c3z(ijk-jwid)*vz(ijk-jwid)
     &           +c4z(ijk)*vz(ijk)
     &           +c5z(ijk-iwid-kwid)*vz(ijk-iwid-kwid)
     &           +c6z(ijk-iwid-jwid-kwid)*vz(ijk-iwid-jwid-kwid)
     &           +c7z(ijk-jwid-kwid)*vz(ijk-jwid-kwid)
     &           +c8z(ijk-kwid)*vz(ijk-kwid))
c
      diverge(ijk)=divergex+divergey+divergez

c      if (n.lt.5) then
c         print *,'in divergence:',n,ijk,
c     &        vx(ijk),vx(ijk-iwid),vx(ijk-jwid),vx(ijk-iwid-jwid),
c     &        vx(ijk-iwid-kwid),vx(ijk-jwid-kwid),
c     &        vx(ijk-iwid-jwid-kwid),vx(ijk-kwid),
c     &        c1x(ijk-iwid),c2x(ijk-iwid-jwid),c3x(ijk-jwid),
c     &        c4x(ijk),c5x(ijk-iwid-kwid),c6x(ijk-iwid-jwid-kwid),
c     &        c7x(ijk-jwid-kwid),c8x(ijk-kwid),divergex
c      end if
c
  100 continue
c
      return
      end
c
      subroutine divpi(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     divpix,divpiy,divpiz)
c
c     a rouTINE to calculate the divergence of a tensor
c     the tensor is cell-centered
c     the divergence, a vector, is vertex-centered
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     ijkvtx(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     divpix(*),divpiy(*),divpiz(*)
c
cdir$ ivdep
      do 100 n=1,nvtx
c
      ijk=ijkvtx(n)
c
c
      divxx=-(c1x(ijk-iwid)*pixx(ijk-iwid)
     &           +(c2x(ijk-iwid-jwid)*pixx(ijk-iwid-jwid)
     &           +(c3x(ijk-jwid)*pixx(ijk-jwid)
     &           +(c4x(ijk)*pixx(ijk)
     &           +(c5x(ijk-iwid-kwid)*pixx(ijk-iwid-kwid)
     &           +(c6x(ijk-iwid-jwid-kwid)*pixx(ijk-iwid-jwid-kwid)
     &           +(c7x(ijk-jwid-kwid)*pixx(ijk-jwid-kwid)
     &           +(c8x(ijk-kwid)*pixx(ijk-kwid)))))))))
c
      divyx=-(c1y(ijk-iwid)*pixy(ijk-iwid)
     &           +(c2y(ijk-iwid-jwid)*pixy(ijk-iwid-jwid)
     &           +(c3y(ijk-jwid)*pixy(ijk-jwid)
     &           +(c4y(ijk)*pixy(ijk)
     &           +(c5y(ijk-iwid-kwid)*pixy(ijk-iwid-kwid)
     &           +(c6y(ijk-iwid-jwid-kwid)*pixy(ijk-iwid-jwid-kwid)
     &           +(c7y(ijk-jwid-kwid)*pixy(ijk-jwid-kwid)
     &           +(c8y(ijk-kwid)*pixy(ijk-kwid)))))))))
c
      divzx=-(c1z(ijk-iwid)*pixz(ijk-iwid)
     &           +(c2z(ijk-iwid-jwid)*pixz(ijk-iwid-jwid)
     &           +(c3z(ijk-jwid)*pixz(ijk-jwid)
     &           +(c4z(ijk)*pixz(ijk)
     &           +(c5z(ijk-iwid-kwid)*pixz(ijk-iwid-kwid)
     &           +(c6z(ijk-iwid-jwid-kwid)*pixz(ijk-iwid-jwid-kwid)
     &           +(c7z(ijk-jwid-kwid)*pixz(ijk-jwid-kwid)
     &           +(c8z(ijk-kwid)*pixz(ijk-kwid)))))))))
c
      divpix(ijk)=divxx+divyx+divzx
c
  100 continue
c
cdir$ ivdep
      do 200 n=1,nvtx
c
      ijk=ijkvtx(n)
c
      divxy=-(c1x(ijk-iwid)*pixy(ijk-iwid)
     &           +(c2x(ijk-iwid-jwid)*pixy(ijk-iwid-jwid)
     &           +(c3x(ijk-jwid)*pixy(ijk-jwid)
     &           +(c4x(ijk)*pixy(ijk)
     &           +(c5x(ijk-iwid-kwid)*pixy(ijk-iwid-kwid)
     &           +(c6x(ijk-iwid-jwid-kwid)*pixy(ijk-iwid-jwid-kwid)
     &           +(c7x(ijk-jwid-kwid)*pixy(ijk-jwid-kwid)
     &           +(c8x(ijk-kwid)*pixy(ijk-kwid)))))))))
c
      divyy=-(c1y(ijk-iwid)*piyy(ijk-iwid)
     &           +(c2y(ijk-iwid-jwid)*piyy(ijk-iwid-jwid)
     &           +(c3y(ijk-jwid)*piyy(ijk-jwid)
     &           +(c4y(ijk)*piyy(ijk)
     &           +(c5y(ijk-iwid-kwid)*piyy(ijk-iwid-kwid)
     &           +(c6y(ijk-iwid-jwid-kwid)*piyy(ijk-iwid-jwid-kwid)
     &           +(c7y(ijk-jwid-kwid)*piyy(ijk-jwid-kwid)
     &           +(c8y(ijk-kwid)*piyy(ijk-kwid)))))))))
c
      divzy=-(c1z(ijk-iwid)*piyz(ijk-iwid)
     &           +(c2z(ijk-iwid-jwid)*piyz(ijk-iwid-jwid)
     &           +(c3z(ijk-jwid)*piyz(ijk-jwid)
     &           +(c4z(ijk)*piyz(ijk)
     &           +(c5z(ijk-iwid-kwid)*piyz(ijk-iwid-kwid)
     &           +(c6z(ijk-iwid-jwid-kwid)*piyz(ijk-iwid-jwid-kwid)
     &           +(c7z(ijk-jwid-kwid)*piyz(ijk-jwid-kwid)
     &           +(c8z(ijk-kwid)*piyz(ijk-kwid)))))))))
c
cdak      divpiy(ijk)=divxy+divyy+divyz
      divpiy(ijk)=divxy+divyy+divzy
c
  200 continue
c
cdir$ ivdep
      do 300 n=1,nvtx
c
      ijk=ijkvtx(n)
c
      divxz=-(c1x(ijk-iwid)*pixz(ijk-iwid)
     &           +(c2x(ijk-iwid-jwid)*pixz(ijk-iwid-jwid)
     &           +(c3x(ijk-jwid)*pixz(ijk-jwid)
     &           +(c4x(ijk)*pixz(ijk)
     &           +(c5x(ijk-iwid-kwid)*pixz(ijk-iwid-kwid)
     &           +(c6x(ijk-iwid-jwid-kwid)*pixz(ijk-iwid-jwid-kwid)
     &           +(c7x(ijk-jwid-kwid)*pixz(ijk-jwid-kwid)
     &           +(c8x(ijk-kwid)*pixz(ijk-kwid)))))))))
c
      divyz=-(c1y(ijk-iwid)*piyz(ijk-iwid)
     &           +(c2y(ijk-iwid-jwid)*piyz(ijk-iwid-jwid)
     &           +(c3y(ijk-jwid)*piyz(ijk-jwid)
     &           +(c4y(ijk)*piyz(ijk)
     &           +(c5y(ijk-iwid-kwid)*piyz(ijk-iwid-kwid)
     &           +(c6y(ijk-iwid-jwid-kwid)*piyz(ijk-iwid-jwid-kwid)
     &           +(c7y(ijk-jwid-kwid)*piyz(ijk-jwid-kwid)
     &           +(c8y(ijk-kwid)*piyz(ijk-kwid)))))))))
c
      divzz=-(c1z(ijk-iwid)*pizz(ijk-iwid)
     &           +(c2z(ijk-iwid-jwid)*pizz(ijk-iwid-jwid)
     &           +(c3z(ijk-jwid)*pizz(ijk-jwid)
     &           +(c4z(ijk)*pizz(ijk)
     &           +(c5z(ijk-iwid-kwid)*pizz(ijk-iwid-kwid)
     &           +(c6z(ijk-iwid-jwid-kwid)*pizz(ijk-iwid-jwid-kwid)
     &           +(c7z(ijk-jwid-kwid)*pizz(ijk-jwid-kwid)
     &           +(c8z(ijk-kwid)*pizz(ijk-kwid)))))))))
c
      divpiz(ijk)=divxz+divyz+divzz
c
  300 continue
c
      return
      end
c
      subroutine gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     psi,gradcx,gradcy,gradcz)
c
c     a routine to calculate a cell-centered grad from a vertex-centered
c     scalar
c
      implicit real*8 (a-h,o-z)
c
      integer ncells,ijkcell(*)
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),
     &     psi(*),
     &     gradcx(*),gradcy(*),gradcz(*)
c
cdir$ ivdep
      do 100 n=1,ncells
c
      ijk=ijkcell(n)
c
c
      rvol=1./(vol(ijk)+1.e-20)
c
      gradcx(ijk)=(c1x(ijk)*psi(ijk+iwid)
     &           +(c2x(ijk)*psi(ijk+iwid+jwid)
     &           +(c3x(ijk)*psi(ijk+jwid)
     &           +(c4x(ijk)*psi(ijk)
     &           +(c5x(ijk)*psi(ijk+iwid+kwid)
     &           +(c6x(ijk)*psi(ijk+iwid+jwid+kwid)
     &           +(c7x(ijk)*psi(ijk+jwid+kwid)
     &           +(c8x(ijk)*psi(ijk+kwid)))))))))
     &             *rvol
c
      gradcy(ijk)=(c1y(ijk)*psi(ijk+iwid)
     &           +(c2y(ijk)*psi(ijk+iwid+jwid)
     &           +(c3y(ijk)*psi(ijk+jwid)
     &           +(c4y(ijk)*psi(ijk)
     &           +(c5y(ijk)*psi(ijk+iwid+kwid)
     &           +(c6y(ijk)*psi(ijk+iwid+jwid+kwid)
     &           +(c7y(ijk)*psi(ijk+jwid+kwid)
     &           +(c8y(ijk)*psi(ijk+kwid)))))))))
     &             *rvol
c
      gradcz(ijk)=(c1z(ijk)*psi(ijk+iwid)
     &           +(c2z(ijk)*psi(ijk+iwid+jwid)
     &           +(c3z(ijk)*psi(ijk+jwid)
     &           +(c4z(ijk)*psi(ijk)
     &           +(c5z(ijk)*psi(ijk+iwid+kwid)
     &           +(c6z(ijk)*psi(ijk+iwid+jwid+kwid)
     &           +(c7z(ijk)*psi(ijk+jwid+kwid)
     &           +(c8z(ijk)*psi(ijk+kwid)))))))))
     &             *rvol
c
  100 continue
c
      return
      end
c
      subroutine gradf(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     f,gradxf,gradyf,gradzf)
c
c     a routine to calculate the gradient of f
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     ijkvtx(*),
     &     f(*),gradxf(*),gradyf(*),gradzf(*)
c
c      tiny=0.125*1.e-3
      tiny=0.0
c
cdir$ ivdep
      do 100 n=1,nvtx
c
      ijk=ijkvtx(n)
c
c
      gradxf(ijk)=-(c1x(ijk-iwid)*(f(ijk-iwid))
     &           +(c2x(ijk-iwid-jwid)*(f(ijk-iwid-jwid))
     &           +(c3x(ijk-jwid)*(f(ijk-jwid))
     &           +(c4x(ijk)*(f(ijk))
     &           +(c5x(ijk-iwid-kwid)*(f(ijk-iwid-kwid))
     &           +(c6x(ijk-iwid-jwid-kwid)*(f(ijk-iwid-jwid-kwid))
     &           +(c7x(ijk-jwid-kwid)*(f(ijk-jwid-kwid))
     &           +(c8x(ijk-kwid)*(f(ijk-kwid))))))))))
c
      gradyf(ijk)=-(c1y(ijk-iwid)*(f(ijk-iwid))
     &           +(c2y(ijk-iwid-jwid)*(f(ijk-iwid-jwid))
     &           +(c3y(ijk-jwid)*(f(ijk-jwid))
     &           +(c4y(ijk)*(f(ijk))
     &           +(c5y(ijk-iwid-kwid)*(f(ijk-iwid-kwid))
     &           +(c6y(ijk-iwid-jwid-kwid)*(f(ijk-iwid-jwid-kwid))
     &           +(c7y(ijk-jwid-kwid)*(f(ijk-jwid-kwid))
     &           +(c8y(ijk-kwid)*(f(ijk-kwid))))))))))
c
      gradzf(ijk)=-(c1z(ijk-iwid)*(f(ijk-iwid))
     &           +(c2z(ijk-iwid-jwid)*(f(ijk-iwid-jwid))
     &           +(c3z(ijk-jwid)*(f(ijk-jwid))
     &           +(c4z(ijk)*(f(ijk))
     &           +(c5z(ijk-iwid-kwid)*(f(ijk-iwid-kwid))
     &           +(c6z(ijk-iwid-jwid-kwid)*(f(ijk-iwid-jwid-kwid))
     &           +(c7z(ijk-jwid-kwid)*(f(ijk-jwid-kwid))
     &           +(c8z(ijk-kwid)*(f(ijk-kwid))))))))))
c
  100 continue
c
      return
      end
c
      subroutine graddiv(ncells,ijkcell,iwid,jwid,kwid,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     ex,ey,ez,divetil,gdivx,gdivy,gdivz)
c
c     a routine to calculate the right-hand-side for Faraday's law
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),ex(*),ey(*),ez(*),divetil(*),
     &     gdivx(*),gdivy(*),gdivz(*)
c
c
c       ************************************************************************
c
C     CALCULATE GRAD(DIV(E))
c
      call list(1,ibp1,1,jbp1,2,kbp1,iwid,jwid,kwid,
     &     ncells,ijkcell)
c
      call divc(ncells,ijkcell,iwid,jwid,kwid,
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &                   ex,ey,ez,DIVETIL)
c
      call list(2,ibp1,2,jbp1,2,kbp1,iwid,jwid,kwid,
     &     ncells,ijkcell)
c
      call gradf(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     DIVETIL,GDIVX,GDIVY,GDIVZ)
c
      call bcperv(ibp1+1,jbp1+1,kbp1+1,iwid,jwid,kwid,
     &     sdlt,cdlt,
     &     GDIVX,GDIVY,GDIVZ)
c
c     it is assumed cartesian=.t.
c      call axisgrad(ibp1,jbp1,iwid,jwid,kwid,
c     &     GDIVX,GDIVY,GDIVZ)
c
c
c     END CALCULATION OF GRAD(DIV(E))
c
c     ********************************************************************
C
      return
      end
c
      subroutine list(i1,i2,j1,j2,k1,k2,iwid,jwid,kwid,
     &     nlist,ijklist)
c
c     a routine to form a singly indexed array of indices of active cells
c
c     nlist ... total number of active cells
c     ijklist ... cell indices
c
      implicit real*8 (a-h,o-z)
c
      dimension ijklist(*)
c
      nlist=0
c
      do 1 k=k1,k2
      do 1 j=j1,j2
      do 1 i=i1,i2
c
      nlist=nlist+1
c
      ijklist(nlist)=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
c
    1 continue
c
      return
      end
c
      subroutine torusbcv(nxp,nyp,nzp,
     &     cdlt,sdlt,strait,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     x,y,z)
c
c     a routine to impose double periodicity
c     for toroidal geometry for vertex vector variables
c     assembles data
c
c     called by ACCEL_3DMHD, VINIT_GMRES
      implicit real*8 (a-h,o-z)
      logical periodic_x,periodic_y,periodic_z
c
      dimension x(nxp,nyp,*),y(nxp,nyp,*),z(nxp,nyp,*)
c
      do 10 k=2,nzp
c
c     periodicity in the toroidal angle
c
      if(periodic_y) then
c
      do 1 i=2,nxp
c
      x(i,2,k)=cdlt*x(i,nyp,k)+sdlt*y(i,nyp,k)+x(i,2,k)
      y(i,2,k)=-sdlt*x(i,nyp,k)+cdlt*y(i,nyp,k)+y(i,2,k)
     &     -strait*dz
      z(i,2,k)=z(i,nyp,k)+z(i,2,k)
c
      x(i,nyp,k)=cdlt*x(i,2,k)-sdlt*y(i,2,k)
      y(i,nyp,k)=sdlt*x(i,2,k)+cdlt*y(i,2,k)
     &     +strait*dz
      z(i,nyp,k)=z(i,2,k)
c
    1 continue
c
      endif
c
c     periodicity in the poloidal angle
c
      if(periodic_x) then
c
      do 2 j=2,nyp
c
      x(2,j,k)=x(nxp,j,k)+x(2,j,k)
      y(2,j,k)=y(nxp,j,k)+y(2,j,k)
      z(2,j,k)=z(nxp,j,k)+z(2,j,k)
c
      x(nxp,j,k)=x(2,j,k)
      y(nxp,j,k)=y(2,j,k)
      z(nxp,j,k)=z(2,j,k)
c
    2 continue
c
      endif
c
   10 continue
c
      return
      end


      subroutine cross_product(nvtx,ijkvtx,iwid,jwid,kwid,
     &     ul,vl,wl,bxn,byn,bzn,cx,cy,cz)
c
c     a routine to calculate the divergence of a vector e
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkvtx(*),
     &     ul(*),vl(*),wl(*),bxn(*),byn(*),bzn(*),
     &     cx(*),cy(*),cz(*)
c
cdir$ ivdep
      do 1 n=1,nvtx
c
      ijk=ijkvtx(n)
c
      cx(ijk)=vl(ijk)*bzn(ijk)-wl(ijk)*byn(ijk)
      cy(ijk)=wl(ijk)*bxn(ijk)-ul(ijk)*bzn(ijk)
      cz(ijk)=ul(ijk)*byn(ijk)-vl(ijk)*bxn(ijk)
c
    1 continue
c
      return
      end
c
