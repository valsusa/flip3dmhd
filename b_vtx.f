      subroutine b_vtx(ncells,ijkcell,iwid,jwid,kwid,
     &    nvtx,ijkvtx,
     &    bx,by,bz,
     &    vol,vvol,
     &    bxv,byv,bzv)
c
c     a routine to calculate a semi-correct vertex magnetic field
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),ijkvtx(*),
     &     bx(*),by(*),bz(*),vvol(*),vol(*),
     &     bxv(*),byv(*),bzv(*)
c
      do n=1,nvtx
      ijk=ijkvtx(n)
c
      bxv(ijk)=0.0d0
      byv(ijk)=0.0d0
      bzv(ijk)=0.0d0
      vvol(ijk)=0.0d0
c
      enddo
c
      do 1 n=1,ncells
      ijk=ijkcell(n)
c
      bxv(ijk+iwid)=bxv(ijk+iwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk+iwid+jwid)=bxv(ijk+iwid+jwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk+jwid)=bxv(ijk+jwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk)=bxv(ijk)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk+iwid+kwid)=bxv(ijk+iwid+kwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk+iwid+jwid+kwid)=bxv(ijk+iwid+jwid+kwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk+jwid+kwid)=bxv(ijk+jwid+kwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      bxv(ijk+kwid)=bxv(ijk+kwid)
     &      +0.25*bx(ijk)*vol(ijk)
c
      byv(ijk+iwid)=byv(ijk+iwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk+iwid+jwid)=byv(ijk+iwid+jwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk+jwid)=byv(ijk+jwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk)=byv(ijk)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk+iwid+kwid)=byv(ijk+iwid+kwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk+iwid+jwid+kwid)=byv(ijk+iwid+jwid+kwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk+jwid+kwid)=byv(ijk+jwid+kwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      byv(ijk+kwid)=byv(ijk+kwid)
     &      +0.25*by(ijk)*vol(ijk)
c
      bzv(ijk+iwid)=bzv(ijk+iwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk+iwid+jwid)=bzv(ijk+iwid+jwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk+jwid)=bzv(ijk+jwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk)=bzv(ijk)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk+iwid+kwid)=bzv(ijk+iwid+kwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk+iwid+jwid+kwid)=bzv(ijk+iwid+jwid+kwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk+jwid+kwid)=bzv(ijk+jwid+kwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      bzv(ijk+kwid)=bzv(ijk+kwid)
     &      +0.25*bz(ijk)*vol(ijk)
c
      vvol(ijk+iwid)=vvol(ijk+iwid)
     &      +0.25*vol(ijk)
c
      vvol(ijk+iwid+jwid)=vvol(ijk+iwid+jwid)
     &      +0.25*vol(ijk)
c
      vvol(ijk+jwid)=vvol(ijk+jwid)
     &      +0.25*vol(ijk)
c
      vvol(ijk)=vvol(ijk)
     &      +0.25*vol(ijk)
c
      vvol(ijk+iwid+kwid)=vvol(ijk+iwid+kwid)
     &      +0.25*vol(ijk)
c
      vvol(ijk+iwid+jwid+kwid)=vvol(ijk+iwid+jwid+kwid)
     &      +0.25*vol(ijk)
c
      vvol(ijk+jwid+kwid)=vvol(ijk+jwid+kwid)
     &      +0.25*vol(ijk)
c
      vvol(ijk+kwid)=vvol(ijk+kwid)
     &      +0.25*vol(ijk)
c
    1 continue
c
      do n=1,nvtx
      ijk=ijkvtx(n)
c
      bxv(ijk)=bxv(ijk)/vvol(ijk)
      byv(ijk)=byv(ijk)/vvol(ijk)
      bzv(ijk)=bzv(ijk)/vvol(ijk)
c
      enddo
c
      return
      end
