       subroutine debug(ncells,ijkcell,iwid,jwid,kwid,
     &     cdlt,sdlt,strait,dz,
     &     ibp1,jbp1,kbp1,nvtx,ijkvtx,xc,yc,zc,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     x,y,z,vol,vvol,gradcx,gradcy,gradcz,divu)
c
      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     ijkcell(*),ijkvtx(*),divu(*),xc(*),yc(*),zc(*),vvol(*),
     &     x(*),y(*),z(*),vol(*),gradcx(*),gradcy(*),gradcz(*)
c
      call divc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     x,y,z,divu)
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     x,gradcx,gradcy,gradcz)
c
      do n=1,ncells
      ijk=ijkcell(n)
      xc(ijk)=0.125*(x(ijk)+x(ijk+iwid)+x(ijk+iwid+jwid)+x(ijk+jwid)
     &      +x(ijk+kwid)+x(ijk+iwid+kwid)
     &      +x(ijk+iwid+jwid+kwid)+x(ijk+jwid+kwid))
      yc(ijk)=0.125*(y(ijk)+x(ijk+iwid)+y(ijk+iwid+jwid)+y(ijk+jwid)
     &      +y(ijk+kwid)+y(ijk+iwid+kwid)
     &      +y(ijk+iwid+jwid+kwid)+y(ijk+jwid+kwid))
      zc(ijk)=0.125*(z(ijk)+z(ijk+iwid)+z(ijk+iwid+jwid)+z(ijk+jwid)
     &      +z(ijk+kwid)+z(ijk+iwid+kwid)
     &      +z(ijk+iwid+jwid+kwid)+z(ijk+jwid+kwid))
      end do
c
c     call torusbc(ibp2,jbp2,kbp2,
c    &     cdlt,sdlt,strait,dz,
c     &     periodic_x,periodic_y,periodic_z,
c    &     xc,yc,zc)
c
      call divv(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     xc,yc,zc,divu)
c
      call torusbc_scalar(ibp1+1,jbp1+1,kbp1+1,
     &     periodic_x,periodic_y,periodic_z,
     &     divu)
c
      call axisavg(ibp1,jbp1,iwid,jwid,kwid,
     &     divu)
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,yc)
c
      call gradf(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     yc,gradcx,gradcy,gradcz)
c
      call axisgrad(ibp1,jbp1,iwid,jwid,kwid,gradcx,gradcy,gradcz)
c
      do 10 n=1,nvtx
      ijk=ijkvtx(n)
      gradcx(ijk)=gradcx(ijk)/vvol(ijk)
      gradcy(ijk)=gradcy(ijk)/vvol(ijk)
      gradcz(ijk)=gradcz(ijk)/vvol(ijk)
      divu(ijk)=divu(ijk)/vvol(ijk)
  10  continue
c
      return
      end
