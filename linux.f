	subroutine exit(i)
	stop
	end

        function dasinh(x)
        implicit none
        real*8 dasinh,x
        if(x.ge.0.d0) then
        dasinh=log(x+sqrt(1.d0+x**2))
        else
        write(*,*)'ARGOMENTO ASINH .LT. 0'
        end if
        return
        end

        function dacosh(x)
        implicit none
        real*8 dacosh,x
        if(x.ge.0.d0) then
        dacosh=2.d0*log(sqrt((x+1.d0)/2.d0)+sqrt((x-1.d0)/2.d0))
        else
        write(*,*)'ARGOMENTO ACOSH .LT. 0'
        end if
        return
        end

        function datanh(x)
        implicit none
        real*8 datanh,x
        if(abs(x).lt.1.d0) then
        datanh=(log(1.d0+x)-log(1.d0-x))/2.d0
        else
        write(*,*)'ABS ARGOMENTO ATANH .GE. 1'
        end if
        return
        end