      subroutine accel_3dmhd
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
c
c   a routine to solve the momentum equation
c
      dtcntr=dt*cntr
      rcntr=1./cntr
c
cdir$ ivdep
      do 1 n=1,nvtx
         ijk=ijkvtx(n)
         ul(ijk)=0.0
         vl(ijk)=0.0
         wl(ijk)=0.0
  1   continue
c
c
      call divpi(nvtx,ijkvtx,iwid,jwid,kwid,
     &           c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &           c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &           c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &           exx,exy,exz,eyy,eyz,ezz,
     &           divpix,divpiy,divpiz)
c
      dummy=0.0
      call torusbcv(ibp1+1,jbp1+1,kbp1+1,
     &     cdlt,sdlt,DUMMY,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     divpix,divpiy,divpiz)
c
cdir$ ivdep
      do 100 n=1,nvtx
      ijk=ijkvtx(n)
      ul(ijk)=dtcntr*divpix(ijk)
      vl(ijk)=dtcntr*divpiy(ijk)
      wl(ijk)=dtcntr*divpiz(ijk)
  100 continue
c
      if(.not.cartesian) then
c
      call axisgrad(ibp1,jbp1,iwid,jwid,kwid,ul,vl,wl)
c
      endif
c
cdir$ ivdep
      do 125 n=1,nvtx
      ijk=ijkvtx(n)
      ul(ijk)=ul(ijk)+dtcntr*mv(ijk)*gx
      vl(ijk)=vl(ijk)+dtcntr*mv(ijk)*gy
      wl(ijk)=wl(ijk)+dtcntr*mv(ijk)*gz
  125 continue
c
cdir$ ivdep
      do 150 n=1,nvtx
      ijk=ijkvtx(n)
      factor=1./(mv(ijk)+1.e-10)
      ul(ijk)=u(ijk)+ul(ijk)*factor
      vl(ijk)=v(ijk)+vl(ijk)*factor
      wl(ijk)=w(ijk)+wl(ijk)*factor
  150 continue
c
c  apply rigid, free-slip wall conditions
c
      call bc_wall(ibp1,jbp1,kbp1,iwid,jwid,kwid,
     c             dx,dy,dz,x,y,z,
     &             c5x,c6x,c7x,c8x,
     &             c5y,c6y,c7y,c8y,
     &             c5z,c6z,c7z,c8z,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & t,tramp,
     &             ul,vl,wl)
c
c
      call torusbc(ibar+2,jbar+2,kbar+2,
     &     cdlt,sdlt,dummy,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     ul,vl,wl)
c
      zero=0.0d0
c
      if(.not.periodic_x) then
c
c     impose no slip conditions on i=2,i=ibp2 boundaries
c
      call list(2,2,2,ibp2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_open(nvtxtmp,ijktmp2,zero,vvi(1),wvi(1),
     &     ul,vl,wl,iwid)
c
      call list(ibp2,ibp2,2,jbp2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_open(nvtxtmp,ijktmp2,zero,vvi(2),wvi(2),
     &     ul,vl,wl,-iwid)
c
      endif
c
c
      if(.not.periodic_y) then
c
c     impose no slip conditions on j=2,j=jbp2 boundaries
c
      call list(2,ibp2,2,2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_noslip(nvtxtmp,ijktmp2,uvi(1),zero,wvi(1),
     &     ul,vl,wl)
c
      call list(2,ibp2,jbp2,jbp2,2,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call bc_noslip(nvtxtmp,ijktmp2,uvi(2),zero,wvi(2),
     &     ul,vl,wl)
c
      endif
c
      if(.not.periodic_z) then
c
c     impose no slip conditions on k=2,k=kbp2 boundaries
c
!      call list(2,ibp2,2,jbp2,2,2,iwid,jwid,kwid,
!     &     nvtxtmp,ijktmp2)
!c
!      call bc_open(nvtxtmp,ijktmp2,uvi(1),vvi(1),zero,
!     &     ul,vl,wl,kwid)
!c
!      call list(2,ibp2,2,jbp2,kbp2,kbp2,iwid,jwid,kwid,
!     &     nvtxtmp,ijktmp2)
!c
!      call bc_open(nvtxtmp,ijktmp2,uvi(2),vvi(2),zero,
!     &     ul,vl,wl,-kwid)
c
      call bc_wall(ibp1,jbp1,kbp1,iwid,jwid,kwid,
     c             dx,dy,dz,x,y,z,
     &             c5x,c6x,c7x,c8x,
     &             c5y,c6y,c7y,c8y,
     &             c5z,c6z,c7z,c8z,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & t,tramp,
     &             ul,vl,wl)
c
      endif

c
c
c  calculate the acceleration (velocity increment)
c
cdir$ ivdep
      do 2 n=1,nvtx
         ijk=ijkvtx(n)
         ax(ijk)=(ul(ijk)-u(ijk))*rcntr
         ay(ijk)=(vl(ijk)-v(ijk))*rcntr
         az(ijk)=(wl(ijk)-w(ijk))*rcntr
  2   continue
c
      call torusbc(ibar+2,jbar+2,kbar+2,
     &     cdlt,sdlt,dummy,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     ax,ay,az)
c
c  calculate the work
c
cdir$ ivdep
      do 3 n=1,nvtx
         ijk=ijkvtx(n)
         work(ijk)=(ax(ijk)**2+ay(ijk)**2+az(ijk)**2)*cntr
  3   continue
c
      return
      end
