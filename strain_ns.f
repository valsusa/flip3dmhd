
      subroutine strain_ns(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     u,v,w,vol,gradcx,gradcy,gradcz)
c
c      implicit real*8 (a-h,o-z)
c
      dimension
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     ijkcell(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     u(*),v(*),w(*),vol(*),gradcx(*),gradcy(*),gradcz(*)
c
c      write(*,*)'ijk = 2664'
c      write(*,*)'gradx,grady,gradz=',gradcx(2664),gradcy(2664),
c     &           gradcz(2664)
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     u,gradcx,gradcy,gradcz)
c      write(*,*)'ijk = 2664'
c      write(*,*)'gradx,grady,gradz=',gradcx(2664),gradcy(2664),
c     &           gradcz(2664)
c
c       write(*,*)'dudz(2664)',dudz(2664)
cdak cdir$ ivdep
      do 100 n=1,ncells
      ijk=ijkcell(n)
      dudx(ijk)=gradcx(ijk)
      dudy(ijk)=gradcy(ijk)
      dudz(ijk)=gradcz(ijk) 
  100 continue
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     v,gradcx,gradcy,gradcz)
c
cdak cdir$ ivdep
      do 200 n=1,ncells
      ijk=ijkcell(n)
      dvdx(ijk)=gradcx(ijk)
      dvdy(ijk)=gradcy(ijk)
      dvdz(ijk)=gradcz(ijk) 
  200 continue
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     w,gradcx,gradcy,gradcz)
c
cdak cdir$ ivdep
      do 300 n=1,ncells
      ijk=ijkcell(n)
      dwdx(ijk)=gradcx(ijk)
      dwdy(ijk)=gradcy(ijk)
      dwdz(ijk)=gradcz(ijk) 
  300 continue
c
      return
      end
