
      subroutine diagonal(ncells,ijkcell,NSTART,
     &     iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     gm1,divu,dt,coef,vvol,vol,diag)
c
c     a routine to calculate the diagonal elements of the
c     matrix representing the Laplacian
c
      implicit real*8 (a-h,o-z)
      dimension ijkcell(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vvol(*),vol(*),diag(*),divu(*),coef(*)
c
      gamdt=(1.+gm1)*dt
c
cdir$ ivdep
      do 1 n=1,NCELLS
c
      ijk=ijkcell(n)
c
      diag(ijk)=
     & +((c1x(ijk)**2+c1y(ijk)**2+c1z(ijk)**2)/vvol(ijk+iwid)
     & +(c2x(ijk)**2+c2y(ijk)**2+c2z(ijk)**2)/vvol(ijk+iwid+jwid)
     & +(c3x(ijk)**2+c3y(ijk)**2+c3z(ijk)**2)/vvol(ijk+jwid)
     & +(c4x(ijk)**2+c4y(ijk)**2+c4z(ijk)**2)/vvol(ijk)
     & +(c5x(ijk)**2+c5y(ijk)**2+c5z(ijk)**2)/vvol(ijk+iwid+kwid)
     & +(c6x(ijk)**2+c6y(ijk)**2+c6z(ijk)**2)/vvol(ijk+iwid+jwid+kwid)
     & +(c7x(ijk)**2+c7y(ijk)**2+c7z(ijk)**2)/vvol(ijk+jwid+kwid)
     & +(c8x(ijk)**2+c8y(ijk)**2+c8z(ijk)**2)/vvol(ijk+kwid))
     & +vol(ijk)/coef(ijk)
c
    1 continue
c
      return
       end
c
      subroutine poisson(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     ITDIM,iwid,jwid,kwid,PRECON,gm1,mv,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     q,qtilde,Aqtilde,itmax,error,DT,CNTR,srce,rho,divu,csq,
     &     coef,residu,Aq,gradpx,gradpy,gradpz,p,diag,div)
c
c     a routine to solve poisson's equation for the pressure
C     USING A CONJUGATE RESIDUAL SOLVER
c     boundary conditions correspond to a topological torus
c     that is periodic in i and j, and with Dirichlet bc's at k=kbp2
c
      implicit real*8 (a-h,o-z)
      real*8 lambda,mv
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          q(*),qtilde(*),p(*),vvol(*),vvolaxis(*),coef(*),div(*),
     &          Aqtilde(*),Aq(*),srce(*),rho(*),divu(*),csq(*),mv(*),
     &          vol(*),residu(*),gradpx(*),gradpy(*),gradpz(*),diag(*)
c
C
      LOGICAL PRECON
C
      nstart=(ibp1-1)*(jbp1-1)+1
      DTSQ=cntr*DT**2
      dtcntr=dt*cntr
c
      if(precon) then
c
      call diagonal(ncells,ijkcell,NSTART,
     &     iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     gm1,divu,dt,coef,mv,vol,diag)
c
      else
c
      DO 1111 N=1,NCELLS
      DIAG(IJKCELL(N))=1.
 1111 CONTINUE
C
      endif
c
c
      do 1112 k=1,kbp1+1
      do 1112 j=1,jbp1+1
      do 1112 i=1,ibp1+1
      ijk=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
      q(ijk)=0.0
      Aq(ijk)=0.0
      qtilde(ijk)=0.0
      aqtilde(ijk)=0.0
      residu(ijk)=0.0
 1112 continue
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,p)
c
      do 1114 n=1,ncells
      ijk=ijkcell(n)
      coef(ijk)=csq(ijk)*rho(ijk)*dtsq+1.e-10
 1114 continue
c
      do 1115 n=1,ncells
      ijk=ijkcell(n)
      srce(ijk)=p(ijk)/coef(ijk)-divu(ijk)/(dtcntr)
 1115 continue
c
c
C     CALCULATE THE INITIAL RESIDUAL ERROR
C
      call residue(ncells,ijkcell,nvtx,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     p,gradpx,gradpy,gradpz,vvol,residu)
c
c
      rsum=0.0
      bsum=0.0
c
      do 32 n=1,ncells
      ijk=ijkcell(n)
      bsum=bsum+abs(srce(ijk)*vol(ijk))
      rsum=rsum+abs(residu(ijk))
      q(ijk)=residu(ijk)+p(ijk)
   32 continue
c
      if(rsum.lt.error) then
      go to 2
      endif
c
      if(bsum.lt.error) bsum=1.0
c
c     compute Aq
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,q)
c
      call residue(ncells,ijkcell,nvtx,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     q,gradpx,gradpy,gradpz,vvol,
     &     Aq)
c
c
      do 33 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=(residu(ijk)-Aq(ijk))
      q(ijk)=q(ijk)-p(ijk)
  33  continue
c
      bottom=0.0
      do 34 n=1,ncells
      ijk=ijkcell(n)
      bottom=bottom+Aq(ijk)*Aq(ijk)/diag(ijk)
   34 continue
c
c     ****************************************************************
c
c     begin conjugate gradient iteration
c
c     ****************************************************************
c
      do 1 iter=1,itmax
c
c     apply pre-conditioner
      do 10 n=1,ncells
      ijk=ijkcell(n)
      qtilde(ijk)=residu(ijk)/diag(ijk)
      qtilde(ijk)=qtilde(ijk)+p(ijk)
   10 continue
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,qtilde)
c
c     calculate Aqtilde
c
      call residue(ncells,ijkcell,nvtx,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     qtilde,gradpx,gradpy,gradpz,vvol,
     &     Aqtilde)
c
c
      do 15 n=1,ncells
      ijk=ijkcell(n)
      aqtilde(ijk)=(residu(ijk)-aqtilde(ijk))
      qtilde(ijk)=qtilde(ijk)-p(ijk)
   15 continue
c
c     calculate LAMBDA
c
      LAMBDA=0.0
c
      do 11 n=1,ncells
      ijk=ijkcell(n)
      LAMBDA=LAMBDA+aqtilde(ijk)*Aq(ijk)/diag(ijk)
   11 continue
c
      LAMBDA=LAMBDA/(bottom+1.e-10)
c
cdir$ ivdep
      do 12 n=1,ncells
      ijk=ijkcell(n)
      q(ijk)=qtilde(ijk)-LAMBDA*q(ijk)
      Aq(ijk)=aqtilde(ijk)-LAMBDA*Aq(ijk)
   12 continue
c
c
      alfa=0.0
      bottom=0.0
      do 5 n=1,ncells
      ijk=ijkcell(ncells)
      alfa=alfa+residu(ijk)*Aq(ijk)/diag(ijk)
      bottom=bottom+Aq(ijk)*Aq(ijk)/diag(ijk)
  5   continue
c
      ALFA=ALFA/(bottom+1.e-10)
c
      resmax=-1.e20
      ijkmax=1
      do 14 n=1,ncells
      ijk=ijkcell(n)
      p(ijk)=p(ijk)+ALFA*q(ijk)
      residu(ijk)=residu(ijk)-ALFA*Aq(ijk)
      if(resmax.lt.abs(residu(ijk))) then
      resmax=abs(residu(ijk))
      ijkmax=ijk
      endif
   14 continue
c
      rnorm=0.0
      dxnorm=0.0
      xnorm=0.0
      do 13 n=1,ncells
      ijk=ijkcell(n)
      rnorm=rnorm+abs(residu(ijk))
      dxnorm=dxnorm+abs(q(ijk))
      xnorm=xnorm+abs(p(ijk))
   13 continue
      dxnorm=dxnorm*abs(alfa)
c
      if(dxnorm.le.error*xnorm.and.rnorm.le.error*bsum)
     &     go to 2
c
c
    1 continue
      write(6,*)'poisson: iteration fails '
      write(6,*)'dxnorm,xnorm,rnorm,bsum'
      write(6,*) dxnorm,xnorm,rnorm,bsum
      return
c
    2 continue
c     iteration has converged
      write(6,*)'poisson: converges in',iter
c
      return
      end
c
      subroutine residue(ncells,ijkcell,nvtx,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     p,gradpx,gradpy,gradpz,vvol,residu)
c
c     a routine to calculate the residual error in the solution
c     of poisson's equation
c
      implicit real*8 (a-h,o-z)
      real*8 mv
c
      dimension ijkcell(*),ijkvtx(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),vvol(*),p(*),div(*),divu(*),
     &     srce(*),coef(*),mv(*),
     &     gradpx(*),gradpy(*),gradpz(*),residu(*)
c
c     calculate residue
c
      call gradf(nvtx,ijkvtx,iwid,jwid,kwid,
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &                   p,gradpx,gradpy,gradpz)
c
      call axisgrad(ibp1,jbp1,iwid,jwid,kwid,
     &     gradpx,gradpy,gradpz)
c
      do 16 n=1,nvtx
      ijk=ijkvtx(n)
      rmv=1./mv(ijk)
      gradpx(ijk)=-gradpx(ijk)*rmv
      gradpy(ijk)=-gradpy(ijk)*rmv
      gradpz(ijk)=-gradpz(ijk)*rmv
   16 continue
c
C
c     calculate div(grad(p)/rho)
c
      call divc(ncells,ijkcell,iwid,jwid,kwid,
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &                   GRADPX,GRADPY,GRADPZ,div)
c
c
      do 15 n=1,ncells
      ijk=ijkcell(n)
      residu(ijk)=(srce(ijk)-p(ijk)/coef(ijk)-div(ijk))*vol(ijk)
   15 continue
c
      return
      end
