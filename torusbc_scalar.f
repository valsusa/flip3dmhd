      subroutine torusbc_scalar(nxp,nyp,nzp,
     &     periodic_x,periodic_y,periodic_z,
     &     x)
c
c     a routine to impose double periodicity
c     for toroidal geometry for vertex variables
c
c     called by VINIT_GMRES
c
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      dimension x(nxp,nyp,*)
c
      do 10 k=2,nzp
c
c     periodicity in the toroidal angle
c
      if(periodic_y) then
c
      do 1 i=2,nxp
c
      x(i,2,k)=x(i,nyp,k)+x(i,2,k)
c
      x(i,nyp,k)=x(i,2,k)
c
    1 continue
c
      endif
c
c
c
c     periodicity in the poloidal angle
c
      if(periodic_x) then
c
      do 2 j=2,nyp
c
      x(2,j,k)=x(nxp,j,k)+x(2,j,k)
c
      x(nxp,j,k)=x(2,j,k)
c
    2 continue
c
      endif
c
   10 continue
c
c
      return
      end
