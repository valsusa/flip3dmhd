	subroutine bottom_state(wspx,wspy,wspz,
     &        bx_bc,by_bc,bz_bc,
     &        p_bc,xc,xsep,u_bc,rho_bc)
      use boundary_module
      implicit real*8 (a-h,o-z)


      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

	pi=acos(-1d0)

	  rlargx=ibar*dx
	  rlargy=jbar*dy
	  rlargz=kbar*dz
	  xcntr=(xr+xl)*.5
	  zcntr=(ze+zf)*.5

      u_bc=1d0

        sinlat=1.d0
        ang_vel=.1d0/(erre0+ze)


         if(grid_type_x.eq.0) then
            br=bxr(1)
            bf=0d0
            br=br*erre0/(erre0+wspz)
            bf=br*(erre0+wspz)*ang_vel*sinlat/u_bc
            sinth=wspx/sqrt((wspz+erre0)**2+wspx**2)
            costh=sqrt(1.d0-sinth**2)
            bz_bc=br*costh-bf*sinth
            bx_bc=br*sinth+bf*costh
            by_bc=0d0
         else
            bz_bc=bxr(1)*tanh(wspx-xcntr)
            by_bc=-bxr(1)/cosh(wspx-xcntr)
            bx_bc=0.d0
         end if



      if(wspz.lt.zcntr/2) then
	  rho_bc=rhr(1)*1d0
	  else
	  rho_bc=rhr(1)
	  end if

	    rho_bc=rho_bc
	  p_bc=1d0
      u_ini=wvi(1)

	  bz_bc=bz_bc+bxr(1)/5d0*sin(4d0*pi*t/twfin)*
     &          cos(2d0*pi*(wspx-xcntr)/rlargx)
c     &          *exp(-(wspx-xcntr)**2/(rlargx/10.0))
      if(t.gt.twfin/2.and. t.lt.twfin/2+twfin/200.) then
c        if(abs(wspx-xcntr).lt.rlargx/2.) then
          rho_bc=rho_bc*4
          u_ini=u_ini*2
          p_bc=p_bc*4
c        end if
      end if


!/(t/10d0+1d0)

	end subroutine bottom_state

