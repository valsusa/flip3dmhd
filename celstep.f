      subroutine celstep(iwid,jwid,kwid,ijkstep)
c
      implicit real*8 (a-h,o-z)
c
c     a routine to calculate the stride
c
      dimension ijkstep(27)
c
      ijkstep(1)=0
      ijkstep(2)=iwid
      ijkstep(3)=iwid+jwid
      ijkstep(4)=jwid
      ijkstep(5)=-iwid+jwid
      ijkstep(6)=-iwid
      ijkstep(7)=-iwid-jwid
      ijkstep(8)=-jwid
      ijkstep(9)=iwid-jwid
c
      ijkstep(10)=-kwid
      ijkstep(11)=+iwid-kwid
      ijkstep(12)=+iwid+jwid-kwid
      ijkstep(13)=+jwid-kwid
      ijkstep(14)=-iwid+jwid-kwid
      ijkstep(15)=-iwid-kwid
      ijkstep(16)=-iwid-jwid-kwid
      ijkstep(17)=-jwid-kwid
      ijkstep(18)=+iwid-jwid-kwid
c
      ijkstep(19)=+kwid
      ijkstep(20)=+iwid+kwid
      ijkstep(21)=+iwid+jwid+kwid
      ijkstep(22)=+jwid+kwid
      ijkstep(23)=-iwid+jwid+kwid
      ijkstep(24)=-iwid+kwid
      ijkstep(25)=-iwid-jwid+kwid
      ijkstep(26)=-jwid+kwid
      ijkstep(27)=+iwid-jwid+kwid
c
      return
      end
