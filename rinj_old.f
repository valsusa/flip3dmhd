	subroutine rinj
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
c
      dimension regvol(8)
c
      dimension ijkv(8)
      dimension ijkc(27),wghtc(27)
      dimension wght1(8),wght2(8),wght3(8),wght4(8),
     &          wght5(8),wght6(8),wght7(8),wght8(8)
      real*8 mikic,mikicx,mikicy,mikicz,k1,k2
      real*8 mikicb,mikicxb,mikicyb,mikiczb,k1b,k2b,kappa,L
	logical testing 

	testing=.false.
c
c	return
c
       pi=acos(-1.) 
c
c	inject at bottom boundary
c
	if(testing) then

	kbot=2
	do i=2,ibp1
	do j=2,jbp1
          ijk=1+(i-1)*iwid+(j-1)*jwid+(kbot-1)*kwid
c	  write(*,*)i,j,x(ijk),y(ijk),z(ijk)
c          write(*,*)iphead(ijk)
c     &    iphead(1),link(iphead(1))

	enddo
	enddo

	endif

       do ii=2,ibp1
       do jj=2,jbp1


c	write(*,*)t,tbot(ii,jj),dtbot(ii,jj),ubot(ii,jj)

       do while(t-tbot(ii,jj)>=dtbot(ii,jj))

	zinj=(t-tbot(ii,jj)-dtbot(ii,jj))*wbot(ii,jj)
	kinj=2+int(zinj/dz)

       ijk=1+(ii-1)*iwid+(jj-1)*jwid+(kinj-1)*kwid


	do kr=1,nrg

      rnpcx=1./real(npcelx(kr))
      rnpcy=1./real(npcely(kr))
      rnpcz=1./real(npcelz(kr))
      do kx=1,npcelx(kr)
      do ky=1,npcely(kr)
	kz=1
c
      xi=(0.5+(kx-1))*rnpcx
      eta=(0.5+(ky-1))*rnpcy
c      zta=(0.5+(kz-1))*rnpcz
      zta=zinj/dz-int(zinj/dz)
c
      call weights(xi,eta,zta,wght)
c
      xi1=(kx-1)*rnpcx
      eta1=(ky-1)*rnpcy
      zta1=(kz-1)*rnpcz
      call weights(xi1,eta1,zta1,wght4)
      xi1=xi1+rnpcx
      call weights(xi1,eta1,zta1,wght1)
      eta1=eta1+rnpcy
      call weights(xi1,eta1,zta1,wght2)
      xi1=xi1-rnpcx
      call weights(xi1,eta1,zta1,wght3)
      eta1=eta1-rnpcy
      zta1=zta1+rnpcz
      call weights(xi1,eta1,zta1,wght8)
      xi1=xi1+rnpcx
      call weights(xi1,eta1,zta1,wght5)
      eta1=eta1+rnpcy
      call weights(xi1,eta1,zta1,wght6)
      xi1=xi1-rnpcx
      call weights(xi1,eta1,zta1,wght7)
c
      ijkreg=8*(kr-1)+1
      istp=1
      jstep=2
      kstep=4
      epsreg=1.e-4



c
c	test existence of memory allocation for 
c       more particles
c
      np=iphead(1)

      if(np.eq.0) then
      write(6,*)'rinj: not enough particles'
      return	
      endif
c
      ipjk=ijk+iwid
      ipjpk=ijk+iwid+jwid
      ijpk=ijk+jwid
c
      ijkp=ijk+kwid
      ipjkp=ijk+iwid+kwid
      ijpkp=ijk+jwid+kwid
      ipjpkp=ijk+iwid+jwid+kwid
c
      wspx=wght(1)*x(ipjk)
     &      +(wght(2)*x(ipjpk)
     &      +(wght(3)*x(ijpk)
     &      +(wght(4)*x(ijk)
     &      +(wght(5)*x(ipjkp)
     &      +(wght(6)*x(ipjpkp)
     &      +(wght(7)*x(ijpkp)
     &      +(wght(8)*x(ijkp))))))))
c
      wspy=wght(1)*y(ipjk)
     &      +(wght(2)*y(ipjpk)
     &      +(wght(3)*y(ijpk)
     &      +(wght(4)*y(ijk)
     &      +(wght(5)*y(ipjkp)
     &      +(wght(6)*y(ipjpkp)
     &      +(wght(7)*y(ijpkp)
     &      +(wght(8)*y(ijkp))))))))
c
      wspz=wght(1)*z(ipjk)
     &      +(wght(2)*z(ipjpk)
     &      +(wght(3)*z(ijpk)
     &      +(wght(4)*z(ijk)
     &      +(wght(5)*z(ipjkp)
     &      +(wght(6)*z(ipjpkp)
     &      +(wght(7)*z(ijpkp)
     &      +(wght(8)*z(ijkp))))))))
c	write(*,*)'attemp inject',kr,kx,ky,kz,ii,jj
c
c	wspx=wspx+(t-tbot(ii,jj)-dtbot(ii,jj))*ubot(ii,jj)
c	wspy=wspy+(t-tbot(ii,jj)-dtbot(ii,jj))*vbot(ii,jj)
	wspz=wspz
	if(wspx.lt.0.0.or.wspx.gt.ibar*dx) goto 1
	if(wspy.lt.0.0.or.wspy.gt.jbar*dy) goto 1
c
c     check whether particle is in region or not
c
      call map3d(ijkreg,istp,jstep,kstep,epsreg,
     &     xvi,yvi,zvi,
     &     wspx,wspy,wspz,wsxi,wseta,wsnu)
c
      if(wsxi*(1.-wsxi).lt.0.0
     &   .or.wseta*(1.-wseta).lt.0.0
     &   .or.wsnu*(1.-wsnu).lt.0.0)  goto 1
c
      rse=((wspx-xcenter(kr))/(rex(kr)+1.e-10))**2
     &   +((wspy-ycenter(kr))/(rey(kr)+1.e-10))**2
     &   +((wspz-zcenter(kr))/(rez(kr)+1.e-10))**2
      rsi=((wspx-xcenter(kr))/(rix(kr)+1.e-10))**2
     &   +((wspy-ycenter(kr))/(riy(kr)+1.e-10))**2
     &   +((wspz-zcenter(kr))/(riz(kr)+1.e-10))**2
      if(rsi.lt.1..or.rse.gt.1.) goto 1
c
c
c	write(*,*)'inject particle',np
c
      iphead(1)=link(np)
      link(np)=iphead(ijk)
      iphead(ijk)=np
c
      px(np)=wspx
      py(np)=wspy
      pz(np)=wspz
c
      k=1+(ijk-1)/kwid
      j=1+(ijk-1-(k-1)*kwid)/jwid
      i=1+(ijk-1-(j-1)*jwid-(k-1)*kwid)/iwid
c
      pxi(np)=i+xi
      peta(np)=j+eta
      pzta(np)=k+zta
c
      x1=wght1(1)*x(ipjk)
     &      +(wght1(2)*x(ipjpk)
     &      +(wght1(3)*x(ijpk)
     &      +(wght1(4)*x(ijk)
     &      +(wght1(5)*x(ipjkp)
     &      +(wght1(6)*x(ipjpkp)
     &      +(wght1(7)*x(ijpkp)
     &      +(wght1(8)*x(ijkp))))))))
      x2=wght2(1)*x(ipjk)
     &      +(wght2(2)*x(ipjpk)
     &      +(wght2(3)*x(ijpk)
     &      +(wght2(4)*x(ijk)
     &      +(wght2(5)*x(ipjkp)
     &      +(wght2(6)*x(ipjpkp)
     &      +(wght2(7)*x(ijpkp)
     &      +(wght2(8)*x(ijkp))))))))
      x3=wght3(1)*x(ipjk)
     &      +(wght3(2)*x(ipjpk)
     &      +(wght3(3)*x(ijpk)
     &      +(wght3(4)*x(ijk)
     &      +(wght3(5)*x(ipjkp)
     &      +(wght3(6)*x(ipjpkp)
     &      +(wght3(7)*x(ijpkp)
     &      +(wght3(8)*x(ijkp))))))))
      x4=wght4(1)*x(ipjk)
     &      +(wght4(2)*x(ipjpk)
     &      +(wght4(3)*x(ijpk)
     &      +(wght4(4)*x(ijk)
     &      +(wght4(5)*x(ipjkp)
     &      +(wght4(6)*x(ipjpkp)
     &      +(wght4(7)*x(ijpkp)
     &      +(wght4(8)*x(ijkp))))))))
      x5=wght5(1)*x(ipjk)
     &      +(wght5(2)*x(ipjpk)
     &      +(wght5(3)*x(ijpk)
     &      +(wght5(4)*x(ijk)
     &      +(wght5(5)*x(ipjkp)
     &      +(wght5(6)*x(ipjpkp)
     &      +(wght5(7)*x(ijpkp)
     &      +(wght5(8)*x(ijkp))))))))
      x6=wght6(1)*x(ipjk)
     &      +(wght6(2)*x(ipjpk)
     &      +(wght6(3)*x(ijpk)
     &      +(wght6(4)*x(ijk)
     &      +(wght6(5)*x(ipjkp)
     &      +(wght6(6)*x(ipjpkp)
     &      +(wght6(7)*x(ijpkp)
     &      +(wght6(8)*x(ijkp))))))))
      x7=wght7(1)*x(ipjk)
     &      +(wght7(2)*x(ipjpk)
     &      +(wght7(3)*x(ijpk)
     &      +(wght7(4)*x(ijk)
     &      +(wght7(5)*x(ipjkp)
     &      +(wght7(6)*x(ipjpkp)
     &      +(wght7(7)*x(ijpkp)
     &      +(wght7(8)*x(ijkp))))))))
      x8=wght8(1)*x(ipjk)
     &      +(wght8(2)*x(ipjpk)
     &      +(wght8(3)*x(ijpk)
     &      +(wght8(4)*x(ijk)
     &      +(wght8(5)*x(ipjkp)
     &      +(wght8(6)*x(ipjpkp)
     &      +(wght8(7)*x(ijpkp)
     &      +(wght8(8)*x(ijkp))))))))
c
      y1=wght1(1)*y(ipjk)
     &      +(wght1(2)*y(ipjpk)
     &      +(wght1(3)*y(ijpk)
     &      +(wght1(4)*y(ijk)
     &      +(wght1(5)*y(ipjkp)
     &      +(wght1(6)*y(ipjpkp)
     &      +(wght1(7)*y(ijpkp)
     &      +(wght1(8)*y(ijkp))))))))
      y2=wght2(1)*y(ipjk)
     &      +(wght2(2)*y(ipjpk)
     &      +(wght2(3)*y(ijpk)
     &      +(wght2(4)*y(ijk)
     &      +(wght2(5)*y(ipjkp)
     &      +(wght2(6)*y(ipjpkp)
     &      +(wght2(7)*y(ijpkp)
     &      +(wght2(8)*y(ijkp))))))))
      y3=wght3(1)*y(ipjk)
     &      +(wght3(2)*y(ipjpk)
     &      +(wght3(3)*y(ijpk)
     &      +(wght3(4)*y(ijk)
     &      +(wght3(5)*y(ipjkp)
     &      +(wght3(6)*y(ipjpkp)
     &      +(wght3(7)*y(ijpkp)
     &      +(wght3(8)*y(ijkp))))))))
      y4=wght4(1)*y(ipjk)
     &      +(wght4(2)*y(ipjpk)
     &      +(wght4(3)*y(ijpk)
     &      +(wght4(4)*y(ijk)
     &      +(wght4(5)*y(ipjkp)
     &      +(wght4(6)*y(ipjpkp)
     &      +(wght4(7)*y(ijpkp)
     &      +(wght4(8)*y(ijkp))))))))
      y5=wght5(1)*y(ipjk)
     &      +(wght5(2)*y(ipjpk)
     &      +(wght5(3)*y(ijpk)
     &      +(wght5(4)*y(ijk)
     &      +(wght5(5)*y(ipjkp)
     &      +(wght5(6)*y(ipjpkp)
     &      +(wght5(7)*y(ijpkp)
     &      +(wght5(8)*y(ijkp))))))))
      y6=wght6(1)*y(ipjk)
     &      +(wght6(2)*y(ipjpk)
     &      +(wght6(3)*y(ijpk)
     &      +(wght6(4)*y(ijk)
     &      +(wght6(5)*y(ipjkp)
     &      +(wght6(6)*y(ipjpkp)
     &      +(wght6(7)*y(ijpkp)
     &      +(wght6(8)*y(ijkp))))))))
      y7=wght7(1)*y(ipjk)
     &      +(wght7(2)*y(ipjpk)
     &      +(wght7(3)*y(ijpk)
     &      +(wght7(4)*y(ijk)
     &      +(wght7(5)*y(ipjkp)
     &      +(wght7(6)*y(ipjpkp)
     &      +(wght7(7)*y(ijpkp)
     &      +(wght7(8)*y(ijkp))))))))
      y8=wght8(1)*y(ipjk)
     &      +(wght8(2)*y(ipjpk)
     &      +(wght8(3)*y(ijpk)
     &      +(wght8(4)*y(ijk)
     &      +(wght8(5)*y(ipjkp)
     &      +(wght8(6)*y(ipjpkp)
     &      +(wght8(7)*y(ijpkp)
     &      +(wght8(8)*y(ijkp))))))))
c
      z1=wght1(1)*z(ipjk)
     &      +(wght1(2)*z(ipjpk)
     &      +(wght1(3)*z(ijpk)
     &      +(wght1(4)*z(ijk)
     &      +(wght1(5)*z(ipjkp)
     &      +(wght1(6)*z(ipjpkp)
     &      +(wght1(7)*z(ijpkp)
     &      +(wght1(8)*z(ijkp))))))))
      z2=wght2(1)*z(ipjk)
     &      +(wght2(2)*z(ipjpk)
     &      +(wght2(3)*z(ijpk)
     &      +(wght2(4)*z(ijk)
     &      +(wght2(5)*z(ipjkp)
     &      +(wght2(6)*z(ipjpkp)
     &      +(wght2(7)*z(ijpkp)
     &      +(wght2(8)*z(ijkp))))))))
      z3=wght3(1)*z(ipjk)
     &      +(wght3(2)*z(ipjpk)
     &      +(wght3(3)*z(ijpk)
     &      +(wght3(4)*z(ijk)
     &      +(wght3(5)*z(ipjkp)
     &      +(wght3(6)*z(ipjpkp)
     &      +(wght3(7)*z(ijpkp)
     &      +(wght3(8)*z(ijkp))))))))
      z4=wght4(1)*z(ipjk)
     &      +(wght4(2)*z(ipjpk)
     &      +(wght4(3)*z(ijpk)
     &      +(wght4(4)*z(ijk)
     &      +(wght4(5)*z(ipjkp)
     &      +(wght4(6)*z(ipjpkp)
     &      +(wght4(7)*z(ijpkp)
     &      +(wght4(8)*z(ijkp))))))))
      z5=wght5(1)*z(ipjk)
     &      +(wght5(2)*z(ipjpk)
     &      +(wght5(3)*z(ijpk)
     &      +(wght5(4)*z(ijk)
     &      +(wght5(5)*z(ipjkp)
     &      +(wght5(6)*z(ipjpkp)
     &      +(wght5(7)*z(ijpkp)
     &      +(wght5(8)*z(ijkp))))))))
      z6=wght6(1)*z(ipjk)
     &      +(wght6(2)*z(ipjpk)
     &      +(wght6(3)*z(ijpk)
     &      +(wght6(4)*z(ijk)
     &      +(wght6(5)*z(ipjkp)
     &      +(wght6(6)*z(ipjpkp)
     &      +(wght6(7)*z(ijpkp)
     &      +(wght6(8)*z(ijkp))))))))
      z7=wght7(1)*z(ipjk)
     &      +(wght7(2)*z(ipjpk)
     &      +(wght7(3)*z(ijpk)
     &      +(wght7(4)*z(ijk)
     &      +(wght7(5)*z(ipjkp)
     &      +(wght7(6)*z(ipjpkp)
     &      +(wght7(7)*z(ijpkp)
     &      +(wght7(8)*z(ijkp))))))))
      z8=wght8(1)*z(ipjk)
     &      +(wght8(2)*z(ipjpk)
     &      +(wght8(3)*z(ijpk)
     &      +(wght8(4)*z(ijk)
     &      +(wght8(5)*z(ipjkp)
     &      +(wght8(6)*z(ipjpkp)
     &      +(wght8(7)*z(ijpkp)
     &      +(wght8(8)*z(ijkp))))))))
c
         x1278 = y1*z2-y2*z1+y7*z8-y8*z7
         x1476 = y1*z4-y4*z1+y7*z6-y6*z7
         x1573 = y1*z5-y5*z1+y7*z3-y3*z7
         x2385 = y2*z3-y3*z2+y8*z5-y5*z8
         x2684 = y2*z6-y6*z2+y8*z4-y4*z8
         x3456 = y3*z4-y4*z3+y5*z6-y6*z5
         x13   = y1*z3-y3*z1
         x16   = y1*z6-y6*z1
         x18   = y1*z8-y8*z1
         x24   = y2*z4-y4*z2
         x25   = y2*z5-y5*z2
         x27   = y2*z7-y7*z2
         x36   = y3*z6-y6*z3
         x38   = y3*z8-y8*z3
         x45   = y4*z5-y5*z4
         x47   = y4*z7-y7*z4
         x57   = y5*z7-y7*z5
         x68   = y6*z8-y8*z6
         c1 =(x25-x45-x24-x2385+x2684-x3456)/12.
         c2 =(x13+x36-x16+x1476-x1573-x3456)/12.
         c3 =(x24+x47-x27-x1278+x1476-x2684)/12.
         c4 =(x18-x38-x13-x1278+x1573-x2385)/12.
         c5 =(x16+x68-x18+x1278-x1476+x2684)/12.
         c6 =(x27-x57-x25+x1278-x1573+x2385)/12.
         c7 =(x38-x68-x36+x2385-x2684+x3456)/12.
         c8 =(x45+x57-x47-x1476+x1573+x3456)/12.
c
      bxdif=0.5*(bxr(2)-bxr(1))
      bxbar=0.5*(bxr(2)+bxr(1))

      bydif=0.5*(byr(2)-byr(1))
      bybar=0.5*(byr(2)+byr(1))

      bzdif=0.5*(bzr(2)-bzr(1))
      bzbar=0.5*(bzr(2)+bzr(1))

      rhodif=0.5*(rhr(2)-rhr(1))
      rhobar=0.5*(rhr(2)+rhr(1))
c
      siedif=0.5*(siep(2)-siep(1))
      siebar=0.5*(siep(2)+siep(1))

c
c
c       profile=1.0
c      profile=tanh((wspz-zcenter(kr))/0.1)
      profile=tanh((wspx-xcenter(kr))/0.1)

	write(*,*)'call',wspx
      call initial_state(wspx,wspy,wspz*0,
     &        bxshindler,byshindler,bzshindler,
     &        pressure_schindler,xc,xsep,uprof)

      pvolume=c1*x1+c2*x2+c3*x3+c4*x4
     &       +c5*x5+c6*x6+c7*x7+c8*x8
      volp(np)=pvolume
      mass(np)=pvolume*(rhodif*profile+rhobar)
      mupx(np)=pvolume*bxshindler
      mupy(np)=pvolume*byshindler
      mupz(np)=pvolume*bzshindler

c
      ep(np)=mass(np)*(siedif*profile+siebar)*pressure_schindler/gm1
c
      ico(np)=icoi(kr)
c

	babs=sqrt(bxshindler**2+byshindler**2+bzshindler**2)
	if(bzshindler<0.) then
	bxshindler=-bxshindler
	byshindler=-byshindler
	bzshindler=-bzshindler
	endif
      up(np)=wvi(kr)*uprof*bxshindler/babs
      wp(np)=wvi(kr)*uprof*bzshindler/babs
      vp(np)=wvi(kr)*uprof*byshindler/babs

c	write(*,*)wp(np),uprof

c
c

1	continue 

c	close loop on kx,ky,kz
	enddo
	enddo
	
c 	close loop on kr
	enddo

c	close do while loop
	tbot(ii,jj)=tbot(ii,jj)+dtbot(ii,jj)
	enddo

c	close loop on ii,jj
	enddo
	enddo

	if(testing) then
	kbot=2
	do i=2,ibp1
	do j=2,jbp1
          ijk=1+(i-1)*iwid+(j-1)*jwid+(kbot-1)*kwid
c	  write(*,*)i,j,x(ijk),y(ijk),z(ijk)
	  np=iphead(ijk)
666	  if(np==0) then
	  continue
	  else
c          write(*,*)ijk,np,link(np)
	  np=link(np)
	  goto 666
	  endif

	enddo
	enddo

	endif

      return

	end subroutine rinj


	subroutine rinjinit
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'

	do i=2,ibp1
	do j=2,jbp1
	k=2
	
	ijk=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid

      call initial_state(x(ijk),y(ijk),0.,
     &        bxshindler,byshindler,bzshindler,
     &        pdummy,xc,xsep,uprof)
	babs=sqrt(bxshindler**2+byshindler**2+bzshindler**2)
	if(bzshindler<0.) then
	bxshindler=-bxshindler
	byshindler=-byshindler
	bzshindler=-bzshindler
	endif
	ubot(i,j)=vwall_bottom*uprof*bxshindler/babs
	vbot(i,j)=vwall_bottom*uprof*byshindler/babs
	wbot(i,j)=vwall_bottom*uprof*bzshindler/babs
	dtbot(i,j)=dz/npcelz(1)/(wbot(i,j)+1.d-10)
	tbot(i,j)=-dtbot(i,j)/2.
	
	enddo
	enddo

	end subroutine rinjinit




