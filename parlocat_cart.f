      subroutine parlocat_cart(ncells,ijkcell,iphead,npart,
     &     periodic_x,periodic_y,periodic_z,
     &     xl,xr,yb,yt,ze,zf,
     &     dx,dy,dz,
     &     px,py,pz,pxi,peta,pzta)
c
c     a routine to calculate the logical coordinates 
c     of a particle on a uniform, rectilinear grid
c
      integer ncells,ijkcell(*),iphead(*)
c
      logical periodic_x,periodic_y,periodic_z
c
      real*8 
     &     xl,xr,yb,yt,ze,zf,
     &     dx,dy,dz,
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
c
c     first, require that particle position be in bounds
c     if periodic, return to principal periodic interval
c     if not periodic, reflect particle position in normal
c
      do n=1,ncells
        ijk=ijkcell(n)
        np=iphead(ijk)
c
   21 if(px(np).lt.xl.or.px(np).gt.xr) then
c
        if(px(np).lt.xl) then
         if(periodic_x) then
           px(np)=px(np)+xr-xl
         else
c           px(np)=xl+2.*(xl-px(np))
           px(np)=xl+(xl-px(np))
         endif
        endif
c
        if(px(np).gt.xr) then
         if(periodic_x) then
           px(np)=px(np)-(xr-xl)
         else
c           px(np)=xr-2.*(px(np)-xr)
           px(np)=xr-(px(np)-xr)
         endif
        endif
        if(py(np).gt.yt) then
c
        go to 21
        endif
c
   22 if(py(np).lt.yb.or.py(np).gt.yt) then
c
        if(py(np).lt.yb) then
         if(periodic_y) then
           py(np)=py(np)+(yt-yb)
         else
           py(np)=py(np)+2.*(yb-py(np))
         endif
        endif
c
        if(py(np).gt.yt) then
         if(periodic_y) then
           py(np)=py(np)-(yt-yb)
         else
           py(np)=yt-2.*(py(np)-yt)
         endif
        endif
        go to 22
        endif
c
c
   23  if(pz(np).lt.ze.or.pz(np).gt.zf) then
        if(pz(np).lt.ze) then
         if(periodic_z) then
           pz(np)=pz(np)+(zf-ze)
         else
c           pz(np)=pz(np)+(zf-ze)
c           pz(np)=ze+2.*(ze-pz(np))
c           pz(np)=ze+(ze-pz(np))
c	leave as is for later removal
         endif
        endif
c
        if(pz(np).gt.zf) then
         if(periodic_z) then
           pz(np)=pz(np)-(zf-ze)
         else
c           pz(np)=zf-2.*(pz(np)-zf)
c           pz(np)=zf-(pz(np)-zf)
c	leave as is for later removal
         endif
        endif
c
c
        go to 23
        endif
        enddo
c
c       calculate the new logical coordinates of the particle
c
        do n=1,ncells
c
         ijk=ijkcell(n)
         np=iphead(ijk)
c
         pxi(np)=2.+(px(np)-xl)/dx
         peta(np)=2.+(py(np)-yb)/dy
         pzta(np)=2.+(pz(np)-ze)/dz
        enddo
c
      return
      end

