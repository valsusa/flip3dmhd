      subroutine MPBJ_precond(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44,
     &     s,sbx,sby,sbz,
     &     x1,x2,x3,x4)
c
c     a routine to  perform multipass block Jacobi
c     preconditioning using the matrix-lite approach
c
c
      logical resist
c
      integer ncells,ijkcell(*),iwid,jwid,kwid,
     &     ijktmp1(*),ijktmp2(*),
     &     nvtx,ijkvtx(*),ibar,jbar,kbar, ijk
c
      real
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     r4pi,dt,vol(*),ul(*),vl(*),wl(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     gradx(*),grady(*),gradz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),p(*),csq(*),
     &     s(*),sbx(*),sby(*),sbz(*),
     &     ptil(*),
     &     rotil(*),bxtil(*),bytil(*),bztil(*),
     &     roq(*),bxq(*),byq(*),bzq(*),
     &     aroq(*),abxq(*),abyq(*),abzq(*),
     &     jx(*),jy(*),jz(*),vvol(*)
c
      real 
     &     x1(ncells),x2(ncells),x3(ncells),x4(ncells)
c
      real 
     &     a11(*), a12(*), a13(*), a14(*),
     &     a21(*), a22(*), a23(*), a24(*),
     &     a31(*), a32(*), a33(*), a34(*),
     &     a41(*), a42(*), a43(*), a44(*),
     &     d11(*), d12(*), d13(*), d14(*),
     &     d21(*), d22(*), d23(*), d24(*),
     &     d31(*), d32(*), d33(*), d34(*),
     &     d41(*), d42(*), d43(*), d44(*)
c
      real rhs1, rhs2, rhs3, rhs4,
     &     xn1,xn2,xn3,xn4,
     &     y1,y2,y3,y4,
     &     alpha, L2_rhs
c
c
c****************************************************************************
c
c   b is in rog,bxq,byq,bzq
c
c   x^n+1 is in x1, x2, x3, x4 (wk1 in gmres)
c
      numpass = 0
      do n = 1,numpass
       alpha = 0.333
       if (n .gt. 1) then
c
c    get xtmp1 in aroq,abxq,abyq,abzq
c
      call J_times_kvec(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     x1,x2,x3,x4,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c    get xtmp2 in rotil,bxtil,bytil,bztil
c
      do  i=1,ncells
       rotil(i)= a11(i)*x1(i) + a12(i)*x2(i)
     &         + a13(i)*x3(i) + a14(i)*x4(i)
       bxtil(i)= a21(i)*x1(i) + a22(i)*x2(i)
     &         + a23(i)*x3(i) + a24(i)*x4(i)
       bytil(i)= a31(i)*x1(i) + a32(i)*x2(i)
     &         + a33(i)*x3(i) + a34(i)*x4(i)
       bztil(i)= a41(i)*x1(i) + a42(i)*x2(i)
     &         + a43(i)*x3(i) + a44(i)*x4(i)
      enddo
c
      else
c
      do  i=1,ncells
       rotil(i)= 0.0
       bxtil(i)= 0.0
       bytil(i)= 0.0
       bztil(i)= 0.0
       aroq(i)= 0.0
       abxq(i)= 0.0
       abyq(i)= 0.0
       abzq(i)= 0.0
       x1(i)= 0.0
       x2(i)= 0.0
       x3(i)= 0.0
       x4(i)= 0.0
      enddo
c
      endif
c
c      write(36,*) 'numpass =',n
c      L2_rhs = 0.0
c      do i = 1,ncells
c       ijk = ijkcell(i)
c       l2_rhs = L2_rhs + abs(roq(i)-aroq(i)) +
c     &  abs(bxq(i)-abxq(i)) + abs(byq(i)-abyq(i))
c     & + abs(bzq(i)-abzq(i))
c       enddo
c
c       write(*,*)"numpass, L2_rhs=",n,L2_rhs
      do  i=1,ncells
c
      rhs1 = roq(i) - aroq(i) + rotil(i)
      rhs2 = bxq(i) - abxq(i) + bxtil(i)
      rhs3 = byq(i) - abyq(i) + bytil(i)
      rhs4 = bzq(i) - abzq(i) + bztil(i)
c
c
c      call lusolve(d11,d12,d13,d14,
c     &               d21,d22,d23,d24,
c     &               d31,d32,d33,d34,
c     &               d41,d42,d43,d44,
c     &               rhs1,rhs2,rhs3,rhs4,
c     &               aroq(i),abxq(i),abyq(i),abzq(i))
c
c
      y1=rhs1
      y2=rhs2-(d21(i)*y1)
      y3=rhs3-(d31(i)*y1+(d32(i)*y2))
      y4=rhs4-(d41(i)*y1+(d42(i)*y2+(d43(i)*y3)))
c
      xn4=y4*d44(i)
      xn3=(y3-(d34(i)*xn4))*d33(i)
      xn2=(y2-(d23(i)*xn3+(d24(i)*xn4)))*d22(i)
      xn1=(y1-(d12(i)*xn2+(d13(i)*xn3+(d14(i)*xn4))))
     &     *d11(i)
c
      x1(i) = alpha*xn1 + (1.0-alpha)*x1(i)
      x2(i) = alpha*xn2 + (1.0-alpha)*x2(i)
      x3(i) = alpha*xn3 + (1.0-alpha)*x3(i)
      x4(i) = alpha*xn4 + (1.0-alpha)*x4(i)
c
      enddo
c
      enddo
c
      do  i=1,ncells
      if(numpass.gt.0) then
       aroq(i)= x1(i)
       abxq(i)= x2(i)
       abyq(i)= x3(i)
       abzq(i)= x4(i)
       else
       aroq(i)= roq(i)
       abxq(i)= bxq(i)
       abyq(i)= byq(i)
       abzq(i)= bzq(i)
       endif
      enddo
c
      return
      end
c
      subroutine ludecomp3(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44)
c
      real 
     &     a11(*), a12(*), a13(*), a14(*),
     &     a21(*), a22(*), a23(*), a24(*),
     &     a31(*), a32(*), a33(*), a34(*),
     &     a41(*), a42(*), a43(*), a44(*),
     &     d11(*), d12(*), d13(*), d14(*),
     &     d21(*), d22(*), d23(*), d24(*),
     &     d31(*), d32(*), d33(*), d34(*),
     &     d41(*), d42(*), d43(*), d44(*)
c
      integer ncells, n
c
c
      do n=1,ncells
c
c
      d11(n)=1./(a11(n)+1.e-30)
c
      d21(n)=a21(n)*d11(n)
      d22(n)=a22(n)-d21(n)*a12(n)
      d23(n)=a23(n)-d21(n)*a13(n)
      d24(n)=a24(n)-d21(n)*a14(n)
c
      d31(n)=a31(n)*d11(n)
      b32=a32(n)-d31(n)*a12(n)
      b33=a33(n)-d31(n)*a13(n)
      b34=a34(n)-d31(n)*a14(n)
c
      d41(n)=a41(n)*d11(n)
      b42=a42(n)-d41(n)*a12(n)
      b43=a43(n)-d41(n)*a13(n)
      b44=a44(n)-d41(n)*a14(n)
c
      d22(n)=1./(d22(n)+1.e-30)
c
      d32(n)=b32*d22(n)
      d33(n)=b33-d32(n)*d23(n)
      d34(n)=b34-d32(n)*d24(n)
c
      d42(n)=b42*d22(n)
      c43=b43-d42(n)*d23(n)
      c44=b44-d42(n)*d24(n)
c
      d33(n)=1./(d33(n)+1.e-30)
c
      d43(n)=c43*d33(n)
      d44(n)=c44-d43(n)*d34(n)
c
      d44(n)=1./(d44(n)+1.e-30)
c
      d12(n) = a12(n)
      d13(n) = a13(n)
      d14(n) = a14(n)
c
      enddo
c
c
      return
      end

c
      subroutine lusolve(
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     rhs1,rhs2,rhs3,rhs4,
     &     xn1,xn2,xn3,xn4)
c
c     a routine to calculate an  inverse
c
      real
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     rhs1,rhs2,rhs3,rhs4,
     &     xn1,xn2,xn3,xn4,
     &     y1,y2,y3,y4
c
      y1=rhs1
      y2=rhs2-(a21*y1)
      y3=rhs3-(a31*y1+(a32*y2))
      y4=rhs4-(a41*y1+(a42*y2+(a43*y3)))
c
      xn4=y4*a44
      xn3=(y3-(a34*xn4))*a33
      xn2=(y2-(a23*xn3+(a24*xn4)))*a22
      xn1=(y1-(a12*xn2+(a13*xn3+(a14*xn4))))
     &     *a11
c
c
c
      return
      end

