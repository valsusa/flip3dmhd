% Generate test particles
i=0;
d=.05
for x=0.025:d:1
for y=0.025:d:1
for z=0.025:d:1
i=i+1;
pos(i,1:3)=[x,y,z];
end
end
end
npart=i
fid = fopen('particle.txt', 'w');
fprintf(fid,' %i\n',npart);
for i=1:npart
bx=0;
by=0;
bz=tanh((pos(i,1)-.5)/.3);
volp=d^3;
rhopart=1;
siepart=1;
ux=0;uy=0;uz=0;ico=1;
fprintf(fid,' %e, ',pos(i,1:3));
fprintf(fid,' %e, ',volp);
fprintf(fid,' %e, ',rhopart);
fprintf(fid,' %e, ',[bx,by,bz]);
fprintf(fid,' %e, ',siepart);
fprintf(fid,' %e, ',[ux,uy,uz]);
fprintf(fid,' %i, ',ico);
fprintf(fid,'\n');
end
fclose(fid);