      subroutine bc_ghost(nxp,nyp,nzp,iwid,jwid,kwid,
     &     bx,by,bz,periodicx,periodicy,periodicz)
c
c     a routine to set ghost cell values of cell-centered
c     vector to zero
c
c     called by VINIT_GMRES
c
      dimension bx(*),by(*),bz(*)
      real*8 inbx,inby,inbz
      logical :: periodicx, periodicy, periodicz
c
c
c     bottom and top ( k=1 and k=nzp)
c
      do i=1,nxp
      do j=1,nyp
c
      ijk=(j-1)*jwid+(i-1)*iwid+1
c
      ijkb=(j-1)*jwid+(i-1)*iwid+1
      ijkt=(nzp-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
c      bx(ijk)=0.0
c      by(ijk)=0.0
c      bz(ijk)=0.0
c      call magnetic_field(i,j,1,inbx,inby,inbz)
c      bx(ijk)=inbx
c      by(ijk)=inby
c      bz(ijk)=inbz
      bx(ijkb)=bx(ijkb+2*kwid)
      by(ijkb)=by(ijkb+2*kwid)
      bz(ijkb)=bz(ijkb+2*kwid)
c      bx(ijkb)=bx(ijkt-kwid)
c      by(ijkb)=by(ijkt-kwid)
c      bz(ijkb)=bz(ijkt-kwid)


c
      ijk=(nzp-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
c      bx(ijk)=0.0
c      by(ijk)=0.0
c      bz(ijk)=0.0
      bx(ijkt)=bx(ijkt-2*kwid)
      by(ijkt)=by(ijkt-2*kwid)
      bz(ijkt)=bz(ijkt-2*kwid)
c      bx(ijkt)=bx(ijkb+kwid)
c      by(ijkt)=by(ijkb+kwid)
c      bz(ijkt)=bz(ijkb+kwid)
c
      enddo
      enddo
c
c
c     right and left ( i=1 and i=nxp)
c
      do j=1,nyp
      do k=1,nzp
c
      ijk=(k-1)*kwid+(j-1)*jwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      ijk=(k-1)*kwid+(j-1)*jwid+(nxp-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      enddo
      enddo
c
c     front and back ( j=1 and j=nyp)
c
      do k=1,nzp
      do i=1,nxp
c
      ijk=(k-1)*kwid+(i-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      ijk=(k-1)*kwid+(nyp-1)*jwid+(i-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      enddo
      enddo
c
      return
      end

      
