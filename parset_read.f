      subroutine parset_read
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
      integer :: nread,iread
c
c
c
       pi=acos(-1.) 
c
c     set up linked list
c
      do n=1,ncells
      iphead(ijkcell(n))=0
      enddo
c
      iphead(1)=1
      do  np=1,npart
c
      pxi(np)=1.
      peta(np)=1.
      pzta(np)=1.
c
      link(np)=np+1
      enddo
      link(npart)=0
c
c
c     set all particle variables for np=0
c
      link(0)=0
      px(0)=0.0
      py(0)=0.0
      pz(0)=0.0
      pxi(0)=0.0
      peta(0)=0.0
      pzta(0)=0.0
      mupx(0)=0.0
      mupy(0)=0.0
      mupz(0)=0.0
      up(0)=0.0
      vp(0)=0.0
      wp(0)=0.0
      ico(0)=0
      ep(0)=0.0
c
      open(20,file='particle.txt',status='unknown',form='formatted')
      write(*,*)'reading'
      read(20,*)nread
      write(*,*)nread

c
      do iread=1,nread
      np=iphead(1)
c
      read(20,*)wspx,wspy,wspz,pvolume,rhopart,bxpart,bypart,bzpart,
     &          siepart,upart,vpart,wpart,icopart
c      write(*,*)wspx,wspy,wspz,pvolume,rhopart,bxpart,bypart,bzpart,
c     &          siepart,upart,vpart,wpart,icopart
c
      px(np)=wspx
      py(np)=wspy
      pz(np)=wspz
c
      k=1+pz(np)/dx
      j=1+py(np)/dy
      i=1+px(np)/dz
c
      ijk=1+(i-1)*iwid+(j-1)*jwid+kwid*(k-1)
      iphead(1)=link(np)
      link(np)=iphead(ijk)
      iphead(ijk)=np

      if(np.eq.0) then
            write(6,*) "parset: not enough particles"
            stop 'parset'
      end if
c
      pxi(np)=1.+px(np)/dx
      peta(np)=1.+py(np)/dy
      pzta(np)=1.+pz(np)/dz
c
      volp(np)=pvolume
      mass(np)=pvolume*rhopart
c
      mupx(np)=pvolume*bxpart
      mupy(np)=pvolume*bypart
      mupz(np)=pvolume*bzpart
c
      ep(np)=mass(np)*siepart/gm1/rhopart
c
      ico(np)=icopart
c
      up(np)=upart
      vp(np)=vpart
      wp(np)=wpart
c

      enddo
c
      write(*,*)'parset: particle initialization completed'
c
      return
      end subroutine parset_read
