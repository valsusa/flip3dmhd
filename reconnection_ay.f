      subroutine reconnection_ay_x(ncells,ijkcell,iwid,jwid,kwid,
     &     ibp2,jbp2,kbp2,dx,dy,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     bxn,byn,bzn,rfsolar)
c
      implicit none
      real*8
     &     c1x(*),c2x(*),c3x(*),c4x(*),
     &     c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),
     &     c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),
     &     c5z(*),c6z(*),c7z(*),c8z(*),vol(*),
     &     dx,dy,dz,
     &     bxn(*),byn(*),bzn(*),rfsolar
      integer ncells,ijkcell(*),iwid,jwid,kwid,
     &     ibp2,jbp2,kbp2
c
      real*8 aymin,aymax,aymaxx,ay,aym1,aym2
      integer i,j,k,ijk
      logical fmin,fmax
c
      rfsolar=0.0d0
      aymin=1.d10
      aymax=-1.d10
      aymaxx=-1.d10
      fmin=.false.
      fmax=.false.
      aym2=0.0d0
      aym1=0.0d0
      ay=0.0d0
      i=ibp2/2
      j=2
      do k=2,int(kbp2*5./6.)
      ijk=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
      ay=aym1-dz*bxn(ijk)

      if ((ay-aym1)*(aym1-aym2).lt.0.0d0) then
          if ((ay-aym1).gt.0.d0) then
	      if(.not.fmin) then
	        aymin=min(aym1,aymin)
              end if
		fmin=.true.
          else
	      if(.not.fmax) then
   	        aymax=max(aymax,aym1)
		fmax=.true.
              end if
          endif
      end if
      if(fmin) aymaxx=max(ay,aymaxx)
      aym2=aym1
      aym1=ay
      enddo
      if(.not.fmin)then
        aymin=0.0
        fmin=.true.
      endif
      if(.not.fmax)then
        aymax=aymaxx
        fmax=.true.
      endif
      if(fmin.and.fmax) rfsolar=(aymax-aymin)
c      write(*,*)'rfsolar',rfsolar
      return
      end


      subroutine reconnection_ay(ncells,ijkcell,iwid,jwid,kwid,
     &     ibp2,jbp2,kbp2,dx,dy,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     bxn,byn,bzn,rfsolar)
c
      implicit none
      real*8
     &     c1x(*),c2x(*),c3x(*),c4x(*),
     &     c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),
     &     c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),
     &     c5z(*),c6z(*),c7z(*),c8z(*),vol(*),
     &     dx,dy,dz,
     &     bxn(*),byn(*),bzn(*),rfsolar
      integer ncells,ijkcell(*),iwid,jwid,kwid,
     &     ibp2,jbp2,kbp2
      real(8) :: ay(ibp2)
c
      integer i,j,k,ijk
c
      rfsolar=0.0d0

      ay=0.0d0
      k=2+(kbp2-2)/2
      j=2
      do i=2,ibp2
      ijk=1+(i-1)*iwid+(j-1)*jwid+(k-1)*kwid
      ay(i)=ay(i-1)+dx*bzn(ijk)
      enddo
      rfsolar=maxval(ay)-minval(ay)
      return
      end

