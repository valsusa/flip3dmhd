      subroutine parlocat_cart(ncells,ijkcell,iphead,npart,
     &     periodic_x,periodic_y,periodic_z,
     &     ibar,jbar,kbar,
     &     xl,xr,yb,yt,ze,zf,
     &     dx,dy,dz,
     &     px,py,pz,pxi,peta,pzta)
c
      use boundary_module
c
c     a routine to calculate the logical coordinates
c     of a particle on a uniform, rectilinear grid
c
      integer ncells,ijkcell(*),iphead(*)
c
      logical periodic_x,periodic_y,periodic_z
c
      real*8
     &     xl,xr,yb,yt,ze,zf,
     &     dx,dy,dz,klen,factor,
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
      integer :: kbar
c
c     first, require that particle position be in bounds
c     if periodic, return to principal periodic interval
c     if not periodic, reflect particle position in normal
c
c
      dzratio=10.
      klen=(zf-ze)/dz/log(dzratio)
      pi=acos(-1.)
      factor=(exp((zf-ze)/dz/klen)-1.)/(zf-ze)
c      write(*,*)'factor=',factor,zf,ze,klen,kbar
c
c
      do n=1,ncells
        ijk=ijkcell(n)
        np=iphead(ijk)
c
c     consider spherically expanding grid
c
      if(grid_type_x.eq.0) then
      xr=(erre0+pz(np))*tanthetax*(ibar-(ibar+1)/2.0)/dble(ibar+1)
      xl=-xr
      end if

   21 if(px(np).lt.xl.or.px(np).gt.xr) then
c
        if(px(np).lt.xl) then
         if(periodic_x) then
           px(np)=px(np)+xr-xl
         else
c           px(np)=xl+2.*(xl-px(np))
c           px(np)=xl+(xl-px(np))
c	leave as is for later removal
	goto 25
         endif
        endif
c
        if(px(np).gt.xr) then
         if(periodic_x) then
           px(np)=px(np)-(xr-xl)
         else
c           px(np)=xr-2.*(px(np)-xr)
c           px(np)=xr-(px(np)-xr)
c	leave as is for later removal
	goto 25
         endif
        endif
c
        go to 21
        endif
25	continue
c
   22 if(py(np).lt.yb.or.py(np).gt.yt) then
c
        if(py(np).lt.yb) then
         if(periodic_y) then
           py(np)=py(np)+(yt-yb)
         else

           py(np)=py(np)+2.*(yb-py(np))
         endif
        endif
c
        if(py(np).gt.yt) then
         if(periodic_y) then
           py(np)=py(np)-(yt-yb)
         else
           py(np)=yt-2.*(py(np)-yt)
         endif
        endif
        go to 22
        endif
c
c
   23  if(pz(np).lt.ze.or.pz(np).gt.zf) then
        if(pz(np).lt.ze) then
         if(periodic_z) then
           pz(np)=pz(np)+(zf-ze)
         else
            if(bcpz.eq.1) then
c reflect
c           pz(np)=pz(np)+(zf-ze)
            pz(np)=ze+2.*(ze-pz(np))
c           pz(np)=ze+(ze-pz(np))
            else
c	leave as is for later removal
            endif
           goto 24
         endif
        endif
c
        if(pz(np).gt.zf) then
         if(periodic_z) then
           pz(np)=pz(np)-(zf-ze)
         else
            if(bcpz.eq.1) then
            pz(np)=zf-2.*(pz(np)-zf)
c           pz(np)=zf-(pz(np)-zf)
            else
c	leave as is for later removal
            end if
        goto 24
         endif
        endif
c
c
        go to 23
        endif
24	continue
        enddo
c
c       calculate the new logical coordinates of the particle
c
        do n=1,ncells
c
         ijk=ijkcell(n)
         np=iphead(ijk)
c
      select case(grid_type_x)
      case(0)
         pxi(np) = 2+(ibar+1)/2.0 + px(np)*dble(ibar+1)/(pz(np)+erre0)
     &    /tanthetax
c      pxi(np)=2.+(px(np)-xl)/(xr-xl)*dble(ibar+1)
c      write(*,*)px(np),pxi(np),pz(np),xl,xr,ibar

c           x(ijk)=(z(ijk)+erre0)*tanthetax*(i-2-(ibar+1)/2.0)/dble(ibar+1)
      case(1)
         pxi(np)=2.+(px(np)-xl)/dx
      case(5)
         pxi(np)=2d0+ibar*(tanh(ascale*(px(np)/(xr-xl)-.5d0))+
     &    tanh(ascale/2d0))/2d0/tanh(ascale/2d0)
      case default
         pxi(np)=2.+(px(np)-xl)/dx
      end select
c         pxi(np)=2.+(xr-xl)/dx*(.5+
c     &           ((px(np)-xl)/(xr-xl)/4.-1./8.)**(1./3.))
         peta(np)=2.+(py(np)-yb)/dy

      select case(grid_type_z)
      case(1)
         pzta(np)=2.+(pz(np)-ze)/dz
      case(2)
         pzta(np)=2.+klen*log(pz(np)*factor+1.)
      case(3)
         pzta(np)=2d0+.5d0*(kbar+kbar*sinh(ascale*(pz(np)-(zf-ze)/2d0)
     &         /(zf-ze))/sinh(ascale/2d0))
      case(4)
         pzta(np)=2d0+.5d0*(kbar+kbar*sin(ascale*(pz(np)-(zf-ze)/2d0)
     &         /(zf-ze))/sin(ascale/2d0))
      case(5)
         pzta(np)=2d0+kbar*(tanh(ascale*(pz(np)/(zf-ze)-.5d0))+
     &    tanh(ascale/2d0))/2d0/tanh(ascale/2d0)
      case default
      pzta(np)=2.+(pz(np)-ze)/dz
      end select
        enddo
c
      return
      end

