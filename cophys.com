*cd cophys
cll   common /cophys/ ................................. 77/03/02 ....
c
      logical :: read_from_file
      integer :: initial_equilibrium, bcpz
      dimension rwz(100)
      real*8 flux, surface_area
      common /cophys/ com5(1),grind , 
     .        gm1   , rho0  , beta  , asq   , a     , wsa   , h     ,
     .        tke   , tie   , etotl , tmass , tmomx , tmomy , tmomz ,
     .        time  , flux,   surface_area,
     .        toti  , totk,   totb,   tote, tginte,
     .        tgbsq , tgbsqx, tgbsqy, tgbsqz,
     .        tgkex , tgkey, tgkez, rfsolar,
     .        tgj_par, tgj_per,
     .        tparke, tgheat, gcur_max,
     .        tmom  , tgheat_par, tgheat_per, 
     .        tbe   , tle   ,
     .        dx    , dy    , dz   ,   
     .        rpl   , rvac  , rwall , rmaj  , cdlhf , sdlhf ,
     .        cdlt  , sdlt  , cdph  , sdph  , cdphhf, sdphhf, dphi  ,
     .        tmax  , tmax0 , tmax1 , tmax2 , tmax3 , 
     .        btorus, bvertcl,rhoi  , siei  , 
     .        racc  , zacc  , tflux , pflux ,
     .        gx    , gy    , gz    , tiesav, tkesav, tbesav,
     .        rq0   , q0,  coll,  efld0,
     .        c5last, dvbmax, rwz, initial_equilibrium, bcpz,
     .        read_from_file
      common/icophys/ 
     .        idvb  , jdvb  , kdvb  
c
