      subroutine stress_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     bxl,byl,bzl,p,r4pi,
     &     exx,exy,exz,eyy,eyz,ezz)
c
c     a routine to calculate the Maxwell stress
c     and combine it with the viscous stress
c
      integer ijkcell(*)
c
      real pixx(*),pixy(*),pixz(*),
     &     piyy(*),piyz(*),pizz(*),
     &     bxl(*),byl(*),bzl(*),p(*),
     &     exx(*),exy(*),exz(*),
     &     eyy(*),eyz(*),ezz(*)
c
      do n=1,ncells
c
      ijk=ijkcell(n)
c
      exx(ijk)=-p(ijk)
     &     -0.5*(bxl(ijk)**2+byl(ijk)**2+bzl(ijk)**2)*r4pi
     &     +bxl(ijk)**2*r4pi+pixx(ijk)
      exy(ijk)=bxl(ijk)*byl(ijk)*r4pi+pixy(ijk)
      exz(ijk)=bxl(ijk)*bzl(ijk)*r4pi+pixz(ijk)
c
      eyy(ijk)=-p(ijk)
     &     -0.5*(bxl(ijk)**2+byl(ijk)**2+bzl(ijk)**2)*r4pi
     &     +byl(ijk)**2*r4pi+piyy(ijk)
      eyz(ijk)=byl(ijk)*bzl(ijk)*r4pi+piyz(ijk)
c
      ezz(ijk)=-p(ijk)
     &     -0.5*(bxl(ijk)**2+byl(ijk)**2+bzl(ijk)**2)*r4pi
     &     +bzl(ijk)**2*r4pi+pizz(ijk)
c
      enddo
c
      return
      end
