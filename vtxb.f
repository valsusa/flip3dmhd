*dk vtxb
      subroutine vtxb(i1,i2,j1,j2,k1,k2,iwid,jwid,kwid,
     &     vol,bxn,byn,bzn,bxv,byv,bzv)
c
c     a routine to calculate the magnetic field at a vertex
c     by averaging cell-centered values
c
      implicit real*8 (a-h,o-z)
c
      dimension bxn(*),byn(*),bzn(*),
     &          bxv(*),byv(*),bzv(*),vol(*)
c
      do 1 k=k1,k2
      do 1 j=j1,j2
      do 1 i=i1,i2
c
      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      rvvol=1./(vol(ijk)
     &         +vol(ijk-iwid)
     &         +vol(ijk-iwid-jwid)
     &         +vol(ijk-jwid)
     &         +vol(ijk-kwid)
     &         +vol(ijk-iwid-kwid)
     &         +vol(ijk-iwid-jwid-kwid)
     &         +vol(ijk-jwid-kwid))
c
      bxv(ijk)=(bxn(ijk)*vol(ijk)
     &               +bxn(ijk-iwid)*vol(ijk-iwid)
     &               +bxn(ijk-iwid-jwid)*vol(ijk-iwid-jwid)
     &               +bxn(ijk-jwid)*vol(ijk-jwid)
     &               +bxn(ijk-kwid)*vol(ijk-kwid)
     &               +bxn(ijk-iwid-kwid)*vol(ijk-iwid-kwid)
     &               +bxn(ijk-iwid-jwid-kwid)*vol(ijk-iwid-jwid-kwid)
     &               +bxn(ijk-jwid-kwid)*vol(ijk-jwid-kwid))*rvvol
c
      byv(ijk)=(byn(ijk)*vol(ijk)
     &               +byn(ijk-iwid)*vol(ijk-iwid)
     &               +byn(ijk-iwid-jwid)*vol(ijk-iwid-jwid)
     &               +byn(ijk-jwid)*vol(ijk-jwid)
     &               +byn(ijk-kwid)*vol(ijk-kwid)
     &               +byn(ijk-iwid-kwid)*vol(ijk-iwid-kwid)
     &               +byn(ijk-iwid-jwid-kwid)*vol(ijk-iwid-jwid-kwid)
     &               +byn(ijk-jwid-kwid)*vol(ijk-jwid-kwid))*rvvol
c
      bzv(ijk)=(bzn(ijk)*vol(ijk)
     &               +bzn(ijk-iwid)*vol(ijk-iwid)
     &               +bzn(ijk-iwid-jwid)*vol(ijk-iwid-jwid)
     &               +bzn(ijk-jwid)*vol(ijk-jwid)
     &               +bzn(ijk-kwid)*vol(ijk-kwid)
     &               +bzn(ijk-iwid-kwid)*vol(ijk-iwid-kwid)
     &               +bzn(ijk-iwid-jwid-kwid)*vol(ijk-iwid-jwid-kwid)
     &               +bzn(ijk-jwid-kwid)*vol(ijk-jwid-kwid))*rvvol
c
    1 continue
c
      return
      end
