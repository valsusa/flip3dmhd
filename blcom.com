c     TYPE DECLARATIONS
c
c
      integer npcelx,npcely,npcelz,icoi,numtot,
     &     modex_bottom,modey_bottom
c
      common/integer_input/
     &     npcelx(nreg),npcely(nreg),npcelz(nreg),icoi(nreg),
     &     modex_bottom,modey_bottom,
     &     numtot(0:nreg)

      real*8 resistivity,
     &     uvi,vvi,wvi,utordrft,xcenter,ycenter,zcenter,
     &     rex,rey,rez,rix,riy,riz,siep,rhr,qom,bxr,byr,bzr,
     &     xvi,yvi,zvi,
     &     uwall_bottom, uwall_bottom_perturbed,
     &     vwall_bottom, vwall_bottom_perturbed,tramp
c
      common/real_input/
     &     eps,resistivity,
     &     uvi(nreg),vvi(nreg),wvi(nreg),utordrft(nreg),
     &     xcenter(nreg),ycenter(nreg),zcenter(nreg),
     &     rex(nreg),rey(nreg),rez(nreg),rix(nreg),riy(nreg),riz(nreg),
     &     siep(nreg), rhr(nreg),qom(nreg),
     &     bxr(nreg),byr(nreg),bzr(nreg),
     &     xvi(8,nreg),yvi(8,nreg),zvi(8,nreg),
     &     uwall_bottom, uwall_bottom_perturbed,
     &     vwall_bottom, vwall_bottom_perturbed,tramp
c
c
cll   common /blcom/  ................................. 77/02/15 ....
      real*8
     &     wate,sie,rho,rhol,number,numberv,color
      common/blc1/
     &     wate(itdim,29),number(itdim),
     .     sie(itdim),rho(itdim),rhol(itdim),
     &     numberv(itdim),color(itdim)
c
      real*8
     &     u,v,w,ul,vl,wl,xptilde,yptilde,zptilde,
     &     upv1,vpv1,wpv1,axv1,ayv1,azv1,mc,sie1p,
     &     dbxv1,dbyv1,dbzv1,
     &     ax,ay,az,mv,umom,vmom,wmom,work,pdv,
     &     csq,vasq,divu,gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,numparc
c
	common/inject/tbot(idxg,idyg),dtbot(idxg,idyg),
     &       ubot(idxg,idyg),vbot(idxg,idyg),wbot(idxg,idyg)
c
      common/fluid/
     &     u(itdim),v(itdim),w(itdim),
     &     ul(itdim),vl(itdim),wl(itdim),
     &     xptilde(itdim),yptilde(itdim),zptilde(itdim),
     &     upv1(itdim),vpv1(itdim),wpv1(itdim),
     &     axv1(itdim),ayv1(itdim),azv1(itdim),
     &     dbxv1(itdim),dbyv1(itdim),dbzv1(itdim),
     &     mc(itdim),sie1p(itdim),
     &     ax(itdim),ay(itdim),az(itdim),
     &     mv(itdim),umom(itdim),vmom(itdim),wmom(itdim),
     &     work(itdim),pdv(itdim),csq(itdim),
     &     vasq(itdim),
     &     divu(itdim),gradx(itdim),grady(itdim),gradz(itdim),
     &     exx(itdim),exy(itdim),exz(itdim),
     &     eyy(itdim),eyz(itdim),ezz(itdim),chden(itdim),
     &     elpot(itdim),divergenceu(itdim),streamf(itdim),
     &     curlcx(itdim),curlcy(itdim),curlcz(itdim),
     &     potdiv(itdim),numparc(itdim)
      integer imp
c
      common/ifluid/
     &     imp

c
      real*8
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     divpix,divpiy,divpiz
c
      common/viscous_stress/
     &     pixx(itdim),pixy(itdim),pixz(itdim),
     &     piyy(itdim),piyz(itdim),pizz(itdim),
     &     divpix(itdim),divpiy(itdim),divpiz(itdim)
c
      real*8
     &     delta,courant,lagrange,
     &     bxn,byn,bzn,bxl,byl,bzl,bxv,byv,bzv,p,
     &     bmagx,bmagy,bmagz,jx,jy,jz,ex,ey,ez,
     &     Ohmic_heating
c
      common/magfield/
     &     delta,
     &     courant,lagrange,
     &     bxn(itdim),byn(itdim),bzn(itdim),
     &     bxl(itdim),byl(itdim),bzl(itdim),
     &     bmagx(itdim),bmagy(itdim),bmagz(itdim),
     &     jx(itdim),jy(itdim),jz(itdim),
     &     ex(itdim),ey(itdim),ez(itdim),
     &     Ohmic_heating(itdim),
     &     bxv(itdim),byv(itdim),bzv(itdim),
     &     divuphix(itdim),divuphiy(itdim),divuphiz(itdim),
     &     dbvx(itdim),dbvy(itdim),dbvz(itdim),
     &     p(itdim)
c
      integer
     &     itmax,itmag,numit
c
      common/isolver/
     &     itmax,itmag,numit
c
      common/solver/
     &     error,
     &     a11(itdim),a12(itdim),a13(itdim),a14(itdim),
     &     a21(itdim),a22(itdim),a23(itdim),a24(itdim),
     &     a31(itdim),a32(itdim),a33(itdim),a34(itdim),
     &     a41(itdim),a42(itdim),a43(itdim),a44(itdim)
c
      common/cr_solver/
     &     roq(itdim),bxq(itdim),byq(itdim),bzq(itdim),
     &     rotil(itdim),bxtil(itdim),bytil(itdim),bztil(itdim),
     &     arotil(itdim),abxtil(itdim),abytil(itdim),abztil(itdim),
     &     paro(itdim),pabx(itdim),paby(itdim),pabz(itdim),
     &     aroq(itdim),abxq(itdim),abyq(itdim),abzq(itdim),
     &     ptil(itdim),
     &     bxtilde(itdim),bytilde(itdim),bztilde(itdim),
     &     s(itdim),sbx(itdim),sby(itdim),sbz(itdim),
     &     dro(itdim),dbx(itdim),dby(itdim),dbz(itdim),
     &     celerr(itdim)
c
      real*8 efnrg,ebnrg,eknrg,charge,thistry,
     &     reconnected_flux
c
      common/pltstuf/
     &     efnrg(nhst),ebnrg(nhst),eknrg(nhst),
     &     charge(nhst),thistry(nhst),reconnected_flux(0:nhst)
c
c
      common/igrid/
     &     iphead(itdim),ijkcell(itdim),ijkvtx(itdim),
     &     ijktmp2(itdim),ijktmp3(itdim),ijktmp4(itdim),
     &     ijkctmp(itdim),istep(idzg)
c
      logical adaptg,explicit,fields,cartesian,
     &     periodic_x,periodic_y,periodic_z,resist,compressible
     &    ,ringingout
c
      common/logical_input/
     &     adaptg,explicit,fields,cartesian,compressible,
     &     ringingout,
     &     periodic_x,periodic_y,periodic_z,resist
c
      real*8
     &     xl,xr,yb,yt,ze,zf,
     &     x,y,z,vol,volpc,vvol,tsix,tsiy,tsiz,
     &     etax,etay,etaz,nux,nuy,nuz,
     &     lama,lamb,lamc,
     &     area_x,area_y,area_z
c
      common/grid/
     &     lama,lamb,lamc,
     &     xl,xr,yb,yt,ze,zf,
     &     x(itdim),y(itdim),z(itdim),
     &     vol(itdim),volpc(itdim),vvol(itdim),
     &     tsix(itdim),tsiy(itdim),tsiz(itdim),
     &     etax(itdim),etay(itdim),etaz(itdim),
     &     nux(itdim),nuy(itdim),nuz(itdim),
     &     area_x(itdim),area_y(itdim),area_z(itdim)
c
      real*8
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z
c
      common/geomcoef/
     &       c1x(itdim),c2x(itdim),c3x(itdim),c4x(itdim),
     &       c5x(itdim),c6x(itdim),c7x(itdim),c8x(itdim),rxv(itdim),
     &       c1y(itdim),c2y(itdim),c3y(itdim),c4y(itdim),
     &       c5y(itdim),c6y(itdim),c7y(itdim),c8y(itdim),
     &       c1z(itdim),c2z(itdim),c3z(itdim),c4z(itdim),
     &       c5z(itdim),c6z(itdim),c7z(itdim),c8z(itdim)
c
c
c     *********************************************
c     common block adapt
c     **********************************************
c
      real*8
     &     wgrid
c
      common/adapt/
     &     wgrid(itdim)
c
      integer 
     &     nptotl,npsampl,link,ico,
     &     npsav
c
      common/ipart/
     &     nptotl,npsampl,
     &     npsav(nlist),
     &     ico(0:npart),
     &     link(0:npart),
     .     iphd2(itdim)

c
      real*8 mupx,mupy,mupz,
     &     px,py,pz,pxi,peta,pzta,up,vp,wp,ep,
     &     mass,qpar,uptilde,vptilde,wptilde,volp
c
      common/part/px(0:npart),py(0:npart),pz(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart),
     &     up(0:npart),vp(0:npart),wp(0:npart),
     &     ep(0:npart),
     &     mass(0:npart),qpar(0:npart),
     &     volp(0:npart),
     &     mupx(0:npart),mupy(0:npart),mupz(0:npart),
     &     uptilde(itdim),vptilde(itdim),wptilde(itdim)
c
