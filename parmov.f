      subroutine parmov(compressible,ncells,ijkcell,iwid,jwid,kwid,
     &                  ibar,jbar,kbar,npart,itdim,
     &                  periodic_x,periodic_y,periodic_z,
     &                  xr,xl,yb,yt,ze,zf,dx,dy,dz,cartesian,
     &                  iphead,link,ijkctmp,iphd2,pxi,peta,pzta,
     &                  wate,ul,vl,wl,upv1,vpv1,wpv1,ax,ay,az,
     &                  axv1,ayv1,azv1,px,py,pz,up,vp,wp,mass,
     &                  dbxv1,dbyv1,dbzv1,
     &                  mupx,mupy,mupz,dbvx,dbvy,dbvz,numparc,vol,
     &                  volp,
     &                  ep,work,pdv,divu,dt,rmaj,cdlt,sdlt,
     &                  ijktmp2,ijktmp3,ijktmp4,area_x,area_y,area_z,
     &                  x,y,z,xptilde,yptilde,zptilde,
     &                  tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,mgeom)
      use boundary_module
      implicit real*8 (a-h,o-z)
      logical periodic_x,periodic_y,periodic_z,
     &     cartesian,compressible
c
      real*8 nux,nuy,nuz,mass,divu,divup,numparc,numparcp,vol,
     &     mupx(0:npart),mupy(0:npart),mupz(0:npart),volp(0:npart),
     &     xr,xl,yb,yt,ze,zf,dx,dy,dz,dbvx(*),dbvy(*),dbvz(*),
     &     dbxv1,dbyv1,dbzv1
c
      dimension ijkcell(*),iphead(*),link(0:npart),ijkctmp(*),
     &          iphd2(*),pxi(0:npart),peta(0:npart),pzta(0:npart),
     &          wate(itdim,*),
     &          ul(*),vl(*),wl(*),upv1(*),vpv1(*),wpv1(*),
     &          ax(*),ay(*),az(*),axv1(*),ayv1(*),azv1(*),
     &          dbxv1(*),dbyv1(*),dbzv1(*),
     &          px(0:npart),py(0:npart),pz(0:npart),
     &          up(0:npart),vp(0:npart),wp(0:npart),
     &          mass(0:npart),ep(0:npart),work(*),pdv(*),divu(*),
     &          ijktmp2(*),ijktmp3(*),ijktmp4(*),
     &          area_x(*),area_y(*),area_z(*),
     &          x(*),y(*),z(*),xptilde(*),yptilde(*),zptilde(*),
     &          tsix(*),tsiy(*),tsiz(*),etax(*),etay(*),etaz(*),
     &          nux(*),nuy(*),nuz(*),numparc(*),vol(*)
      dimension ijkstep(27),ijkvstp(8)
c
c
c     *******************************************
c
c     an explicit particle mover 
c
c     *********************************************
c
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
	xc=ibar*dx/2.d0
	yc=jbar*dy/2.d0
	zc=kbar*dz/2.d0
c
      call celstep(iwid,jwid,kwid,ijkstep)
      call vtxindx(iwid,jwid,kwid,ijkvstp)
c
cdir$ ivdep
c
      do 1111 n=1,ncells
      iphd2(ijkcell(n))=0
      ijkctmp(n)=ijkcell(n)
 1111 continue
c
      newcell=ncells
c
    1 continue
c
c     check for more particles to process
c
      newcell_nxt=0
      do 21 n=1,newcell
      ijk=ijkctmp(n)
  211 if(iphead(ijk).gt.0) then
      np=iphead(ijk)
      newcell_nxt=newcell_nxt+1
      ijkctmp(newcell_nxt)=ijkctmp(n)
      endif
   21 continue
      newcell=newcell_nxt
c
c     if there are more, process particles as though there were one
c     in every cell in the mesh
c
      if(newcell.eq.0) go to 1000
c
c     calculate weights for trilinear interpolation
c
      call watev(newcell,ijkctmp,iphead,itdim,
     &     pxi,peta,pzta,wate)
c
c    interpolate grid velocity to particle
c
      call trilinp(newcell,ijkctmp,itdim,iwid,jwid,kwid,
     &     iphead,pxi,peta,pzta,
     &     wate,ul,vl,wl,upv1,vpv1,wpv1)
c
c     interpolate grid acceleration to particles
c
      call trilinp(newcell,ijkctmp,itdim,iwid,jwid,kwid,
     &     iphead,pxi,peta,pzta,
     &     wate,ax,ay,az,axv1,ayv1,azv1)
c
c     interpolate change in magnetization to particles 
c

c     advance particle positions and velocity
c
cdir$ ivdep
      do 305 n=1,newcell
c
      ijk=ijkctmp(n)
      np=iphead(ijk)
c
      dnull=1.d0
      if(circular_wall) then
      rgeom=sqrt((px(np)-xc)**2+(pz(np)-zc)**2)
      if(rgeom.ge.5d0-min(dx,dz)/2d0) dnull=0.d0
      end if
c
      px(np)=px(np)+upv1(ijk)*dt*dnull
      py(np)=py(np)+vpv1(ijk)*dt*dnull
      pz(np)=pz(np)+wpv1(ijk)*dt*dnull
c
      up(np)=up(np)+axv1(ijk)*dnull
      vp(np)=vp(np)+ayv1(ijk)*dnull
      wp(np)=wp(np)+azv1(ijk)*dnull
c
  305 continue
c
c
c     adjust particle internal energy
c
      do 340 ll=1,8
cdif$ ivdep
      do 340 n=1,newcell
      ijk=ijkctmp(n)
      np=iphead(ijk)
      index=ijk+ijkvstp(ll)
      workp=mass(np)*(work(index)*wate(ijk,ll))
      ep(np)=ep(np)+workp*dnull
  340 continue
c
cdir$ ivdep
      do 345 n=1,newcell
      ijk=ijkctmp(n)
      np=iphead(ijk)
      workp=-0.5*mass(np)*
     &     (axv1(ijk)**2+ayv1(ijk)**2+azv1(ijk)**2)
      ep(np)=ep(np)+workp*dnull
  345 continue
c
      call watec(newcell,ijkctmp,iphead,itdim,
     &                 pxi,peta,pzta,wate)
c
c
c     interpolate change in magnetization to particles
c
      call triquad(newcell,ijkctmp,itdim,ijkstep,
     &     dbvx,dbvy,dbvz,wate,dbxv1,dbyv1,dbzv1)
c
cdir$ ivdep
      do n=1,newcell
      ijk=ijkctmp(n)
      np=iphead(ijk)
c
      mupx(np)=mupx(np)+mass(np)*dbxv1(ijk)
      mupy(np)=mupy(np)+mass(np)*dbyv1(ijk)
      mupz(np)=mupz(np)+mass(np)*dbzv1(ijk)
c
      enddo
c

c
      do 350 ll=1,27
cdir$ ivdep
      do 350 n=1,newcell
      ijk=ijkctmp(n)
      index=ijk+ijkstep(ll)
      np=iphead(ijk)
      pdvp= mass(np)*wate(ijk,ll)*pdv(index)
      ep(np)=ep(np)+pdvp*dnull
  350 continue

      do n=1,newcell
      divup=0.0d0
      numparcp=0.0d0
      ijk=ijkctmp(n)
      np=iphead(ijk)
      do ll=1,27
      index=ijk+ijkstep(ll)
c      divup=divup+wate(ijk,ll)*divu(index)*vol(index)
      divup=divup+wate(ijk,ll)*divu(index)
      numparcp=numparcp+wate(ijk,ll)*numparc(index)
      enddo
	  if(compressible) then
c     compressible
c      volp(np)=volp(np)+divup*dt*dnull/(numparcp+1d-10)
       volp(np)=volp(np)*(1d0+divup*dt*dnull)
      else
c     incompressible
      volp(np)=volp(np)
      end if
      enddo
c
c
c     calculate new natural coordinates for the particle
c
      if (cartesian) then
c
      call parlocat_cart(newcell,ijkctmp,iphead,npart,
     &     periodic_x,periodic_y,periodic_z,
     &     ibar,jbar,kbar,
     &     xl,xr,yb,yt,ze,zf,
     &     dx,dy,dz,
     &     px,py,pz,pxi,peta,pzta)
c
      else
c
      call parlocat(newcell,ijkctmp,iphead,iwid,jwid,kwid,
     &     IJKTMP2,IJKTMP3,IJKTMP4,RMAJ,DZ,AXV1,area_x,area_y,area_z,dt,
     &     itdim,npart,ibar,jbar,kbar,mgeom,cdlt,sdlt,
     &     wate,x,y,z,AXV1,AYV1,AZV1,
     &     XPTILDE,YPTILDE,ZPTILDE,upv1,vpv1,wpv1,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
      endif
c
      do 501 n=1,newcell
      ijk=ijkctmp(n)
      np=iphead(ijk)
      if(np.gt.0) then
c
      inew=int(pxi(np))
      jnew=int(peta(np))
      knew=int(pzta(np))
c
c      if((inew-2)*(ibp1-inew).lt.0)  write(6,*) 'np=',np,'inew=',inew
      if((jnew-2)*(jbp1-jnew).lt.0)  write(6,*) 'np=',np,'jnew=',jnew
c      if((knew-2)*(kbp1-knew).lt.0)  write(6,*) 'np=',np,'knew=',knew
c
c      inew=min(ibp1,max(2,inew))
      jnew=min(jbp1,max(2,jnew))
c      knew=min(kbp1,max(2,knew))
c
      ijknew=(knew-1)*kwid+(jnew-1)*jwid+(inew-1)*iwid+1
c
      if((knew-2)*(kbp1-knew).lt.0.or.(inew-2)*(ibp1-inew).lt.0) then
c	remove particle
c
      iphead(ijk)=link(np)
      link(np)=iphead(1)
      iphead(1)=np
c	write(*,*)'removed particle',np,pz(np)
c
      else
c
      iphead(ijk)=link(np)
      link(np)=iphd2(ijknew)
      iphd2(ijknew)=np
c
      endif
c
      endif
c
  501 continue
c
      go to 1
c
c
 1000 continue
c
cdir$ ivdep
      do 1001 n=1,ncells
      ijk=ijkcell(n)
      iphead(ijk)=iphd2(ijk)
      iphd2(ijk)=0
 1001 continue
c
c
      return
      end

