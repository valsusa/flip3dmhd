      subroutine metricc(ncells,ijkcell,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz)
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &   c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),vol(*),
     &     tsix(*),tsiy(*),tsiz(*),
     &     etax(*),etay(*),etaz(*),
     &     nux(*),nuy(*),nuz(*)
c
      real*8 nux,nuy,nuz
c
      do 1 n=1,ncells
c
      ijk=ijkcell(n)
c
      rvol=1./vol(ijk)
c
      tsix(ijk)=(c1x(ijk)+c2x(ijk)+c5x(ijk)+c6x(ijk))*rvol
      tsiy(ijk)=(c1y(ijk)+c2y(ijk)+c5y(ijk)+c6y(ijk))*rvol
      tsiz(ijk)=(c1z(ijk)+c2z(ijk)+c5z(ijk)+c6z(ijk))*rvol
c
      etax(ijk)=(c2x(ijk)+c3x(ijk)+c6x(ijk)+c7x(ijk))*rvol
      etay(ijk)=(c2y(ijk)+c3y(ijk)+c6y(ijk)+c7y(ijk))*rvol
      etaz(ijk)=(c2z(ijk)+c3z(ijk)+c6z(ijk)+c7z(ijk))*rvol
c
      nux(ijk)=(c5x(ijk)+c6x(ijk)+c7x(ijk)+c8x(ijk))*rvol
      nuy(ijk)=(c5y(ijk)+c6y(ijk)+c7y(ijk)+c8y(ijk))*rvol
      nuz(ijk)=(c5z(ijk)+c6z(ijk)+c7z(ijk)+c8z(ijk))*rvol
c
    1 continue
c
      return
      end
