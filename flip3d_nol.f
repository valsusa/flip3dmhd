*dk main
c torus main program -- version winner (=gorgeous with pfile)
      program flip3d 
c
      use boundary_module
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
      include 'nkmhd.com'

c
      character*80 name
c------------------------------------------------------------------
c               main program
c------------------------------------------------------------------
c
cll   1. open files:
c        --------------
      call begin(name)
c
cll   2. initialize run:
c        --------------
      call initiv
	  call plotinit(ibp2,jbp2,kbp2,name)
c
c
c     do j=1,3
c     do i=1,itdim
c     wate(i,j)=0.0
c     enddo
c     enddo
c     call debug(ncells,ijkcell,iwid,jwid,kwid,
c    &     cdlt,sdlt,strait,dz,
c    &     ibp1,jbp1,kbp1,nvtx,ijkvtx,wate(1,1),wate(1,2),wate(1,3),
c    &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
c    &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
c    &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
c    &     x,y,z,vol,vvol,gradx,grady,gradz,divu)
c
      if(read_from_file) then
      call parset_read
      else
      call parset
      end if
c
c
c
      call rinjinit

c
c     interpolate particle quantities to the grid
c
c
      call parcelc(ncells,ijkcell,ijkctmp,iwid,jwid,kwid,
     &      periodic_x,periodic_y,periodic_z,
     &      itdim,npart,ibp2,jbp2,kbp2,mc,volpc,sie1p,wate,
     &      ep,mass,volp,
     &      mupx,mupy,mupz,bxn,byn,bzn,numparc,
     &      iphead,link,iphd2,pxi,peta,pzta)
c
c
      call parcelv(ncells,ijkcell,nvtx,ijkvtx,nrg,iwid,jwid,kwid,dt,
     &     ijkctmp,
     &     itdim,iphead,iphd2,link,
     &     ico,mass,pxi,peta,pzta,up,vp,wp,
     &     wate,
     &     mv,umom,vmom,wmom,numberv,color)
           write(*,*)'finished parset'
c
c     ******************************************************************
c
c     generate an adaptive grid
c
c     *******************rinjini**********************************************
c
      if(adaptg) then
c
      call meshgen(ibp2,jbp2,kbp2,x,y,z,wgrid,vol,dt,taug,numgrid,
     &     WATE(1,7),WATE(1,8),WATE(1,9),
     &     WATE(1,10),WATE(1,11),WATE(1,12),
     &     WATE(1,13),WATE(1,14),WATE(1,15),
     &     LAMA,LAMB,LAMC,
     &     WATE(1,1),WATE(1,2),WATE(1,3),
     &     DELTA,PSI,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,
     &     WATE(1,4),WATE(1,5),WATE(1,6),
     &     iwid,jwid,kwid,ijkcell,ISTEP,IOTA,
     &     periodic_x,periodic_y,periodic_z,
     &     toroid,strait,rmaj,RWALL,
     &     BTORUS,Q0,
     &     cdlt,sdlt,dz)
c
      call celindex(2,ibp1,2,jbp1,2,kbp1,iwid,jwid,kwid,
     &     ijkcell,ijkctmp,ncells,ijkvtx,nvtxkm,nvtx)
c
      call metric(ncells,ijkcell,iwid,jwid,kwid,
     &     x,y,z,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,divpix)
c
      endif
c
c
cdak      call second(stim)
cdak      call getjtl (itlm)
c
      call budget
c
      itlwd=0
      t1=stim
      t2=t1
      trl=t1
c      tmin = (.02 + .005) * fibar*fjbar*fkbar
      tmin=0.0
cdak      tlm = itlm - tmin
      tlm = 300000.0
      write(*,*)'time limit, tlm=',tlm
      if (tlm .gt. 0) go to 1
c        (time limit too small for problem)
         write (kpt,9020) tmin
         write (kfm,9020) tmin
         call exit(1)
c
cll   3. time-step: integrate equations in time:
c        --------------------------------------
    1 continue
c
c
c     >>>>>>>>>>>>>>> ncyc-loop <<<<<<<<<<<<<<<
c
      do 1000 ncyc= ncyc1,ncyc2
c
cl       (calculate timestep)
        call timstp
c
c
   30    continue
c
         t=t+dt
         told=t2
cdak         call second(t2)
c
c      compute compute time/cycle/cell in millisec
c
         xx=(t2-told)*rbjbkb*1000.
         grind=xx
c     ****************************************************
c
c     prepare for the next computation cycle
c
c     ****************************************************
c
      call vinit
c
         if(lpr.eq.0) go to 50
c
            if((ncyc-1).eq.0.or.(mod(ncyc,5)-1).eq.0) pflag=1.
            if (pflag.gt.0.) then
            	write (kpt,9000)
          	  write(*,9000) ncyc,t,dt,courant,lagrange
     &,numit,grind,iddt
            endif
            pflag=-1.
c
c  NEED GRAPHICS HERE
c
         call output
c
   50    continue
c
c     <<<<<<<<<<<<<Solve Equations of Motion>>>>>>>>>>>>>>>>>
c
c
      call strain(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     ul,vl,wl,vol,gradx,grady,gradz)
c
      call stress(ncells,ijkcell,iwid,jwid,kwid,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     exx,exy,exz,eyy,eyz,ezz,mu,lam)
c
c
      if(nomag) then
c
      if (imp.eq.1) then
c
c----------------------------------
c implicit pressure solution
c----------------------------------
c
      do n=1,ncells
      p(ijkcell(n))=0.0
      end do
c
      call accel_3dmhd
c
      call strain(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     ul,vl,wl,vol,gradx,grady,gradz)
c
      do n=1,ncells
      ijk=ijkcell(n)
      divu(ijk)=exx(ijk)+eyy(ijk)+ezz(ijk)
      end do
c
      call eos(p,rho,vol,sie,csq,gm1,ncells,ijkcell)
c
c     this version of the source is contained in implctp_cr.f
c
      numit=itmax
        call poisson(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     ITDIM,iwid,jwid,kwid,PRECON,mv,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     wate(1,1),wate(1,2),wate(1,3),numit,error,DT,CNTR,wate(1,4),
     &     rho,divu,csq,wate(1,5),wate(1,6),wate(1,7),wate(1,8),
     &     wate(1,9),wate(1,10),p,wate(1,11),wate(1,12))
c
      itsub=20
c
c    calculate coef and srce before calling Poisson
c
      dtsq=cntr*dt**2
      dtcntr=dt*cntr
c
c      do n=1,ncells
cc      ijk=ijkcell(n)
c      wate(ijk,26)=1./(csq(ijk)*rho(ijk)*dtsq+1.e-10)
c      wate(ijk,27)=p(ijk)*wate(ijk,26)-divu(ijk)/dtcntr
cc      enddo
cc
c      call poisson(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
c     &     ITDIM,iwid,jwid,kwid,PRECON,gm1,mv,
c     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
c     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
c     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
c     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
c     &     itsub,numit,error,rnorm,DT,CNTR,wate(1,27),rho,divu,csq,
c     &     wate(1,26),wate(1,25),wate(1,24),wate(1,23),wate(1,1),bnorm,
c     &     gradx,grady,gradz,p,wate(1,22))
c
c
      else
c
c-------------------------------------------
c     explicit pressure
c-------------------------------------------
c
      call eos(p,rho,vol,sie,csq,gm1,ncells,ijkcell)
c
      endif
c
      call accel_3dmhd
c
      else
c
c     solve implicit MHD equations
c
 1111 continue
c
      call eos(p,rho,vol,sie,csq,gm1,ncells,ijkcell)
c
      do n=1,ncells
c
      ijk=ijkcell(n)
c
      rhol(ijk)=rho(ijk)
      bxl(ijk)=bxn(ijk)
      byl(ijk)=byn(ijk)
      bzl(ijk)=bzn(ijk)
c
      enddo
c
      call list(1,ibp2,1,jbp2,1,kbp2,iwid,jwid,kwid,
     &     nvtxtmp,ijktmp2)
c
      call setzero(nvtxtmp,ijktmp2,jx)
c
      call setzero(nvtxtmp,ijktmp2,jy)
c
      call setzero(nvtxtmp,ijktmp2,jz)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      error=3.e-2
      itmax=10
      eps=1.e-5
c
      call mfnk_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp3,ijktmp4,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     dt,error,r4pi,eps,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     roq,bxq,byq,bzq,rotil,bxtil,bytil,bztil,ptil,
     &     arotil,abxtil,abytil,abztil,paro,pabx,paby,pabz,
     &     aroq,abxq,abyq,abzq,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     xptilde,yptilde,zptilde,
     &     upv1,vpv1,wpv1,
     &     axv1,ayv1,azv1,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     u,v,w,mv,gm1,numit,itmax,totnrg,
     &     ul,vl,wl,vol,csq,p,
     &     rho,bxn,byn,bzn,
     &     rhol,bxl,byl,bzl,
     &     s,sbx,sby,sbz,etax,
     &     dro,dbx,dby,dbz,
     &     celerr)
c
c      write(*,*)'ijk = 2664'
c      write(*,*)'dudz=',dudz(2664)
c      write(*,*)'gradx,grady,gradz=',gradx(2664),grady(2664),
c     &           gradz(2664)
      if(numit.gt.itmax) then
      t=t-0.5*dt
      dt=0.5*dt
      write(*,*) 'flip3d:  time step cut by itercg at ncyc=', ncyc
      go to 1111
      endif
c
      endif
c
      call strain_ns(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     xptilde,yptilde,zptilde,
     &     upv1,vpv1,wpv1,
     &     axv1,ayv1,azv1,
     &     u,v,w,vol,gradx,grady,gradz)
c
      call divphi_3dmhd(ncells,ijkcell,
     &     xptilde,yptilde,zptilde,
     &     upv1,vpv1,wpv1,
     &     axv1,ayv1,azv1,
     &     vol,
     &     bxn,byn,bzn,bmagx,bmagy,bmagz,
     &     divuphix,divuphiy,divuphiz)
c
      if(resist) then
c
      call resistive_diff
c
      endif
c
      call budget
c
c	call terminator
c

      call parmov(compressible,ncells,ijkcell,iwid,jwid,kwid,
     &                  ibar,jbar,kbar,npart,itdim,
     &                  periodic_x,periodic_y,periodic_z,
     &                  xr,xl,yb,yt,ze,zf,dx,dy,dz,cartesian,
     &                  iphead,link,ijkctmp,iphd2,pxi,peta,pzta,
     &                  wate,ul,vl,wl,upv1,vpv1,wpv1,ax,ay,az,
     &                  axv1,ayv1,azv1,px,py,pz,up,vp,wp,mass,
     &                  dbxv1,dbyv1,dbzv1,
     &                  mupx,mupy,mupz,dbvx,dbvy,dbvz,numparc,vol,
     &                  volp,
     &                  ep,work,pdv,divu,dt,rmaj,cdlt,sdlt,
     &                  ijktmp2,ijktmp3,ijktmp4,area_x,area_y,area_z,
     &                  x,y,z,xptilde,yptilde,zptilde,
     &                  tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,mgeom)
c
c
      call rinj 
c
c
      call parcelv(ncells,ijkcell,nvtx,ijkvtx,nrg,iwid,jwid,kwid,dt,
     &     ijkctmp,
     &     itdim,iphead,iphd2,link,
     &     ico,mass,pxi,peta,pzta,up,vp,wp,
     &     wate,
     &     mv,umom,vmom,wmom,numberv,color)
c
c
      call parcelc(ncells,ijkcell,ijkctmp,iwid,jwid,kwid,
     &      periodic_x,periodic_y,periodic_z,
     &      itdim,npart,ibp2,jbp2,kbp2,mc,volpc,sie1p,wate,
     &      ep,mass,volp,
     &      mupx,mupy,mupz,bxn,byn,bzn,numparc,
     &      iphead,link,iphd2,pxi,peta,pzta)
c
cdak         call second (t3)
cg       if (t.ge.twfin .or. t3-stim.ge.tlm) goto 1010
         if (t.ge.twfin) goto 1010
c
         if (t2-t1.ge.dcttd)  go to 60
c
         if(t+1.e-10.lt.pttd) go to 70
c
         pttd=pttd+dpttd
c
   60    continue
         t1= t2
c
   70    continue
c
 1000 continue
c
c     >>>>>>>>>>>>>>> end of ncyc-loop <<<<<<<<<<<<<<<
c
      ncyc= ncyc2
c
cll   4. terminate run:
c        -------------
 1010 continue
c
      tout = t
c
c  NEED GRAPHICS HERE
c
      if (lpr.ne.0) call output
      call plotclose
c
c
      stop
c
 9000 format(5h ncyc,i4,3h t=,1pe9.2,4h dt=,e9.2,8h courant,1pe9.2,
     *9h lagrange,e9.2,
     *7h numit=,i2,
     1 7h grind=,0pf7.3,1x,a1)
 9010 format (1x,i4,1pe9.2,1e11.3,1x,3e8.1,
     .   2e10.3,3e8.1,e10.3,i4,i3,e8.1,4x,e8.1)
 9020 format (34h>>> error: time limit must exceed ,1pe9.2,
     .        11hseconds <<<)
c
      end



