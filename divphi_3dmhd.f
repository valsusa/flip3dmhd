      subroutine divphi_3dmhd(ncells,ijkcell,
     &     dudxc,dudyc,dudzc,
     &     dvdxc,dvdyc,dvdzc,
     &     dwdxc,dwdyc,dwdzc,vol,
     &     bxn,byn,bzn,bxl,byl,bzl,
     &     divuphix,divuphiy,divuphiz)
c
c      a routine to calculate (grad(u))dot(grad(phi))
c      to advance the particle magnetic moments
c
      dimension ijkcell(*),
     &     dudxc(*),dudyc(*),dudzc(*),
     &     dvdxc(*),dvdyc(*),dvdzc(*),
     &     dwdxc(*),dwdyc(*),dwdzc(*),vol(*),
     &     bxn(*),bxl(*),byn(*),byl(*),bzn(*),bzl(*),
     &     divuphix(*),divuphiy(*),divuphiz(*)
c
      do n=1,ncells
c
      ijk=ijkcell(n)
c
      gradphix=bxl(ijk)-bxn(ijk)
      gradphiy=byl(ijk)-byn(ijk)
      gradphiz=bzl(ijk)-bzn(ijk)
c
      divu=dudxc(ijk)+dvdyc(ijk)+dwdzc(ijk)
c
      divuphix(ijk)=(dudxc(ijk)*gradphix
     &     +(dvdxc(ijk)*gradphiy
     &     +(dwdxc(ijk)*gradphiz
     &     -(gradphix*divu))))
     &     *vol(ijk)
c
      divuphiy(ijk)=(dudyc(ijk)*gradphix
     &     +(dvdyc(ijk)*gradphiy
     &     +(dwdyc(ijk)*gradphiz
     &     -(gradphiy*divu))))
     &     *vol(ijk)
c
      divuphiz(ijk)=(dudzc(ijk)*gradphix
     &     +(dvdzc(ijk)*gradphiy
     &     +(dwdzc(ijk)*gradphiz
     &     -(gradphiz*divu))))
     &     *vol(ijk)
c
      enddo
c
      return
      end

