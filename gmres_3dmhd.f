      subroutine gmres_3dmhd(tol_GMRES,itsub,q,
     &     ijktmp1,ijktmp2,
     &     Jq,wk1,
     &     dro, dbx, dby, dbz,
     &     ncells,num_eq,ijkcell,iwid,jwid,kwid,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44)
c
c
      logical resist
c
      integer ncells,num_eq,itsub,
     &     ijktmp1(*),ijktmp2(*),
     &     ijkcell(*),iwid,jwid,kwid,
     &     nvtx,ijkvtx(*),ibar,jbar,kbar
c
c
      real*8 
     &          Jq(num_eq*ncells),q(num_eq*ncells,itsub),
     &          wk1(num_eq*ncells)
c
      real*8 dro(*),dbx(*),dby(*),dbz(*),
     &     a11(*),a12(*),a13(*),a14(*),
     &     a21(*),a22(*),a23(*),a24(*),
     &     a31(*),a32(*),a33(*),a34(*),
     &     a41(*),a42(*),a43(*),a44(*),
     &     d11(*),d12(*),d13(*),d14(*),
     &     d21(*),d22(*),d23(*),d24(*),
     &     d31(*),d32(*),d33(*),d34(*),
     &     d41(*),d42(*),d43(*),d44(*)

c
      real*8
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     r4pi,dt,vol(*),ul(*),vl(*),wl(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     gradx(*),grady(*),gradz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),p(*),csq(*),
     &     s(*),sbx(*),sby(*),sbz(*),
     &     ptil(*),
     &     rotil(*),bxtil(*),bytil(*),bztil(*),
     &     roq(*),bxq(*),byq(*),bzq(*),
     &     aroq(*),abxq(*),abyq(*),abzq(*),
     &     jx(*),jy(*),jz(*),vvol(*)
c
c
      real*8 tol_GMRES
c
c  ????????????????????????????????????????????????????????
c
c  status of cdlt,sdlt,straight,dz,delta, resistivity are unkown
c
c  ??????????????????????????????????????????????????????????
c
c	local variables
c
      DIMENSION g(21,2),s_local(21),h(21,21)
C
      real*8 rnorm
c
      integer numrstrt, iter, n, ii, iprecond
c
c   *************************************************************
c
       iprecond = 0
       numrstrt = 0
 1000  continue
c
c     ZERO WORKING ARRAYS
c
      do 1112 n=1,num_eq*ncells
      Jq(n)=0.0
 1112 continue
c
      do 1114 ii=1,itsub+1
      s_local(ii)=0.0
      g(ii,1)=0.0
      g(ii,2)=0.0
      do 1114 jj=1,itsub+1
         h(ii,jj)=0.0
 1114 continue
c
      do 1113 ii=1,itsub
      do 1113 n=1,num_eq*ncells
      q(n,ii)=0.0
 1113 continue
c
C     CALCULATE THE INITIAL RESIDUAL ERROR
c     for numrstrt = 0 this is just -(s,sbx,sby,sbz)
C
      if(numrstrt .eq. 0) then
c
      do n=1,ncells
         nn = (n-1)*num_eq +1
         q(nn,1) = -s(n) 
         q(nn+1,1) = -sbx(n) 
         q(nn+2,1) = -sby(n) 
         q(nn+3,1) = -sbz(n) 
         dro(n) = 0.0
         dbx(n) = 0.0
         dby(n) = 0.0
         dbz(n) = 0.0
      enddo
c
      else
c
c     input vector is (dro,dbx,dby,dbz)
c
      call J_times_kvec(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     dro,dbx,dby,dbz,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c     output vector is (aroq,abxq,abyq,abzq)
c
      do n=1,ncells
         nn = (n-1)*num_eq +1
         q(nn,1) = -s(n) - aroq(n)
         q(nn+1,1) = -sbx(n) - abxq(n)
         q(nn+2,1) = -sby(n) - abyq(n)
         q(nn+3,1) = -sbz(n) - abzq(n)
      enddo
c
      endif
c
      dot=0.0
c
      do 22 n=1,num_eq*ncells
      dot=dot+q(n,1)*q(n,1)
   22 continue
c
      dot=sqrt(dot)
      s_local(1)=dot
c
      do 25 n=1,num_eq*ncells
      q(n,1)=q(n,1)/dot
  25  continue
c
c     ****************************************************************
c
c     begin gmres
c
c     ****************************************************************
c
      do 1 iter=1,itsub
c     
c       write(*,*)'in gmres_MHD, iter =', iter
c     apply preconditioner
c
      do 32 n=1,ncells
       nn = (n-1)*num_eq + 1
      roq(n)=q(nn,iter)
      bxq(n)=q(nn+1,iter)
      byq(n)=q(nn+2,iter)
      bzq(n)=q(nn+3,iter)
  32  continue
c
c
      if(iprecond .eq. 1) then
      call precond(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq)
      elseif(iprecond .eq. 2) then
       call MPBJ_precond(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44,
     &     s,sbx,sby,sbz,
     &     wk1(1),wk1(ncells+1),wk1(2*ncells+1),
     &     wk1(3*ncells+1))
      endif
c
c    preconditioned krylov vector now in (aroq,abxq,abyq,abzq)
c    move it to (roq,bxq,byq,bzq)
c
      if(iprecond .gt. 0) then
      do n = 1,ncells
       roq(n) = aroq(n)
       bxq(n) = abxq(n)
       byq(n) = abyq(n)
       bzq(n) = abzq(n)
      enddo
      endif
c
c     compute J times preconditioned q 
c
      call J_times_kvec(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c   output vector is (aroq,abxq,abyq,abzq)
      do n=1,ncells
       nn = (n-1)*num_eq + 1
      Jq(nn)=aroq(n)
      Jq(nn+1)=abxq(n)
      Jq(nn+2)=abyq(n)
      Jq(nn+3)=abzq(n)
      enddo
c
c    orthogonalize:
c
      do 13 k=1,iter
      dot=0.0
      do 11 n=1,num_eq*ncells
      dot=dot+Jq(n)*q(n,k)
   11 continue
      h(k,iter)=dot
cdir$ ivdep
      do 12 n=1,num_eq*ncells
      Jq(n)=Jq(n)-dot*q(n,k)
   12 continue
   13 continue
c
c
      dot=0.0
      do 14 n=1,num_eq*ncells
      dot=dot+Jq(n)*Jq(n)
   14 continue
      dot=sqrt(dot)
c
      iterp1=iter+1
      h(iterp1,iter)=dot
c
c     apply previous Givens rotations to h (update QR factorization)
c
      do 16 k=1,iter-1
      ws=g(k,1)*h(k,iter)-g(k,2)*h(k+1,iter)
      h(k+1,iter)=g(k,2)*h(k,iter)+g(k,1)*h(k+1,iter)
      h(k,iter)=ws
  16  continue 
c     
c     compute next Givens rotation
      g1=h(iter,iter)
      g2=h(iterp1,iter)
      ws=sqrt(g1*g1+g2*g2)
      g1=g1/ws
      g2=-g2/ws
      g(iter,1)=g1
      g(iter,2)=g2
c
c     apply g to h
      h(iter,iter)=g1*h(iter,iter)-g2*h(iterp1,iter)
      h(iterp1,iter)=0.0
c
c     apply g to s
      ws=g1*s_local(iter)-g2*s_local(iterp1)
      s_local(iterp1)=g2*s_local(iter)+g1*s_local(iterp1)
      s_local(iter)=ws
c
c     |s_local(iter+1)| is the norm of the current residual
c     check for convergence
      rnorm=abs(s_local(iterp1))
c      write(*,*) 'tol_GMRES,it,resGMRES=',tol_GMRES,
c     &           iter + numrstrt*itsub, rnorm
c
      if((rnorm.le.tol_GMRES).or.(iter.eq.itsub)) go to 2
c
c     normalize next q
c
      do 15 n=1,num_eq*ncells
      q(n,iterp1)=Jq(n)/dot
  15  continue
c
    1 continue
c
    2 continue
c    
c    update the solution
c    solve h*y=sbar  (sbar contains elements 1,..,iter of s)
c    store y in s
      s_local(iter)=s_local(iter)/h(iter,iter)
      do ii=iter-1,1,-1
        ws=0.0
          do jj=ii+1,iter 
          ws=ws+h(ii,jj)*s_local(jj)
          enddo
        s_local(ii)=(s_local(ii)-ws)/h(ii,ii)
      end do
c
c    compute new preconditioned linear update vector
c
      do n=1,num_eq*ncells
      wk1(n)=0.
      enddo
      do 18 ii=1,iter
cdir$ ivdep
      do 18 n=1,num_eq*ncells
      wk1(n)=wk1(n)+s_local(ii)*q(n,ii)
  18  continue
c
c   preconditioned linear iteration update vector is in wk1
c   move it to (roq,bxq,byq,bzq)
c
      do n = 1,ncells
       nn = (n-1)*num_eq + 1
       roq(n) = wk1(nn)
       bxq(n) = wk1(nn+1)
       byq(n) = wk1(nn+2)
       bzq(n) = wk1(nn+3)
      enddo
c
c  unprecondition linear iteration update vector
c  and put it in (aroq,abxq,abyq,abzq)
c
      if(iprecond .eq. 1)then
      call precond(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq)
      elseif(iprecond .eq. 2)then
      call MPBJ_precond(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     d11,d12,d13,d14,
     &     d21,d22,d23,d24,
     &     d31,d32,d33,d34,
     &     d41,d42,d43,d44,
     &     s,sbx,sby,sbz,
     &     wk1(1),wk1(ncells+1),wk1(2*ncells+1),
     &     wk1(3*ncells+1))
      endif
c
C   add linear iteration update vector to initial guess
c   note: inital guess is zero if numrstrt = 0
c
      if(iprecond .gt. 0) then
      do n = 1,ncells
       dro(n) = dro(n) + aroq(n)
       dbx(n) = dbx(n) + abxq(n)
       dby(n) = dby(n) + abyq(n)
       dbz(n) = dbz(n) + abzq(n)
      enddo
      else
      do n = 1,ncells
       dro(n) = dro(n) + roq(n)
       dbx(n) = dbx(n) + bxq(n)
       dby(n) = dby(n) + byq(n)
       dbz(n) = dbz(n) + bzq(n)
      enddo
      endif
c
      if(rnorm .gt. tol_GMRES .and. numrstrt .lt. 4) then
       numrstrt = numrstrt + 1
       goto 1000
      endif
c
c     input vector is (dro,dbx,dby,dbz)
c
      call J_times_kvec(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     dro,dbx,dby,dbz,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c     output vector is (aroq,abxq,abyq,abzq)
c
      do n=1,ncells
         nn = (n-1)*num_eq +1
         q(nn,1) = -s(n) - aroq(n)
         q(nn+1,1) = -sbx(n) - abxq(n)
         q(nn+2,1) = -sby(n) - abyq(n)
         q(nn+3,1) = -sbz(n) - abzq(n)
      enddo
c
c
      dot=0.0
c
      do 222 n=1,num_eq*ncells
      dot=dot+q(n,1)*q(n,1)
  222 continue
c
      dot=sqrt(dot)


      return
      end
c




