      subroutine bc_current(nxp,nyp,nzp,
     &     cdlt,sdlt,strait,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     x,y,z)
c
c     a routine to impose double periodicity
c     for toroidal geometry
c     for vertex vector, assembles final result
c
c	  NOTE:
c	  conducting wall: set current to zero to avoid currents there that would result in infinite fields
c	  open bc: the current should be left alone at its value, do nothing to it
c
c     called by RESISTIVE_DIFF
c
      use boundary_module
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      dimension x(nxp,nyp,*),y(nxp,nyp,*),z(nxp,nyp,*)
c
      do 10 k=2,nzp
c
c     periodicity in the toroidal angle
c
      if(periodic_y) then
c
      do 1 i=2,nxp
c
      x(i,2,k)=cdlt*x(i,nyp,k)+sdlt*y(i,nyp,k)+x(i,2,k)
      y(i,2,k)=-sdlt*x(i,nyp,k)+cdlt*y(i,nyp,k)+y(i,2,k)
     &     -strait*dz
      z(i,2,k)=z(i,nyp,k)+z(i,2,k)
c
      x(i,nyp,k)=cdlt*x(i,2,k)-sdlt*y(i,2,k)
      y(i,nyp,k)=sdlt*x(i,2,k)+cdlt*y(i,2,k)
     &     +strait*dz
      z(i,nyp,k)=z(i,2,k)
c
    1 continue
c
      else
c
      do i=2,nxp
c
      x(i,2,k)=0.
      y(i,2,k)=0.
      z(i,2,k)=0.
c
      x(i,nyp,k)=0.
      y(i,nyp,k)=0.
      z(i,nyp,k)=0.
c
      enddo
c
      endif
c
c     periodicity in the poloidal angle
c
      if(periodic_x) then
c
      do 2 j=2,nyp
c
      x(2,j,k)=x(nxp,j,k)+x(2,j,k)
      y(2,j,k)=y(nxp,j,k)+y(2,j,k)
      z(2,j,k)=z(nxp,j,k)+z(2,j,k)
c
      x(nxp,j,k)=x(2,j,k)
      y(nxp,j,k)=y(2,j,k)
      z(nxp,j,k)=z(2,j,k)
c
    2 continue
c
      else
c
c	added by GLAP
c
      do j=2,nyp
c
      x(2,j,k)=0.0
      y(2,j,k)=0.0
      z(2,j,k)=0.0
c
      x(nxp,j,k)=0.0
      y(nxp,j,k)=0.0
      z(nxp,j,k)=0.0
c
      enddo
c
      endif
c
   10 continue
c
c     in z
c
c
      if(periodic_z) then
c
      do i=2,nxp
      do j=2,nyp
c
      x(i,j,2)=x(i,j,nzp)+x(i,j,2)
      y(i,j,2)=y(i,j,nzp)+y(i,j,2)
      z(i,j,2)=z(i,j,nzp)+z(i,j,2)
c
      x(i,j,nzp)=x(i,j,2)
      y(i,j,nzp)=y(i,j,2)
      z(i,j,nzp)=z(i,j,2)
c
      enddo
      enddo
c
      else
c

      do i=2,nxp
      do j=2,nyp
c
c      x(i,j,2)=x(i,j,2)+x(i,j,1)
c      y(i,j,2)=y(i,j,2)+y(i,j,1)
c      z(i,j,2)=z(i,j,2)+z(i,j,1)
c
      if(boundary_bottom.ne.4)then
      x(i,j,2)=0.0
      y(i,j,2)=0.0
      z(i,j,2)=0.0
      end if
c
      if(boundary_top.ne.4)then
      x(i,j,nzp)=0.0
      y(i,j,nzp)=0.0
      z(i,j,nzp)=0.0
      end if
c
      enddo
      enddo
c
      end if
c
      return
      end
