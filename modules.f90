module boundary_module

! convention bottom
!
!  1 - line tying
!  2 - assigned movement
!  3 - periodic
!  4 - open boundary

! comments
! Top boundary: case 2 not implemented

logical :: circular_wall
integer :: boundary_bottom, boundary_top
real(8) :: ascale
integer :: grid_type_x,grid_type_z

real(8) :: tanthetax,tanthetay,erre0

end module boundary_module
