      subroutine gmres_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,PRECON,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     itsub,iter,error,rnorm,DT,CNTR,srce,
     &     div,residu,Aq,Ax,rhs,q,bnorm,
     &     gradpx,gradpy,gradpz,p,diag)
c
c
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          p(*),
     &          Aq(*),Ax(*),rhs(*),q(itdim,20),
     &          vol(*),div(*),residu(*),srce(*),
     &          gradpx(*),gradpy(*),gradpz(*),diag(*)
c
       dimension g(21,2),s(21),h(21,21)
C
      LOGICAL PRECON
c     
c     solving system A.p=srce
C
c
c     ZERO WORKING ARRAYS, note ensure that **all** of the array is zeroed
c     (use ncells and ijkcell to zero rather than nvtx and ijkvtx); I found that
c     if these arrays are not zeroed completely, non-zero values from the 
c     previous time step will be carried through the calculation and will affect
c     the gradient and divergence calculations. I would assume that correct 
c     implementation of the boundary conditions would prevent any previous
c     non-zero values from leaking thru to the current calculation. Maybe the
c     routine bc_scalar should be checked more closely..

cSJC      do 1112 n=1,nvtx
c      ijk=ijkvtx(n)
c      Aq(ijk)=0.0
c 1112 continue

      do 1112 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=0.0
 1112 continue
c
      do 1114 ii=1,itsub+1
      s(ii)=0.0
      g(ii,1)=0.0
      g(ii,2)=0.0
      do 1114 jj=1,itsub+1
         h(ii,jj)=0.0
 1114 continue
c
      do 1113 ii=1,itsub
cSJC      do 1113 n=1,nvtx
c      ijk=ijkvtx(n)
c      q(ijk,ii)=0.0
         do 1113 n=1,ncells
            ijk=ijkcell(n)
            q(ijk,ii)=0.0
 1113 continue
c
c
C     CALCULATE THE INITIAL RESIDUAL ERROR, residu=srce-A.p_0 where p_0 is the 
c     initial pressure
C
c
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     p,gradpx,gradpy,gradpz,residu)
c
c
      dot=0.0d0
c
      do 22 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+residu(ijk)*residu(ijk)
   22 continue
c
c
      dot=sqrt(dot)
      s(1)=dot
c
c      write(*,*) 'in gmres, s(1) (L_2(r_0)) =',s(1)
c
c     first Krylov vector..q_1 = (srce-A.p_0)/||srce-A.p_0||

      if(dot.gt.0.0d0) then
      do 25 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,1)=residu(ijk)/dot
  25  continue

      else
c
      do n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,1)=0.0d0
      enddo
c
      endif
c
c     ****************************************************************
c
c     begin gmres
c
c     ****************************************************************
c
      do 1 iter=1,itsub
c     
c     apply preconditioner, i.e find y=M^(-1).q
c
      itest = 0
c
      if(itest .eq. 0)then
      do 32 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=q(ijk,iter)/diag(ijk)
  32  continue
c
      else
c
c   new preconditioner
c
c   put q into rhs
c
      do 332 n=1,nvtx
      ijk=ijkvtx(n)
      rhs(ijk)=q(ijk,iter)
  332 continue
c
      call mfmpj(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     DT,CNTR,srce,
     &     div,residu,Aq,Ax,rhs,
     &     gradpx,gradpy,gradpz,p,diag)
c
      endif


c
      zero=0.0d0
c
c     compute A times preconditioned q, ie find A(M^(-1)q)
c
cdir$ ivdep
      do 33 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=p(ijk)+Aq(ijk)
   33 continue
c
C
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     Aq,gradpx,gradpy,gradpz,Aq)
c
c
cdir$ ivdep
      do 35 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=(residu(ijk)-Aq(ijk))
   35 continue

c
c     have found vector A(M^(-1).q) now orthogonalize this with previous
c     Krylov vectors
c
      do 13 k=1,iter
      dot=0.0d0
      do 11 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+Aq(ijk)*q(ijk,k)
   11 continue
      h(k,iter)=dot
cdir$ ivdep
      do 12 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=Aq(ijk)-dot*q(ijk,k)
   12 continue
   13 continue
c
c
      dot=0.0d0
      do 14 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+Aq(ijk)*Aq(ijk)
   14 continue
      dot=sqrt(dot)
c
      iterp1=iter+1
      h(iterp1,iter)=dot
c
c     apply previous Givens rotations to h (update QR factorization)
c
      do 16 k=1,iter-1
      ws=g(k,1)*h(k,iter)-g(k,2)*h(k+1,iter)
      h(k+1,iter)=g(k,2)*h(k,iter)+g(k,1)*h(k+1,iter)
      h(k,iter)=ws
  16  continue 
c     
c     compute next Givens rotation
      g1=h(iter,iter)
      g2=h(iterp1,iter)
      ws=sqrt(g1*g1+g2*g2)+1.d-50
      g1=g1/ws
      g2=-g2/ws
      g(iter,1)=g1
      g(iter,2)=g2
c
c     apply g to h
      h(iter,iter)=g1*h(iter,iter)-g2*h(iterp1,iter)
      h(iterp1,iter)=0.0
c
c     apply g to s
      ws=g1*s(iter)-g2*s(iterp1)
      s(iterp1)=g2*s(iter)+g1*s(iterp1)
      s(iter)=ws

cSJC  |s(iter+1)| is the norm of the current residual, but these
c     norms are based on the solution of the system Abar.y = srce 
c     where Abar = A(M^(-1))
c
c     |s(iter+1)| is the norm of the current residual
c     check for convergence
      rnorm=abs(s(iterp1))
c      write(*,*) 'in gmres, iter, rnorm) =',iter,rnorm
      if((rnorm.le.error*bnorm).or.(iter.eq.itsub)) go to 2
c
c     normalize next Krylov vector q
c
      do 15 n=1,nvtx
      ijk=ijkvtx(n)
      q(ijk,iterp1)=Aq(ijk)/dot
  15  continue
c
    1 continue
c
    2 continue
c    
c    update the solution
c    solve h*y=sbar  (sbar contains elements 1,..,iter of s)
c    store y in s
      s(iter)=s(iter)/(h(iter,iter)+1.d-50)
      do ii=iter-1,1,-1
        ws=0.0
          do jj=ii+1,iter 
          ws=ws+h(ii,jj)*s(jj)
          enddo
        s(ii)=(s(ii)-ws)/(h(ii,ii)+1.d-50)
      end do

      if(itest .eq. 0)then
c     the solution to be found is p = p_0 + p_k, where p_0 is the initial
c     pressure (and the initial Krylov vector q_1 = (srce-A.p_0)/||srce-A.p_0||); 
c     we are about to solve for y_k=M.p_k = s_1.q_1 + ....s_iter.q_iter,
c     for consistency we must 1st apply the pre-conditioner matrix to our
c     initial guess p_0, we'll therefore find y = M.(p_0 + p_k)
      do 17 n=1,nvtx
      ijk=ijkvtx(n)
      p(ijk)=diag(ijk)*p(ijk)
 17   continue

c     
      do 18 ii=1,iter
cdir$ ivdep
      do 18 n=1,nvtx
      ijk=ijkvtx(n)
      p(ijk)=p(ijk)+s(ii)*q(ijk,ii)
  18  continue
c

c     we now have solved for y=M.p = M.(p_0 + p_k), now find p=M^(-1).y 
      do 19 n=1,nvtx
      ijk=ijkvtx(n)
      p(ijk)=p(ijk)/diag(ijk)
  19  continue
c
      else
c
c     for a more expensive preconditioner I only want to call 
c     it once
c
c     put preconditioned update in rhs
c
      do 117 n=1,nvtx
      ijk=ijkvtx(n)
      rhs(ijk)= 0.0
 117  continue

      do 118 ii=1,iter
      do 118 n=1,nvtx
      ijk=ijkvtx(n)
      rhs(ijk)=rhs(ijk)+s(ii)*q(ijk,ii)
  118 continue
c
c     unprecondition into Aq
c
      call mfmpj(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     DT,CNTR,srce,
     &     div,residu,Aq,Ax,rhs,
     &     gradpx,gradpy,gradpz,p,diag)
c
c    add to initial quess
c
      do 119 n=1,nvtx
      ijk=ijkvtx(n)
      p(ijk)= p(ijk) + Aq(ijk)
 119  continue
c
      endif
c
c  compute norm of actual residual at end of cycle, ||srce-A.p||
c
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     p,gradpx,gradpy,gradpz,residu)
c
c
      dot=0.0
c
      do 222 n=1,nvtx
      ijk=ijkvtx(n)
      dot=dot+residu(ijk)*residu(ijk)
  222 continue
      dot=dsqrt(dot)
c

      return
      end
c
c
