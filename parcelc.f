      subroutine parcelc(ncells,ijkcell,ijkctmp,iwid,jwid,kwid,
     &      periodic_x,periodic_y,periodic_z,
     &      itdim,npart,ibp2,jbp2,kbp2,mc,volpc,sie1p,wate,
     &      ep,mass,volp,
     &      mupx,mupy,mupz,bxn,byn,bzn,numparc,
     &      iphead,link,iphd2,pxi,peta,pzta)
c
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      real*8 volpc,mc,mass,mupx,mupy,mupz,numparc
c
      dimension mc(*),volpc(*),sie1p(*),wate(itdim,*),
     &          bxn(*),byn(*),bzn(*),numparc(*),
     &          iphead(*),link(0:npart),iphd2(*),ijkcell(*),ijkctmp(*),
     &          ep(0:npart),mass(0:npart),volp(0:npart),
     &          pxi(0:npart),peta(0:npart),pzta(0:npart),
     &          mupx(0:npart),mupy(0:npart),mupz(0:npart)
      dimension ijkstep(27)
c
c      a routine to interpolate particle data to the grid
c
      ibp1=ibp2-1
      jbp1=jbp2-1
      kbp1=kbp2-1
c
      ibar=ibp1-1
      jbar=jbp1-1
      kbar=kbp1-1
c
      call celstep(iwid,jwid,kwid,ijkstep)
c
      do 10 k=1,kbp2
      do 10 j=1,jbp2
cdir$ ivdep
      do 10 i=1,ibp2
      ijk=(i-1)*iwid+(j-1)*jwid+(k-1)*kwid+1
      mc(ijk)=0.0
      numparc(ijk)=0.0
      volpc(ijk)=0.0
      sie1p(ijk)=0.0
      bxn(ijk)=0.0
      byn(ijk)=0.0
      bzn(ijk)=0.0
  10  continue
c
      do 11 l=1,27
cdir$ ivdep
      do 11 n=1,ncells
      wate(ijkcell(n),l)=0.0
  11  continue
c
cdakcdir$ ivdep
c
      ntmp=0
      do 100 n=1,ncells
      ijk=ijkcell(n)
      np=iphead(ijk)
      if(np.ne.0) then
      ntmp=ntmp+1
      ijkctmp(ntmp)=ijk
      iphd2(ijk)=np
      endif
  100 continue
c
c
c
    1 continue
c
      if(ntmp.eq.0) go to 1000
c
      call watec(ntmp,ijkctmp,iphd2,itdim,
     &                 pxi,peta,pzta,wate)
c
c     calculate the mass
        do 300 ll=1,27
cdir$ ivdep
      do 3001 n=1,ntmp
      ijk=ijkctmp(n)
      index=ijk+ijkstep(ll)
      np=iphd2(ijk)
      mc(index)=mc(index)+mass(np)*wate(ijk,ll)
      numparc(index)=numparc(index)+wate(ijk,ll)
      volpc(index)=volpc(index)+volp(np)*wate(ijk,ll)
      bxn(index)=bxn(index)+mupx(np)*wate(ijk,ll)
      byn(index)=byn(index)+mupy(np)*wate(ijk,ll)
      bzn(index)=bzn(index)+mupz(np)*wate(ijk,ll)
      sie1p(index)=sie1p(index)+ep(np)*wate(ijk,ll)
 3001 continue
  300 continue
c
      ntmp_nxt=0
      do 500 n=1,ntmp
      ijk=ijkctmp(n)
      np=link(iphd2(ijk))
      if(np.gt.0) then
      ntmp_nxt=ntmp_nxt+1
      iphd2(ijk)=np
      ijkctmp(ntmp_nxt)=ijk
      endif
  500 continue
c
      ntmp=ntmp_nxt
      go to 1
c
c
 1000 continue
c
c     impose boundary conditions on particle data
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     mc)
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     numparc)
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     volpc)
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     bxn)
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     byn)
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     bzn)
c
      call bc_particles(ibar,jbar,kbar,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     sie1p)
c
      return
      end
