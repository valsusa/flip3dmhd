*dk wates
      subroutine watec(ncells,ijkcell,iphead,itdim,
     &                 pxi,peta,pzta,wate)
c
c
c     a routine to calculate the weights for triquadratic
c     interpolation
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),iphead(*),pxi(0:*),peta(0:*),pzta(0:*),
     &     wate(itdim,*)
      real*8 nu
c
      do 1 n=1,ncells
c
      ijk=ijkcell(n)
c
      np=iphead(ijk)
      i=int(pxi(np))
      j=int(peta(np))
      k=int(pzta(np))
c
      the=pxi(np)-real(i)-0.5
      zeta=peta(np)-real(j)-0.5
      nu=pzta(np)-real(k)-0.5
c
      wi=0.75-the**2
      wim=0.5*(0.5-the)**2
      wip=0.5*(0.5+the)**2
c
      wj=0.75-zeta**2
      wjm=0.5*(0.5-zeta)**2
      wjp=0.5*(0.5+zeta)**2
c
      wk=0.75-nu**2
      wkm=0.5*(0.5-nu)**2
      wkp=0.5*(0.5+nu)**2
c
c     k-plane
c
      wate(ijk,1)=wi*wj*wk
      wate(ijk,2)=wip*wj*wk
      wate(ijk,3)=wip*wjp*wk
      wate(ijk,4)=wi*wjp*wk
      wate(ijk,5)=wim*wjp*wk
      wate(ijk,6)=wim*wj*wk
      wate(ijk,7)=wim*wjm*wk
      wate(ijk,8)=wi*wjm*wk
      wate(ijk,9)=wip*wjm*wk
c
c     k-1 - plane
c
      wate(ijk,10)=wi*wj*wkm
      wate(ijk,11)=wip*wj*wkm
      wate(ijk,12)=wip*wjp*wkm
      wate(ijk,13)=wi*wjp*wkm
      wate(ijk,14)=wim*wjp*wkm
      wate(ijk,15)=wim*wj*wkm
      wate(ijk,16)=wim*wjm*wkm
      wate(ijk,17)=wi*wjm*wkm
      wate(ijk,18)=wip*wjm*wkm
c
c     k+1 - plane
c
      wate(ijk,19)=wi*wj*wkp
      wate(ijk,20)=wip*wj*wkp
      wate(ijk,21)=wip*wjp*wkp
      wate(ijk,22)=wi*wjp*wkp
      wate(ijk,23)=wim*wjp*wkp
      wate(ijk,24)=wim*wj*wkp
      wate(ijk,25)=wim*wjm*wkp
      wate(ijk,26)=wi*wjm*wkp
      wate(ijk,27)=wip*wjm*wkp
c
    1 continue
c
      return
      end
