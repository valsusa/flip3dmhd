      subroutine diagonal(ncells,ijkcell,NSTART,
     &     iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     divu,dt,coef,vvol,vol,diag)
c
c     a routine to calculate the diagonal elements of the
c     matrix representing the Laplacian
c
      implicit real*8 (a-h,o-z)
      dimension ijkcell(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vvol(*),vol(*),diag(*),divu(*),coef(*)
c
cdir$ ivdep
      do 1 n=NSTART,NCELLS
c
      ijk=ijkcell(n)
c
      diag(ijk)=
     &    +((c1x(ijk)**2+c1y(ijk)**2+c1z(ijk)**2)/vvol(ijk+iwid)
     &    +(c2x(ijk)**2+c2y(ijk)**2+c2z(ijk)**2)/vvol(ijk+iwid+jwid)
     &    +(c3x(ijk)**2+c3y(ijk)**2+c3z(ijk)**2)/vvol(ijk+jwid)
     &    +(c4x(ijk)**2+c4y(ijk)**2+c4z(ijk)**2)/vvol(ijk)
     &    +(c5x(ijk)**2+c5y(ijk)**2+c5z(ijk)**2)/vvol(ijk+iwid+kwid)
     &   +(c6x(ijk)**2+c6y(ijk)**2+c6z(ijk)**2)/vvol(ijk+iwid+jwid+kwid)
     &    +(c7x(ijk)**2+c7y(ijk)**2+c7z(ijk)**2)/vvol(ijk+jwid+kwid)
     &    +(c8x(ijk)**2+c8y(ijk)**2+c8z(ijk)**2)/vvol(ijk+kwid))
     &    +vol(ijk)/coef(ijk)
c
    1 continue
c
cdir$ ivdep
      do 2 n=1,NSTART-1
c
      ijk=ijkcell(n)
c
      diag(ijk)=
     &    +((c1x(ijk)**2+c1y(ijk)**2+c1z(ijk)**2)/vvol(ijk+iwid)
     &    +(c2x(ijk)**2+c2y(ijk)**2+c2z(ijk)**2)/vvol(ijk+iwid+jwid)
     &    +(c3x(ijk)**2+c3y(ijk)**2+c3z(ijk)**2)/vvol(ijk+jwid)
     &    +(c4x(ijk)**2+c4y(ijk)**2+c4z(ijk)**2)/vvol(ijk)
     &    +(c5x(ijk)**2+c5y(ijk)**2+c5z(ijk)**2)/vvol(ijk+iwid+kwid)
     &   +(c6x(ijk)**2+c6y(ijk)**2+c6z(ijk)**2)/vvol(ijk+iwid+jwid+kwid)
     &    +(c7x(ijk)**2+c7y(ijk)**2+c7z(ijk)**2)/vvol(ijk+jwid+kwid)
     &    +(c8x(ijk)**2+c8y(ijk)**2+c8z(ijk)**2)/vvol(ijk+kwid))
     &    +vol(ijk)/coef(ijk)
c
    2 continue
c
      return
       end
c
      subroutine poisson(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     ITDIM,iwid,jwid,kwid,PRECON,gm1,mv,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     itsub,itmax,error,rnorm,DT,CNTR,srce,rho,divu,csq,
     &     coef,div,residu,Aq,q,bnorm,
     &     gradpx,gradpy,gradpz,p,diag)
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          p(*),vvol(*),vvolaxis(*),
     &          Aq(*),q(itdim,20),
     &          srce(*),rho(*),divu(*),csq(*),coef(*),
     &          vol(*),div(*),residu(*),
     &          gradpx(*),gradpy(*),gradpz(*),diag(*)
c
      real*8 mv(*)
      LOGICAL PRECON
C
C
      nstart=(ibp1-1)*(jbp1-1)+1
      DTSQ=cntr*DT**2
      dtcntr=dt*cntr
c
c     set source term
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,p)
c
      do 1115 n=1,ncells
      ijk=ijkcell(n)
      coef(ijk)=csq(ijk)*rho(ijk)*dtsq+1.e-10
 1115 continue
c
      do 1116 n=1,ncells
      ijk=ijkcell(n)
      srce(ijk)=p(ijk)/coef(ijk)-divu(ijk)/(dtcntr)
 1116 continue
c
      do  n=1,ncells
      ijk=ijkcell(n)
      diag(ijk)=0.0
      enddo
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,diag)
c
      call residue(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     p,gradpx,gradpy,gradpz,vvol,residu)
c
c
      bnorm=0.0
      do n=1,ncells
      ijk=ijkcell(n)
      bnorm=bnorm+abs(residu(ijk))
      enddo
c
      if(bnorm.lt.error) then
      bnorm=1.0
      end if

c     set preconditioner
c
      if(precon) then
c
      call diagonal(ncells,ijkcell,NSTART,
     &     iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     divu,dt,coef,mv,vol,diag)
c
      else
c
      DO 1111 N=1,NCELLS
      DIAG(IJKCELL(N))=1.
 1111 CONTINUE
C
      endif
c
      do it=1,itmax
      call gmres(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     ITDIM,iwid,jwid,kwid,PRECON,gm1,mv,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     itsub,iter,error,rnorm,DT,CNTR,srce,rho,divu,csq,
     &     coef,div,residu,Aq,q,bnorm,
     &     gradpx,gradpy,gradpz,p,diag)
c
       if(iter.lt.itsub) go to 98
       end do
c
       write(0,*) 'gmres fails', (it-1)*itsub+iter, rnorm
       go to 99
  98   continue
       write(0,*) 'gmres converges ', (it-1)*itsub+iter, rnorm
  99   continue
c
      return
      end
c
      subroutine gmres(ncells,ijkcell,nvtxkm,nvtx,ijkvtx,
     &     ITDIM,iwid,jwid,kwid,PRECON,gm1,mv,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,vvol,vvolaxis,
     &     itsub,iter,error,rnorm,DT,CNTR,srce,rho,divu,csq,
     &     coef,div,residu,Aq,q,bnorm,
     &     gradpx,gradpy,gradpz,p,diag)
c
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          p(*),vvol(*),vvolaxis(*),
     &          Aq(*),q(itdim,20),
     &          srce(*),rho(*),divu(*),csq(*),coef(*),
     &          vol(*),div(*),residu(*),
     &          gradpx(*),gradpy(*),gradpz(*),diag(*)
c
       real*8 mv(*)
       dimension g(21,2),s(21),h(21,21)
C
      LOGICAL PRECON
C
C
      nstart=(ibp1-1)*(jbp1-1)+1
      DTSQ=cntr*DT**2
      dtcntr=dt*cntr
c
c
c     ZERO WORKING ARRAYS
c
      do 1112 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=0.0
 1112 continue
c
      do 1114 ii=1,itsub+1
      s(ii)=0.0
      g(ii,1)=0.0
      g(ii,2)=0.0
      do 1114 jj=1,itsub+1
         h(ii,jj)=0.0
 1114 continue
c
      do 1113 ii=1,itsub
      do 1113 n=1,ncells
      ijk=ijkcell(n)
      q(ijk,ii)=0.0
 1113 continue
c
c
C     CALCULATE THE INITIAL RESIDUAL ERROR
C
      call residue(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     p,gradpx,gradpy,gradpz,vvol,residu)
c
c
      dot=0.0
c
      do 22 n=1,ncells
      ijk=ijkcell(n)
      dot=dot+residu(ijk)*residu(ijk)
   22 continue
c
      dot=sqrt(dot)
      s(1)=dot
c
      do 25 n=1,ncells
      ijk=ijkcell(n)
      q(ijk,1)=residu(ijk)/dot
  25  continue
c
c     ****************************************************************
c
c     begin gmres
c
c     ****************************************************************
c
      do 1 iter=1,itsub
c     
c     apply preconditioner
c
      do 32 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=-q(ijk,iter)/diag(ijk)
  32  continue
c
c     compute A times preconditioned q 
c
cdir$ ivdep
      do 33 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=p(ijk)+Aq(ijk)
   33 continue
c
      call bcphi(ibp1,jbp1,kbp1,iwid,jwid,kwid,Aq)
c
      call residue(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     Aq,gradpx,gradpy,gradpz,vvol,Aq)
c
c
cdir$ ivdep
      do 35 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=(residu(ijk)-Aq(ijk))
   35 continue
c
c
c    orthogonalize:
c
      do 13 k=1,iter
      dot=0.0
      do 11 n=1,ncells
      ijk=ijkcell(n)
      dot=dot+Aq(ijk)*q(ijk,k)
   11 continue
      h(k,iter)=dot
cdir$ ivdep
      do 12 n=1,ncells
      ijk=ijkcell(n)
      Aq(ijk)=Aq(ijk)-dot*q(ijk,k)
   12 continue
   13 continue
c
c
      dot=0.0
      do 14 n=1,ncells
      ijk=ijkcell(n)
      dot=dot+Aq(ijk)*Aq(ijk)
   14 continue
      dot=sqrt(dot)
c
      iterp1=iter+1
      h(iterp1,iter)=dot
c
c     apply previous Givens rotations to h (update QR factorization)
c
      do 16 k=1,iter-1
      ws=g(k,1)*h(k,iter)-g(k,2)*h(k+1,iter)
      h(k+1,iter)=g(k,2)*h(k,iter)+g(k,1)*h(k+1,iter)
      h(k,iter)=ws
  16  continue 
c     
c     compute next Givens rotation
      g1=h(iter,iter)
      g2=h(iterp1,iter)
      ws=sqrt(g1*g1+g2*g2)
      g1=g1/ws
      g2=-g2/ws
      g(iter,1)=g1
      g(iter,2)=g2
c
c     apply g to h
      h(iter,iter)=g1*h(iter,iter)-g2*h(iterp1,iter)
      h(iterp1,iter)=0.0
c
c     apply g to s
      ws=g1*s(iter)-g2*s(iterp1)
      s(iterp1)=g2*s(iter)+g1*s(iterp1)
      s(iter)=ws
c
c     |s(iter+1)| is the norm of the current residual
c     check for convergence
      rnorm=abs(s(iterp1))
      if((rnorm.le.error*bnorm).or.(iter.eq.itsub)) go to 2
c
c     normalize next q
c
      do 15 n=1,ncells
      ijk=ijkcell(n)
      q(ijk,iterp1)=Aq(ijk)/dot
  15  continue
c
    1 continue
c
    2 continue
c    
c    update the solution
c    solve h*y=sbar  (sbar contains elements 1,..,iter of s)
c    store y in s
      s(iter)=s(iter)/h(iter,iter)
      do ii=iter-1,1,-1
        ws=0.0
          do jj=ii+1,iter 
          ws=ws+h(ii,jj)*s(jj)
          enddo
        s(ii)=(s(ii)-ws)/h(ii,ii)
      end do
c
c    compute new p
      do 17 n=1,ncells
      ijk=ijkcell(n)
      p(ijk)=-diag(ijk)*p(ijk)
  17  continue
c
      do 18 ii=1,iter
cdir$ ivdep
      do 18 n=1,ncells
      ijk=ijkcell(n)
      p(ijk)=p(ijk)+s(ii)*q(ijk,ii)
  18  continue
c
      do 19 n=1,ncells
      ijk=ijkcell(n)
      p(ijk)=-p(ijk)/diag(ijk)
  19  continue
c
      return
      end
c
c
      subroutine residue(ncells,ijkcell,nvtx,nvtxkm,ijkvtx,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     srce,coef,mv,gm1,div,divu,
     &     p,gradpx,gradpy,gradpz,vvol,residu)
c
c     a routine to calculate the residual error in the solution
c     of poisson's equation
c
      implicit real*8 (a-h,o-z)
      real*8 mv
c
      dimension ijkcell(*),ijkvtx(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),vvol(*),p(*),div(*),divu(*),
     &     srce(*),coef(*),mv(*),
     &     gradpx(*),gradpy(*),gradpz(*),residu(*)
c
c     calculate residue
c
      call gradf(nvtx,ijkvtx,iwid,jwid,kwid,
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &                   p,gradpx,gradpy,gradpz)
c
      call axisgrad(ibp1,jbp1,iwid,jwid,kwid,
     &     gradpx,gradpy,gradpz)
c
      do 16 n=1,nvtxkm
      ijk=ijkvtx(n)
      rmv=1./mv(ijk)
      gradpx(ijk)=-gradpx(ijk)*rmv
      gradpy(ijk)=-gradpy(ijk)*rmv
      gradpz(ijk)=-gradpz(ijk)*rmv
   16 continue
      factor=0.5*(1.+1./real(2*kbp1))
      do 17 n=nvtxkm+1,nvtx
      ijk=ijkvtx(n)
      rmv=factor/mv(ijk)
      gradpx(ijk)=-gradpx(ijk)*rmv
      gradpy(ijk)=-gradpy(ijk)*rmv
      gradpz(ijk)=-gradpz(ijk)*rmv
   17 continue
c
C
c     calculate div(grad(p)/rho)
c
      call divc(ncells,ijkcell,iwid,jwid,kwid,
     &                   c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &                   c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &                   c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &                   GRADPX,GRADPY,GRADPZ,div)
c
c
      do 15 n=1,ncells
      ijk=ijkcell(n)
      residu(ijk)=(srce(ijk)-p(ijk)/coef(ijk)-div(ijk))*vol(ijk)
   15 continue
c
      return
      end
