     subroutine parlocat_box(ncellsp,ijkctmp,iphead,iwid,jwid,kwid,
     &     IJKTMP2,IJKTMP3,IJKTMP4,RMAJ,DZ,ALFA,AX,AY,AZ,DT,
     &     itdim,npart,ibar,jbar,kbar,mgeom,cdlt,sdlt,
     &     wate,x,y,z,BXV,BYV,BZV,
     &     XPTILDE,YPTILDE,ZPTILDE,UPTILDE,VPTILDE,WPTILDE,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c     ********************************************************
c
c     a routine to locate a particle on a grid
c
c     *******************************************************
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkctmp(*),iphead(*),wate(itdim,*),
     &     IJKTMP2(*),IJKTMP3(*),IJKTMP4(*),ALFA(*),
     &     x(*),y(*),z(*),BXV(*),BYV(*),BZV(*),
     &     XPTILDE(*),YPTILDE(*),ZPTILDE(*),
     &     UPTILDE(*),VPTILDE(*),WPTILDE(*),
     &     AX(*),AY(*),AZ(*),
     &     tsix(*),tsiy(*),tsiz(*),
     &     etax(*),etay(*),etaz(*),
     &     nux(*),nuy(*),nuz(*),
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     up(0:npart),vp(0:npart),wp(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
c	
	dx=x(1+iwid)-x(1)
	dy=y(1+jwid)-y(1)
	dz=z(1+kwid)-z(1)
	where(py<0.) 
	py=py+jbar*dy
	end where
	where(py>jbar*dy)
	py=py-jbar*dy
	end where
	pxi=px/dx+2.
	peta=py/dy+2.
	pzta=pz/dz+2.
	return
	end