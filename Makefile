ARCH =gfortran
                                                                                
ifeq ($(ARCH),gfortran)
F90= gfortran
LD= gfortran
LDFLAGS = -L/usr/local/lib -L/usr/lib -L/usr/local/Cellar/netcdf-fortran/4.6.1/lib -L /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib
FFLAGS =       -O -fdefault-real-8 -fdefault-double-8 -fallow-argument-mismatch
#-fno-underscoring
endif

ifeq ($(ARCH),linux)
                                                                                
#LAHEY - linux
F90= lf95
LD= lf95
FFLAGS =       -O --dbl
endif                                                                                

ifeq ($(ARCH),intel)

#INTEL 
F90= ifort
LD= ifort
LDFLAGS = #-pg
FFLAGS =  -nowarn  -O3 -r8  
#-check all
                 
endif
    
ifeq ($(ARCH),g95)

#GNU G95
F90= g95
LD= g95
LDFLAGS =    -L/usr/lib 
FFLAGS =   -O3 -r8  -fno-second-underscore
#FFLAGS =   -r8 -O3

endif 

ifeq ($(ARCH),mac)                                                                     
#ABSOFT - MAC
F90= f95
LD= f95
LDFLAGS = -P
FFLAGS =  -w  -O3 -N113  -YEXT_NAMES=LCS -YEXT_SFX=_ 
#FFLAGS =  -w  -Rs -N113  -YEXT_NAMES=LCS -YEXT_SFX=_ 
                                                               
endif

#lf95 lahey computers
#LIBS = libnetcdf.a
# G4 laptop
#LIBS = /Users/lapenta/Desktop/stuff/netcdf-3.6.0-p1/lib/libnetcdf.a
# LIBS = /Users/administrator/Documents/fortran/netcdf-3.6.1/lib/libnetcdf.a /usr/lib/libSystemStubs.a
#for laptop MAC Intel
#LIBS = /Users/gianni/Documents/fortran/netcdf-3.6.1/lib/libnetcdf.a  /Developer/SDKs/MacOSX10.4u.sdk/usr/lib/libSystemStubs.a
#for office pc in leuven
#LIBS = /usr/local/netcdf/lib/libnetcdf.a
# For IGPP MACS use below
#LIBS=/sw/lib/libnetcdf.a
# For ulisse
#LIBS=/usr/lib/libnetcdf.a
#For CLUSTER KU LEUVEN
#LIBS=/data/home/u0052182/netcdf-3.6.1/lib/libnetcdf.a
#LIBS=/data/home/u0052182/libnetcdf.a
# For new computer office KU Leuven
#LIBS= /home/gianni/netcdf-3.6.2/lib/libnetcdf.a
#LIBS= $(NETCDF)
#LIBS = /usr/local/lib/libnetcdf.a

LIBS = -lnetcdff


CMD	=	xflip

SRCS=   accel_3dmhd.f axisavg.f begin.f bcc.f bc_wall.f budget.f celdex.f \
	b_vtx.f \
	bc_ghost.f bc_particles.f  bc_scalar.f bc_current.f bc_noslip.f \
	celindex.f celstep.f chnglst.f debug.f dummyc.f\
	diagonal.f divphi_3dmhd.f \
	eos.f flip3d_nol.f geom_jub.f \
	gmres_3dmhd.f gmres_vtx.f \
	gridinit_log.f implctp_cr1.f \
        initiv.f J_times_kvec.f map3d.f map3d_axis.f map3d_surf.f\
        ludecomp.f MPBJ_precond.f mfmpj.f \
        magnetopause.f \
        metric.f metricc.f mfnk_3dmhd.f \
        mshset.f parcelc.f parcelv.f \
        parlocat_log.f \
        parmov.f parrefl.f parset_new.f partcles.f \
        precond.f poisson_cg.f poisson_vtx.f \
        residu_3dmhd.f residue_vtx.f sensitiv_3dmhd.f stress_3dmhd.f \
        resistive_diff.f \
        rotate.f setzero.f timstp.f torusbc_scalar.f \
        trilin.f triquad.f triple.f vinit_gmres.f volume_vtx.f \
        vtxindx.f watec.f watev.f weights.f \
	svector.f sgrid3d.f strain.f strain_ns.f stress.f test.f vtxb.f \
	reconnection_ay.f linux.f rinj.f computee.f ncdlib.f initial_state.f \
	source_surface.f  parset_read.f

SRCS90= vast_kind_param_M.f90 modules.f90 poisson_fft.f90 dfftpack.f90 
        
OBJECTS = $(SRCS90:%.f90=%.o) $(SRCS:%.f=%.o) 


.SUFFIXES:
.SUFFIXES: .o .f .f90

.PHONY: clean tar


.f.o:
	$(F90) -c $(FFLAGS) $<

.f90.o:
	$(F90) -c $(FFLAGS) $<

xflip:  $(OBJECTS)
#	$(F90) -c -O -w  -YEXT_NAMES=LCS -YEXT_SFX=_  ncdlib.f 
	$(LD) $(LDFLAGS) -o $(CMD) $(OBJECTS)  $(LIBS)


clean:
	rm -f xflip *.o core *.dat fort.* *.vtk *.mod


