      subroutine bc_noslip(nvtx,ijkvtx,ubc,vbc,wbc,u,v,w)
c
c     a routine to impose no slip conditions on a surface
c     of the mesh
c
      real u(*),v(*),w(*)
c
      integer nvtx,ijkvtx(*)
c
      do n=1,nvtx
c
      ijk=ijkvtx(n)
c
      u(ijk)=ubc
      v(ijk)=vbc
      w(ijk)=wbc
c
      enddo
c
      return
      end

      subroutine bc_open(nvtx,ijkvtx,ubc,vbc,wbc,u,v,w,istep)
c
c     a routine to impose open conditions on a surface
c     of the mesh
c
      real u(*),v(*),w(*)
c
      integer nvtx,ijkvtx(*)
c
      do n=1,nvtx
c
      ijk=ijkvtx(n)
c
      u(ijk)=u(ijk+istep)
      v(ijk)=v(ijk+istep)
      w(ijk)=w(ijk+istep)
c
      enddo
c
      return
      end
