      subroutine residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,
     &     ul,vl,wl,
     &     gradcx,gradcy,gradcz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c     a routine to calculate the residual error
c     for 3d mhd
c
c      include 'bypass.com'
      logical resist
c
      integer ncells,ijkcell(*),iwid,jwid,kwid,
     &     nlist(*),
     &     nvtx,ijkvtx(*),ibar,jbar,kbar
c
      real 
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     r4pi,dt,vol(*),
     &     ul(*),vl(*),wl(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     gradcx(*),gradcy(*),gradcz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),p(*),
     &     s(*),sbx(*),sby(*),sbz(*),
     &     jx(*),jy(*),jz(*),vvol(*)

c
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
      call stress_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     bxl,byl,bzl,p,r4pi,
     &     exx,exy,exz,eyy,eyz,ezz)
c
      call accel_3dmhd
c
      call strain_ns(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     ul,vl,wl,vol,gradcx,gradcy,gradcz)
c
      if(resist) then
c
c      call resistive_diff
c
      endif
      
      do nt=1,ncells
c
      ijk=ijkcell(nt)
      n=nlist(ijk)
c
      divu=(dudx(ijk)+dvdy(ijk)+dwdz(ijk))
c
c      if (ringingout) then
c      vol_norm=volpc(ijk)
c      else
      vol_norm=vol(ijk)
c      end if
c
      s(n)=(rol(ijk)-ro(ijk))*vol(ijk)
     &     +rol(ijk)*divu*vol(ijk)*dt
c
      sbx(n)=(bxl(ijk)-bxn(ijk))*vol_norm
     & +(bxl(ijk)*divu
     & -(bxl(ijk)*dudx(ijk)+byl(ijk)*dudy(ijk)+bzl(ijk)*dudz(ijk)))
     & *vol_norm*dt
c     &  +gradcx(ijk)*vol(ijk)*dt
c
      sby(n)=(byl(ijk)-byn(ijk))*vol_norm
     & +(byl(ijk)*divu
     & -(bxl(ijk)*dvdx(ijk)+byl(ijk)*dvdy(ijk)+bzl(ijk)*dvdz(ijk)))
     & *vol_norm*dt
c     &  +gradcy(ijk)*vol(ijk)*dt
c
      sbz(n)=(bzl(ijk)-bzn(ijk))*vol_norm
     & +(bzl(ijk)*divu
     & -(bxl(ijk)*dwdx(ijk)+byl(ijk)*dwdy(ijk)+bzl(ijk)*dwdz(ijk)))
     & *vol_norm*dt
c     &  +gradcz(ijk)*vol(ijk)*dt
c
      enddo
c
      return 
      end
