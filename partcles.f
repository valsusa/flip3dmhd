 
      subroutine area_bc(i1,i2,j1,j2,ks,iwid,jwid,kwid,updown,
     &     x,y,z,ax,ay,az)
c
c     a routine to calculate the directed areas over a surface of constant index ks
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*),ax(*),ay(*),az(*)
c
      do 1 j=j1,j2
           do 1 i=i1,i2
c
      ijk=(ks-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      x1=0.5*(x(ijk+iwid)+x(ijk+iwid+jwid)
     &       -x(ijk)-x(ijk+jwid))
c
      x2=0.5*(x(ijk+jwid)+x(ijk+iwid+jwid)
     &       -x(ijk)-x(ijk+iwid))
c
      y1=0.5*(y(ijk+iwid)+y(ijk+iwid+jwid)
     &       -y(ijk)-y(ijk+jwid))
c
      y2=0.5*(y(ijk+jwid)+y(ijk+iwid+jwid)
     &       -y(ijk)-y(ijk+iwid))
c
      z1=0.5*(z(ijk+iwid)+z(ijk+iwid+jwid)
     &       -z(ijk)-z(ijk+jwid))
c
      z2=0.5*(z(ijk+jwid)+z(ijk+iwid+jwid)
     &       -z(ijk)-z(ijk+iwid))
c
      ax(ijk)=(y1*z2-y2*z1)*updown
      ay(ijk)=(z1*x2-z2*x1)*updown
      az(ijk)=(x1*y2-x2*y1)*updown
c
    1 continue
c
      return
      end
      subroutine chek_surf(i1,i2,j1,j2,ksurf,iwid,jwid,kwid,
     &     x,y,z,
     &     px,py,pz,up,vp,wp,ax,ay,az,dt,
     &     nlist,ijktmp3)
c
c     a routine to make a list of those polygons the particle orbit
c     may have intersected
c
      implicit real*8 (a-h,o-z)
c
      dimension ax(*),ay(*),az(*),
     &     x(*),y(*),z(*),
     &     ijktmp3(*)
c
      nlist=0
c
      do 1 i=i1,i2
      do 1 j=j1,j2
c
      ijk=(ksurf-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      testout=(px-x(ijk))*ax(ijk)
     &          +(py-y(ijk))*ay(ijk)
     &          +(pz-z(ijk))*az(ijk)
c
      testin=(up*ax(ijk)+vp*ay(ijk)+wp*az(ijk))*dt
c
ctest    ********************************************
c
      if(testout.ge.0..and.testout.lt.testin) then
c
      nlist=nlist+1
      ijktmp3(nlist)=ijk-kwid
      endif
c
    1 continue
c
      return
      end
      subroutine closest_vertex(i1,i2,j1,j2,k1,k2,
     &     iwid,jwid,kwid,
     &     x,y,z,xp,yp,zp,
     &     imin,jmin,kmin,ijkmin)
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*)
c
      dmin=1.e10
c
      do 1 k=k1,k2
      do 1 j=j1,j2
      do 1 i=i1,i2
c
      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      d=sqrt((xp-x(ijk))**2+(yp-y(ijk))**2+(zp-z(ijk))**2)
c
      if(d.lt.dmin) then
c
      dmin=d
      imin=i
      jmin=j
      kmin=k
      ijkmin=ijk
c
      endif
c
    1 continue
c
      return
      end
      subroutine look_evrywher(i1,i2,j1,j2,k1,k2,iwid,jwid,kwid,
     &     eps,succes,
     &     x,y,z,xp,yp,zp,pxi,peta,pzta)
c
c     a routine to examine every cell in the mesh to locate a particle
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*)
c
      logical succes
      real*8 nu
c
      succes=.false.
c
      do 1 k=k1,k2
      do 1 j=j1,j2
      do 1 i=i1,i2
c
      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      call map3d(ijk,iwid,jwid,kwid,eps,
     &     x,y,z,
     &     xp,yp,zp,tsi,eta,nu)
c
      if (tsi*(1.-tsi).ge.0.
     &     .and.eta*(1.-eta).ge.0.
     &     .and.nu*(1.-nu).ge.0.) then
c
      succes=.true.
c
      pxi=i+tsi
      peta=j+eta
      pzta=k+nu
c
      go to 601
c
      endif
c
    1 continue
c
  601 continue
c
      return
      end
      subroutine parbctor(ncellsp,ijkctmp,iphead,npart,
     &     iwid,jwid,kwid,ibar,jbar,kbar,cdlt,sdlt,
     &     x,y,z,uptilde,vptilde,wptilde,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c     **********************************************
c
c     a routine to apply boundary conditions to particle motion in a torus
c
c     **********************************************
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkctmp(*),iphead(*),
     &     x(*),y(*),z(*),uptilde(*),vptilde(*),wptilde(*),
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     up(0:npart),vp(0:npart),wp(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
c
      logical succes
      real*8 nu
c
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
      eps=1.e-4
c
c
      do 374 n=1,ncellsp
c
      ijk=ijkctmp(n)
      np=iphead(ijk)
c
      inew=int(pxi(np))
      jnew=int(peta(np))
      knew=int(pzta(np))
c
  380 continue
c
      inew=int(pxi(np))
      if((ibp1-inew)*(inew-2).lt.0) then
      if(inew.lt.2) pxi(np)=pxi(np)+real(ibar)
      if(inew.gt.ibp1) pxi(np)=pxi(np)-real(ibar)
      go to 380
      endif
c
c
  390 continue
c
      jnew=int(peta(np))
c
      if((jbp1-jnew)*(jnew-2).lt.0) then
c
      if(jnew.lt.2) then
c
      peta(np)=peta(np)+real(jbar)
c
      wspx=cdlt*px(np)-sdlt*py(np)
      wspy=sdlt*px(np)+cdlt*py(np)
      px(np)=wspx
      py(np)=wspy
c
      wsup=cdlt*up(np)-sdlt*vp(np)
      wsvp=sdlt*up(np)+cdlt*vp(np)
      up(np)=wsup
      vp(np)=wsvp
c
      wsup=cdlt*uptilde(ijk)-sdlt*vptilde(ijk)
      wsvp=sdlt*uptilde(ijk)+cdlt*vptilde(ijk)
      uptilde(ijk)=wsup
      vptilde(ijk)=wsvp
c
      endif
c
      if(jnew.gt.jbp1) then
c
      peta(np)=peta(np)-real(jbar)
c
      wspx=cdlt*px(np)+sdlt*py(np)
      wspy=-sdlt*px(np)+cdlt*py(np)
      px(np)=wspx
      py(np)=wspy
c
      wsup=cdlt*up(np)+sdlt*vp(np)
      wsvp=-sdlt*up(np)+cdlt*vp(np)
      up(np)=wsup
      vp(np)=wsvp
c
      wsup=cdlt*uptilde(ijk)+sdlt*vptilde(ijk)
      wsvp=-sdlt*uptilde(ijk)+cdlt*vptilde(ijk)
      uptilde(ijk)=wsup
      vptilde(ijk)=wsvp
c
      endif
c
      go to 390
c
      endif
c
  374 continue
c
      return
      end
      subroutine parbccyl(ncellsp,ijkctmp,iphead,npart,
     &     iwid,jwid,kwid,ibar,jbar,kbar,dz,
     &     x,y,z,px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c     **********************************************
c
c     a routine to apply boundary conditions to particle motion in a torus
c
c     **********************************************
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkctmp(*),iphead(*),
     &     x(*),y(*),z(*),
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     up(0:npart),vp(0:npart),wp(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
c
      logical succes
      real*8 nu
c
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
      eps=1.e-4
c
      do 374 n=1,ncellsp
c
      ijk=ijkctmp(n)
      np=iphead(ijk)
c
      inew=int(pxi(np))
      jnew=int(peta(np))
      knew=int(pzta(np))
c
  380 continue
c
      inew=int(pxi(np))
      if((ibp1-inew)*(inew-2).lt.0) then
      if(inew.lt.2) pxi(np)=pxi(np)+real(ibar)
      if(inew.gt.ibp1) pxi(np)=pxi(np)-real(ibar)
      go to 380
      endif
c
c
  390 continue
c
      jnew=int(peta(np))
c
      if((jbp1-jnew)*(jnew-2).lt.0) then
c
      if(jnew.lt.2) then
c
      peta(np)=peta(np)+real(jbar)
c
      py(np)=py(np)+dz
c
      endif
c
      if(jnew.gt.jbp1) then
c
      peta(np)=peta(np)-real(jbar)
c
      py(np)=py(np)-dz
c
      endif
c
      go to 390
c
      endif
c
  374 continue
c
      return
      end
      subroutine parlocat(ncellsp,ijkctmp,iphead,iwid,jwid,kwid,
     &     IJKTMP2,IJKTMP3,IJKTMP4,RMAJ,DZ,ALFA,AX,AY,AZ,DT,
     &     itdim,npart,ibar,jbar,kbar,mgeom,cdlt,sdlt,
     &     wate,x,y,z,BXV,BYV,BZV,
     &     XPTILDE,YPTILDE,ZPTILDE,UPTILDE,VPTILDE,WPTILDE,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c     ********************************************************
c
c     a routine to locate a particle on a grid
c
c     *******************************************************
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkctmp(*),iphead(*),wate(itdim,*),
     &     IJKTMP2(*),IJKTMP3(*),IJKTMP4(*),ALFA(*),
     &     x(*),y(*),z(*),BXV(*),BYV(*),BZV(*),
     &     XPTILDE(*),YPTILDE(*),ZPTILDE(*),
     &     UPTILDE(*),VPTILDE(*),WPTILDE(*),
     &     AX(*),AY(*),AZ(*),
     &     tsix(*),tsiy(*),tsiz(*),
     &     etax(*),etay(*),etaz(*),
     &     nux(*),nuy(*),nuz(*),
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     up(0:npart),vp(0:npart),wp(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
c
      real*8 nu,nux,nuy,nuz
c
      logical succes
c
      one=1.
c
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
      do 380 n=1,ncellsp
c
      ijktmp2(n)=ijkctmp(n)
c
  380 continue
c
      nchek=ncellsp
c
c
c
c     ******************************************************************
c
c     calculate new natural coordinates
c
c     ******************************************************************
      itry=0
c
c
    1 continue
c
      itry=itry+1
c
c
      nchek_nxt=0
c
      do 373 n=1,nchek
c
      ijk=ijktmp2(n)
      np=iphead(ijk)
c
      iold=int(pxi(np))
      iold=min(iold,ibp1)
      iold=max(2,iold)
c
      jold=int(peta(np))
      jold=min(jold,jbp1)
      jold=max(2,jold)
c
      kold=int(pzta(np))
      kold=min(kold,kbp1)
      kold=max(2,kold)
c
c
      ijkold=(kold-1)*kwid+(jold-1)*jwid+(iold-1)*iwid+1
c
      eps=1.e-4
c
      call map3d(ijkold,iwid,jwid,kwid,eps,
     &     x,y,z,
     &     px(np),py(np),pz(np),tsi,eta,nu)
c
c      epsn=1.e-7
      epsn=0.0
      ichange=0.5*(int(sign(one,tsi-1.-epsn)
     &     +sign(one,tsi+epsn)))
c
      jchange=0.5*(int(sign(one,eta-1.-epsn)
     &     +sign(one,eta+epsn)))
c
      kchange=0.5*(int(sign(one,nu-1.-epsn)
     &     +sign(one,nu+epsn)))
c
      inew=iold+ichange
      jnew=jold+jchange
      knew=kold+kchange
c
      ijknew=(knew-1)*kwid+(jnew-1)*jwid+(inew-1)*iwid+1
c
      if(ijknew.ne.ijkold) then
c
      nchek_nxt=nchek_nxt+1
      ijktmp2(nchek_nxt)=ijktmp2(n)
c
c
      pxi(np)=inew+0.5
      peta(np)=jnew+0.5
      pzta(np)=knew+0.5
c
c
      else
c
       pxi(np)=real(iold)+tsi
       peta(np)=real(jold)+eta
       pzta(np)=real(kold)+nu
c
      endif
c
c
  373 continue
c
      nchek=nchek_nxt
c
c
c      check for particles outside the domain
c
       nout=0
c
      do 400 n=1,nchek
      ijk=ijktmp2(n)
      np=iphead(ijk)
c
      inew=int(pxi(np))
      jnew=int(peta(np))
      knew=int(pzta(np))
c
      if(inew.lt.2.or.inew.gt.ibp1
     &     .or.jnew.lt.2.or.jnew.gt.jbp1
     &     .or.knew.lt.2.or.knew.gt.kbp1) then
c
      nout=nout+1
      ijktmp3(nout)=ijk
      endif
c
  400 continue
c
c     check for axis crossings
c
      do 384 n=1,nout
c
      ijk=ijktmp3(n)
      np=iphead(ijk)
c
      inew=int(pxi(np))
      jnew=int(peta(np))
      knew=int(pzta(np))
c
         if(jnew.ge.2.and.jnew.le.jbp1
     &     .and.knew.lt.2) then
c
      iaxis=inew
      jaxis=jnew
c
c     the particle has crossed the axis
c
      call map3d_axis(2,ibp1,jnew,jnew,iaxis,jaxis, iwid,jwid,kwid,eps,
     &     x,y,z,
     &     px(np),py(np),pz(np),tsi,eta,nu,succes)
c
      if(.not.succes) then
c
      call map3d_axis(2,ibp1,2,jbp1,iaxis,jaxis,iwid,jwid,kwid,eps,
     &     x,y,z,
     &     px(np),py(np),pz(np),tsi,eta,nu,succes)
c
      endif
      if(succes) then
      pxi(np)=iaxis+tsi
      peta(np)=jaxis+eta
      pzta(np)=2.+nu
      else
      pzta(np)=2.
      endif
c
      endif
c
  384 continue
c
c_parloco      if(nout.gt.0.and.itry.eq.1) then
c_parloco      call parloco(nout,ijktmp3,iphead,iwid,jwid,kwid,
c_parloco     &     RMAJ,DZ,ALFA,DT,ifail,
c_parloco     &     itdim,npart,ibar,jbar,kbar,mgeom,cdlt,sdlt,
c_parloco     &     wate,x,y,z,bxv,byv,bzv,
c_parloco     &     xptilde,yptilde,zptilde,uptilde,vptilde,wptilde,
c_parloco     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,
c_parloco     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c_parloco      endif
c
      do 374 n=1,nout
c
      ijk=ijktmp3(n)
      np=iphead(ijk)
c
 3810 continue
c
      inew=int(pxi(np))
c
      if(inew.lt.2.or.inew.gt.ibp1) then
c
      if(inew.lt.2) pxi(np)=pxi(np)+real(ibar)
      if(inew.gt.ibp1) pxi(np)=pxi(np)-real(ibar)
c
      go to 3810
c
      endif
c
  374 continue
c
c     nout is the number of particles for which intersections
c     have still not been found
c
c
c
      if(nout.gt.0) then
c
      nout_nxt=0
c
c     calculate directed areas over k=kbp2 surface
c
      updown=1.
      call area_bc(2,ibp1,2,jbp1,kbp1+1,iwid,jwid,kwid,updown,
     &     x,y,z,ax,ay,az)
c
c     one particle at a time, make list of surface elements
c     which the particle may have passed through
c
      do 510 n=1,nout
      ijk=ijktmp3(n)
      np=iphead(ijk)
c
      succes=.false.
c
      knew=int(pzta(np))
      if(knew.gt.kbp1) then
c
c
      call chek_surf(2,ibp1,2,jbp1,kbp1+1,iwid,jwid,kwid,
     &     x,y,z,
     &     px(np),py(np),pz(np),uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &     ax,ay,az,dt,nlist,ijktmp4)
c
c      if(nlist.gt.0) write(*,*) 'chek_surf, nlist=',nlist
c     calculate intersection with boundary in candidate cells
c
      knew=kbp1
c
      do 520 nl=1,nlist
      ijknew=ijktmp4(nl)
c
      call map3d_surf(ijk,ijknew,iwid,jwid,kwid,eps,ifail,
     &     iphead,itdim,x,y,z,
     &     px(np),py(np),pz(np),
     &     uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &     xi,eta,alfa(ijk))
c
c      write(*,*) 'np=',np,'ijknew=',ijknew,'ifail=',ifail
c
      if(ifail.ne.1) then
      alfa(ijk)=alfa(ijk)/dt
c
      if(xi*(1.-xi).ge.0.
     &     .and.eta*(1.-eta).ge.0.
     &     .and.alfa(ijk)*(1.-alfa(ijk)).ge.0.) then
c
c     particle has been found
c
      succes=.true.
c
      knew=(ijknew-1)/kwid+1
      jnew=(ijknew-(knew-1)*kwid-1)/jwid+1
      inew=(ijknew-(knew-1)*kwid-(jnew-1)*jwid-1)/iwid+1
c
      pxi(np)=xi+inew
      peta(np)=eta+jnew
c
      pzta(np)=kbp1+1-1.e-6
c
      ll=1
c
      call parrefl(ll,ijktmp3(n),iphead,npart,itdim,
     &     iwid,jwid,kwid,x,y,z,wate,bxv,byv,bzv,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c      write(*,*) 'particle found and reflected, np=', np
c
      go to 521
c
      endif
c
      endif
c
  520 continue
c
      endif
c
  521 if(.not.succes) then
c
      nout_nxt=nout_nxt+1
      ijktmp3(nout_nxt)=ijk
c
c
      endif
c
  510 continue
c
      nout=nout_nxt
      endif
c
c
c     check j=jbp2 surface for crossings
c
      if(nout.gt.0) then
      nout_nxt=0
c
      updown=1.
c
      call area_bc(2,kbp1,2,ibp1,jbp1+1,kwid,iwid,jwid,updown,
     &     x,y,z,ax,ay,az)
c
c
      do 610 n=1,nout
c
      ijk=ijktmp3(n)
      np=iphead(ijk)
      jsurf=jbp1+1
c
c      if nlist>0, call periodic bc's
c
      call chek_surf(2,kbp1,2,ibp1,jsurf,kwid,iwid,jwid,
     &     x,y,z,
     &     px(np),py(np),pz(np),uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &     ax,ay,az,dt,nlist,ijktmp4)
c
      succes=.false.
c
      if(nlist.gt.0) then
c
c     ******************************************************************
c     DIAGNOSTIC
      cross=-1.
      ijksurf=5*kwid+jbp1*jwid+5*iwid+1
      phisurf=atan(y(ijksurf)/x(ijksurf))
      parphinp=atan(py(np)/(px(np)+1.e-6))
      parphin=atan((py(np)-vp(np)*dt)/(px(np)-up(np)*dt+1.e-6))
      if(parphinp.gt.phisurf.and.parphin.lt.phisurf) then
c      write(*,*) 'particle np=',np,'has crossed j=jbp2'
      cross=1.
      endif
      do 620 nl=1,nlist
c
      ijknew=ijktmp4(nl)
c
      call map3d_surf(ijk,ijknew,kwid,iwid,jwid,eps,ifail,
     &     iphead,itdim,x,y,z,
     &     px(np),py(np),pz(np),
     &     uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &     zta,xi,alfa(ijk))
c
      if(ifail.eq.0) then
c
      alfa(ijk)=alfa(ijk)/dt
c      write(*,*) 'loop 610, ifail=',ifail,'ijknew,np=',ijknew,np
c
      if(xi*(1.-xi).ge.0.
     &     .and.alfa(ijk)*(1.-alfa(ijk)).ge.0.
     &     .and.zta*(1.-zta).ge.0.) then
c
      succes=.true.
c
c      write(*,*) '610 loop, xi,zta,alfa=', xi,zta,alfa(ijk)
      peta(np)=jsurf
      if(rmaj.gt.0.0) then
c
       call parbctor(1,ijktmp3(n),iphead,npart,
     &     iwid,jwid,kwid,ibar,jbar,kbar,cdlt,sdlt,
     &     x,y,z,uptilde,vptilde,wptilde,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
      else
c
       call parbccyl(1,ijktmp3(n),iphead,npart,
     &     iwid,jwid,kwid,ibar,jbar,kbar,dz,
     &     x,y,z,px,py,pz,up,vp,wp,pxi,peta,pzta)
c
      endif
      go to 621
      endif
c
      endif
  620 continue
      endif
c
  621 continue
      if(.not.succes) then
c
      nout_nxt=nout_nxt+1
      ijktmp3(nout_nxt)=ijk
      endif
c
c
  610 continue
c
      nout=nout_nxt
      endif
c
      if(nout.gt.0) then
      nout_nxt=0
c
      updown=-1.
      call area_bc(2,kbp1,2,ibp1,2,kwid,iwid,jwid,updown,
     &     x,y,z,ax,ay,az)
c
c
      do 710 n=1,nout
c
      ijk=ijktmp3(n)
      np=iphead(ijk)
      jsurf=2
c
c      if nlist>0, call periodic bc's
c
      call chek_surf(2,kbp1,2,ibp1,jsurf,kwid,iwid,jwid,
     &     x,y,z,
     &     px(np),py(np),pz(np),uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &     ax,ay,az,dt,nlist,ijktmp4)
c
      succes=.false.
c
      if(nlist.gt.0) then
c
c     ******************************************************************
c     DIAGNOSTIC
      cross=-1.
      ijksurf=5*kwid+jwid+5*iwid+1
      phisurf=atan(y(ijksurf)/x(ijksurf))
      parphinp=atan(py(np)/(px(np)+1.e-6))
      parphin=atan((py(np)-vp(np)*dt)/(px(np)-up(np)*dt+1.e-6))
      if(parphinp.lt.phisurf.and.parphin.gt.phisurf) then
c      write(*,*) 'particle np=',np,'has crossed j=2'
      cross=1.
      endif
      do 720 nl=1,nlist
c
      ijknew=ijktmp4(nl)
c
      call map3d_surf(ijk,ijknew,kwid,iwid,jwid,eps,ifail,
     &     iphead,itdim,x,y,z,
     &     px(np),py(np),pz(np),
     &     uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &     zta,xi,alfa(ijk))
c
      if(ifail.eq.0) then
      alfa(ijk)=alfa(ijk)/dt
c      write(*,*) 'loop 710, ifail=',ifail,'ijknew,np=',ijknew,np
c
      if(xi*(1.-xi).ge.0.
     &     .and.alfa(ijk)*(1.-alfa(ijk)).ge.0.
     &     .and.zta*(1.-zta).ge.0.) then
c
      succes=.true.
c
c      write(*,*) '710 loop, xi,zta,alfa=', xi,zta,alfa(ijk)
      peta(np)=jsurf-1
      if(rmaj.gt.0.0) then
c
       call parbctor(1,ijktmp3(n),iphead,npart,
     &     iwid,jwid,kwid,ibar,jbar,kbar,cdlt,sdlt,
     &     x,y,z,uptilde,vptilde,wptilde,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
      else
c
       call parbccyl(1,ijktmp3(n),iphead,npart,
     &     iwid,jwid,kwid,ibar,jbar,kbar,dz,
     &     x,y,z,px,py,pz,up,vp,wp,pxi,peta,pzta)
c
      endif
      go to 721
c
      endif
c
      endif
  720 continue
      endif
c
  721 continue
      if(.not.succes) then
c
      nout_nxt=nout_nxt+1
      ijktmp3(nout_nxt)=ijk
      endif
c
  710 continue
c
      nout=nout_nxt
c
      endif
c
c     **********************************************
      if(nchek.gt.0.and.itry.lt.10) go to 1
c
      nchek_nxt=0
c
c    after an exhaustive search of the boundary,
c
cc      do a global search for the particle
cc
c      if(nchek.gt.0) then
cc
c      do 810 n=1,nchek
c      ijk=ijktmp2(n)
c      np=iphead(ijk)
cc
c      call look_evrywher(2,ibp1,2,jbp1,2,kbp1,
c     &     iwid,jwid,kwid,eps,succes,
c     &     x,y,z,
c     &     px(np),py(np),pz(np),
c     &     pxi(np),peta(np),pzta(np))
cc
cc       after global search
c      if(succes) then
cc
c       write(*,*) 'look_evrywher called, succes=', succes
c       write(*,*)  'pxi,peta,pzta=',pxi(np),peta(np),pzta(np)
c      else
cc
c      nchek_nxt=nchek_nxt+1
c      ijktmp2(nchek_nxt)=ijk
cc
c      endif
c
  810 continue
c
      if(nchek.gt.0) then
c      write(*,*) 'nchek particles',nchek,'not found at ncyc=',ncyc
c      endif
c
      endif
c
 1000 continue
c
c
      return
      end
      subroutine parloco(nout,ijkctmp,iphead,iwid,jwid,kwid,
     &     RMAJ,DZ,ALFA,DT,ifail,
     &     itdim,npart,ibar,jbar,kbar,mgeom,cdlt,sdlt,
     &     wate,x,y,z,bxv,byv,bzv,
     &     xptilde,yptilde,zptilde,uptilde,vptilde,wptilde,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c     ********************************************************
c
c     a routine to locate a particle on a grid
c
c     *******************************************************
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkctmp(*),iphead(*),wate(itdim,*),
     &     x(*),y(*),z(*), ALFA(*),
     &     tsix(*),tsiy(*),tsiz(*),
     &     bxv(*),byv(*),bzv(*),
     &     xptilde(*),yptilde(*),zptilde(*),
     &     uptilde(*),vptilde(*),wptilde(*),
     &     etax(*),etay(*),etaz(*),
     &     nux(*),nuy(*),nuz(*),
     &     px(0:npart),py(0:npart),pz(0:npart),
     &     up(0:npart),vp(0:npart),wp(0:npart),
     &     pxi(0:npart),peta(0:npart),pzta(0:npart)
c
      real*8 nux,nuy,nuz
c
      logical succes
      ibp1=ibar+1
      jbp1=jbar+1
      kbp1=kbar+1
c
c
      do 1 n=1,nout
c
      ijk=ijkctmp(n)
      np=iphead(ijk)
c
      pzta(np)=real(kbar)+2.-1.e-3
c
      xptilde(ijk)=px(np)
      yptilde(ijk)=py(np)
      zptilde(ijk)=pz(np)
c
c     uptilde(ijk)=up(np)
c     vptilde(ijk)=vp(np)
c     wptilde(ijk)=wp(np)
c
      ALFA(IJK)=0.0
C
    1 continue
c
      eps=1.e-4
c
c
c     **********************************************
c
      nout_nxt=0
      do 2 n=1,nout
c
      ijk=ijkctmp(n)
      np=iphead(ijk)
c
      xi=pxi(np)
      eta=peta(np)
      zta=pzta(np)
c
      iold=int(pxi(np))
      jold=int(peta(np))
      kold=int(pzta(np))
c
      ijkold=(kold-1)*kwid+(jold-1)*jwid+(iold-1)*iwid+1
c
c
      call map3d_surf(ijk,iwid,jwid,kwid,eps,ifail,
     &                iphead,itdim,wate,x,y,z,
     &                xptilde(ijk),yptilde(ijk),zptilde(ijk),
     &                uptilde(ijk),vptilde(ijk),wptilde(ijk),
     &                xi,eta,kbp1+1,alfa(ijk))
c
c
      delxi=xi-iold
      deleta=eta-jold
      alfa(ijk)=alfa(ijk)/dt
c
      if(delxi*(1.-delxi).ge.0.
     &     .and.deleta*(1.-deleta).ge.0.
     &     .and.alfa(ijk)*(1.-alfa(ijk)).ge.0.
     &     .and.ifail.eq.0) then
c
      pxi(np)=xi
      peta(np)=eta
      pzta(np)=kbp1+.999999
c
      ll=1
      call parrefl(ll,ijkctmp(n),iphead,npart,itdim,
     &     iwid,jwid,kwid,x,y,z,wate,bxv,byv,bzv,
     &     px,py,pz,up,vp,wp,pxi,peta,pzta)
c
c
      else
      nout_nxt=nout_nxt+1
      ijkctmp(nout_nxt)=ijkctmp(n)
c
      end if
c
c
  2   continue
c
      nout=nout_nxt
c
c
      return
      end
c
      subroutine trilinp(ncells,ijkcell,itdim,iwid,jwid,kwid,
     &     iphead,pxi,peta,pzta,
     &     wate,bxv,byv,bzv,bxpn,bypn,bzpn)
c
      implicit real*8 (a-h,o-z)
c
      dimension wate(itdim,*),bxv(*),byv(*),bzv(*),
     &     bxpn(*),bypn(*),bzpn(*),ijkcell(*)
c
      dimension iphead(*),pxi(0:*),peta(0:*),pzta(0:*)
c
cdir$ ivdep
      do 41 n=1,ncells
c
      ijk=ijkcell(n)
      np=iphead(ijk)
c
      inew=int(pxi(np))
      jnew=int(peta(np))
      knew=int(pzta(np))
c
      ijknew=(knew-1)*kwid+(jnew-1)*jwid+(inew-1)*iwid+1
c
c
c
      bxpn(ijk)=(wate(ijk,1)*bxv(ijknew+iwid)
     &          +(wate(ijk,2)*bxv(ijknew+iwid+jwid)
     &          +(wate(ijk,3)*bxv(ijknew+jwid)
     &          +(wate(ijk,4)*bxv(ijknew)
     &          +(wate(ijk,5)*bxv(ijknew+iwid+kwid)
     &          +(wate(ijk,6)*bxv(ijknew+iwid+jwid+kwid)
     &          +(wate(ijk,7)*bxv(ijknew+jwid+kwid)
     &          +(wate(ijk,8)*bxv(ijknew+kwid)))))))))
c
      bypn(ijk)=(wate(ijk,1)*byv(ijknew+iwid)
     &          +(wate(ijk,2)*byv(ijknew+iwid+jwid)
     &          +(wate(ijk,3)*byv(ijknew+jwid)
     &          +(wate(ijk,4)*byv(ijknew)
     &          +(wate(ijk,5)*byv(ijknew+iwid+kwid)
     &          +(wate(ijk,6)*byv(ijknew+iwid+jwid+kwid)
     &          +(wate(ijk,7)*byv(ijknew+jwid+kwid)
     &          +(wate(ijk,8)*byv(ijknew+kwid)))))))))
c
      bzpn(ijk)=(wate(ijk,1)*bzv(ijknew+iwid)
     &          +(wate(ijk,2)*bzv(ijknew+iwid+jwid)
     &          +(wate(ijk,3)*bzv(ijknew+jwid)
     &          +(wate(ijk,4)*bzv(ijknew)
     &          +(wate(ijk,5)*bzv(ijknew+iwid+kwid)
     &          +(wate(ijk,6)*bzv(ijknew+iwid+jwid+kwid)
     &          +(wate(ijk,7)*bzv(ijknew+jwid+kwid)
     &          +(wate(ijk,8)*bzv(ijknew+kwid)))))))))
c
   41 continue
c
      return
      end
