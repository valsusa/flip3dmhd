       subroutine eos(p,ro,vol,sie,csq,gm1,ncells,ijkcell)
c
      implicit real*8 (a-h,o-z)
c
      dimension p(*),ro(*),vol(*),sie(*),csq(*),ijkcell(*)
c
      zero=0.0
c
      do 100 n=1,ncells
      ijk=ijkcell(n)
      p(ijk)=gm1*ro(ijk)*sie(ijk)
      p(ijk)=max(zero,p(ijk))
      csq(ijk)=gm1*(gm1+1.0)*sie(ijk)
  100 continue
      return
      end
