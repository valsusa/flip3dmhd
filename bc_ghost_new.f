      subroutine bc_ghost(nxp,nyp,nzp,iwid,jwid,kwid,
     &     bx,by,bz,periodicx,periodicy,periodicz)
c
c     a routine to set ghost cell values of cell-centered
c     vector to zero
c
c	  this routine is called but it appears to have no effects on the results
c
c     called by VINIT_GMRES
c
c
	  use boundary_module
c
      dimension bx(*),by(*),bz(*)
      real*8 inbx,inby,inbz
      logical :: periodicx, periodicy, periodicz
c
c
c     bottom and top ( k=1 and k=nzp)
c
      if(periodicz) then 
c
      do j=1,nyp
      do i=1,nxp
c
      ijk1=(j-1)*jwid+(i-1)*iwid+1
      ijkend=(nzp-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      bx(ijk1)=bx(ijkend-kwid)
      by(ijk1)=by(ijkend-kwid)
      bz(ijk1)=bz(ijkend-kwid)
c
      bx(ijkend)=bx(ijk1+kwid)
      by(ijkend)=by(ijk1+kwid)
      bz(ijkend)=bz(ijk1+kwid)
c
      enddo
      enddo
      else
c
      if(bcpz==0) then
c
c	applis Dirichelet (sets B to zero)
c
      do j=1,nyp
      do i=1,nxp
c
      ijk=(j-1)*jwid+(i-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      ijk=(nzp-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      enddo
      enddo
c
      else
c
c	applies neuman (derivative of B is zero)
c
      do j=1,nyp
      do i=1,nxp
c
      ijk1=(j-1)*jwid+(i-1)*iwid+1
      ijkend=(nzp-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      bx(ijk1)=bx(ijk1+1)
      by(ijk1)=by(ijk1+1)
      bz(ijk1)=bz(ijk1+1)
c
      bx(ijkend)=bx(ijkend-1)
      by(ijkend)=by(ijkend-1)
      bz(ijkend)=bz(ijkend-1)
c
      enddo
      enddo
c
      end if
c
      end if
c
c
c
c     right and left ( i=1 and i=nxp)
c
      if(periodicx) then 
c
      do k=1,nzp
      do j=1,nyp
c
      ijk1=(k-1)*kwid+(j-1)*jwid+1      
      ijkend=(k-1)*kwid+(j-1)*jwid+(nxp-1)*iwid+1
c
      bx(ijk1)=bx(ijkend-iwid)
      by(ijk1)=by(ijkend-iwid)
      bz(ijk1)=bz(ijkend-iwid)
c
      bx(ijkend)=bx(ijk1+iwid)
      by(ijkend)=by(ijk1+iwid)
      bz(ijkend)=bz(ijk1+iwid)
c
      enddo
      enddo
      else
c      
c	applies dirichelet      
c      
      do j=1,nyp
      do k=1,nzp
c
      ijk=(k-1)*kwid+(j-1)*jwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      ijk=(k-1)*kwid+(j-1)*jwid+(nxp-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      enddo
      enddo
c
	  end if
c
c     front and back ( j=1 and j=nyp)
c
      if(periodicy) then 
c
      do k=1,nzp
      do i=1,nxp
c
      ijk1=(k-1)*kwid+(i-1)*iwid+1      
      ijkend=(k-1)*kwid+(nyp-1)*jwid+(i-1)*iwid+1
c
      bx(ijk1)=bx(ijkend-jwid)
      by(ijk1)=by(ijkend-jwid)
      bz(ijk1)=bz(ijkend-jwid)
c
      bx(ijkend)=bx(ijk1+jwid)
      by(ijkend)=by(ijk1+jwid)
      bz(ijkend)=bz(ijk1+jwid)
c
      enddo
      enddo
      else
c
c	applies dirichelet
c
      do k=1,nzp
      do i=1,nxp
c
      ijk=(k-1)*kwid+(i-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      ijk=(k-1)*kwid+(nyp-1)*jwid+(i-1)*iwid+1
c
      bx(ijk)=0.0
      by(ijk)=0.0
      bz(ijk)=0.0
c
      enddo
      enddo
c
      end if
c
      return
      end

      
