      integer cdfid,cdfidh,cdfidp,timdim,timdimh,timdimp
      common/icplot/iplot,iout(25)
c variable id's for netcdf
      integer
     &    idtime,
     &    idtimep,
     &    idnx,idny,idnz,
     &    idu,idv,idw,
     &    idrho,
     &    idp,
     &    idcurlx,idcurly,idcurlz,
     &    iddiv,
     &    idsie,
     &    idnum,
     &    idjx,idjy,idjz,
     &    idbx,idby,idbz,
     &    idex,idey,idez,
     &    idwght,
     &    idxp,idyp,idzp,idcolor,idpid
      common/icplot/
     &    timdim,
     &    idtime,
     &    idtimep,
     &    idnx,idny,idnz,	
     &    idu,idv,idw,
     &    idrho,
     &    idp,
     &    idcurlx,idcurly,idcurlz,
     &    iddiv,
     &    idsie,idelpot,idchden,
     &    idnum,
     &    idjx,idjy,idjz,
     &    idbx,idby,idbz,
     &    idex,idey,idez,
     &    idwght,
     &    idxp,idyp,idzp,idcolor,idpid
c  history id's for netcdf
      common/icplot/
     &    timdimh,
     &    idtimeh,
     &    idtote,idtoti,idtotb,idtotk,idtgheat,idgcur_max,idtginte,
     &    idtgbsq,idtgbsqx,idtgbsqy,idtgbsqz,
     &    idtgkex,idtgkey,idtgkez,idtrfsolar,
     &    idtgheat_par,idtgheat_per,
     &    idflux,idsurface_area,idj_par,idj_per,
     &    idef,ideb,idek,idchrg,idvort,idstreamf,iddiverg,idpotdiv
      common/cplot/
     &    cdfid,cdfidh,cdfidp
