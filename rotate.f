*dk rotate
      subroutine rotate(ibar,jbar,kbar,
     &     rmaj,dz,strait,iota,istep,
     &     delt,dtheta,dzstr,dphi,
     &     cdlt,sdlt,cdlhf,sdlhf,
     &     cdph,sdph,cdphhf,sdphhf)
c
      implicit real*8 (a-h,o-z)
c
      real*8 iota
      dimension iota(*),istep(*)
c     calculate elements of rotation matrix for toroidal direction
c
      pi=acos(-1.)
c
      rfibar=1./real(ibar)
      rfjbar=1./real(jbar)
c
         dtheta=2.*pi*rfibar
         delt=0.0
      if(rmaj.gt.0.0)   delt=dz/rmaj
         dphi = delt*rfjbar
         dzstr=strait*dz*rfjbar
c
         cdlt = cos(delt)
         sdlt = sin(delt)
         cdlhf= sqrt(.5*(1.+cdlt))
         sdlhf= sqrt(.5*(1.-cdlt))
         cdph = cos(dphi)
         sdph = sin(dphi)
         cdphhf = cos(dphi/2.)
         sdphhf = sin(dphi/2.)
c
c
c     define for quasi-ballooning coordinates
c
      do 1 k=1,kbar+2
c
      istep(k)=int(iota(k)*ibar+0.5)*delt/(2.*pi)
c
    1 continue
c
      return
      end
