      subroutine mfmpj(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     ITDIM,iwid,jwid,kwid,
     &     ibp1,jbp1,kbp1,cdlt,sdlt,strait,dz,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     DT,CNTR,srce,
     &     div,residu,Aq,Ax,rhs,
     &     gradpx,gradpy,gradpz,p,diag)
c
c
      implicit real*8 (a-h,o-z)
c
      logical periodic_x,periodic_y,periodic_z
c
      dimension ijkcell(*),ijkvtx(*),
     &          c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &          c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &          c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &          p(*), Aq(*),Ax(*), rhs(*),
     &          vol(*),div(*),residu(*),srce(*),
     &          gradpx(*),gradpy(*),gradpz(*),diag(*)
c
c     
c     
c     apply preconditioner, i.e find y=M^(-1).q
c
      do i = 1,3
      if (i .eq. 1) then
       do n=1,nvtx
        ijk=ijkvtx(n)
        Aq(ijk)=0.0
        Ax(ijk)=0.0
       end do
      endif

      do 32 n=1,nvtx
      ijk=ijkvtx(n)
      Aq(ijk)=(rhs(ijk)-Ax(ijk))/diag(ijk) + Aq(ijk)
c      Aq(ijk)=-rhs(ijk)/diag(ijk) 
  32  continue
c
      zero=0.0d0
c
c     compute A times x (Aq)
c
      do 34 n = 1,ncells
       ijk = ijkcell(n)
       Ax(ijk) = 0.0
   34 continue
c
      do 33 n=1,nvtx
      ijk=ijkvtx(n)
      Ax(ijk)=p(ijk)+Aq(ijk)
   33 continue
c
C
      call residue_vtx(ncells,ijkcell,nvtx,ijkvtx,
     &     periodic_x,periodic_y,periodic_z,
     &     cdlt,sdlt,strait,dz,
     &     iwid,jwid,kwid,ibp1,jbp1,kbp1,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     div,srce,
     &     Ax,gradpx,gradpy,gradpz,Ax)
c
c
      do 35 n=1,nvtx
      ijk=ijkvtx(n)
      Ax(ijk)=(residu(ijk)-Ax(ijk))
   35 continue
      end do

      return
      end
c
c
