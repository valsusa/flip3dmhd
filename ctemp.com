*cd ctemp

cll   common /ctemp/  ................................. 77/06/08 ....
      dimension  cx(8),cy(8),cz(8),cpl(8)
      dimension  uv(20),vv(20),wv(20)
      dimension  ivp(8),wght(8)
      dimension  fxp(8),fyp(8),fzp(8),rsq(8)
      dimension  fmf(100),fef(100),fvf(100)
      dimension  fpxf(100),fpyf(100),fpzf(100)
      dimension  fbxf(100),fbyf(100),fbzf(100)
      dimension  fpxft(100),fpyft(100),fpzft(100)
      dimension  xv(20),yv(20),zv(20)
      dimension  ucm(100),vcm(100),wcm(100),cm(100)
      dimension  wsin(3,20),wcos(3,20),tsi(3,12,20),rot(3,12,20)
      common /ctemp/  com6(1)       ,
     .        cx    , cy    , cz    , cpl   , uv    , vv    , wv    ,
     .        ivp   , fxp   , fyp   , fzp   , rsq   ,
     .        fmf   , fef   , fvf   , fbxf  , fbyf  , fbzf  ,
     .        xv    , yv    , zv    ,
     .        twx(9000)     , omg   ,
     &     uvv(ijmx),vvv(ijmx),wvv(ijmx),
     &     xpc(ijmx),ypc(ijmx),zpc(ijmx),
     &     wght
      equivalence
     .             (twx(7001),wsin),
     .             (twx(7061),wcos),
     .             (twx(7121),tsi),
     .             (twx(7841),rot)
      equivalence  (fmf   , ucm   , fpxf  ),
     .             (fef   , vcm   , fpyf  ),
     .             (fvf   , wcm   , fpzf  ),
     .             (fbxf  , fpxft , cm    ),
     .             (fbyf  , fpyft ),
     .             (fbzf  , fpzft )
c
