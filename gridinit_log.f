*dk gridinit
      subroutine gridinit(ibar,jbar,kbar,iwid,jwid,kwid,
     &     delt,dphi,dtheta,dx,dy,dz,rwall,rmaj,dzstr,istep,
     &     del1,del2,del3,del4,del5,del6,del7,
     &     x,y,z,cartesian,xl,xr,yb,yt,ze,zf)
c
      use boundary_module
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*),istep(*)
      logical cartesian
c
c     impose a twist on the mesh
c     one rotation over the length of the mesh in j
c
      if (cartesian) then
c	call gridinit_cart(ibar,jbar,kbar,iwid,jwid,kwid,
c     &   dx,dy,dz,x,y,z,xl,xr,yb,yt,ze,zf)

      if(grid_type_x.ne.0) then
      call gridinit_log(ibar,jbar,kbar,iwid,jwid,kwid,
     &   dx,dy,dz,x,y,z,xl,xr,yb,yt,ze,zf)
      else
      call gridinit_sphere(ibar,jbar,kbar,iwid,jwid,kwid,
     &   dx,dy,dz,x,y,z,xl,xr,yb,yt,ze,zf)
      endif
	  return
      endif
      rfibar=1./float(ibar)
      rfjbar=1./float(jbar)
      rfkbar=1./float(kbar)
c
      pi=acos(-1.)
c
      do 90 k=1,kbar+2
c
      dtdj=float(istep(k))*dtheta*rfjbar
      dt0=-0.5*float(istep(k))*dtheta
c
      do 88 j=1,jbar+2
c
      phiang=(j-2)*dphi
c
      do 86 i=1,ibar+2
c
      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      theta=(i-2)*dtheta+(j-2)*dtdj+dt0
      theta0=theta-0.5*pi
      delr=rwall*rfkbar*(1.+del1*cos(theta0)
     &                 +del2*cos(2.*(theta0+0.5*pi))
     &                 +del3*cos(3.*theta0)
     &                 +del4*cos(4.*theta0)
     &                 +del5*cos(5.*theta0)
     &                 +del7*cos(7.*theta0))
c
         rr = (k-2)*delr
         ws = rmaj+rr*sin(theta)
         xx =ws*cos(phiang)
         x(ijk) = xx
         yy =ws*sin(phiang)+float(j-2)*dzstr
         y(ijk) = yy
         z(ijk) = rr*cos(theta)
c
   86 continue
c
   88 continue
c
   90 continue
c
c
      return
      end

      subroutine gridinit_cart(ibar,jbar,kbar,iwid,jwid,kwid,
     &   dx,dy,dz,x,y,z,xl,xr,yb,yt,ze,zf)
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*)

      do k=1,kbar+2
      do j=1,jbar+2
      do i=1,ibar+2

      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1

      x(ijk)=(i-2)*dx
      y(ijk)=(j-2)*dy
      z(ijk)=(k-2)*dz

      enddo
      enddo
      enddo

      xl=0.
      xr=float(ibar)*dx
      yb=0.
      yt=float(jbar)*dy
      ze=0.
      zf=float(kbar)*dz
      return
      end


      subroutine gridinit_log(ibar,jbar,kbar,iwid,jwid,kwid,
     &   dx,dy,dz,x,y,z,xl,xr,yb,yt,ze,zf)
c
      use boundary_module
c
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*)
      real*8 klen,dzratio

      xl=0.
      xr=float(ibar)*dx
      yb=0.
      yt=float(jbar)*dy
      ze=0.
      zf=float(kbar)*dz

      dzratio=10.
      klen=float(kbar)/log(dzratio)
      pi=acos(-1.)

      do k=1,kbar+2
      do j=1,jbar+2
      do i=1,ibar+2

      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1

      select case(grid_type_x)
      case(1)
c     uniform grid
      x(ijk)=(i-2)*dx
c
      case(5)
c
      if(i.ge.2) then
      x(ijk)=(xr-xl)/ascale*atanh(2d0/ibar*(i-2)*tanh(ascale/2d0)-
     &  tanh(ascale/2d0))+(xr-xl)/2d0
      else
      x(ijk)=(i-2)*dx
      endif
c
      case default
c     uniform grid - as default
      z(ijk)=(k-2)*dz
c
      end select
c      x(ijk)=xl+(xr-xl)*(.5+4.*(dble(i-2)/dble(ibar)-.5)**3)
c      x(ijk)=xl+sin((i-2)*dx/(xr-xl)*pi/2.)
c
c	cylindrical coordinates
c
c      y(ijk)=(j-2)*dy*(1.+x(ijk))
c
c	cartesian coordinates
c
      y(ijk)=(j-2)*dy
c
      select case(grid_type_z)
      case(1)
c     uniform grid
      z(ijk)=(k-2)*dz
c
      case(2)
c     grid with smallest cells near bottom boundary and growing moving up
c      (e.g.Solar surface)
      z(ijk)=(zf-ze)*(exp(dble(k-2)/klen)-1.)/(exp(dble(kbar)/klen)-1.)
c
      case(3)
c     grid concentrated at both ends
      z(ijk)=(zf-ze)/ascale*asinh((2d0*(k-2)-kbar)/kbar*
     & sinh(ascale/2d0))+(zf-ze)/2d0
c
      case(4)
c     grid weakly concentrated in the center, Attention to the value of
c      ascale so that the generating function will not become negative
c      initiv has some bound checks on it to make it work
      z(ijk)=(zf-ze)/ascale*asin((2d0*(k-2)-kbar)/kbar*
     & sin(ascale/2d0))+(zf-ze)/2d0
c
      case(5)
c     grid concentrated in the center with hyperbolic tangent profile
      if(k.ge.2) then
      z(ijk)=(zf-ze)/ascale*atanh(2d0/kbar*(k-2)*tanh(ascale/2d0)-
     &  tanh(ascale/2d0))+(zf-ze)/2d0
      else
      z(ijk)=(k-2)*dz
      endif
c
      case default
c     uniform grid - as default
      z(ijk)=(k-2)*dz
c
      end select

      enddo
      enddo
      enddo


      return
      end

      subroutine gridinit_sphere(ibar,jbar,kbar,iwid,jwid,kwid,
     &   dx,dy,dz,x,y,z,xl,xr,yb,yt,ze,zf)
c
      use boundary_module
      implicit real*8 (a-h,o-z)
c
      dimension x(*),y(*),z(*)


      do k=1,kbar+2
      do j=1,jbar+2
      do i=1,ibar+2

      ijk=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1

      z(ijk)=(k-2)*dz

      x(ijk)=(z(ijk)+erre0)*tanthetax*(i-2-(ibar+1)/2.0)/dble(ibar+1)
      y(ijk)=(j-2)*dy
c      y(ijk)=(z(ijk)+erre0)*sinthetay*(j-2-(jbar+1)/2.0)/dble(jbar+1)

c      write(*,*) x(ijk),y(ijk),z(ijk)
      enddo
      enddo
      enddo

      yb=0.
      yt=float(jbar)*dy
      ze=0.
      zf=float(kbar)*dz
      xr=(erre0+ze)*tanthetax*(ibar-(ibar+1)/2.0)/dble(ibar+1)
      xl=-xr
      return
      end

