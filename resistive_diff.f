      subroutine resistive_diff
c
c     a routine to calculate diffusion of the magnetic field due to
c     resistance
c
      use boundary_module
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'

c
      real*8 edotj
c
	  xc=ibar*dx/2.d0
	  yc=jbar*dy/2.d0
	  zc=kbar*dz/2.d0
c
c     calculate the electric field due to resistance
c     to current flow
c
      call curlv(nvtx,ijkvtx,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     bxl,byl,bzl,jx,jy,jz)
c
c     impose periodic boundary conditions
c
      zero=0.0
c
      call bc_current(ibp1+1,jbp1+1,kbp1+1,
     &     cdlt,sdlt,zero,dz,
     &     periodic_x,periodic_y,periodic_z,
     &     jx,jy,jz)
c
c     calculate the values of the magnetic field
c     at the vertices of the mesh, and store in gradx,grady,gradz
c
      call b_vtx(ncells,ijkcell,iwid,jwid,kwid,
     &    nvtx,ijkvtx,
     &    bxl,byl,bzl,
     &    vol,upv1,
     &    bxv,byv,bzv)
c
c     calculate the electric field
c
      do n=1,nvtx
c
      ijk=ijkvtx(n)
c
      rvvol=1./vvol(ijk)
      jx(ijk)=jx(ijk)*rvvol
      jy(ijk)=jy(ijk)*rvvol
      jz(ijk)=jz(ijk)*rvvol
c
      jdotb=bxv(ijk)*jx(ijk)+byv(ijk)*jy(ijk)+bzv(ijk)*jz(ijk)
      bsq=bxv(ijk)**2+byv(ijk)**2+bzv(ijk)**2+1.d-20
c
c     the parallel current will not contribute to resistive diffusion
c
cjub      ex(ijk)=resistivity*(jx(ijk)-bxv(ijk)*jdotb/bsq)
cjub      ey(ijk)=resistivity*(jy(ijk)-byv(ijk)*jdotb/bsq)
cjub      ez(ijk)=resistivity*(jz(ijk)-bzv(ijk)*jdotb/bsq)
c	
	  r=sqrt((x(ijk)-xc)**2+(z(ijk)-zc)**2)
	  if(r.ge.5d0-min(dx,dz)/2d0 .and. circular_wall) then
        resprof=1d-3
	  else
	    resprof=1d0/sqrt(((sie(ijk)
     &     +sie(ijk-iwid)
     &	   +sie(ijk-iwid-jwid)
     &     +sie(ijk-jwid)
     &     +sie(ijk-kwid)
     &     +sie(ijk-iwid-kwid)
     &     +sie(ijk-iwid-jwid-kwid)
     &     +sie(ijk-jwid-kwid))/8d0)**3)

	  endif
c
      resprof=1d0
c
      ex(ijk)=resistivity*jx(ijk)*resprof
      ey(ijk)=resistivity*jy(ijk)*resprof
      ez(ijk)=resistivity*jz(ijk)*resprof
c
c     only the parallel current contributes to resistive diffusion
c
cjub      ex(ijk)=resistivity*bxv(ijk)*jdotb/bsq
cjub      ey(ijk)=resistivity*byv(ijk)*jdotb/bsq
cjub      ez(ijk)=resistivity*bzv(ijk)*jdotb/bsq
c
      enddo
   
c
c     calculate the curl of the electric field
c
      call curlc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     ex,ey,ez,gradx,grady,gradz)
c
c
      do n=1,ncells
      ijk=ijkcell(n)
c
c     calculate the energy dissipated by resistive diffusion
c
      edotj=0.125*(ex(ijk)*jx(ijk)
     &     +ex(ijk+iwid)*jx(ijk+iwid)
     &     +ex(ijk+iwid+jwid)*jx(ijk+iwid+jwid)
     &     +ex(ijk+jwid)*jx(ijk+jwid)
     &     +ex(ijk+kwid)*jx(ijk+kwid)
     &     +ex(ijk+iwid+kwid)*jx(ijk+iwid+kwid)
     &     +ex(ijk+iwid+jwid+kwid)*jx(ijk+iwid+jwid+kwid)
     &     +ex(ijk+jwid+kwid)*jx(ijk+jwid+kwid))
c
      edotj=edotj+0.125*(ey(ijk)*jy(ijk)
     &     +ey(ijk+iwid)*jy(ijk+iwid)
     &     +ey(ijk+iwid+jwid)*jy(ijk+iwid+jwid)
     &     +ey(ijk+jwid)*jy(ijk+jwid)
     &     +ey(ijk+kwid)*jy(ijk+kwid)
     &     +ey(ijk+iwid+kwid)*jy(ijk+iwid+kwid)
     &     +ey(ijk+iwid+jwid+kwid)*jy(ijk+iwid+jwid+kwid)
     &     +ey(ijk+jwid+kwid)*jy(ijk+jwid+kwid))
c
      edotj=edotj+0.125*(ez(ijk)*jz(ijk)
     &     +ez(ijk+iwid)*jz(ijk+iwid)
     &     +ez(ijk+iwid+jwid)*jz(ijk+iwid+jwid)
     &     +ez(ijk+jwid)*jz(ijk+jwid)
     &     +ez(ijk+kwid)*jz(ijk+kwid)
     &     +ez(ijk+iwid+kwid)*jz(ijk+iwid+kwid)
     &     +ez(ijk+iwid+jwid+kwid)*jz(ijk+iwid+jwid+kwid)
     &     +ez(ijk+jwid+kwid)*jz(ijk+jwid+kwid))
c
c
c
      Ohmic_heating(ijk)=edotj
     &   *dt*vol(ijk)

      bxl(ijk)=bxl(ijk)-gradx(ijk)*dt
      byl(ijk)=byl(ijk)-grady(ijk)*dt
      bzl(ijk)=bzl(ijk)-gradz(ijk)*dt
c
      enddo
c
      return
      end
