      subroutine magnetopause(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     color,gradx,grady,gradz,
     &     bxn,byn,bzn,cjump,
     &     reconnected_flux,surface_area)
c
      integer ncells,ijkcell(*),iwid,jwid,kwid
c
      real*8 
     &     c1x(*),c2x(*),c3x(*),c4x(*),
     &     c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),
     &     c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),
     &     c5z(*),c6z(*),c7z(*),c8z(*),vol(*),
     &     color(*),gradx(*),grady(*),gradz(*),
     &     bxn(*),byn(*),bzn(*),cjump,
     &     reconnected_flux,surface_area

c
c     a routine to calculate the reconnected flux at the magnetopause
c     the magnetic field is projected on to the gradient of color,
c     a vertex-centered characteristic function that has different constant
c     values in the magnetosphere and solar wind
c
      if(cjump.eq.0.d0) cjump=1.0d0
      gradcsq=0.0d0
      bzsq=0.0d0
c
c     compute the gradient of color
c
      call gradc(ncells,ijkcell,iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     color,gradx,grady,gradz)
c
c     project the magnetic field on to the gradient of color,
c     and sum the reconnected flux
c
      reconnected_flux=0.0d0
      surface_area=0.0d0
c
      do n=1,ncells
      ijk=ijkcell(n)
c
      reconnected_flux=reconnected_flux
     &     +(dabs(bxn(ijk))*gradx(ijk)
     &      +dabs(byn(ijk))*grady(ijk)
     &      +dabs(bzn(ijk))*gradz(ijk))*vol(ijk)

      surface_area=surface_area
     &    +dsqrt(gradx(ijk)**2+grady(ijk)**2+gradz(ijk)**2)
     &    *vol(ijk)

c
      enddo
c
      return
      end
