      subroutine output
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
      include 'netcdf.inc'
      include 'cplot.com'
c
      real*4   pxtmp(npart),pytmp(npart),pztmp(npart),
     &       colortmp(npart),pidtmp(npart),
     &       plotx(itdim),ploty(itdim),plotz(itdim),scalar
      integer*4 icount(nreg)
c
c     write history variables every time step:
c     write current time to file
         scalar=t
         call ncvpt(cdfidh,idtimeh,ncyc,1,scalar,ircode)
c     write history variables
         scalar=toti
         call ncvpt(cdfidh,idtoti,ncyc,1,scalar,ircode)
         scalar=tginte
         call ncvpt(cdfidh,idtginte,ncyc,1,scalar,ircode)
         scalar=totb
         call ncvpt(cdfidh,idtotb,ncyc,1,scalar,ircode)
         scalar=tgbsq
         call ncvpt(cdfidh,idtgbsq,ncyc,1,scalar,ircode)
         scalar=tgbsqx
         call ncvpt(cdfidh,idtgbsqx,ncyc,1,scalar,ircode)
         scalar=tgbsqy
         call ncvpt(cdfidh,idtgbsqy,ncyc,1,scalar,ircode)
         scalar=tgbsqz
         call ncvpt(cdfidh,idtgbsqz,ncyc,1,scalar,ircode)
         scalar=tgkex
         call ncvpt(cdfidh,idtgkex,ncyc,1,scalar,ircode)
         scalar=tgkey
         call ncvpt(cdfidh,idtgkey,ncyc,1,scalar,ircode)
         scalar=tgkez
         call ncvpt(cdfidh,idtgkez,ncyc,1,scalar,ircode)
         scalar=rfsolar
         call ncvpt(cdfidh,idtrfsolar,ncyc,1,scalar,ircode)
         scalar=totk
         call ncvpt(cdfidh,idtotk,ncyc,1,scalar,ircode)
         scalar=tote
         call ncvpt(cdfidh,idtote,ncyc,1,scalar,ircode)
         scalar=tgheat
         call ncvpt(cdfidh,idtgheat,ncyc,1,scalar,ircode)
         scalar=tgheat_par
         call ncvpt(cdfidh,idtgheat_par,ncyc,1,scalar,ircode)
         scalar=tgheat_per
         call ncvpt(cdfidh,idtgheat_per,ncyc,1,scalar,ircode)
         scalar=gcur_max
         call ncvpt(cdfidh,idgcur_max,ncyc,1,scalar,ircode)
         scalar=flux
         call ncvpt(cdfidh,idflux,ncyc,1,scalar,ircode)
         scalar=surface_area
         call ncvpt(cdfidh,idsurface_area,ncyc,1,scalar,ircode)
         scalar=tgj_par
         call ncvpt(cdfidh,idj_par,ncyc,1,scalar,ircode)
         scalar=tgj_per
         call ncvpt(cdfidh,idj_per,ncyc,1,scalar,ircode)
c
c
c     check if it is time to plot other data:
c           if(numit.ge.100) go to 1
            if(ncyc.eq.1) go to 1
            if(t+1.e-10.lt.tout) return
            ixto=ixto+1
c           make sure plotting interval is nonzero
c           use the last nonzero plotting interval:
            if(dto(ixto).eq.0.) ixto=ixto-1
            tout=tout+dto(ixto)
c
  1   continue
c
c     increment the plot counter:
      iplot=iplot+1
c
c     write current time to file
         scalar=t
         call ncvpt(cdfid,idtime,iplot,1,scalar,ircode)
         call ncredf(cdfid,ircode)
         call ncapt(cdfid,idtime,'ncyc',NCLONG,1,ncyc,ircode)
         call ncendf(cdfid,ircode)
         call ncvpt(cdfidp,idtimep,iplot,1,scalar,ircode)
         call ncredf(cdfidp,ircode)
         call ncapt(cdfidp,idtimep,'ncyc',NCLONG,1,ncyc,ircode)
         call ncendf(cdfidp,ircode)
c
c     write other grid variables as required:
c     grid
         do n=1,nvtx
         ijk=ijkvtx(n)
         plotx(ijk)=x(ijk)
         ploty(ijk)=y(ijk)
         plotz(ijk)=z(ijk)
         enddo
c
         call writevar(cdfid,idnx,plotx,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idny,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idnz,plotz,iplot,ibp2,jbp2,kbp2)
c     velocity
      if (iout(1).ne.0)  then
         do n=1,nvtx
         ijk=ijkvtx(n)
         plotx(ijk)=ul(ijk)
         ploty(ijk)=vl(ijk)
         plotz(ijk)=wl(ijk)
         enddo
c
         call writevar(cdfid,idu,plotx,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idv,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idw,plotz,iplot,ibp2,jbp2,kbp2)
      end if
c
c     density
      if (iout(2).ne.0) then
         do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=rho(ijk)
         enddo
c
         call writevar(cdfid,idrho,plotx,iplot,ibp2,jbp2,kbp2)
      end if
c
c     pressure
      if (iout(3).ne.0) then
         do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=p(ijk)
         enddo
c
         call writevar(cdfid,idp,plotx,iplot,ibp2,jbp2,kbp2)
      end if
c
c
c     divu
      if (iout(5).ne.0) then
         do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=divu(ijk)
         enddo
c
         call writevar(cdfid,iddiv,plotx,iplot,ibp2,jbp2,kbp2)
      end if
c
c     sie
      if (iout(6).ne.0) then
         do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=sie(ijk)
         enddo
c
         call writevar(cdfid,idsie,plotx,iplot,ibp2,jbp2,kbp2)
      end if
c
c     current
c
c     magnetic field
      if (iout(9).ne.0) then
         do n=1,nvtx
         ijk=ijkvtx(n)
         plotx(ijk)=bxv(ijk)
         ploty(ijk)=byv(ijk)
         plotz(ijk)=bzv(ijk)
         enddo
c
         call writevar(cdfid,idbx,plotx,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idby,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idbz,plotz,iplot,ibp2,jbp2,kbp2)
      end if
c
c     current
      if (iout(8).ne.0) then
         do n=1,nvtx
         ijk=ijkvtx(n)
         plotx(ijk)=jx(ijk)
         ploty(ijk)=jy(ijk)
         plotz(ijk)=jz(ijk)
         enddo
c
         call writevar(cdfid,idjx,plotx,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idjy,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idjz,plotz,iplot,ibp2,jbp2,kbp2)
      end if
c
c     electric stuff
      if (iout(10).ne.0) then

      call compute_e

c
c	electric field
c
         do n=1,nvtx
         ijk=ijkvtx(n)
         plotx(ijk)=ex(ijk)
         ploty(ijk)=ey(ijk)
         plotz(ijk)=ez(ijk)
         enddo
c
         call writevar(cdfid,idex,plotx,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idey,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idez,plotz,iplot,ibp2,jbp2,kbp2)
c
c	charge and potential
c
         do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=chden(ijk)
         ploty(ijk)=elpot(ijk)
	     plotz(ijk)=streamf(ijk)
         enddo
         plotx=chden
         ploty=elpot
	     plotz=streamf
c
         call writevar(cdfid,idchden,plotx,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idelpot,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idstreamf,plotz,iplot,ibp2,jbp2,kbp2)
c
		 do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=curlcy(ijk)
	 ploty(ijk)=divergenceu(ijk)
	 plotz(ijk)=potdiv(ijk)
         enddo
	 call writevar(cdfid,idvort,plotx,iplot,ibp2,jbp2,kbp2)
	 call writevar(cdfid,iddiverg,ploty,iplot,ibp2,jbp2,kbp2)
         call writevar(cdfid,idpotdiv,plotz,iplot,ibp2,jbp2,kbp2)

      end if
c
c
c     weight (for adaptive grid)
      if (iout(11).ne.0) then
         do n=1,ncells
         ijk=ijkcell(n)
         plotx(ijk)=wgrid(ijk)
         enddo
c
         call writevar(cdfid,idwght,plotx,iplot,ibp2,jbp2,kbp2)
      end if
c
c     particles:
      if (iout(20).ne.0) then
c
c     repack particles:
         nptotl=0
         do 10 kr=1,nrg
           icount(kr)=1
           nptotl=nptotl+numtot(kr)
  10     continue
c
c
         newcell=0
         do 20 n=1,ncells
         ijk=ijkcell(n)
         np=iphead(ijk)
         if(np.ne.0) then
         newcell=newcell+1
         ijkctmp(newcell)=ijk
         iphd2(ijk)=np
         end if
  20     continue
c
c
  15     continue
c
         if(newcell.eq.0) go to 500
c
         do 100 n=1,newcell
         ijk=ijkctmp(n)
         np=iphd2(ijk)
         kr=ico(np)
         iploc=numtot(kr-1)+icount(kr)
         pxtmp(iploc)=px(np) 
         pytmp(iploc)=py(np) 
         pztmp(iploc)=pz(np) 
         colortmp(iploc)=float(ico(np))
         pidtmp(iploc)=float(np)
         icount(kr)=icount(kr)+1
 100     continue
c
         newcell_nxt=0
         do 200 n=1,newcell
         ijk=ijkctmp(n)
         np=link(iphd2(ijk))
         if(np.gt.0) then
            newcell_nxt=newcell_nxt+1
            iphd2(ijk)=np
            ijkctmp(newcell_nxt)=ijk
         end if
  200    continue
         newcell=newcell_nxt
c
         go to 15
c
  500    continue
c
c       
         call writepar(cdfidp,idxp,pxtmp,iplot,nptotl)
         call writepar(cdfidp,idyp,pytmp,iplot,nptotl)
         call writepar(cdfidp,idzp,pztmp,iplot,nptotl)
         call writepar(cdfidp,idcolor,colortmp,iplot,nptotl)
         call writepar(cdfidp,idpid,pidtmp,iplot,nptotl)
         call ncredf(cdfidp,ircode)
         call ncapt(cdfidp,idxp,'numtot',NCLONG,nrg,numtot(1),ircode)
         call ncendf(cdfidp,ircode)
c
      end if 
c
c     sync files:
      call ncsnc(cdfid,ircode)
      call ncsnc(cdfidp,ircode)
      call ncsnc(cdfidh,ircode)
c
      return
      end
c
c---------------------------------------------------------------
      subroutine plotclose
      include 'cplot.com'
      include 'netcdf.inc'
      call ncclos(cdfid,ircode)
      call ncclos(cdfidh,ircode)
      call ncclos(cdfidp,ircode)
      return
      end
c
c---------------------------------------------------------------
      subroutine plotinit(ibp2,jbp2,kbp2,name)
c
c Routine to create netcdf file and define variable, dimensions, etc.
c
      include 'netcdf.inc'
      include 'corgan.com'
      include 'cplot.com'
      integer  dims(4) 
      character*80 name
c
c     initialize counter for plots
      iplot=0
c
c     create netcdf files
      cdfid = nccre('Cel.dat',ncclob,ircode)
      cdfidp = nccre('Celp.dat',ncclob,ircode)
      cdfidh = nccre('Celh.dat',ncclob,ircode)
c
c put name from input file in as global attribute
      ncr=80
      call ncaptc(cdfid,ncglobal,'name',ncchar,80,name,ircode)
      call ncaptc(cdfidp,ncglobal,'name',ncchar,80,name,ircode)
      call ncaptc(cdfidh,ncglobal,'name',ncchar,80,name,ircode)
c put date, time as global attributes
      call ncapt(cdfid,ncglobal,'date',ncfloat,1,ddate,ircode)
      call ncapt(cdfid,ncglobal,'hour',ncfloat,1,hour,ircode)
      call ncapt(cdfidp,ncglobal,'date',ncfloat,1,ddate,ircode)
      call ncapt(cdfidp,ncglobal,'hour',ncfloat,1,hour,ircode)
      call ncapt(cdfidh,ncglobal,'date',ncfloat,1,ddate,ircode)
      call ncapt(cdfidh,ncglobal,'hour',ncfloat,1,hour,ircode)
c
c define dimensions & get netcdf dimension ids (unlimited must be first?)
      timdimh = ncddef(cdfidh,'time',ncunlim,ircode)
      timdim = ncddef(cdfid,'time',ncunlim,ircode)
      timdimp = ncddef(cdfidp,'time',ncunlim,ircode)
      nxdim = ncddef(cdfid,'nx',ibp2,ircode)
      nydim = ncddef(cdfid,'ny',jbp2,ircode)
      nzdim = ncddef(cdfid,'nz',kbp2,ircode)
      npdim = ncddef(cdfidp,'np',npart,ircode)
c
c define time variable & get id
      dims(1) = timdim
      idtime = ncvdef(cdfid,'t',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtimeh = ncvdef(cdfidh,'t',NCFLOAT,1,dims,ircode)
      dims(1) = timdimp
      idtimep = ncvdef(cdfidp,'t',NCFLOAT,1,dims,ircode)
c
c define history variables
      dims(1) = timdimh
      idtote = ncvdef(cdfidh,'tote',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtoti = ncvdef(cdfidh,'toti',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtginte = ncvdef(cdfidh,'tginte',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtotk = ncvdef(cdfidh,'totk',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtotb = ncvdef(cdfidh,'totb',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgbsq = ncvdef(cdfidh,'tgbsq',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgbsqx = ncvdef(cdfidh,'tgbsqx',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgbsqy = ncvdef(cdfidh,'tgbsqy',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgbsqz = ncvdef(cdfidh,'tgbsqz',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idj_par = ncvdef(cdfidh,'jpar',
     &     NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idj_per = ncvdef(cdfidh,'jper',
     &     NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgkex = ncvdef(cdfidh,'tgkex',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgkey = ncvdef(cdfidh,'tgkey',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgkez = ncvdef(cdfidh,'tgkez',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtrfsolar = ncvdef(cdfidh,'rfsolar',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgheat = ncvdef(cdfidh,'tgheat',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgheat_par = ncvdef(cdfidh,'tgheat_par',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idtgheat_per = ncvdef(cdfidh,'tgheat_per',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idgcur_max = ncvdef(cdfidh,'gcur_max',NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idflux = ncvdef(cdfidh,'flux',
     &     NCFLOAT,1,dims,ircode)
      dims(1) = timdimh
      idsurface_area = ncvdef(cdfidh,'surface_area',
     &     NCFLOAT,1,dims,ircode)
c
c define grid variables x,y,z (if(plots) always write these)
      dims(1)=nxdim
      dims(2)=nydim
      dims(3)=nzdim
      dims(4)=timdim
      idnx = ncvdef(cdfid,'x',NCFLOAT,4,dims,ircode)
      idny = ncvdef(cdfid,'y',NCFLOAT,4,dims,ircode)
      idnz = ncvdef(cdfid,'z',NCFLOAT,4,dims,ircode)
c
c define other grid variables, as required:
c
      dims(1)=nxdim
      dims(2)=nydim
      dims(3)=nzdim
      dims(4)=timdim
c
c     velocity
      if (iout(1).ne.0)  then
         idu = ncvdef(cdfid,'uvel',NCFLOAT,4,dims,ircode)
         idv = ncvdef(cdfid,'vvel',NCFLOAT,4,dims,ircode)
         idw = ncvdef(cdfid,'wvel',NCFLOAT,4,dims,ircode)
      end if
c
c     density
      if (iout(2).ne.0) then
        idrho = ncvdef(cdfid,'density',NCFLOAT,4,dims,ircode)
      end if
c
c     pressure
      if (iout(3).ne.0) then
        idp = ncvdef(cdfid,'pressure',NCFLOAT,4,dims,ircode)
      end if
c
c     curlu
      if (iout(4).ne.0) then
        idcurlx = ncvdef(cdfid,'curlx',NCFLOAT,4,dims,ircode)
        idcurly = ncvdef(cdfid,'curly',NCFLOAT,4,dims,ircode)
        idcurlz = ncvdef(cdfid,'curlz',NCFLOAT,4,dims,ircode)
      end if
c
c     divu
      if (iout(5).ne.0) then
        iddiv= ncvdef(cdfid,'divu',NCFLOAT,4,dims,ircode)
      end if
c
c     chden
      if (iout(10).ne.0) then
        idchden= ncvdef(cdfid,'chden',NCFLOAT,4,dims,ircode)
      end if
c
c     elpot
      if (iout(10).ne.0) then
        idelpot= ncvdef(cdfid,'elpot',NCFLOAT,4,dims,ircode)
      end if
c
c     streamf
      if (iout(10).ne.0) then
        idstreamf= ncvdef(cdfid,'streamf',NCFLOAT,4,dims,ircode)
      end if	  
c
c     vorticity
      if (iout(10).ne.0) then
        idvort= ncvdef(cdfid,'vorticity',NCFLOAT,4,dims,ircode)
      end if	  
c
c     divergence
      if (iout(10).ne.0) then
        iddiverg= ncvdef(cdfid,'divergence',NCFLOAT,4,dims,ircode)
      end if	  
c
c     vorticity
      if (iout(10).ne.0) then
        idpotdiv= ncvdef(cdfid,'potdiv',NCFLOAT,4,dims,ircode)
      end if	  
c
c     sie
      if (iout(6).ne.0) then
        idsie= ncvdef(cdfid,'sie',NCFLOAT,4,dims,ircode)
      end if
c
c     current
      if (iout(8).ne.0) then
        idjx= ncvdef(cdfid,'jx',NCFLOAT,4,dims,ircode)
        idjy= ncvdef(cdfid,'jy',NCFLOAT,4,dims,ircode)
        idjz= ncvdef(cdfid,'jz',NCFLOAT,4,dims,ircode)
      end if
c
c     magnetic field
      if (iout(9).ne.0) then
        idbx = ncvdef(cdfid,'bx',NCFLOAT,4,dims,ircode)
        idby = ncvdef(cdfid,'by',NCFLOAT,4,dims,ircode)
        idbz = ncvdef(cdfid,'bz',NCFLOAT,4,dims,ircode)
      end if
c
c     electric field
      if (iout(10).ne.0) then
        idex = ncvdef(cdfid,'ex',NCFLOAT,4,dims,ircode)
        idey = ncvdef(cdfid,'ey',NCFLOAT,4,dims,ircode)
        idez = ncvdef(cdfid,'ez',NCFLOAT,4,dims,ircode)
      end if
c
c     weight (for adaptive grid)
      if (iout(11).ne.0) then
        idwght = ncvdef(cdfid,'weight',NCFLOAT,4,dims,ircode)
      end if
c
c     particles:
      if (iout(20).ne.0) then
c  define particle positions and id:
      dims(1)=npdim
      dims(2)=timdimp
      idxp = ncvdef(cdfidp,'xp',NCFLOAT,2,dims,ircode)
      idyp = ncvdef(cdfidp,'yp',NCFLOAT,2,dims,ircode)
      idzp = ncvdef(cdfidp,'zp',NCFLOAT,2,dims,ircode)
      idcolor = ncvdef(cdfidp,'color',NCFLOAT,2,dims,ircode)
      idpid = ncvdef(cdfidp,'pid',NCFLOAT,2,dims,ircode)
      end if
c
c history variables:
      if (iout(21).ne.0) then
      end if
c
c end definition section 
      call ncendf(cdfid,ircode)
      call ncendf(cdfidp,ircode)
      call ncendf(cdfidh,ircode)
c
      return
      end
c---------------------------------------------------------------
         subroutine writepar(cdfid,idvar,var,iplot,npart)

         integer cdfid
         integer istart(2),icount(2)
         real*4 var(npart)
       
c generic netcdf write of particle data assuming start at 1
         istart(1)=1
         istart(2)=iplot
c
         icount(1)=npart
         icount(2)=1
c
         call ncvpt(cdfid,idvar,istart,icount,var,ircode)
c
        return
        end
c---------------------------------------------------------------

         subroutine writevar(cdfid,idvar,var,iplot,nxp,nyp,nzp)

         integer cdfid
         integer istart(4),icount(4)
         real*4 var(nxp,nyp,nzp)
       
c generic netcdf write of grid quantities assuming start at 1
         istart(1)=1
         istart(2)=1
         istart(3)=1
         istart(4)=iplot
c
         icount(1)=nxp
         icount(2)=nyp
         icount(3)=nzp
         icount(4)=1
c
         call ncvpt(cdfid,idvar,istart,icount,var,ircode)
c
        return
        end
c---------------------------------------------------------------
