try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

flip = LegacyVTKReader( FileNames=['/Users/Gianni/Documents/workspace/FlipMHD/flip1.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip2.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip3.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip4.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip5.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip6.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip7.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip8.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip9.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip10.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip11.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip12.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip13.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip14.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip15.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip16.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip17.vtk', '/Users/Gianni/Documents/workspace/FlipMHD/flip18.vtk'] )

AnimationScene1 = GetAnimationScene()
AnimationScene1.EndTime = 17.0
AnimationScene1.PlayMode = 'Snap To TimeSteps'

RenderView1 = GetRenderView()
DataRepresentation18 = Show()
DataRepresentation18.ScalarOpacityUnitDistance = 0.17320508075688776
DataRepresentation18.Representation = 'Outline'
DataRepresentation18.EdgeColor = [0.0, 0.0, 0.50000762951094835]

RenderView1.CameraPosition = [0.44999998807907104, -2.5614586256006264, 0.44999998807907104]
RenderView1.CameraClippingRange = [2.0858440512655489, 4.183880463022966]

Slice7 = Slice( SliceType="Plane" )

Slice7.SliceOffsetValues = [0.0]
Slice7.SliceType.Origin = [0.45000000000000001, 0.45000000000000001, 0.45000000000000001]
Slice7.SliceType = "Plane"

a1_density_PVLookupTable = GetLookupTableForArray( "density", 1 )

DataRepresentation19 = Show()
DataRepresentation19.EdgeColor = [0.0, 0.0, 0.50000762951094835]
DataRepresentation19.ColorAttributeType = 'POINT_DATA'
DataRepresentation19.ColorArrayName = 'density'
DataRepresentation19.Texture = []
DataRepresentation19.LookupTable = a1_density_PVLookupTable

Slice7.SliceType.Normal = [0.0, 1.0, 0.0]

AnimationScene1.AnimationTime = 1.0

RenderView1.ViewTime = 1.0

a1_density_PVLookupTable.RGBPoints = [0.9999958872795105, 0.23000000000000001, 0.29899999999999999, 0.754, 0.99999988079071045, 0.70599999999999996, 0.016, 0.14999999999999999]

AnimationScene1.AnimationTime = 2.0

RenderView1.ViewTime = 2.0

AnimationScene1.AnimationTime = 3.0

RenderView1.ViewTime = 3.0

AnimationScene1.AnimationTime = 4.0

RenderView1.ViewTime = 4.0

AnimationScene1.AnimationTime = 5.0

RenderView1.ViewTime = 5.0

AnimationScene1.AnimationTime = 6.0

RenderView1.ViewTime = 6.0

AnimationScene1.AnimationTime = 7.0

RenderView1.ViewTime = 7.0

AnimationScene1.AnimationTime = 8.0

RenderView1.ViewTime = 8.0

AnimationScene1.AnimationTime = 9.0

RenderView1.ViewTime = 9.0

AnimationScene1.AnimationTime = 10.0

RenderView1.ViewTime = 10.0

AnimationScene1.AnimationTime = 11.0

RenderView1.ViewTime = 11.0

AnimationScene1.AnimationTime = 12.0

RenderView1.ViewTime = 12.0

AnimationScene1.AnimationTime = 13.0

RenderView1.ViewTime = 13.0

AnimationScene1.AnimationTime = 14.0

RenderView1.ViewTime = 14.0

AnimationScene1.AnimationTime = 16.0

RenderView1.ViewTime = 16.0

AnimationScene1.AnimationTime = 17.0

RenderView1.ViewTime = 17.0

AnimationScene1.AnimationTime = 0.0

RenderView1.ViewTime = 0.0

a1_density_PVLookupTable.RGBPoints = [0.0, 0.23000000000000001, 0.29899999999999999, 0.754, 1.0280463695526123, 0.70599999999999996, 0.016, 0.14999999999999999]

AnimationScene1.AnimationTime = 1.0

RenderView1.ViewTime = 1.0

AnimationScene1.AnimationTime = 2.0

RenderView1.ViewTime = 2.0

AnimationScene1.AnimationTime = 3.0

RenderView1.ViewTime = 3.0

AnimationScene1.AnimationTime = 5.0

RenderView1.ViewTime = 5.0

AnimationScene1.AnimationTime = 6.0

RenderView1.ViewTime = 6.0

AnimationScene1.AnimationTime = 7.0

RenderView1.ViewTime = 7.0

AnimationScene1.AnimationTime = 8.0

RenderView1.ViewTime = 8.0

AnimationScene1.AnimationTime = 9.0

RenderView1.ViewTime = 9.0

AnimationScene1.AnimationTime = 10.0

RenderView1.ViewTime = 10.0

AnimationScene1.AnimationTime = 11.0

RenderView1.ViewTime = 11.0

AnimationScene1.AnimationTime = 12.0

RenderView1.ViewTime = 12.0

AnimationScene1.AnimationTime = 13.0

RenderView1.ViewTime = 13.0

AnimationScene1.AnimationTime = 14.0

RenderView1.ViewTime = 14.0

AnimationScene1.AnimationTime = 15.0

RenderView1.ViewTime = 15.0

AnimationScene1.AnimationTime = 16.0

RenderView1.ViewTime = 16.0

AnimationScene1.AnimationTime = 17.0

RenderView1.ViewTime = 17.0

SetActiveSource(flip)
Calculator2 = Calculator()

Calculator2.AttributeMode = 'point_data'

Calculator2.Function = 'bx*iHat+by*jHat+bz*kHat'

DataRepresentation20 = Show()
DataRepresentation20.Texture = []
DataRepresentation20.ScalarOpacityUnitDistance = 0.17320508075688776
DataRepresentation20.Representation = 'Outline'
DataRepresentation20.EdgeColor = [0.0, 0.0, 0.50000762951094835]

DataRepresentation18.Visibility = 0

StreamTracer2 = StreamTracer( SeedType="Point Source" )

StreamTracer2.SeedType.Center = [0.45000000000000001, 0.45000000000000001, 0.45000000000000001]
StreamTracer2.SeedType.Radius = 0.090000000000000011
StreamTracer2.Vectors = ['POINTS', 'Result']
StreamTracer2.SeedType = "Point Source"
StreamTracer2.MaximumStreamlineLength = 0.90000000000000002

StreamTracer2.SeedType.NumberOfPoints = 100

DataRepresentation21 = Show()
DataRepresentation21.EdgeColor = [0.0, 0.0, 0.50000762951094835]
DataRepresentation21.ColorAttributeType = 'POINT_DATA'
DataRepresentation21.ColorArrayName = 'density'
DataRepresentation21.Texture = []
DataRepresentation21.LookupTable = a1_density_PVLookupTable

Render()
