      subroutine itercg_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     dt,error,r4pi,eps,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     roq,bxq,byq,bzq,rotil,bxtil,bytil,bztil,ptil,
     &     arotil,abxtil,abytil,abztil,paro,pabx,paby,pabz,
     &     aroq,abxq,abyq,abzq,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     u,v,w,mv,gm1,numit,itmax,totnrg,
     &     ul,vl,wl,vol,csq,p,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,
     &     s,sbx,sby,sbz,eh,
     &     dro,dbx,dby,dbz,
     &     celerr)
c
C***BEGIN PROLOGUE ITERCG
C***DATE WRITTEN 911225
C***REVISION DATE 920328
C***KEYWORD NONLINEAR CONJUGATE RESIDUAL ITERATION
C***AUTHOR BRACKBILL, J. U. (LOS ALAMOS NATIONAL LABORATORY)
C***PURPOSE
c     To solve the implicit MHD equations, including the mass continuity,
c     Faraday's, and momentum equations, Ap=y
c     where A depends upon ro, Bx, By, Bz
C
C     xnorm is the L1 norm of the solution vector
C     ynorm is the L1 norm of the RHS
C     dxnorm is the L1 norm of the adjustment vector
C     rnorm is the L1 norm of the residual
C     eps is the error tolerance for convergence
C     SASUM is a "BLAS" routine to sum the absolute vale of a vector
C     SDOT is a "BLAS" routine to form the inner product of two vectors
C***ROUTINES CALLED SENSITIV LUDECOMP RESIDUM PRECOND
C***END PROLOGUE ITERCG
c
      logical resist
c
      integer ncells,ijkcell(*),iwid,jwid,kwid,numit,itmax,
     &     ijktmp1(*),
     &     nvtx,ijkvtx(*),ibar,jbar,kbar
c
      real
     &     dt,error,r4pi,eps,
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     a11(*),a12(*),a13(*),a14(*),
     &     a21(*),a22(*),a23(*),a24(*),
     &     a31(*),a32(*),a33(*),a34(*),
     &     a41(*),a42(*),a43(*),a44(*),
     &     roq(*),bxq(*),byq(*),bzq(*),
     &     rotil(*),bxtil(*),bytil(*),bztil(*),ptil(*),
     &     arotil(*),abxtil(*),abytil(*),abztil(*),
     &     paro(*),pabx(*),paby(*),pabz(*),
     &     aroq(*),abxq(*),abyq(*),abzq(*),
     &     gradx(*),grady(*),gradz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     u(*),v(*),w(*),mv(*),gm1,totnrg,
     &     ul(*),vl(*),wl(*),vol(*),csq(*),p(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),
     &     s(*),sbx(*),sby(*),sbz(*),eh(*),
     &     dro(*),dbx(*),dby(*),dbz(*),
     &     celerr(*),jx(*),jy(*),jz(*),vvol(*)
c
      real lambda_ro,lambda_bx,lambda_by,lambda_bz
c
 
      numit=0.0
c
c     calculate L1 norm of RHS
c
      ynorm=0.0
c
      do 2001 n=1,ncells
      ijk=ijkcell(n)
      ynorm=ynorm
     &     +(abs(ro(ijk))+abs(bxn(ijk))+abs(byn(ijk))+abs(bzn(ijk)))
     &     *vol(ijk)
 2001 continue
c     calculate the Jacobi matrix for pre-conditioner, P
c
c
      delta=1.e-8
      dt_test=dt
      call sensitiv_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt_test,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil,
     &     s,sbx,sby,sbz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44)

c
c     perform LU decomposition on Jacobi matrix
c    
c
      call ludecomp(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44)
c
      do 2009 n=1,ncells
c
      paro(n)=0.0
      pabx(n)=0.0
      paby(n)=0.0
      pabz(n)=0.0
c
c     calculate symmetrization matrix element
c
      ijk=ijkcell(n)
c      Bsq=bxn(ijk)**2+byn(ijk)**2+bzn(ijk)**2
c      eh(n)=sqrt((gm1+1.)*p(ijk)/(Bsq+1.e-20))
      eh(n)=1.
c*
c
 2009 continue
c
      bottom_ro=1.e-20
      bottom_bx=1.e-20
      bottom_by=1.e-20
      bottom_bz=1.e-20
c
c     ********************************************************************
c
c     BEGIN ITERATION
c
c     *******************************************************************
c
   25 continue
      numit=numit+1
c
c     residum evaluates the residual error in the solution
c     with current values of the magnetic field and pressure
c
c    residum calls:
c      -stresmhd ... evaluates Maxwell+fluid stress tensor
c      -accelb ... solves momnetum equation including magnetic
c                  field, pressure, viscosity, and body forces
c
c     residum returns s,sbx,sby,sbz ... residual errors in
c     continuity and Faraday's equations
c
c
      rnormsav=rnorm
c
c
      call residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)

c     compute L1 norm of residue
c
      rnorm=sasum(ncells,s,1)
     &     +sasum(ncells,sbx,1)
     &     +sasum(ncells,sby,1)
     &     +sasum(ncells,sbz,1)
c
      rnorms=sasum(ncells,s,1)
      rnormbx=sasum(ncells,sbx,1)
      rnormby=sasum(ncells,sby,1)
      rnormbz=sasum(ncells,sbz,1)
     &     rnorms,rnormbx,rnormby,rnormbz
c     changed by Brackbill 2 May 1996
      if(numit.gt.10.and.rnorm.gt.YNORM) then
      numit=itmax+1
      return
      endif
c
c     call preconditioner, which executes one step of Newton iteration
c     using Jacobi matrix calculated in SENSITIV
c     returns dro, dBx,dBy,dBz ... adjustment to iterates=trial search dir.
c
c
      call precond(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     s,sbx,sby,sbz,
     &     dro,dbx,dby,dbz)
c
c
c
      do 2029 m=1,ncells
      ijk=ijkcell(m)
c
c
c     increment the iteration variables to calculate trial values
c
      rotil(ijk)=rol(ijk)+dro(m)
      bxtil(ijk)=bxl(ijk)+dbx(m)
      bytil(ijk)=byl(ijk)+dby(m)
      bztil(ijk)=bzl(ijk)+dbz(m)
      ptil(ijk)=p(ijk)+csq(ijk)*dro(m)
      ptil(ijk)=amax1(ptil(ijk),0.0)
      rotil(ijk)=amax1(rotil(ijk),0.0)
c
2029  continue
c
c
c     calculate Aqtilde, the residual with trial values
c
c
c
      call residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rotil,bxtil,bytil,bztil,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil)

c
c     check the Jacobi step
c
      if(numit.gt.1.and.rnorm.gt.rnormsav) then
      rnormck=sasum(ncells,arotil,1)
     &       +sasum(ncells,abxtil,1)
     &       +sasum(ncells,abytil,1)
     &       +sasum(ncells,abztil,1)
c
      endif
c     calculate change in the residual due to change in iterate
c     values
c
      do 2049 n=1,ncells
c
      arotil(n)=-s(n)+arotil(n)
      abxtil(n)=-sbx(n)+abxtil(n)
      abytil(n)=-sby(n)+abytil(n)
      abztil(n)=-sbz(n)+abztil(n)
c
c     symmetrize
c
      arotil(n)=arotil(n)/(eh(n)+1.e-20)
c
 2049 continue
c
c     calculate lambda
c
      lambda_ro=0.0
      lambda_bx=0.0
      lambda_by=0.0
      lambda_bz=0.0
c     lambda orthogonalizes new direction wrt to old direction vectors,
c     lambda(numit)=(Aqtilde, PAq(numit-1))/(Aq(numit-1),PAq(numit-1))
c     q is change in iterate, qtil is change calculated by pre-conditioner
c     when numit=0, denominator and numerator=0
c
      lambda_ro=sdot(ncells,arotil,1,paro,1)
         lambda_bx=+sdot(ncells,abxtil,1,pabx,1)
         lambda_by=+sdot(ncells,abytil,1,paby,1)
         lambda_bz=+sdot(ncells,abztil,1,pabz,1)
c
      lambda_ro=lambda_ro/(bottom_ro+1.e-20)
      lambda_bx=lambda_bx/(bottom_bx+1.e-20)
      lambda_by=lambda_by/(bottom_by+1.e-20)
      lambda_bz=lambda_bz/(bottom_bz+1.e-20)
c
c      if(numit.gt.5.and.rnorm.gt.rnormsav) then
c      endif
c
c     calculate q and Aq
c
      do 2079 n=1,ncells
c
      ijk=ijkcell(n)
c
      roq(n)=dro(n)-lambda_ro*roq(n)
      bxq(n)=dbx(n)-lambda_bx*bxq(n)
      byq(n)=dby(n)-lambda_by*byq(n)
      bzq(n)=dbz(n)-lambda_bz*bzq(n)
c
      aroq(n)=arotil(n)-lambda_ro*aroq(n)
      abxq(n)=abxtil(n)-lambda_bx*abxq(n)
      abyq(n)=abytil(n)-lambda_by*abyq(n)
      abzq(n)=abztil(n)-lambda_bz*abzq(n)
c
c     symmetrize
c
      s(n)=s(n)/(eh(n)+1.e-20)
c
 2079 continue
c
c     calculate PAq
c
c
      call precond(ncells,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44,
     &     aroq,abxq,abyq,abzq,
     &     paro,pabx,paby,pabz)
c
c     calculate alpha to orthogonalize conjugate vector
c     alpha(numit)=-(A(p-p(numit),PAq(numit))/(Aq(numit),PAq(numit))
c     note: bottom=denominator will be used to calculate
c     lambda next iteration
c
c
      alpha_ro=-sdot(ncells,s,1,paro,1)
      alpha_bx=-sdot(ncells,sbx,1,pabx,1)
      alpha_by=-sdot(ncells,sby,1,paby,1)
      alpha_bz=-sdot(ncells,sbz,1,pabz,1)
cc
      bottom_ro=sdot(ncells,aroq,1,paro,1)
      bottom_bx=sdot(ncells,abxq,1,pabx,1)
      bottom_by=sdot(ncells,abyq,1,paby,1)
      bottom_bz=sdot(ncells,abzq,1,pabz,1)
c
c
c
      alpha_ro=alpha_ro/(bottom_ro+1.e-50)
      alpha_bx=alpha_bx/(bottom_bx+1.e-50)
      alpha_by=alpha_by/(bottom_by+1.e-50)
      alpha_bz=alpha_bz/(bottom_bz+1.e-50)
c
c     calculate new values for iteration variables
c
cdir$ ivdep
      do 2099 n=1,ncells
c
      ijk=ijkcell(n)
c
      rol(ijk)=rol(ijk)+alpha_ro*roq(n)
      bxl(ijk)=bxl(ijk)+alpha_bx*bxq(n)
      byl(ijk)=byl(ijk)+alpha_by*byq(n)
      bzl(ijk)=bzl(ijk)+alpha_bz*bzq(n)
      p(ijk)=p(ijk)+alpha_ro*csq(ijk)*roq(n)
      p(ijk)=amax1(p(ijk),0.)
      rol(ijk)=amax1(rol(ijk),0.0)
c
 2099 continue
c
c     calculate L1 norm of adjustments
c
      dxnorm=sasum(ncells,roq,1)
     &      +sasum(ncells,bxq,1)
     &      +sasum(ncells,byq,1)
     &      +sasum(ncells,bzq,1)
c
      xnorm=0.0
c
      do 2059 n=1,ncells
      ijk=ijkcell(n)
      xnorm=xnorm
     &     +abs(rol(ijk))+abs(bxl(ijk))+abs(byl(ijk))+abs(bzl(ijk))
 2059 continue
c
c     when both error and change in iterate are both sufficiently small,
c     iteration has converged
c
      ratio=rnorm/(ynorm+1.e-50)
      xratio=dxnorm/(xnorm+1.e-50)
c
      if(xratio.le.eps.and.ratio.le.eps.and.numit.gt.2) go to 1025
C
      if(numit.gt.itmax) go to 9925
      go to 25
 9925 continue
      write(6,9926) numit,ratio
 9926 format(" itercg failed to converge, numit=",i4,"ratio=",1pe10.2)
      return
 1025 continue
c
      return
      end
