      subroutine sensitiv_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,asq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil,
     &     s,sbx,sby,sbz,
     &     a11,a12,a13,a14,
     &     a21,a22,a23,a24,
     &     a31,a32,a33,a34,
     &     a41,a42,a43,a44)
c
c     a routine to calculate the Jacobian for the 3D MHD equations
c
c     the elements are calculated by numerical differentiation
c
      logical resist
c
      integer ncells,ijkcell(*),iwid,jwid,kwid,
     &     ijktmp1(*),ntmp1,ncount,nlist(*),
     &     nvtx,ijkvtx(*),ibar,jbar,kbar
c
      real
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     r4pi,dt,vol(*),ul(*),vl(*),wl(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     gradx(*),grady(*),gradz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),p(*),asq(*),
     &     s(*),sbx(*),sby(*),sbz(*),
     &     arotil(*),abxtil(*),abytil(*),abztil(*),ptil(*),
     &     rotil(*),bxtil(*),bytil(*),bztil(*),
     &     a11(*),a12(*),a13(*),a14(*),
     &     a21(*),a22(*),a23(*),a24(*),
     &     a31(*),a32(*),a33(*),a34(*),
     &     a41(*),a42(*),a43(*),a44(*),
     &     jx(*),jy(*),jz(*),vvol(*)
c
      do n=1,ncells
      ijk=ijkcell(n)
      nlist(ijk)=n
      enddo
c
c     calculate reference values for residual errors
c
      call residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c     construct eight lists of cells
c     each member of a list shares no vertices with any other member
c     the variation will thus be independent
c
      ncount=0
c
      do kstart=2,3
      do jstart=2,3
      do istart=2,3
c
      ntmp1=0
c
      do k=kstart,kbar+1,2
     	 do j=jstart,jbar+1,2
 		do i=istart,ibar+1,2
c
                ntmp1=ntmp1+1
      		ijktmp1(ntmp1)=(k-1)*kwid+(j-1)*jwid+(i-1)*iwid+1
c
      		enddo
     	 enddo
      enddo

c     calculate elements in the first column
c
      do n=1,ncells
c
      ijk=ijkcell(n)
c
      rotil(ijk)=rol(ijk)
      ptil(ijk)=p(ijk)
      enddo
c
      do n=1,ntmp1
c
      ijk=ijktmp1(n)
c
      delta_ro=delta*(1.+abs(rol(ijk)))
      rotil(ijk)=rol(ijk)+delta_ro
      ptil(ijk)=p(ijk)+asq(ijk)*delta_ro
c
      ncount=ncount+1
      enddo
c
      call residu_3dmhd(ntmp1,ijktmp1,iwid,jwid,kwid,
     &     nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rotil,bxl,byl,bzl,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil)
c
      do nt=1,ntmp1
c
      ijk=ijktmp1(nt)
      n=nlist(ijk)
c
      delta_ro=delta*(1.+abs(rol(ijk)))
c
      a11(n)=(arotil(n)-s(n))/delta_ro
      a21(n)=(abxtil(n)-sbx(n))/delta_ro
      a31(n)=(abytil(n)-sby(n))/delta_ro
      a41(n)=(abztil(n)-sbz(n))/delta_ro
c
      enddo
c
c
      do n=1,ncells
      ijk=ijkcell(n)
      bxtil(ijk)=bxl(ijk)
      enddo
c

c     calculate elements in second column
c
      do n=1,ntmp1
c
      ijk=ijktmp1(n)
c
      delta_bx=delta*(1.+abs(bxl(ijk)))
      bxtil(ijk)=bxl(ijk)+delta_bx
c
      enddo
c
      call residu_3dmhd(ntmp1,ijktmp1,iwid,jwid,kwid,
     &     nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxtil,byl,bzl,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil)
c
      do nt=1,ntmp1
c
      ijk=ijktmp1(nt)
      n=nlist(ijk)
c
      delta_bx=delta*(1.+abs(bxl(ijk)))
c
      a12(n)=(arotil(n)-s(n))/delta_bx
      a22(n)=(abxtil(n)-sbx(n))/delta_bx
      a32(n)=(abytil(n)-sby(n))/delta_bx
      a42(n)=(abztil(n)-sbz(n))/delta_bx
c
      enddo

c
      do n=1,ncells
      ijk=ijkcell(n)
      bytil(ijk)=byl(ijk)
      enddo
c
c     calculate elements in third column
c
c
      do n=1,ntmp1
c
      ijk=ijktmp1(n)
c
      delta_by=delta*(1.+abs(byl(ijk)))
      bytil(ijk)=byl(ijk)+delta_by
c
      enddo
c
      call residu_3dmhd(ntmp1,ijktmp1,iwid,jwid,kwid,
     &     nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,bytil,bzl,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil)
c
      do nt=1,ntmp1
c
      ijk=ijktmp1(nt)
      n=nlist(ijk)
c
      delta_by=delta*(1.+abs(byl(ijk)))
c
      a13(n)=(arotil(n)-s(n))/delta_by
      a23(n)=(abxtil(n)-sbx(n))/delta_by
      a33(n)=(abytil(n)-sby(n))/delta_by
      a43(n)=(abztil(n)-sbz(n))/delta_by
c
      enddo
c
c     calculate elements of fourth column
c
c
      do n=1,ncells
      ijk=ijkcell(n)
      bztil(ijk)=bzl(ijk)
      enddo
c
      do n=1,ntmp1
c
      ijk=ijktmp1(n)
c
      delta_bz=delta*(1.+abs(bzl(ijk)))
      bztil(ijk)=bzl(ijk)+delta_bz
c
      enddo
c
      call residu_3dmhd(ntmp1,ijktmp1,iwid,jwid,kwid,
     &     nlist,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bztil,p,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     arotil,abxtil,abytil,abztil)
c
c
      do nt=1,ntmp1
c
      ijk=ijktmp1(nt)
      n=nlist(ijk)
c
      delta_bz=delta*(1.+abs(bzl(ijk)))
c
      a14(n)=(arotil(n)-s(n))/delta_bz
      a24(n)=(abxtil(n)-sbx(n))/delta_bz
      a34(n)=(abytil(n)-sby(n))/delta_bz
      a44(n)=(abztil(n)-sbz(n))/delta_bz
c
      enddo
c
c     end sub-loops over grid
      enddo
      enddo
      enddo
c
c      do n=1,ncells
c
c      ijk=ijkcell(n)
c
c      write(37,*) 'ijk,n=',ijk,n
c      write(37,*) a11(n),a12(n),a13(n),a14(n)
c      write(37,*) a21(n),a22(n),a23(n),a24(n)
c      write(37,*) a31(n),a32(n),a33(n),a34(n)
c      write(37,*) a41(n),a42(n),a43(n),a44(n)
c
c      enddo

c
      return
      end




