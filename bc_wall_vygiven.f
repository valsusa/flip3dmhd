      subroutine bc_wall(ibp1,jbp1,kbp1,iwid,jwid,kwid,
     c                   dx,dy,dz,x,y,z,
     c                   c5x,c6x,c7x,c8x,
     c                   c5y,c6y,c7y,c8y,
     c                   c5z,c6z,c7z,c8z,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & t,tramp,
     c                   ul,vl,wl)
c
      implicit real*8 (a-h,o-z)
c
c     a routine to impose rigid, free-slip wall conditions
c
       dimension
     c   c5x(*),c6x(*),c7x(*),c8x(*),
     c   c5y(*),c6y(*),c7y(*),c8y(*),
     c   c5z(*),c6z(*),c7z(*),c8z(*),
     c   x(*),y(*),z(*),
     c   ul(*),vl(*),wl(*)
c
      real*8 normx,normy,normz
c
      integer modex_bottom,modey_bottom,ibar,jbar
      ibar=ibp1-1
      jbar=jbp1-1
c
      pi=acos(-1.d0)
      ktop=kbp1+1
      kbot=2
      do 1 j=2,jbp1+1
      do 1 i=2,ibp1+1
          ijk=1+(i-1)*iwid+(j-1)*jwid+(ktop-1)*kwid
          normx=c8x(ijk-kwid)+c5x(ijk-iwid-kwid)
     &         +c6x(ijk-iwid-jwid-kwid)+c7x(ijk-jwid-kwid)
          normy=c8y(ijk-kwid)+c5y(ijk-iwid-kwid)
     &         +c6y(ijk-iwid-jwid-kwid)+c7y(ijk-jwid-kwid)
          normz=c8z(ijk-kwid)+c5z(ijk-iwid-kwid)
     &         +c6z(ijk-iwid-jwid-kwid)+c7z(ijk-jwid-kwid)
c
          udotn=(ul(ijk)*normx+vl(ijk)*normy+wl(ijk)*normz)
     &         /(normx**2+normy**2+normz**2+1.e-10)
c
          ul(ijk)=ul(ijk)-normx*udotn
          vl(ijk)=vl(ijk)-normy*udotn
          wl(ijk)=wl(ijk)-normz*udotn
c      wl(ijk)=0.0
c	ul(ijk)=ul(ijk-kwid)
c	vl(ijk)=vl(ijk-kwid)
c	wl(ijk)=wl(ijk-kwid)
c
      ijk=1+(i-1)*iwid+(j-1)*jwid+(kbot-1)*kwid
c
c	ul(ijk)=ul(ijk+kwid)
c	vl(ijk)=vl(ijk+kwid)
c	wl(ijk)=wl(ijk+kwid)
c
c     tangential velocity can be prescribed
c
      wave_number_x=2.*pi*modex_bottom/(ibar*dx)
      wave_number_y=2.*pi*modey_bottom/(jbar*dy)
      ftime=t/tramp
      if(ftime.ge.1.0) ftime=1.0
c      ftime=sin(2.0*pi*t/tramp)
      fspace=sin(wave_number_x*x(ijk))
c      if(abs(fspace).lt.0.5) then
c	fspace=0.0
c      else if (fspace.ge.0.5) then
c        fspace=fspace-.5
c      else
c        fspace=fspace+.5
c      end if
c
c      vl(ijk)=vwall_bottom
c     &     +vwall_bottom_perturbed*fspace
c      vl(ijk)=vl(ijk)*ftime
c      ul(ijk)=0.0
c      wl(ijk)=0.0
c
          normx=c8x(ijk-kwid)+c5x(ijk-iwid-kwid)
     &         +c6x(ijk-iwid-jwid-kwid)+c7x(ijk-jwid-kwid)
          normy=c8y(ijk-kwid)+c5y(ijk-iwid-kwid)
     &         +c6y(ijk-iwid-jwid-kwid)+c7y(ijk-jwid-kwid)
          normz=c8z(ijk-kwid)+c5z(ijk-iwid-kwid)
     &         +c6z(ijk-iwid-jwid-kwid)+c7z(ijk-jwid-kwid)
c
          udotn=(ul(ijk)*normx+vl(ijk)*normy+wl(ijk)*normz)
     &         /(normx**2+normy**2+normz**2+1.e-10)
c
          ul(ijk)=ul(ijk)-normx*udotn
          vl(ijk)=vl(ijk)-normy*udotn
          wl(ijk)=wl(ijk)-normz*udotn
c
  1   continue
c
c      write(*,*)'bcwall:',vwall_bottom_perturbed
      return
      end
 
