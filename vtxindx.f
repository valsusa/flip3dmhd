*dk vtxindx
      subroutine vtxindx(iwid,jwid,kwid,ijkv)
c
c     a routine to calculate the indices of cell vertices
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkv(8)
c
      ijkv(1)=iwid
      ijkv(2)=iwid+jwid
      ijkv(3)=jwid
      ijkv(4)=0
      ijkv(5)=iwid+kwid
      ijkv(6)=iwid+jwid+kwid
      ijkv(7)=jwid+kwid
      ijkv(8)=kwid
c
      return
      end
