*dk initsub
c*************************************************************
c*                                                           *
      subroutine initiv
c*                                                           *
c*************************************************************
c
      use boundary_module
c
      implicit real*8 (a-h,o-z)

      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'cplot.com'
      include 'flip3d.com'

c
      namelist / celin /initial_equilibrium,read_from_file,bcpz,
     & tfrcn,PRECON,ITMAX,itmag,cartesian,
     & ibar,jbar,kbar,kplas,instab,nflm,EXPLICIT,error,precon,
     & dx,dy,dz,
     &   eps,
     &    resist,resistivity,
     &  NOMAG,periodic_x,periodic_y,periodic_z,
     &     del1,del2,del3,del4,del5,del7,
     & btorus ,bvertcl, efld0, q0,nphist,
     &  numgrid,taug,delta,
     & tmax,tmax0,tmax1,tmax2,tmax3,
     & lpr,itouch,idivb,nwrtp,ncyc2,
     & rpldum,rvacdum,rwalldum,rmajdum,dzdum,
     & gtype,a0,b0,
     & dtdum,tlimd,dcttd,dpttd,
     & dt,tlimd,dcttd,dpttd,twfin,dtmax,
     & dto,dtoc,iout,
     & lama,lamb,lamc,adaptg,newstuf,fields,
     & rhoi,siei,beta,resist,
     & omga,eps,mgeom,mu,lam,
     & tfrcn,zmutrn,zmulon,
     & uwall_bottom,uwall_bottom_perturbed,modex_bottom,
     & vwall_bottom,vwall_bottom_perturbed,modey_bottom,
     & tramp,
     & rpl,rvac,rwall,rmaj,dz,iota,gx,gy,gz,gm1,imp,boundary_bottom,
     & boundary_top,circular_wall,ascale,grid_type_x,grid_type_z,
     & compressible,ringingout,tanthetax,tanthetay,erre0
c
      namelist / reg / nrg,npcelx,npcely,npcelz,
     &     rhr,siep,icoi,utordrft,qom,
     &     bxr,byr,bzr,
     &     xcenter,ycenter,zcenter,rex,rey,rez,rix,riy,riz,
     &     xvi,yvi,zvi,uvi,vvi,wvi
c
cl    *** additional declarations from initiv ***
c
      logical  cntrun
      common /eigdat/ zm,zk,amp,rota,rmax,eigen(100),deigen(100)
      data amp/0./,rota/0./,rmax/.95/
c
c---------------------------------------------------------------
c
c  default values:
      initial_equilibrium=1
      read_from_file=.false.
      bcpz=0
      mgeom=1
      compressible=.true.
      ringingout=.false.
c
	  cntrun=.false.
c
      imp=0
c
c     Cartesian geometry is the default
c
      grid_type_x=1
      grid_type_z=1
      ascale=10.d0
      cartesian=.true.
      nomag=.true.
      resist=.false.
      resistivity=0.0d0
      eps=1.e-5
      itmag=25
      lama=0.
      lamb=0.
      lamc=0.
      adaptg=.false.
      fields=.false.
cdak      call dateh  (ddateh)
cdak      call timeh (hour)
      gm1= .6666667
      r4pi=1.
      dtpdv=1.e20
      periodic_x=.true.
      periodic_y=.true.
      periodic_z=.false.
c
cll   1.  read parameters from cards:
c         --------------------------
c
       write(*,*)'reading celin'
      read(5,celin)
      write(59,celin)
c
c     default plot interval
      ixto=1
      if(dto(ixto).eq.0.) dto(ixto)=twfin/4.0

c
c
cll   2.  check if continuation of previous run:
c         -------------------------------------
      if (ibar.lt.0 .and. jbar.eq.0) stop
      if (ibar.ne.0) goto 30
c
cl    2.1 continuation -- read parameters and all data from tape:
c         ------------------------------------------------------
      cntrun = .true.
c
c
c
c
c
   20 continue
c
      ncyc1= ncyc+1
c
      if (touch)  itouch= 1
c
c
      if (wrtp)   nwrtp = 1
c
      go to 40
c
cl    2.2 read further parameters from data cards:
c         ---------------------------------------
   30 continue
c
      write(*,*)'reading reg'
      read(5,reg)
c
      strait=0.0
      toroid=1.0
      if(rmaj.ge.1.0e+05)  rmaj= 0.
      if(rmaj.eq.0.0)      strait=1.0
      if(rmaj.eq.0.0)      toroid=0.0
c
      touch= .false.
      if (itouch.gt.0)             touch= .true.
c
      wrtp= .false.
      if (nwrtp.gt.0)  wrtp= .true.
c
c
      kvac= kplas+2
      if(.not.cartesian) then
      dx  = rwall
      dy  = rmaj
      a   = rvac
      endif
c
      ncyc1= 1
      if (ncyc2.lt.0)  ncyc2= 77777
c
c
c
   40 continue
c
      if (tmax0.le.0.)  tmax0= 1.
      if (tmax1.le.0.)  tmax1= 1.
      if (tmax2.le.0.)  tmax2= 1.
      if (tmax3.le.0.)  tmax3= 1.
      if (tmax .le.0.)  tmax= (tmax0+tmax1+tmax2)/3.
c
c
cll   3.  print input data
c         ----------------
      kpr= kpt
c
      write (59,celin)
      write(59,reg)
         kpr= kfm
c
c      write(*,*)'continuation run',cntrun
      if (cntrun)  go to 120
c
c
cll   4.  calculate data for run-setup:
c         -----------------------------
      ibp1= ibar+1
      jbp1= jbar+1
      kbp1= kbar+1
      ibp2= ibar+2
      jbp2= jbar+2
      kbp2= kbar+2
      kvac= kplas+2
      kvp = kvac+1
      kvm = kvac-1
      kvm2= kvac-2
      iwid = 1
      jwid = ibp2*iwid
      kwid = jbp2*jwid
      iper = ibar*iwid
      jper = jbar*jwid
      fibar=real(ibar)
      rfibar= 1./fibar
      fjbar=real(jbar)
      rfjbar= 1./fjbar
      fkbar=real(kbar)
      rfkbar= 1./fkbar
      rbjbkb=1./(ibar*jbar*kbar)
      pi=acos(-1.)
      ilim= kbp2*kwid

      if(grid_type.eq.4) then
      if(ascale.lt.1d0) ascale=10d0
      ascale=acos(-1d0)*(1d0-1./ascale)
      end if

cdak      call second(time)
      if (ilim .le. itdim) goto 105
         call exit(1)
  105 continue
c
      if (pars.gt.0.)  itlwd= itlwd+ncr3
      itlwtt= 1.1*itlwd
c
      t= 0.
c      mu= 0.
c      lam= 0.
      tout=t+dto(ixto)
      pttd=t+dpttd
      ncyc=0
      numtd=0
      drbar=0.9*rwall
c      dtpos=dt*0.1
c      dt=dtpos
      dtc= dt
      dtv= dt
      nomnit= 10
      numit = 10
      a0fac=a0/(2.*(1.+a0**2))
      rdt=1./(dt+1.e-20)
      kxi=-1
      colamu=(1.+1.67)/(lam+mu+mu+1.e-10)
      urght=0.
      ulft=0.
      vfrnt=1.
      vbck=1.
      vfrnt=0.
      vbck=0.
      wbtm=0.
      wtp=0.
      thrd=1.0/3.0
      sxth=1.0/6.0
      itrlmp=25
c
c
      h = 2.0*pi/dz
c
c
  120 continue
c
c
cll   5.  define mesh and initialize diagnostics:
c         ---------------------------------------
c
      call celindex(2,ibp1,2,jbp1,2,kbp1,iwid,jwid,kwid,
     &     ijkcell,ijkctmp,ncells,ijkvtx,nvtxkm,nvtx)
c
c
cl    5.1 calculate mesh:
c         --------------
      call mshset
c
c
      return
      end
