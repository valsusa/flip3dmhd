      subroutine J_times_kvec(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp1,ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     delta,r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rol,bxl,byl,bzl,p,csq,
     &     roq,bxq,byq,bzq,
     &     aroq,abxq,abyq,abzq,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     s,sbx,sby,sbz)
c
c     a routine to approximate the Jacobian  times
c     a krylov vector (roq,bxq,byq,bzq) for the 3D MHD equations.
c     the resultant vector is stored in (aroq,abxq,abyq,abzq).
c
c
      logical resist
c
      integer ncells,ijkcell(*),iwid,jwid,kwid,
     &     ijktmp1(*),ijktmp2(*),
     &     nvtx,ijkvtx(*),ibar,jbar,kbar
c
      real
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     r4pi,dt,vol(*),ul(*),vl(*),wl(*),
     &     pixx(*),pixy(*),pixz(*),piyy(*),piyz(*),pizz(*),
     &     gradx(*),grady(*),gradz(*),
     &     exx(*),exy(*),exz(*),eyy(*),eyz(*),ezz(*),
     &     dudx(*),dudy(*),dudz(*),
     &     dvdx(*),dvdy(*),dvdz(*),
     &     dwdx(*),dwdy(*),dwdz(*),
     &     ro(*),bxn(*),byn(*),bzn(*),
     &     rol(*),bxl(*),byl(*),bzl(*),p(*),csq(*),
     &     s(*),sbx(*),sby(*),sbz(*),
     &     ptil(*),
     &     rotil(*),bxtil(*),bytil(*),bztil(*),
     &     roq(*),bxq(*),byq(*),bzq(*),
     &     aroq(*),abxq(*),abyq(*),abzq(*),
     &     jx(*),jy(*),jz(*),vvol(*)
c
      real eps_NK, vec_scale
c
c
c
c  ????????????????????????????????????????????????????????
c
c  status of cdlt,sdlt,straight,dz,delta, resistivity are unkown
c
c  ??????????????????????????????????????????????????????????
c
c****************************************************************************
c
c     calculate eps_NK
c
      eps_NK=0.0e0
      vec_scale=0.0e0
c
      do  n=1,ncells
      ijk=ijkcell(n)
      eps_NK= eps_NK + 
     &     (abs(rol(ijk))+abs(bxl(ijk))+abs(byl(ijk))+abs(bzl(ijk)))
      vec_scale= vec_scale + 
     &     (abs(roq(n))+abs(bxq(n))+abs(byq(n))+abs(bzq(n)))
      enddo
c
      eps_NK = 1.0e-6*eps_NK/vec_scale 
c
c     calculate physics solution vector plus
c     perturbed Krylov vector. 
c
      do  n=1,ncells
      ijk=ijkcell(n)
      ijktmp2(ijk)=n
       rotil(ijk) = rol(ijk) + eps_NK*roq(n)
       bxtil(ijk) = bxl(ijk) + eps_NK*bxq(n)
       bytil(ijk) = byl(ijk) + eps_NK*byq(n)
       bztil(ijk) = bzl(ijk) + eps_NK*bzq(n)
       ptil(ijk)  = p(ijk) + csq(ijk)*eps_NK*roq(n)
c
      enddo
c
c     calculate perturbed values for residual errors
c     and store them temporarily in aroq, abxq, abyq, abzq
c
      call residu_3dmhd(ncells,ijkcell,iwid,jwid,kwid,
     &     ijktmp2,
     &     nvtx,ijkvtx,ibar,jbar,kbar,cdlt,sdlt,strait,dz,
     &     resist,resistivity,jx,jy,jz,vvol,
     &     r4pi,dt,vol,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     pixx,pixy,pixz,piyy,piyz,pizz,
     &     ro,bxn,byn,bzn,
     &     rotil,bxtil,bytil,bztil,ptil,
     &     ul,vl,wl,
     &     gradx,grady,gradz,
     &     exx,exy,exz,eyy,eyz,ezz,
     &     dudx,dudy,dudz,
     &     dvdx,dvdy,dvdz,
     &     dwdx,dwdy,dwdz,
     &     aroq,abxq,abyq,abzq)

c     Now approximate Jacobian matrix times the Krylov vector, q,
c     and store in aroq, abxq, abyq, abzq
c
      do n=1,ncells
c
c
      aroq(n) = (aroq(n) - s(n))/eps_NK
      abxq(n) = (abxq(n) - sbx(n))/eps_NK
      abyq(n) = (abyq(n) - sby(n))/eps_NK
      abzq(n) = (abzq(n) - sbz(n))/eps_NK
c
      enddo
c
c   Return to GMRES with J x q stored in
c   aroq, abxq, abyq, abzq
c
      return
      end




