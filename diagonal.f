      subroutine diagonal_vtx(nvtx,ijkvtx,
     &     iwid,jwid,kwid,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     dt,vol,diag)
c
c     a routine to calculate the diagonal elements of the
c     matrix representing the Laplacian
c
      implicit real*8 (a-h,o-z)
      dimension ijkvtx(*),
     &     c1x(*),c2x(*),c3x(*),c4x(*),c5x(*),c6x(*),c7x(*),c8x(*),
     &     c1y(*),c2y(*),c3y(*),c4y(*),c5y(*),c6y(*),c7y(*),c8y(*),
     &     c1z(*),c2z(*),c3z(*),c4z(*),c5z(*),c6z(*),c7z(*),c8z(*),
     &     vol(*),diag(*)
c
cdir$ ivdep
      do 1 n=1,NVTX
c
      ijk=ijkvtx(n)
c
      diag(ijk)=
     &    +((c1x(ijk-iwid)**2+c1y(ijk-iwid)**2+c1z(ijk-iwid)**2)
     &    /(vol(ijk-iwid)+1.d-50)
     &    +(c2x(ijk-iwid-jwid)**2+c2y(ijk-iwid-jwid)**2
     &    +c2z(ijk-iwid-jwid)**2)/(vol(ijk-iwid-jwid)+1.d-50)
     &    +(c3x(ijk-jwid)**2+c3y(ijk-jwid)**2+c3z(ijk-jwid)**2)
     &    /(vol(ijk-jwid)+1.d-50)
     &    +(c4x(ijk)**2+c4y(ijk)**2+c4z(ijk)**2)/(vol(ijk)+1.d-50)
     &    +(c5x(ijk-iwid-kwid)**2+c5y(ijk-iwid-kwid)**2
     &    +c5z(ijk-iwid-kwid)**2)/(vol(ijk-iwid-kwid)+1.d-50)
     &    +(c6x(ijk-iwid-jwid-kwid)**2
     &    +c6y(ijk-iwid-jwid-kwid)**2+c6z(ijk-iwid-jwid-kwid)**2)
     &    /(vol(ijk-iwid-jwid-kwid)+1.d-50)
     &    +(c7x(ijk-jwid-kwid)**2+c7y(ijk-jwid-kwid)**2
     &    +c7z(ijk-jwid-kwid)**2)/(vol(ijk-jwid-kwid)+1.d-50)
     &    +(c8x(ijk-kwid)**2+c8y(ijk-kwid)**2
     &    +c8z(ijk-kwid)**2)/(vol(ijk-kwid)+1.d-50))
c
    1 continue
c
      return
       end
