      subroutine watev(ncells,ijkcell,iphead,itdim,
     &     pxi,peta,pzta,wate)
c
c     a routine to calculate the interpolation weights for trilinear
c     interpolation
c
      implicit real*8 (a-h,o-z)
c
      dimension ijkcell(*),iphead(*),pxi(0:*),peta(0:*),pzta(0:*),
     &     wate(itdim,*)
c
      do 372 n=1,ncells
c
      ijk=ijkcell(n)
c
      inew=int(pxi(iphead(ijk)))
      jnew=int(peta(iphead(ijk)))
      knew=int(pzta(iphead(ijk)))
c
c
      pxi2=pxi(iphead(ijk))-real(inew)
      peta2=peta(iphead(ijk))-real(jnew)
      pzta2=pzta(iphead(ijk))-real(knew)
c
c     calculate interpolation weights
c
      wi=1.-pxi2
      wip=pxi2
c
      wj=1.-peta2
      wjp=peta2
c
      wk=1.-pzta2
      wkp=pzta2
c
      wate(ijk,1)=wip*wj*wk
      wate(ijk,2)=wip*wjp*wk
      wate(ijk,3)=wi*wjp*wk
      wate(ijk,4)=wi*wj*wk
c
      wate(ijk,5)=wip*wj*wkp
      wate(ijk,6)=wip*wjp*wkp
      wate(ijk,7)=wi*wjp*wkp
      wate(ijk,8)=wi*wj*wkp
c
  372 continue
c
      return
      end
