! Compile with 
! ifort read_netcdf.f90 -lnetcdf

program read_netcdf
  use netcdf
  implicit none
  integer status
 ! This is the name of the data file we will read.
  character (len = 20), parameter :: FILE_NAME = "Celh.dat"     
  character (len = 10) :: fixname ="flip.txt" 
  character (len = 20) :: nome

  integer :: len
  integer :: fid
  integer, parameter :: dimensions = 6
  integer, dimension(dimensions) :: nd
  integer :: ncid, nDims, nVars, nGlobalAtts, unlimdimid,dimid,varid,varid1,varid2
  integer, allocatable :: varidvec(:)
  character (len = 20), allocatable :: nomevec(:)  
  integer  :: xtype, ndimsvar
  integer:: nAtts
  integer :: nx,ny,nz,nt
  integer :: ix,iy,it,is
  real(8) :: dx,dy,dz,value1,value2
  real, dimension(102,102) ::pot
 

 ! Open the file.
 status =  nf90_open(FILE_NAME, nf90_nowrite, ncid)
if (status /= nf90_noerr) call handle_err(status)
 status = nf90_inquire(ncid, nDims, nVars, nGlobalAtts, unlimdimid)
if (status /= nf90_noerr) call handle_err(status)
 write(*,*)nDims, nVars, nGlobalAtts, unlimdimid

do dimid=1,nDims 
 status = nf90_inquire_dimension(ncid, dimid, nome, len)
 write(*,*), nome, len
 nd(dimid)=len
enddo
nt=nd(1)
nVars=nVars+1
allocate(nomevec(nVars))
do varid=1,nVars
 status =  nf90_inquire_variable(ncid, varid, nomevec(varid), xtype, ndimsvar, nd, nAtts)
 write(*,*), nomevec(varid), ndimsvar
 nd(dimid)=len
enddo


status = nf90_inq_varid(ncid, "t", VarId1)
status =  nf90_inquire_variable(ncid, VarId1, nome, xtype, ndimsvar, nd, nAtts)
status = nf90_inq_varid(ncid, "toti", VarId2)
status =  nf90_inquire_variable(ncid, VarId2, nome, xtype, ndimsvar, nd, nAtts)




fid=10
open (unit = fid, file = fixname)
call write_var1D_txt(ncid,fid,nVars,nt)
close(fid)

CALL system('nohup /Applications/Octave.app/Contents/Resources/bin/octave legge.m &')

end  program read_netcdf

 subroutine handle_err(status)
integer :: status
write(*,*)'errore'
stop
end subroutine handle_err

subroutine write_var1D_txt(ncid,fid,nVars,nt)
  use netcdf
  implicit none 
  integer :: status,nt,fid,it,varid,nVars,ncid
  real(8) :: value(nVars)
  integer, dimension(3) ::start
   
do it=1,nt
do varid=1,nVars
status = nf90_get_var(ncid, varid, value(varid), start=(/ it /)) 
enddo
write(fid,*)value
enddo

end subroutine write_var1D_txt