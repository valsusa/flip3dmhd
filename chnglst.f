      subroutine chnglst(np,iphed0,iphed1,link)
c
      implicit real*8 (a-h,o-z)
c
c     a routine for moving a particle from the beginning of one
c     list to the beginning of another
c
c     the arguments of chnglst are:
c     np...the index of a particle changing cells
c     iphed0...header for the donating list
c     iphed1...header for the receiving list
c     link...the pointer for particle np
c
      iphed0=link
      link=iphed1
      iphed1=np
c
      return
      end
