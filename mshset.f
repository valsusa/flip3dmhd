      subroutine mshset
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'blcom.com'
c
      data rhmn/.05/, pkmn/.02/
c
c
c     package for a three-dimensional torus
c     theta increases with i
c     phiang increases with j
c     r increases with k
c     rwall is the minor radius
c     rmaj is the major radius
c     scyllac package
         a = rvac
      do 10 j=1,jbp2
   10 rwz(j) = rwall
c
cll   1.  geometric radial zoning:
         ws= rwall/rvac
      if (abs(rvac*fkbar/(rwall*real(kplas))-1.).le.1.e-06)  go to 3
         ratio= 1.2
      do 1 it= 1,35
         fwos= (ratio**kbar-1.)/(ratio**kvm2-1.)-ws
         fwosp= (real(kbar-kvm2)*ratio**(kbar+kvm2)
     1          -fkbar*ratio**kbar+real(kvm2)*ratio**kvm2)
     2          /(ratio*(ratio**kvm2-1.)**2)
         ratio= ratio-fwos/fwosp
      if (abs(fwos/fwosp).lt.1.e-05)  go to 2
    1 continue
    2 continue
         dr = rvac*(ratio-1.)/(ratio**kvm2-1.)
      go to 4
    3 continue
         ratio= 1.0
         dr= rwall*rfkbar
    4 continue
         rat = ratio
c
c     define rotation, iota
c
      rfkbp2=1./real(kbp2)
c
c
c     define rotation matrix for toroidal geometry
c
      call rotate(ibar,jbar,kbar,
     &     rmaj,dz,strait,iota,istep,
     &     delt,dtheta,dzstr,dphi,
     &     cdlt,sdlt,cdlhf,sdlhf,
     &     cdph,sdph,cdphhf,sdphhf)
c
c
      call gridinit(ibar,jbar,kbar,iwid,jwid,kwid,
     &     delt,dphi,dtheta,dx,dy,dz,rwall,rmaj,dzstr,istep,
     &     del1,del2,del3,del4,del5,del6,del7,
     &     x,y,z,cartesian,xl,xr,yb,yt,ze,zf)
c
c
         kvacx= kvac
      if (touch)  kplas= kbar
      if (touch)  rvac = rwall
         kvac= kplas+2
         kvp = kvac+1
         kvm = kvac-1
         kvm2= kvac-2
c
cll   2.  set boundaries of expanded view into k=1 plane:
         i1jk = 1
         phiang = -dphi
         size = 2.*rvac
      do 99 j=1,jbp2
         ijk = i1jk
         cphi = cos(phiang)
         sphi = sin(phiang)
         xx = rmaj+size
         zz = -size
      do 98 l=1,4
         x(ijk) = xx*cphi
         y(ijk) = xx*sphi
         z(ijk) = zz
      go to (91,92,93,98), l
   91 zz = size
      go to 98
   92 xx = rmaj-size
      go to 98
   93 zz = -size
   98    ijk = ijk+1
         phiang = phiang+dphi
   99    i1jk = i1jk+jwid
c
cll   3.  calculate geometric coefficients:
       call geom(ncells,ijkcell,nvtx,ijkvtx,iwid,jwid,kwid,
     &     periodic_x,periodic_y,periodic_z,
     &     ibar,jbar,kbar,nvtxkm,
     &     cdlt,sdlt,dz,x,y,z,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,
     &     vol,vvol)
c
c      call metric(x,y,z,tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz)
c      call metric(ncells,ijkcell,iwid,jwid,kwid,
c     &     x,y,z,
c     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz,sie)
c
      call metricc(ncells,ijkcell,
     &     c1x,c2x,c3x,c4x,c5x,c6x,c7x,c8x,
     &     c1y,c2y,c3y,c4y,c5y,c6y,c7y,c8y,
     &     c1z,c2z,c3z,c4z,c5z,c6z,c7z,c8z,vol,
     &     tsix,tsiy,tsiz,etax,etay,etaz,nux,nuy,nuz)
c
cll   4.  calculate ic for magnetic fields:
c
c     rigid rotor screw pinch equilibrium
c     beta = total beta w/o poloidal field
c     siei is the specific internal energy on axis
c     floor on rho is rhmn*rho on axis
c     floor on p is pkmn*p0
c
         bzisq = btorus*btorus
         gamma = .5*log(((2.-beta)+2.*sqrt(1.-beta))/beta)
         tanhgm = tanh(gamma)
         phi0 = rq0*btorus*rpl**2*tanhgm/
     1          ((rmaj+strait*dz/2./pi)*(1.+tanhgm))
c
c     compute delta pbar
c
         wsr = -.5*dr
         pbar = 0.
 1010 wsr = wsr+dr
         wsb = (wsr/rpl)**2+gamma
         wsc = tanh(wsb)
         btht = phi0/wsr*(wsc-tanhgm)/(1.-tanhgm)
         pbar = pbar-dr*btht*btht/wsr
      if(wsc.lt.(1.-1.e-4))  go to 1010
         wsr = wsr+.5*dr
         pbar = pbar-2.*(phi0/wsr)**2
c
c     p0 is the pressure on axis
c
         p0 = .5*bzisq*beta-pbar
         pbar = .5*bzisq-pbar
         rho0 = rhoi
      if(siei.ne.0.)  rho0 = p0/(gm1*siei)
c
C
      DUMMY=0.0
C
c
cll   5.  calculate rho and csq:
      do 450 k=2,kbp2
         ijk= 1+(k-1)*kwid
      do 410 j=1,jbp2
         ijkr=ijk+iper
         ipjk=ijk+1
         ipjkr=ipjk+iper
         rho(ijk)=rho(ijkr)
         vol(ijk)=vol(ijkr)
         rho(ipjkr)=rho(ipjk)
         vol(ipjkr)=vol(ipjk)
         ijk=ijk+jwid
  410 continue
         ijk= 1+(k-1)*kwid
      do 420 i=1,ibp2
         ijkb=ijk+jper
         ijpk=ijk+jwid
         ijpkb=ijpk+jper
         rho(ijk)=rho(ijkb)
         vol(ijk)=vol(ijkb)
         rho(ijpkb)=rho(ijpk)
         vol(ijpkb)=vol(ijpk)
         ijk=ijk+1
  420 continue
  450 continue
c
c     *********************************
c     baal3 sets boundary conditions for cell-centered variables
c     ************************************
      call bcc
c
      return
      end
