*cd corgan
c
cll   common /corgan/ ................................. 77/02/26 ....
      parameter (clite=1.,fourpi=1.,cntr=1.0)
      parameter (nhst=5000,ijmx=2000)
      parameter (nsampl=1024)
      parameter (nreg=2)
      parameter (idx=20,idy=20,idz=100)
      parameter (idxg=idx+2,idyg=idy+2,idzg=idz+2)
      parameter (itdim=idxg*idyg*idzg)
      parameter (idxyzg=idxg*idyg*idzg)
      parameter (npart=1000000,npart_object=5000,nspecies_object=1)
      parameter (nreg_object=2)
      parameter (nlist=150)
      logical    touch,frctn,outflg,wrtp,periodic,
     &     newstuf,PRECON,
     &     nomag,expnd
      common/lcorgan/
     &     touch,frctn,outflg,wrtp,periodic,
     &     newstuf,PRECON,nomag,expnd
c
      dimension  idisp(4)
      real*8
     .        dto,dtoc,
     .        dcttd , dpttd , pttd  ,
     .        tout  , tmovi,
     .        pars  , strait, toroid,
     .        tlimd , twfin , tt,
     .        ddate , hour  ,
     .        iota ,
     .        tlm   , stim  , t1    , t2    ,
     .        pflag
      common /real_stuff/ 
     .        dcttd , dpttd , pttd  , 
     .        tout  , tmovi,  
     .        pars  , strait, toroid, 
     .        tlimd , twfin , tt,    
     .        ddate , hour  , 
     .        iota , 
     .        dto(10),   
     .        dtoc(10),  
     .        tlm   , stim  , t1    , t2    ,
     .        pflag  
c
      common /icorgan/
     .        krd   , kpr   , kpt   , kfm   , kpl   , ktpin , ktpout,
     .        jbnm  , itlm  , numtd , mgeom , itlwd ,
     .        itrlmp, 
     .        lpr   , idisp ,
     .        ncyc  , ncyc1 , ncyc2 , 
     .        nmeflm, nflm  ,
     .        ncr1  , ncr2  , ncr3  , ixto   
