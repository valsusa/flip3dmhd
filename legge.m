load flip.txt;
t=flip(2:end,1);
v=flip(2:end,2);
tote=flip(2:end,3);
toti=flip(2:end,4);
tginte=flip(2:end,5);
totk=flip(2:end,6);
totb=flip(2:end,7);
tgbsq=flip(2:end,8);
tgbsqx=flip(2:end,9);
tgbsqy=flip(2:end,10);
tgbsqz=flip(2:end,11);
jpar=flip(2:end,12);
jper =flip(2:end,13);
tgkex=flip(2:end,14);
tgkey=flip(2:end,15);
tgkez =flip(2:end,16);
rfsolar=flip(2:end,17);
tgheat=flip(2:end,18);
tgheat_par =flip(2:end,19);
tgheat_per=flip(2:end,20);
gcur_max=flip(2:end,21);
flux=flip(2:end,22);
surface_area =flip(2:end,23);

system("rm *.png")

plot(t,tote,t,toti,t,totk,t,totb)
legend('E','I','K','B')
print -dpng energy-part.png
plot(t,tginte,t,tgbsq,t,tgkex+tgkey+tgkez)
legend('I','B','K')
print -dpng energy-grid.png
plot(t,tgbsqx,t,tgbsqy,t,tgbsqz)
legend('B-X','B-Y','B-Z')
print -dpng energy-bcomp.png
plot(t,tgkex,t,tgkey,t,tgkez)
legend('K-X','K-Y','K-Z')
print -dpng energy-kcomp.png
plot(t,jpar,t,jper,t,gcur_max)
legend('Jpar','Jper','Max')
print -dpng curr.png
plot(t,tgheat,t,tgheat_par,t,tgheat_per)
legend('Heat','Hpar','Hperp')
print -dpng heat.png
plot(t,rfsolar,t,surface_area)
legend('Recon1','Recon2')
print -dpng recon.png

system("convert *.png figure.pdf")