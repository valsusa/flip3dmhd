      subroutine initial_state(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
c
      use boundary_module
      implicit real*8 (a-h,o-z)
      include 'corgan.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
      include 'cindex.com'

      select case(initial_equilibrium)
      case(1)
       call rope(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(2)
       call fountain(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(3)
       call harris(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(4)
       call harris_90(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(5)
       call stratified(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(6)
       call einaudi(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(7)
       call fadeev(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(8)
       call fangibson(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(9)
       call double_harris(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case(10)
       call fountain_sphere(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
      case default
            call uniform(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
c        write(*,*)'The initial state is uniform'
c     &              'check the input file and try again'
c        stop
      end select

      end subroutine initial_state


      subroutine rope(wspx,wspy,wspz,
     &        bx_rsx,by_rsx,bz_rsx,
     &        p_rsx,xc,xsep,uprof,rhoprof)
c
      use boundary_module
      implicit real*8 (a-h,o-z)
      include 'corgan.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'
      include 'cindex.com'

        pi=acos(-1.d0)

       xc=ibar*dx/2.d0
       yc=jbar*dy/2.d0
       zc=kbar*dz/2.d0

       wid=xc/10.d0*2
       mode=1
       xcp=xc+wid/2.d0*sin(2.d0*pi*wspy*mode/dy/jbar)
       zcp=zc+wid/2.d0*cos(2.d0*pi*wspy*mode/dy/jbar)

        r=sqrt((wspx-xcp)**2+(wspz-zcp)**2)
        th=acos((wspx-xcp)/(r+1.d-10))
       cth=(wspx-xcp)/(r+1.d-10)
       sth=(wspz-zcp)/(r+1.d-10)


       bth0=bxr(1)
       bth=bth0*r/(r**2+wid**2+1.d-10)
       p_rsx=.5d0*bth0**2*wid**2/(r**2+wid**2+1.d-10)**2
       p_rsx=p_rsx+.025d0
c        rhoprof=1d0
              rhoprof=sqrt(p_rsx)
        uprof=1.d0

       rgeom=sqrt((wspx-xc)**2+(wspz-zc)**2)

        if(rgeom.ge.5d0-min(dx,dz)/2d0 .and. circular_wall) then
        rhoprof=1d2
c        p_rsx=p_rsx*1d2
c        bth=bth/1d2
        uprof=0d0
         endif

       by_rsx=byr(1)
       bx_rsx=-sth*bth
       bz_rsx=cth*bth


      end subroutine rope


      subroutine fountain(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=(xr+xl)*.5
         zcntr=rlargz/2.


         bz_ini=bxr(1)*tanh(wspx-xcntr)
         by_ini=-bxr(1)/cosh(wspx-xcntr)
         bx_ini=0.d0


      u_ini=1d0

      p_ini= siep(1) * gm1

      rho_ini = p_ini / siep(1) / gm1

      if(wspz.lt.zcntr/2) then
         rho_ini=rho_ini*1d0
         else
         rho_ini=rho_ini
         end if


         epsilon=0.d-1*bxr(1)

      fkx=pi/rlargx
      fky=2.*pi/rlargy
      fkz=2*pi/rlargz

           bx_ini=bx_ini-epsilon*cos(fkx*(wspx-xcntr))
     &         *(sin(fkz*(wspz-zcntr+zcntr/2))+2*(wspz-zcntr+zcntr/2)*
     &           cos(fkz*(wspz-zcntr+zcntr/2)))*fkz
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr+zcntr/2)**2)
           bz_ini=bz_ini+epsilon*(sin(fkx*(wspx-xcntr))+
     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
     &  *cos(fkz*(wspz-zcntr+zcntr/2))*fkx
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr+zcntr/2)**2)

c      write(*,*)'initia',wspx,wspy,wspz,bz_ini,by_ini
      end subroutine fountain

      subroutine fountain_sphere(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      use boundary_module
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=(xr+xl)*.5
         zcntr=(zf+ze)*.5


         u_ini=1d0

         sinlat=1.d0
         ang_vel=.1d0/(erre0+ze)
         br=bxr(1)
         bf=0d0

         if(grid_type_x.eq.0) then
            br=br*erre0/(erre0+wspz)
            bf=br*(erre0+wspz)*ang_vel*sinlat/u_ini
            sinth=wspx/sqrt((wspz+erre0)**2+wspx**2)
            costh=sqrt(1.d0-sinth**2)
         else
            sinth=0.d0
            costh=1.d0
         end if
         bz_ini=br*costh-bf*sinth
         bx_ini=br*sinth+bf*costh
         by_ini=0.d0
c         bz_ini=bxr(1)*tanh(wspx-xcntr)
c         by_ini=-bxr(1)/cosh(wspx-xcntr)



      p_ini= siep(1) * gm1

      rho_ini = p_ini / siep(1) / gm1

      if(wspz.lt.zcntr/2) then
         rho_ini=rho_ini*1d0
         else
         rho_ini=rho_ini
         end if

      end subroutine fountain_sphere



      subroutine harris(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.

      elle=1d0

         bx_ini=bxr(1)*tanh((wspz-zcntr)/elle)
         by_ini=0d0
         bz_ini=0d0

      epsilon=1d-1*bxr(1)
      fkx=2.*pi/rlargx
      fky=2.*pi/rlargy
      fkz=pi/rlargz
         bz_ini=bz_ini+epsilon*sin(fkx*(wspx-xcntr))
     &             *cos(fkz*(wspz-zcntr))*fkx
         bx_ini=bx_ini+epsilon*cos(fkx*(wspx-xcntr))
     &             *sin(fkz*(wspz-zcntr))*fkz

      p_ini=1d0/10d0+1d0/cosh((wspz-zcntr)/elle)**2
      p_ini = p_ini * bxr(1)**2/2.0 !/2d0 !+0.*7.3452/cosh(wspz-zcntr)
c      p_ini=rhr(1)/10d0*bxr(1)**2+bxr(1)**2/
c     & cosh((wspz-zcntr)/elle)**2/2d0

      rho_ini = p_ini / siep(1) / gm1

      u_ini=0d0

      end subroutine harris



      subroutine harris_90(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.


         bz_ini=bxr(1)*tanh(wspx-xcntr)
         by_ini=0d0 !-byr(1)/cosh(wspz-zcntr)
         bx_ini=0.d0

      epsilon=1d-1*bxr(1)
      fkx=pi/rlargx
      fky=2.*pi/rlargy
      fkz=2d0*pi/rlargz
         bz_ini=bz_ini+epsilon*sin(fkx*(wspx-xcntr))
     &             *cos(fkz*wspz-fkz*zcntr)*fkx
         bx_ini=bx_ini+epsilon*cos(fkx*(wspx-xcntr))
     &             *sin(fkz*wspz-fkz*zcntr)*fkz

      rho_ini=.15d0+bxr(1)**2/cosh(wspx-xcntr)**2/2d0 !+0.*7.3452/cosh(wspz-zcntr)
         p_ini=.15d0+bxr(1)**2/cosh(wspx-xcntr)**2/2d0

      u_ini=1d0



      end subroutine harris_90


      subroutine stratified(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.


         bz_ini=bxr(1)*tanh(wspx-xcntr)
         by_ini=-bxr(1)/cosh(wspx-xcntr)
         bx_ini=0.d0

!         stratified plasma tested with legnth scale: 1, 3, 10
      rho_ini=1d-2+(1d0-tanh((wspz-zcntr/2)/10d0))

         p_ini=1d0
      u_ini=0d0

         epsilon=1.d-1

      fkx=pi/rlargx
      fky=2.*pi/rlargy
      fkz=2*pi/rlargz

!      Formula usata in passato ma con errore
!
!           bx_ini=bx_ini-epsilon*cos(fkx*(wspx-xcntr))
!     &         *(sin(fkz*(wspz-zcntr-zcntr/2))+2*(wspz-zcntr-zcntr/2)*
!     &           cos(fkz*(wspz-zcntr-zcntr/2)))*fkz
!     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr+zcntr/2)**2)
!           bz_ini=bz_ini+epsilon*(sin(fkx*(wspx-xcntr))+
!     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
!     &  *cos(fkz*(wspz-zcntr-zcntr/2))*fkx
!     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr+zcntr/2)**2)

           bx_ini=bx_ini-epsilon*cos(fkx*(wspx-xcntr))
     &         *(sin(fkz*(wspz-zcntr+zcntr/2))+2*(wspz-zcntr+zcntr/2)*
     &           cos(fkz*(wspz-zcntr+zcntr/2)))*fkz
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr+zcntr/2)**2)
           bz_ini=bz_ini+epsilon*(sin(fkx*(wspx-xcntr))+
     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
     &  *cos(fkz*(wspz-zcntr+zcntr/2))*fkx
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr+zcntr/2)**2)

      end subroutine stratified

      subroutine einaudi(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.


         bx_ini=bxr(1)*tanh(wspz-zcntr)
         by_ini=-byr(1)/cosh(wspz-zcntr)
         bz_ini=bzr(1)

      epsilon=1d-1
      fkx=2.*pi/rlargx
      fky=2.*pi/rlargy
      fkz=pi/rlargz
         bz_ini=bz_ini+epsilon*sin(fkx*(wspx-xcntr))
     &             *cos(fkz*wspz-fkz*zcntr)*fkx
         bx_ini=bx_ini+epsilon*cos(fkx*(wspx-xcntr))
     &             *sin(fkz*wspz-fkz*zcntr)*fkz

      rho_ini=1d0 !+0.*7.3452/cosh(wspz-zcntr)
         p_ini=1d0

      u_ini=uvi(1)*tanh(wspz-zcntr)



      end subroutine einaudi

      subroutine uniform(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr,thick,theta

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.

      theta=.05
         bx_ini=bxr(1)*sin(theta)
         by_ini=bxr(1)*cos(theta)
         bz_ini=bzr(1)

      epsilon=1d-5
      fkx=2.*pi/rlargx
      fky=2.*pi/rlargy
      fkz=pi/rlargz
         bz_ini=bz_ini+epsilon*sin(fkx*(wspx-xcntr))
     &             *cos(fkz*wspz-fkz*zcntr)*fkx
         bx_ini=bx_ini+epsilon*cos(fkx*(wspx-xcntr))
     &             *sin(fkz*wspz-fkz*zcntr)*fkz


      rho_ini=1d0 !+0.*7.3452/cosh(wspz-zcntr)
         p_ini=1d0

      thick=3
      u_ini=tanh((wspz-zcntr-thick*.3*sin(2*fkx*(wspx-xcntr)))/thick)



      end subroutine uniform

      subroutine fadeev(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'



      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr,epsil,elle
      real(8) :: fkx, fky, fkz, el_rec

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.

      el_rec=0.645497
      epsil=0.7

      bx_ini = bxr(1)*sinh((wspz-zcntr)/el_rec)/
     &   (epsil*cos((wspx-xcntr)/el_rec) + cosh((wspz-zcntr)/el_rec))
      by_ini = byr(1)
      bz_ini = epsil*bxr(1)*sin((wspx-xcntr)/el_rec)/
     &   (epsil*cos((wspx-xcntr)/el_rec) + cosh((wspz-zcntr)/el_rec))

      epsilon=-1d-1*bxr(1)
      fkx=2.*pi/rlargx
      fky=2.*pi/rlargy
      fkz=pi/rlargz
         bz_ini=bz_ini+epsilon*sin(fkx*(wspx-xcntr))
     &             *cos(fkz*(wspz-zcntr))*fkx
         bx_ini=bx_ini+epsilon*cos(fkx*(wspx-xcntr))
     &             *sin(fkz*(wspz-zcntr))*fkz

      rho_ini=1d0
         p_ini=bxr(1)**2/2*(1.d0-epsil**2)/
     &(epsil*cos((wspx-xcntr)/el_rec) +
     & cosh((wspz-zcntr)/el_rec))**2

      u_ini=0d0


!    Fadeev island chain equilibrium profile


!      flvx=uvi(kr)*flvx*segno + uvip(kr)*flvx*segno*cos(2.*pi/(yt - yb)*
!     & mpert*py(np))
!      flvy=vvi(kr)*flvy*segno + vvip(kr)*flvx*segno*cos(2.*pi/(yt - yb)*
!     & mpert*py(np))
!      flvz=wvi(kr)*flvz*segno + wvip(kr)*flvx*segno*cos(2.*pi/(yt - yb)*
!     & mpert*py(np))

      end subroutine fadeev

      subroutine fangibson(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         ycntr=rlargy/2.
         zcntr=0d0

         rfg=.375d0
         afg=.1d0
         qfg=-1d0
         B0=1d0
         Bt=9d0*B0
c
c         sperical coordinates with axis along y
c     y and z are interchanged from usual
c
         rr=sqrt((wspx-xcntr)**2+(wspy-ycntr)**2+(wspz-zcntr)**2)
         cth=(wspy-ycntr)/(rr+1d-10)
         sth=sqrt(1d0-cth**2)
         cfi=(wspx-xcntr)/(rr*sth+1d-10)
         sfi=(wspz-zcntr)/(rr*sth+1d-10)

         omega2=rr**2+rfg**2-2d0*rr*rfg*sth**2
      omega=sqrt(omega2)

      dAdOm=-qfg*Bt*exp(-omega2/afg**2)*omega
      pOmpth=-2d0/omega*rr*rfg*sth*cth
      pOmpr=1d0/omega*(rr-rfg*sth**2)
         bfi=afg*Bt/(rr*sth+1d-10)*exp(-omega2/afg**2)
         br=dAdOm*pOmpth/(rr**2*sth+1d-10)
         bth=-dAdOm*pOmpr/(rr*sth+1d-10)
!         if(omega2/afg**2.le.1d0) then
!         bfi=Bt
!         else
!         bfi=0d0
!         end if
         bx_ini=-bfi*sfi+cth*cfi*bth+sth*cfi*br
         bz_ini=cfi*bfi+cth*sfi*bth+sth*sfi*br
         by_ini=-sth*bth+cth*br+byr(1)*exp(-(wspz-zcntr)/afg**2/16)

      rho_ini=1d0
         p_ini=1d0
      u_ini=0d0

      iarcade=1
      select case(iarcade)
      case(1)
c
c          Force free arcade
c
      B0=.3d0
      cappa=pi/rlargy
      elle=2.d0/rlargz
      if(elle.le.cappa)then
      alfa=sqrt(cappa**2-elle**2)
      else
      elle=cappa
      alfa=0d0
      end if
      by_ini=by_ini+elle/cappa*B0*cos(cappa*(wspy-ycntr))*
     & exp(-elle*(wspz-zcntr))
      bx_ini=bx_ini+alfa/cappa*B0*cos(cappa*(wspy-ycntr))*
     & exp(-elle*(wspz-zcntr))
      bz_ini=bz_ini-B0*sin(cappa*(wspy-ycntr))*
     & exp(-elle*(wspz-zcntr))
      case(2)
c
c          Potential field arcade (immersed dpole)
c
      emme=pi*B0
      zcntr=-rlargz/4
      rr=sqrt((wspx-xcntr)**2+(wspy-ycntr)**2+(wspz-zcntr)**2)
         cth=(wspy-ycntr)/(rr+1d-10)
         sth=sqrt(1d0-cth**2)
         sfi=(wspx-xcntr)/(rr*sth+1d-10)
         cfi=(wspz-zcntr)/(rr*sth+1d-10)
         br=2d0*emme*cth/(rr**3+1d-10)
         bth=emme*sth/(rr**3+1d-10)
         bfi=0d0
         bx_ini=-bfi*sfi+cth*cfi*bth+sth*cfi*br
         bz_ini=cfi*bfi+cth*sfi*bth+sth*sfi*br
         by_ini=-sth*bth+cth*br
      case default
        write(*,*)'The initial state is undefined, ',
     &              'check the input file and try again'
        stop
      end select
         end subroutine fangibson

         subroutine double_harris(wspx,wspy,wspz,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,xc,xsep,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
c
      include 'corgan.com'
      include 'cindex.com'
      include 'numpar.com'
      include 'cprplt.com'
      include 'cophys.com'
      include 'ctemp.com'
      include 'blcom.com'
      include 'flip3d.com'


      real(8) :: rlargx,rlargy,rlargz,xcntr,zcntr,zcntr1,zcntr2

         rlargx=ibar*dx
         rlargy=jbar*dy
         rlargz=kbar*dz
         xcntr=rlargx/2.
         zcntr=rlargz/2.
         zcntr1=rlargz/4.
         zcntr2=rlargz*3./4.

      elle=1.d0

         bx_ini=bxr(1)*(-1d0+tanh((wspz-zcntr1)/elle))
         bx_ini=bx_ini+bxr(1)*tanh((zcntr2-wspz)/elle)
         by_ini=0d0
         bz_ini=0d0

      epsilon_x=1d-1*bxr(1)
      epsilon_gem=0d-1*bxr(1)
      fkx=2.*pi/rlargx
      fky=2.*pi/rlargy
      fkz=2.*pi/rlargz
c
c          GEM perturbation
c
         bz_ini=bz_ini+epsilon_gem*sin(fkx*(wspx-xcntr))
     &             *sin(fkz*(wspz-zcntr1))*fkx
         bx_ini=bx_ini+epsilon_gem*cos(fkx*(wspx-xcntr))
     &             *cos(fkz*(wspz-zcntr1))*fkz
c
c          x-point perturbation (first layer)
c
         bx_ini=bx_ini-epsilon_x*cos(fkx*(wspx-xcntr))
     &         *(sin(fkz*(wspz-zcntr1))+2*(wspz-zcntr1)*
     &           cos(fkz*(wspz-zcntr1)))*fkz
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr1)**2)
         bz_ini=bz_ini+epsilon_x*(sin(fkx*(wspx-xcntr))+
     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
     &  *cos(fkz*(wspz-zcntr1))*fkx
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr1)**2)
c
c          x-point perturbation (second layer)
c
         bx_ini=bx_ini+epsilon_x*cos(fkx*(wspx-xcntr))
     &         *(sin(fkz*(wspz-zcntr2))+2*(wspz-zcntr2)*
     &           cos(fkz*(wspz-zcntr2)))*fkz
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr2)**2)
         bz_ini=bz_ini-epsilon_x*(sin(fkx*(wspx-xcntr))+
     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
     &  *cos(fkz*(wspz-zcntr2))*fkx
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr2)**2)


      p_ini=rhr(1)/10d0+rhr(1)/cosh((wspz-zcntr1)/elle)**2
      p_ini=p_ini+rhr(1)/cosh((wspz-zcntr2)/elle)**2
      p_ini = p_ini * bxr(1)**2/2.0

      rho_ini = p_ini / siep(1) / gm1

      u_ini=0.d0

      end subroutine double_harris
